import React from 'react';

class FormNav extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			labels: {}
		};
		this.handleClick = this.handleClick.bind(this);
	}
	handleClick(e) {
		// this.props.goToStep($(e.target).data('step'));
	}
	componentDidMount() {
		let parsedData = window.siteData;
		this.setState({
			labels: parsedData.labels
		});
	}
	render() {
		let { labels } = this.state;

		let activeClass = (ownStep, activeStep) => {
			if (ownStep === activeStep) {
				return 'form-nav-item active';
			} else {
				return 'form-nav-item';
			}
		};
		return(
			<div className="form-nav">
				<div className={activeClass(1, this.props.activeStep)} onClick={this.handleClick} data-step={1}>{labels['tab-general']}</div>
				<div className={activeClass(2, this.props.activeStep)} onClick={this.handleClick} data-step={2}>{labels['tab-team']}</div>
				<div className={activeClass(3, this.props.activeStep)} onClick={this.handleClick} data-step={3}>{labels['tab-contacts']}</div>
			</div>
		);
	}
}

export default FormNav;
