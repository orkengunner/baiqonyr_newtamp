'use strict';

// main entry point file
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './configureStore';
import FormContainer from './FormContainer';

ReactDOM.render(
  <Provider store={store}>
  	<FormContainer />
  </Provider>,
  document.getElementById('formReact')
);
