import React, { Component } from 'react';

class ModalError extends Component {
	render() {
		let errorTexts = () => {
			if (this.props.errors) {
				return Object.keys(this.props.errors).map((item, i) => {
					return(
						<div className="text has-error" key={i}>
							{/*<strong className="strong">{`${item}: `}</strong>*/}
							{this.props.errors[item][0]}
						</div>
					);
				});
			} else {
				return(
					<div className="text">
						В процессе обработки формы произошла неполадка. Попробуйте еще раз чуть позже.
					</div>
				);
			}
		};
		return(
			<div className="modal fade modal--error" id="modalError" 
					 tabIndex="-1" role="dialog" aria-labelledby="modalErrorLabel" aria-hidden="true"
					 ref={el => {this.modal = el;}}>
			  <div className="modal-dialog">
			    <div className="modal-content">
			      <div className="modal-header">
			        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
			        	<span aria-hidden="true" className="icon--close"></span>
			        </button>
			        <h3 className="title--primary">{this.props.title}</h3>
			      </div>
			      <div className="modal-body">
			        {errorTexts()}
			        <button type="button" className="btn btn--primary" data-dismiss="modal" aria-label="Close">ОК</button>
			      </div>
			    </div>
			  </div>
			</div>
		);
	}
}

export default ModalError;
