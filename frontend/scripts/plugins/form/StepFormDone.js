import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Control } from 'react-redux-form';
import store from './configureStore';
import { actions } from 'react-redux-form';
import FadeInWrapper from './FadeInWrapper';
import InputsList from './InputsList';
import Loader from './Loader';

class StepTeam extends Component {
	componentDidMount() {
		$('html, body').animate({ 
			scrollTop: $('#formReact').offset().top 
		}, 500, () => {
			$('body').removeClass('is-scrolling');
		});
	}
	render() {
		return(
			<div className="form-step form-step--result">
				<FadeInWrapper>
					<h4 className="title--secondary">Ваша заявка принята!</h4>
					<p className="text">Мы получили вашу заявку, спасибо. После того, как жюри рассмотрит все работы, мы свяжемся с вами и сообщим результаты.</p>
				</FadeInWrapper>
			</div>			
		);
	}
}

export default connect()(StepTeam);
