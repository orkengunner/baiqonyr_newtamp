import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Control, Errors, actions } from 'react-redux-form';
import store from './configureStore';
import { removeJob, addJob } from './actions';

class InputsListItem extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isRemovable: false
		};
		this.handleClick = this.handleClick.bind(this);
		this.enableSubmit = this.enableSubmit.bind(this);
	}
	enableSubmit() {
		const { dispatch, forms, user } = this.props;
		if (this.props.index === 0) {
			switch (this.props.job) {
				case 'author_name': 
					dispatch(actions.setValidity('user.author_name[0]', true));
					break;
				case 'producer_name': 
					dispatch(actions.setValidity('user.producer_name[0]', true));
					break;
				case 'director_name':
					dispatch(actions.setValidity('user.director_name[0]', true));
					break;
				case 'scenarist_name':
					dispatch(actions.setValidity('user.scenarist_name[0]', true));
					break;
				case 'actor_name':
					dispatch(actions.setValidity('user.actor_name[0]', true));
					break;
				case 'operator_name':
					dispatch(actions.setValidity('user.operator_name[0]', true));
					break;
			}
		}
	}
	handleClick(e) {
		const { dispatch, job, index } = this.props;
		this.state.isRemovable
			? removeJob(dispatch, job, index) 
			: addJob(dispatch, job);
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.isRemovable !== this.props.isRemovable) {
			this.setState({
				isRemovable: nextProps.isRemovable
			});
		}
	}
	render() {
		const { forms, job, index, controlButton, isRemovable, placeholder, btnTextRemove, btnTextAdd } = this.props;
		// console.log('job:', job);
		return(
			<div className={`form-group-item ${(forms.user.$form.valid === false 
						&& !forms.user[job][index].valid) ? ' has-error' : ''}`}>
				<Control.text model={`.${job}[${index}]`} 
							        className="form-control" placeholder={placeholder}
							        onChange={this.enableSubmit} />
				{controlButton
						 &&	<button type="button" className="btn btn--secondary" onClick={this.handleClick}>
									{isRemovable 
										? <span><span className="sign">-</span>{' '}{btnTextRemove}</span> 
										: <span><span className="sign">+</span>{' '}{btnTextAdd}</span>}
								</button>}
				{(job === 'author_name' || job === 'director_name' || job === 'scenarist_name' || job === 'actor_name' || job === 'operator_name') 
					&& <Errors model={`.${job}[${index}]`} className="help-block" />}
			</div>
		);
	}	
}

InputsListItem = connect(({user, forms}) => ({user, forms}))(InputsListItem);

const InputsList = ({user, dispatch, job, controlButton, placeholder, btnTextRemove, btnTextAdd}) => {
	// console.log('*****job:', job);
	return(
		<div className="form-group-item-wrapper">
			{user[job].map((name, i) =>
				<InputsListItem 
					index={i} 
					key={i}
					job={job}
					controlButton={controlButton}
					placeholder={placeholder}
					btnTextRemove={btnTextRemove}
					btnTextAdd={btnTextAdd}
					isRemovable={i === user[job].length - 1 ? false : true}
				/>
			)}
		</div>		
	);
};

export default connect(({user}) => ({user}))(InputsList);
