import LocalizedStrings from 'react-localization';
import store from './configureStore';


const utils = {
	fetchSelects: (url) => {
		return $.ajax({url}).done((data) => {
				// console.log('done:');
				// console.log('data:', data);
				return data;
			}).fail((err) => {
				// console.log('fail:');
				// console.log(err);
				return err;
			});
	},
	submitFormAjax: (url, formData, userId, showModalError) => {
		return $.ajax({
			url,
			method: 'POST',
			data: {
				register_id: userId,
				_csrf: yii.getCsrfToken(),
				// _csrf: 'test',
				...formData
			},
		}).done((data) => {
			// console.log('done:');
			// console.log('data:', data);
			return data;
		}).fail((err) => {
			// console.log('fail:');
			// console.log(err);
			showModalError();
			return err;
		});
	},
	formatSelectsArray: (arr) => {
		return arr.map((item) => {
			return { value: item.id, label: item.title };
		});
	}
};

export default utils;
