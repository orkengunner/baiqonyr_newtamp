import { actions } from 'react-redux-form';

export const pushTeamInputs = (dispatch, isAnim) => {
	dispatch(actions.push('user.author_name', ''));
	dispatch(actions.push('user.producer_name', '')); 
	dispatch(actions.push('user.director_name', '')); 
	dispatch(actions.push('user.actor_name', '')); 
	dispatch(actions.push('user.scenarist_name', ''));
	dispatch(actions.push('user.workshop_name', '')); 
	// console.log('isAnim:', isAnim);
	if (isAnim === false) {
		dispatch(actions.push('user.operator_name', ''));
		dispatch(actions.push('user.sound_engineer_name', ''));
		dispatch(actions.push('user.production_director_name', '')); 
		dispatch(actions.push('user.design_director_name', '')); 		
	}
};

export const pushLegalCheckboxes = (dispatch) => {
	dispatch(actions.push('user.user_reglament_read', true)); 
	dispatch(actions.push('user.user_data_agree', true)); 
	dispatch(actions.change('user.user_reglament_read', true)); 
	dispatch(actions.change('user.user_data_agree', true)); 
};

export const changeStep = (dispatch, step) => {
	dispatch(actions.change('step', step)); 
};

export const resetForm = (dispatch, formName) => {
	dispatch(actions.reset(formName)); 
};

export const removeJob = (dispatch, job, index) => {
	dispatch(actions.remove(`user.${job}`, index));
}

export const addJob = (dispatch, job) => {
	dispatch(actions.push(`user.${job}`, '' ));
}

export const changeInitialLanguage = (dispatch, langIndex) => {
	dispatch(actions.change('user.language', langIndex));
}

export const changeUserLangLocale = (dispatch, langString) => {
	dispatch(actions.change('lang', langString));
}

export const setAnimationCategory = (dispatch, isAnimation) => {
	dispatch(actions.change('isAnim', isAnimation));
}

export function toggleCheckboxValue(model, value) {
  return (dispatch) => {
		// console.log('model:', model, 'value:', value);
		dispatch(actions.toggle(model));
		dispatch(actions.setValidity(model, value ? true : false));
  };
}
