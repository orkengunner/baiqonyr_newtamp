import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Control, Errors, actions } from 'react-redux-form';
import Select from 'react-select';
import InputMask from 'react-input-mask';
import store from './configureStore';
import cropImage from './cropImage';
import utils from './utils';
import FadeInWrapper from './FadeInWrapper';
import Loader from './Loader';
import {
  changeStep,
  changeInitialLanguage,
  changeUserLangLocale,
  setAnimationCategory,
} from './actions';

const fetchSelects = utils.fetchSelects;
const formatSelectsArray = utils.formatSelectsArray;

const SelectContainer = (props) => {
  let currentSelect = (type) => {
    if (type === 'category') {
      return (
        <Select
          simpleValue
          value={props.value}
          placeholder={props.placeholder}
          noResultsText='Такой категории не существует'
          clearable={false}
          options={props.itemsCategory}
          onChange={props.onChange}
        />
      );
    } else {
      return (
        <Select
          multi
          value={props.value}
          placeholder={props.placeholder}
          noResultsText='Такого жанра не существует'
          options={props.itemsGenre}
          onChange={(val) => props.onChange(val.map((v) => v.value))}
        />
      );
    }
  };
  return (
    <div className='select-wrapper--combo'>
      {currentSelect(props.selectType)}
    </div>
  );
};

const MovieYearMaskedInput = (props) => (
  <InputMask
    className='form-control'
    type='text'
    id='movieYear'
    value={props.value}
    onChange={props.onChange}
    placeholder={props.placeholder}
    mask='9999'
    maskChar='г'
  />
);

const MovieDurationMaskedInput = (props) => (
  <InputMask
    className='form-control'
    type='text'
    id='movieDuration'
    value={props.value}
    onChange={props.onChange}
    placeholder='__чч __мм'
    mask='99чч 99мм'
    maskChar='_'
  />
);

class StepGeneral extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemsGenre: [],
      itemsCategory: [],
      dataIsLoaded: false,
      labels: {},
      placeholders: {},
      imageNotice: [],
    };
  }
  componentDidMount() {
    const { dispatch } = this.props;
    let url = `${window.location.origin}/${$('#formReact').data(
      'lang'
    )}/jobs/get-data/`;
    // let url = '../data/data.json';

    // initialize image upload and crop plugin
    cropImage(this.cropContainer);

    // set default values to have values
    // even if user doesnt interact with them
    changeInitialLanguage(dispatch, '1');

    let parsedData = window.siteData;

    this.setState(
      {
        itemsGenre: formatSelectsArray(parsedData.genres),
        itemsCategory: formatSelectsArray(parsedData.categories),
        labels: parsedData.labels,
        placeholders: parsedData.placeholders,
        imageNotice: parsedData.imageNotice,
      },
      () => {
        this.setState({
          dataIsLoaded: true,
        });
      }
    );
  }
  render() {
    let { placeholders, labels } = this.state;
    const { dispatch, user, forms } = this.props;
    console.log('GeneralSTEP __==');
    return (
      <div className='form-step'>
        <FadeInWrapper>
          <div className='form-row'>
            <div
              className={`form-group form-group--movie-name
								${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('title') &&
                  !forms.user.title.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <label htmlFor='movieName' className='control-label'>
                {labels.title}
              </label>
              <Control.text
                model='.title'
                className='form-control'
                name='name'
                placeholder={placeholders.title}
                id='movieName'
                onChange={() => {
                  dispatch(actions.setValidity('user.title', true));
                }}
              />
              <Errors model='.title' className='help-block' />
            </div>
          </div>
          <div className='form-row flex'>
            <div
              className={`form-group form-group--category
					  		${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('category_id') &&
                  !forms.user.category_id.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <label htmlFor='movieType' className='control-label'>
                {labels.category_id}
              </label>
              <Control
                model='.category_id'
                component={
                  this.state.dataIsLoaded ? SelectContainer : () => <div></div>
                }
                itemsCategory={this.state.itemsCategory}
                selectType='category'
                placeholder={placeholders.category_id}
                onChange={(val) => {
                  dispatch(actions.setValidity('user.category_id', true));
                  setAnimationCategory(dispatch, val === 3);
                }}
              />
              <Errors model='.category_id' className='help-block' />
            </div>
            <div className='form-group form-group--genre'>
              <label htmlFor='movieType' className='control-label'>
                {labels.genres}
              </label>
              <Control
                model='.genres'
                component={
                  this.state.dataIsLoaded ? SelectContainer : () => <div></div>
                }
                itemsGenre={this.state.itemsGenre}
                selectType='genre'
                placeholder={placeholders.genres}
              />
            </div>
          </div>
          <div className='form-row'>
            <div
              className={`form-group form-group--language
					  		${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('language') &&
                  !forms.user.language.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <label htmlFor='movieLanguage' className='control-label'>
                {labels.language}
              </label>
              <div className='select-wrapper'>
                <Control.select
                  model='.language'
                  className='form-control'
                  id='movieLanguage'
                  defaultValue='1'
                  onChange={() => {
                    dispatch(actions.setValidity('user.language', true));
                  }}
                >
                  <option value='2'>{labels['lang_kaz']}</option>
                  <option value='1'>{labels['lang_rus']}</option>
                  <option value='3'>{labels['lang_eng']}</option>
                </Control.select>
              </div>
              <Errors model='.language' className='help-block' />
            </div>
            <div
              className={`form-group form-group--year
					  		${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('year') &&
                  !forms.user.year.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <label htmlFor='movieYear' className='control-label'>
                {labels.year}
              </label>
              <Control.text
                model='.year'
                component={MovieYearMaskedInput}
                placeholder={placeholders.year}
                onChange={() => {
                  dispatch(actions.setValidity('user.year', true));
                }}
              />
              <Errors model='.year' className='help-block' />
            </div>

            <div
              className={`form-group form-group--duration
					  		${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('duration') &&
                  !forms.user.duration.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <label htmlFor='movieDuration' className='control-label'>
                {labels.duration}
              </label>
              <Control.text
                model='.duration'
                className='form-control'
                type='text'
                id='movieDuration'
                placeholder={placeholders.duration}
                maxLength='3'
                onChange={() => {
                  dispatch(actions.setValidity('user.duration', true));
                }}
              />
              <Errors model='.duration' className='help-block' />
            </div>
          </div>
          <div className='form-row'>
            <div
              className={`form-group form-group--link
								${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('link') &&
                  !forms.user.link.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <label htmlFor='movieLink' className='control-label'>
                {labels.link}
              </label>
              <Control.text
                model='.link'
                className='form-control'
                type='text'
                id='movieLink'
                placeholder={placeholders.link}
                onChange={() => {
                  dispatch(actions.setValidity('user.link', true));
                }}
              />
              <Errors model='.link' className='help-block' />
            </div>
          </div>
          <div className='form-row'>
            <div
              className={`form-group form-group--description
								${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('text') &&
                  !forms.user.text.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <label htmlFor='movieDescription' className='control-label'>
                {labels.text}
              </label>
              <Control.textarea
                model='.text'
                className='form-control textarea'
                id='movieDescription'
                placeholder={placeholders.text}
                onChange={() => {
                  dispatch(actions.setValidity('user.text', true));
                }}
              />
              <Errors model='.text' className='help-block' />
            </div>

            <div
              className={`form-group form-group--image
								${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('image') &&
                  !forms.user.image.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <div
                className='cropper cropper-post-cover'
                ref={(node) => {
                  this.cropContainer = node;
                }}
              >
                <button
                  type='button'
                  className='btn--cropper link js-upload-trigger'
                >
                  {labels.image}
                </button>
                <div className='cropit-preview'></div>
                <input
                  type='file'
                  className='cropit-image-input'
                  name='movie_cover'
                  id='movieCover'
                />
                <input
                  type='hidden'
                  className='cropit-image-input-hidden'
                  name='movie_cover_url'
                  id='movieCoverUrl'
                />
                <button
                  type='button'
                  className='btn btn--remove hidden'
                  title='Удалить постер'
                >
                  <span className='icon icon--remove link--image'></span>
                </button>
                <button
                  type='button'
                  className='btn btn--reupload link js-upload-trigger hidden'
                >
                  Заменить постер
                </button>
              </div>
              <div className='tip'>
                <div className='tip-unit'>
                  {this.state.imageNotice.map((el, i) => {
                    if (i === 0) {
                      return (
                        <div className='title' key={i}>
                          {el}
                        </div>
                      );
                    } else if (i < 5) {
                      return (
                        <p className='text' key={i}>
                          {el}
                        </p>
                      );
                    }
                  })}
                </div>
                <div className='tip-unit'>
                  {this.state.imageNotice.map((el, i) => {
                    if (i === 5) {
                      return (
                        <div className='title' key={i}>
                          {el}
                        </div>
                      );
                    } else if (i > 5) {
                      return (
                        <p className='text' key={i}>
                          {el}
                        </p>
                      );
                    }
                  })}
                </div>
              </div>
              <Errors model='.image' className='help-block' />
            </div>
          </div>

          <div className='form-footer'>
            <button
              type='submit'
              className={`btn btn--primary btn--bordered${
                forms.user.$form.valid ? '' : ' is-disabled'
              }`}
            >
              {labels['submit-general']}
              <Loader />
            </button>
          </div>
        </FadeInWrapper>
      </div>
    );
  }
}

export default connect(({ user, forms }) => ({ user, forms }))(StepGeneral);
