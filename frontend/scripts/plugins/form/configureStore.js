import * as Redux from 'redux';
import { combineForms } from 'react-redux-form';
import { setRemoveIndex } from './reducers';
import thunk from 'redux-thunk';

const { createStore, applyMiddleWare } = Redux;

const store = Redux.createStore(combineForms({
	step: 1,
	isAnim: false,
  user: {}
}), Redux.applyMiddleware(thunk));

export default store;
