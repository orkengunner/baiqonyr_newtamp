import React from 'react';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

const FadeInWrapper = ({children}) => (
	<CSSTransitionGroup
		transitionName="step"
		transitionAppear={true}
		transitionAppearTimeout={350}
		transitionEnter={false}
		transitionLeave={false}
		component="div"
		className="react-animation-wrapper">
		{children}
	</CSSTransitionGroup>
);

export default FadeInWrapper;
