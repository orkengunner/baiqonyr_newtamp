import React, { Component } from 'react';
import { connect } from 'react-redux';
import InputMask from 'react-input-mask';
import { Control, Errors, actions } from 'react-redux-form';
import FadeInWrapper from './FadeInWrapper';
import Loader from './Loader';
import isEmpty from 'validator/lib/isEmpty';
import { toggleCheckboxValue } from './actions';

const PhoneMaskedInput = (props) => (
  <InputMask
    className='form-control'
    type='text'
    id='userPhone'
    value={props.value}
    onChange={props.onChange}
    placeholder='+7 xxx xxx-xx-xx'
    mask='+7 999 999-99-99'
    maskChar='x'
  />
);

class StepContacts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      labels: {},
      placeholders: {},
    };
  }
  componentDidMount() {
    const { dispatch } = this.props;

    let parsedData = window.siteData;

    $('html, body').animate(
      {
        scrollTop: $('#formReact').offset().top,
      },
      500,
      () => {
        $('body').removeClass('is-scrolling');
      }
    );
    this.setState({
      labels: parsedData.labels,
      placeholders: parsedData.placeholders,
    });
  }
  render() {
    let { placeholders, labels } = this.state;
    const { dispatch, user, forms } = this.props;
    const userForm = user;
    return (
      <div className='form-step'>
        <FadeInWrapper>
          <div className='form-section'>
            <div className='form-row'>
              <div
                className={`form-group form-group--name
									${
                    forms.user.$form.valid === false &&
                    forms.user.hasOwnProperty('contact_name') &&
                    !forms.user.contact_name.valid
                      ? ' has-error'
                      : ''
                  }`}
              >
                <label htmlFor='userName' className='control-label'>
                  {labels.contact_name}
                </label>
                <Control.text
                  model='.contact_name'
                  className='form-control'
                  id='userName'
                  placeholder={placeholders.contact_name}
                  onChange={() => {
                    dispatch(actions.setValidity('user.contact_name', true));
                  }}
                />
                <Errors model='.contact_name' className='help-block' />
              </div>
            </div>
            <div className='form-row'>
              <div
                className={`form-group form-group--country
									${
                    forms.user.$form.valid === false &&
                    forms.user.hasOwnProperty('country') &&
                    !forms.user.country.valid
                      ? ' has-error'
                      : ''
                  }`}
              >
                <label htmlFor='userCountry' className='control-label'>
                  {labels.country}
                </label>
                <Control.text
                  model='.country'
                  className='form-control'
                  id='userCountry'
                  placeholder={placeholders.country}
                  onChange={() => {
                    dispatch(actions.setValidity('user.country', true));
                  }}
                />
                <Errors model='.country' className='help-block' />
              </div>
            </div>
            <div className='form-row'>
              <div
                className={`form-group form-group--phone
									${
                    forms.user.$form.valid === false &&
                    forms.user.hasOwnProperty('phone') &&
                    !forms.user.phone.valid
                      ? ' has-error'
                      : ''
                  }`}
              >
                <label htmlFor='userPhone' className='control-label'>
                  {labels.phone || 'Номер телефона'}
                </label>
                <div className='phone-group'>
                  <Control.text
                    model='.phone'
                    component={PhoneMaskedInput}
                    onChange={() => {
                      dispatch(actions.setValidity('user.phone', true));
                    }}
                  />
                  <Errors model='.phone' className='help-block' />
                </div>
              </div>
            </div>
            <div className='form-row'>
              <div
                className={`form-group form-group--email
									${
                    forms.user.$form.valid === false &&
                    forms.user.hasOwnProperty('email') &&
                    !forms.user.email.valid
                      ? ' has-error'
                      : ''
                  }`}
              >
                <label htmlFor='userEmail' className='control-label'>
                  {labels.email}
                </label>
                <Control.text
                  model='.email'
                  className='form-control'
                  id='userEmail'
                  placeholder={placeholders.email}
                  onChange={() => {
                    dispatch(actions.setValidity('user.email', true));
                  }}
                />
                <Errors model='.email' className='help-block' />
              </div>
            </div>
          </div>

          <div className='form-section'>
            <div
              className={`form-group checkbox
								${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('user_reglament_read') &&
                  !forms.user.user_reglament_read.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <Control.checkbox
                model='.user_reglament_read'
                id='uesrReglamentRead'
                changeAction={toggleCheckboxValue}
              />
              <label htmlFor='uesrReglamentRead' className='control-label'>
                {labels['terms-begin']}{' '}
                <a
                  href='#'
                  className='link'
                  data-toggle='modal'
                  data-target='#modalReglament'
                >
                  {labels['terms-end']}
                </a>
              </label>
              {/*<Errors model=".user_reglament_read" className="help-block" />*/}
            </div>
            <div
              className={`form-group checkbox
								${
                  forms.user.$form.valid === false &&
                  forms.user.hasOwnProperty('user_data_agree') &&
                  !forms.user.user_data_agree.valid
                    ? ' has-error'
                    : ''
                }`}
            >
              <Control.checkbox
                model='.user_data_agree'
                id='userDataAgree'
                changeAction={toggleCheckboxValue}
              />
              <label htmlFor='userDataAgree' className='control-label'>
                {labels['access']}
              </label>
              {/*<Errors model=".user_data_agree" className="help-block" />*/}
            </div>
          </div>

          <div className='form-footer'>
            <button
              type='submit'
              className={`btn btn--primary${
                forms.user.$form.valid ? '' : ' is-disabled'
              }`}
            >
              {labels['submit-contacts']}
              <Loader />
            </button>
          </div>
        </FadeInWrapper>
      </div>
    );
  }
}

export default connect(({ user, forms }) => ({ user, forms }))(StepContacts);
