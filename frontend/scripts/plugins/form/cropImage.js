import store from './configureStore';
import { actions } from 'react-redux-form';

const cropImage = function(el) {
  let cropperContainer = $(el),
      uploadTrigger = $('.js-upload-trigger'),
      btnCropper = $('.btn--cropper'),
      btnRemove = $('.cropper .btn--remove'),
      btnReupload = $('.cropper .btn--reupload');

  cropperContainer.cropit({
    // exportZoom: 2,
    smallImage: 'stretch',
    onImageLoaded: function() {
      btnCropper.hide();
      cropperContainer.find('.cropit-preview').show();
      btnRemove.removeClass('hidden');
      btnReupload.removeClass('hidden');
      cropperContainer.find('.cropit-image-input-hidden').val(cropperContainer.cropit('export'));
      if (!$('.cropper').hasClass('has-image')) {
        $('.cropper').addClass('has-image');
      }
      cropperContainer.attr('data-remove','0');
    },
    onOffsetChange: function() {
      var imageSrc = cropperContainer.cropit('export');
      cropperContainer.find('.cropit-image-input-hidden').val(imageSrc);
      // console.log(imageSrc);
      store.dispatch(actions.change('user.image', imageSrc));
      store.dispatch(actions.setValidity('user.image', true));
    }
  });

  uploadTrigger.on('click', function() {
    $(this).parent().find('.cropit-image-input').click();
  });
  btnRemove.on('click', function() {
    btnCropper.show();
    btnRemove.addClass('hidden');
    btnReupload.addClass('hidden');
    cropperContainer.removeClass('has-image');
    cropperContainer.attr('data-remove','1');
    cropperContainer.find('.cropit-image-input-hidden').val('');
    cropperContainer.find('.cropit-preview').hide();
    cropperContainer.find('.cropit-preview-image').attr('src', '');
    cropperContainer.find('.cropit-image-input').val('');
    store.dispatch(actions.change('user.image', ''));
  });
};

export default cropImage;
