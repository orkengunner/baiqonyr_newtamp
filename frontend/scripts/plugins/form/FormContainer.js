import React from 'react';
import { connect } from 'react-redux';
import { Control, Form, actions } from 'react-redux-form';
import StepGeneral from './StepGeneral';
import StepTeam from './StepTeam';
import StepContacts from './StepContacts';
import StepFormDone from './StepFormDone';
import FormNav from './FormNav';
import utils from './utils';
import uuid from 'uuid';
import store from './configureStore';
import { pushTeamInputs, pushLegalCheckboxes, changeStep, resetForm } from './actions';

const submitFormAjax = utils.submitFormAjax;
const userId = uuid.v1;

class FormContainer extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			userId: userId(),
			modalErrorTitle: '',
			modalErrors: [],
			isLoading: false
		};

		this.showModalError = this.showModalError.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.goToStep = this.goToStep.bind(this);
	}
	goToStep(newStep) {
		const { dispatch, step, isAnim } = this.props;
		// differenet order of reseting form and rendering new step
		// becuse of jobs input array items need first to be unmounted
		// and then reseted if go from this step
		// and vice versa if it is the new step
		if (newStep === 2) {
			resetForm(dispatch, 'user');
			pushTeamInputs(dispatch, isAnim);
			changeStep(dispatch, newStep);
		} else {
			changeStep(dispatch, newStep);
			resetForm(dispatch, 'user');
			if (newStep === 3) {
				pushLegalCheckboxes(dispatch);
			}
		}
	}
	showModalError(errors, title) {
		this.setState({
			modalErrorTitle: title || 'Что-то пошло не так:(',
			modalErrors: errors || null
		}, () => {
			$('.modal--error').modal('show');
		});
	}
	handleSubmit(formData) {
		const { dispatch, forms, user, step, isAnim } = this.props;
		let url = `${window.location.origin}${window.location.href.match(/\/kz\//) ? '/kz' : ''}/request/${step}/`;

		// console.log('formData for Submit:', formData);

		if (step === 2 && (user['author_name'][0].length === 0 
				|| user['director_name'][0].length === 0
				|| user['actor_name'][0].length === 0 
				|| (isAnim === false && user['operator_name'][0].length === 0)  
				|| user['scenarist_name'][0].length === 0)) {
			if (user['author_name'][0].length === 0) {
				dispatch(actions.setErrors('user.author_name[0]', 'Укажите автора'));
			} 
			if (user['director_name'][0].length === 0) {
				dispatch(actions.setErrors('user.director_name[0]', 'Укажите режиссера'));
			}
			if (user['scenarist_name'][0].length === 0) {
				dispatch(actions.setErrors('user.scenarist_name[0]', 'Укажите сценариста'));
			}	
			if (isAnim === false && user['operator_name'][0].length === 0) {
				dispatch(actions.setErrors('user.operator_name[0]', 'Укажите оператора'));
			}
			if (user['actor_name'][0].length === 0) {
				dispatch(actions.setErrors('user.actor_name[0]', 'Укажите актеров'));
			}			
		} else {
			this.setState({isLoading: true});
			// *********************************
		  // SEND USER SUBMITTED DATA TO SERVER VIA AJAX
		  submitFormAjax(url, formData, this.state.userId, this.showModalError).then((dataString) => {
		  	let data = JSON.parse(dataString);
		  	this.setState({isLoading: false});
		  	if (data.success) {
		  	  this.goToStep(step + 1);
		  	} else if (data.success === false) {
		  		// console.log('data.errors:', data.errors);
		  		// get error from server and set them for every control
		  		Object.keys(data.errors).forEach((item) => {
			  		dispatch(actions.setErrors(`user.${item}`, {
			  		  // hasError: data.errors.hasOwnProperty(item) && data.errors[item][0]
			  		  hasError: window.siteData.labels['error-check-field']
			  		}));
		  		});
		  	}
			});
			// *********************************
		}

		// MARKUP ONLY!!!
		// this.setState(prevState => ({
		// 	activeStep: (prevState.activeStep < 3) 
		// 		? (+formData.step + 1) 
		// 		: prevState.activeStep,
		// 	isLoading: false
		// }));
	}
	handleChange(modelValues) {
		// console.log('modelValues:', modelValues);
	}
	render() {
		const { step } = this.props;
		let currentFormStep = (stepArg) => {
			switch(stepArg) {
				case 1: return <StepGeneral />;
				case 2: return <StepTeam onSubmit={this.handleSubmit} />;
				case 3: return <StepContacts />
				case 4: return <StepFormDone />;
				default: return <StepGeneral />;
			}
		};
		return(
			<section className="form-application">
				<Form model="user" className={`form form--profile${this.state.isLoading ? ' is-loading' : ''}`} 
							onSubmit={(val) => this.handleSubmit(val)}
							onChange={this.handleChange}
							onUpdate={this.handleUpdate}>
					<div className="form-wrapper">
						<h3 className="form-title">{window.siteData.labels.form_title}</h3>
						{step !== 4 && <FormNav activeStep={step} goToStep={this.goToStep}/>}
						{currentFormStep(step)}
					</div>
				</Form>				
			</section>
		);
	}
}

export default connect(({forms, user, step, isAnim}) => ({forms, user, step, isAnim}))(FormContainer);
