import React from 'react';

const Loader = () => (
	<svg viewBox="0 0 100 100" className="icon icon--loader">
	  <g preserveAspectRatio="xMidYMid" className="uil-ring-alt">
	    <circle cx="50" cy="50" r="40" stroke="currentColor" fill="none" strokeWidth="5" strokeLinecap="round">
	        <animate attributeName="stroke-dashoffset" dur="2s" repeatCount="indefinite" from="0" to="502"></animate>
	        <animate attributeName="stroke-dasharray" dur="2s" repeatCount="indefinite" values="150.6 100.4;1 250;150.6 100.4"></animate>
	    </circle>
	  </g>
	</svg>
);

export default Loader;
