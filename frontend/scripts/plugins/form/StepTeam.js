import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Control } from 'react-redux-form';
import store from './configureStore';
import { actions } from 'react-redux-form';
import FadeInWrapper from './FadeInWrapper';
import InputsList from './InputsList';
import Loader from './Loader';
import { changeStep } from './actions';

class StepTeam extends Component {
	constructor(props) {
		super(props);
		this.state = {
			labels: {},
			placeholders: {}
		};
	}
	componentDidMount() {
		const { dispatch } = this.props;
				
		let parsedData = window.siteData;
		console.log(window.siteData);

		$('html, body').animate({ 
			scrollTop: $('#formReact').offset().top 
		}, 500, () => {
			$('body').removeClass('is-scrolling');
		});
		this.setState({
			labels: parsedData.labels,
			placeholders: parsedData.placeholders
		});
	}
	render() {
		let { placeholders, labels } = this.state;
		const { forms, user, isAnim } = this.props;
		console.log('StepTeam _____ ')
		// console.log('isAnim:', isAnim);
		return(
			<div className="form-step form-step--team">
				<FadeInWrapper>
					<div className="form-row">
						<div className="form-group">
							<label className="control-label">{labels.author_name}</label>
							<InputsList job="author_name" controlButton={true} placeholder={placeholders.author_name} 
													btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button} />
						</div>
					</div>

					<div className="form-row">
						<div className="form-group">
							<label className="control-label">{labels.producer_name}</label>
							<InputsList job="producer_name" controlButton={true} placeholder={placeholders.producer_name} 
													btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button} />
						</div>
					</div>

					<div className="form-row">
						<div className="form-group">
							<label className="control-label">{labels.director_name}</label>
							<InputsList job="director_name" controlButton={true} placeholder={placeholders.director_name}
													btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button} />
						</div>
					</div>
					<div className="form-row">
						<div className="form-group">
							<label className="control-label">{labels.scenarist_name}</label>
							<InputsList job="scenarist_name" controlButton={true} placeholder={placeholders.scenarist_name}
													btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button} />
						</div>
					</div>
					<div className="form-row">
						<div className="form-group">
							{/*<label className="control-label">{`${isAnim ? 'Актеры озвучки' : 'Актеры'}`}</label>*/}
							<label className="control-label">{labels.actor_name}</label>
							<InputsList job="actor_name" controlButton={true} placeholder={placeholders.actor_name}
													btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button} />
						</div>
					</div>
					{isAnim === false && <div className="form-row">
											<div className="form-group">
												<label className="control-label">{labels.operator_name}</label>
												<InputsList job="operator_name" controlButton={true} placeholder={placeholders.operator_name}
																		btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button} />
											</div>
										</div>}


					{isAnim === false && <div className="form-row">
											<div className="form-group">
												<label className="control-label">{labels.sound_engineer_name}</label>
												<InputsList job="sound_engineer_name" controlButton={true} placeholder={placeholders.sound_engineer_name}
																		btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button} />
											</div>
										</div>}					

					{isAnim === false && <div className="form-row">
											<div className="form-group">
												<label className="control-label">{labels.production_director_name}</label>
												<InputsList job="production_director_name" controlButton={true} 
																		btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button}
																		placeholder={placeholders.production_director_name} />
											</div>
										</div>}
					{isAnim === false && <div className="form-row">
											<div className="form-group">
												<label className="control-label">{labels.design_director_name}</label>
												<InputsList job="design_director_name" controlButton={true} 
																		btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button}
																		placeholder={placeholders.design_director_name} />
											</div>
										</div>}
					<div className="form-row">
						<div className="form-group">
							<label className="control-label">{labels.workshop_name}</label>
							<InputsList job="workshop_name" placeholder={placeholders.workshop_name}
													btnTextAdd={labels.add_button} btnTextRemove={labels.remove_button} />
						</div>
					</div>
					<div className="form-footer">
						<button type="submit"
							className={`btn btn--primary btn--bordered${forms.user.$form.valid ? '' : ' is-disabled'}`}>
							{labels['submit-team']}
							<Loader />
						</button>
					</div>
				</FadeInWrapper>
			</div>			
		);
	}
}

export default connect(({user, forms, isAnim}) => ({user, forms, isAnim}))(StepTeam);
