var wUploads = function () {
  this.init = function (initData) {
    var $container = $(initData.container),
      $img = $container.find('img'),
      $remove = $container.find('.remove-img'),
      $hide = $container.find('.wHide'),
      $hint = $container.find('.wHint');

    $remove.click(function () {
      initRemoveLink(this);
    });

    var initRemoveLink = function (el) {
      $hide.hide();
      mAjax.doit($(el).data('url'), {}, 'GET');
      if ($(el).data('default')) $img.attr('src', $(el).data('default'));
      if (initData.list || initData.multiple) {
        $(el).closest($(el).data('container')).remove();
      }
      return false;
    };

    $($container.find('input[type=file]')).fileupload({
      dataType: 'json',
      dropZone: $('#dropzone'),
      start: function (e) {
        $('body').css('cursor', 'wait');
        $hide.show();
      },
      done: function (e, data) {
        var postChannel = window.location.pathname.split('/')[2],
          uploadedImagesNumber,
          imagesLimit = Number($('.attachments-limit span').text());
        $('body').css('cursor', 'default');
        if (typeof initData.onlyCallback != 'undefined')
          eval(initData.onlyCallback)(data.result);
        else {
          console.log(data.result);
          if (data.result.success) {
            if (data.result.multiple || initData.list) {
              //var rm_id = 'id'+new Date().getTime();

              var html = $(initData.template)
                .html()
                .replace(/\{\{key\}\}/g, data.result.index)
                .replace('{{src}}', data.result.file.url)
                .replace(/\{\{index\}\}/g, new Date().getTime());

              if (data.result.multiple) $('.multiple_list').append(html);
              else if (initData.list) $('.multiple_list').html(html);

              var removeImg = $container.find('.remove-img');
              removeImg.eq(removeImg.length - 1).click(function () {
                initRemoveLink(this);
              });
            } else {
              $img.attr('src', data.result.file.url);
              $img.show();
              if (typeof initData.hint != 'undefined')
                $hint.html(
                  initData.hint
                    .replace('%file%', data.result.file.url)
                    .replace('%size%', data.result.file.size)
                );
            }
          } else {
            $hint.html(data.result.file.error);
          }
          if (typeof initData.callback != 'undefined')
            eval(initData.callback)(data.result);
        }

        $('.multiple_list img').on('dragstart', function (event) {
          event.preventDefault();
        });
      },
      progressall: function (e, data) {
        var progress = parseInt((data.loaded / data.total) * 100, 10);
        /*$('#progress .progress-bar').css(
                 'width',
                 progress + '%'
                 );*/
        console.log(progress);
      },
    });

    $(function () {
      $('#sortable').sortable();
    });
  };
};
