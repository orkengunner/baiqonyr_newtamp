/**
 * Created by Vitaliy Prokhonenkov on 04.01.2016.
 */
function declension(count, _1, _2_4, _5_0)
{
    if(count < 0) return false;
    count = count.toString();
    var _count;
    var len = count.length;

    if(len < 1) {
        var exclusion = parseInt(count[len-2] + count[len-1])
    } else {
        var exclusion = parseInt(count)
    }

    if(exclusion >= 10 && exclusion <= 20) _count = 0;
    else if (count > 19) _count = count[len-1];
    else _count = count;

    var str;

    switch(parseInt(_count)) {
        case 1:
            str = _1;
            break;
        case 2:
        case 3:
        case 4:
            str = _2_4;
            break;
        case 0:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            str = _5_0;
            break;
    }

    return str;
}