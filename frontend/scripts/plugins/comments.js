/**
 * Created by Vital on 26.02.2015.
 */
var Comments = function (container) {
  this.container = container;
  this.moveForm = function (itemID) {
    $('#btnReply' + itemID).css('display', 'none');
    $('#btnReply' + $('#Comments_parentID').val()).css('display', '');
    $('#btnReplyCancel' + itemID).css('display', '');
    $('#btnReplyCancel' + $('#Comments_parentID').val()).css('display', 'none');

    $('#' + $('#curContainer').val() + ' .submit-form').appendTo(
      '#' + this.container + itemID
    );

    //$('#'+this.container+itemID).html($('#'+$('#curContainer').val()).html());
    //$('#'+$('#curContainer').val()).html('');

    $('#curContainer').val(this.container + itemID);
    $('#Comments_parentID').val(itemID);
    $('#' + this.container + itemID + ' textarea').focus();

    jQuery.scrollTo('#commentsForm', 1000);
  };

  this.initComments = function (itemID) {
    $('.reply').click(
      { container: container, moveForm: this.moveForm },
      function (e) {
        var itemID = $(this).data('id');
        e.data.moveForm(itemID);
      }
    );
    this.initKeyPress();
    $('#curContainer').val(this.container + (itemID ? itemID : ''));
  };

  this.initKeyPress = function () {
    $('#' + $('#curContainer').val() + ' textarea').keypress(function (e) {
      if (e.ctrlKey && e.keyCode == 13) {
        $('#' + $('#curContainer').val() + ' form').submit();
      }
    });
  };
};
