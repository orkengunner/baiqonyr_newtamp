/**
 * @class
 */
class SmoothScroll {
	/**
	 * @constructor
	 * @param {string} wrapper - selector of wrapper element of anchor links
	 */
	constructor(wrapper) {
		this.wrapper = wrapper;
		this.links = [...document.querySelectorAll(`${this.wrapper} a[href^="#"]`)];
		this.callback = false;

		this.init();
	}

	/**
	 * @private
	 */
	init() {
		const self = this;
		this.links.forEach(anchor => {
			anchor.addEventListener('click', function(e) {
				e.preventDefault();
				document.querySelector(this.getAttribute('href')).scrollIntoView({
					behavior: 'smooth'
				});
				if (self.callback) {
					self.completeCallback();
				}
			});
		});
	}

	/**
	 * @public
	 * @param {object} func - callback function to run after scrolling
	 */
	onComplete(func) {
		if (func && typeof func === 'function') {
			this.callback = true;
			this.completeCallback = func;
		}
	}
}

export default SmoothScroll;
