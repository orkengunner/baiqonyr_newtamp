// 'use strict';

// $(document).ready(function() {
// /
// /

// show next modal a bit earlier
// because without there is shaking
// on transitions between two
// var modalControl = (function() {
// 	$('.js-open-next-modal').on('click', function(e) {
// 		setTimeout(function() {
// 			$($(e.target).data('next-modal')).modal('show');
// 		}, 400);
// 	});
// })();

// var swiperHeroMobile = new Swiper('.swiper-hero-mobile', {
// 	nextButton: '.faces .swiper-controls .arrow--right',
// 	prevButton: '.faces .swiper-controls .arrow--left',
// 	slidesPerView: 1,
// 	pagination: '.swiper-hero-mobile-pagination',
// 	loop: true,
// 	paginationClickable: true
// });

// var swiperMoviesOnline = new Swiper('.swiper-movie-online', {
// 	nextButton: '.movies-online .swiper-controls .arrow--right',
// 	prevButton: '.movies-online .swiper-controls .arrow--left',
// 	slidesPerView: $(window).width() < 481 ? 3 : 6,
// 	spaceBetween: $(window).width() < 481 ? 7 : 20
// });

// var swiper = new Swiper('.swiper-container-coverflow', {
// 	nextButton: '.swiper-controls .arrow--right',
// 	prevButton: '.swiper-controls .arrow--left',
// 	effect: 'coverflow',
// 	grabCursor: true,
// 	centeredSlides: true,
// 	slidesPerView: 'auto',
// 	loop: 'true',
// 	loopedSlides: 5,
// 	spaceBetween: 15,
// 	coverflow: {
// 		rotate: 10,
// 		alpha: 0.5,
// 		stretch: 0,
// 		depth: 200,
// 		modifier: 1,
// 		slideShadows: false
// 	},
// 	onSetTranslate: function(s) {
// 		var transform = s.translate,
// 			center = -transform + s.width / 2;
// 		for (var a = 0; a < s.slides.length; a++) {
// 			var slide = s.slides.eq(a);
// 			var slideSize = s.slidesSizesGrid[a];
// 			var slideOffset = slide[0].swiperSlideOffset;
// 			var offsetMultiplier =
// 				1 +
// 				(Math.abs(center - slideOffset - slideSize / 2) / slideSize) *
// 					s.params.coverflow.modifier;
// 			var slideOpacity = 1 / offsetMultiplier;
// 			if (slideOpacity <= 0.3) {
// 				slideOpacity = 0;
// 			}
// 			slide[0].style.opacity = slideOpacity;
// 		}
// 	}
// });

// var inputMask = (function() {
// 	var maskedInput = $('.js-masked');
// 	for (var i = 0; i < maskedInput.length; i++) {
// 		if (
// 			$(maskedInput[i])
// 				.parents('.form-group')
// 				.hasClass('form-group--phone')
// 		) {
// 			$(maskedInput[i]).mask('+7 999 999 99 99', {
// 				autoclear: false
// 			});
// 		} else if (
// 			$(maskedInput[i])
// 				.parents('.form-group')
// 				.hasClass('form-group--duration')
// 		) {
// 			$(maskedInput[i]).mask('99мм 99сс', {
// 				placeholder: '_',
// 				autoclear: false
// 			});
// 		}
// 	}
// })();

// galleries in article
// var articlesGallery = function(gal) {
// 	var articleGallerySlider = gal.find('.article-slider').lightSlider({
// 		gallery: true,
// 		item: 1,
// 		loop: true,
// 		thumbItem: $(window).width() < 481 ? 2 : 5,
// 		thumbMargin: 10,
// 		slideMargin: 0,
// 		enableDrag: false,
// 		currentPagerPosition: 'left',
// 		onSliderLoad: function(el) {
// 			el.lightGallery({
// 				selector: '.article-slider .lslide'
// 			});
// 		},
// 		onAfterSlide: function() {
// 			var activeSlide = gal.find('.article-slider').find('.active'),
// 				slideDescriptionData = activeSlide.data('description'),
// 				slideDescription = gal.find('.article-gallery-description');

// 			slideDescription.text(slideDescriptionData);
// 		}
// 	});
// };

// var articlesGalleryLoop = function() {
// 	for (var i = 0; i < $('.article-gallery').length; i++) {
// 		articlesGallery($($('.article-gallery')[i]));
// 	}
// };

// if ($('.article-gallery').length > 0) {
// 	articlesGalleryLoop();
// }

// var menuMobile = function() {
// 	var menuToggle = $('.js-menu-toggle'),
// 		searchToggleBtn = $('.form--secondary .icon--search');

// 	if ($(window).width() > 480 && $(window).width() < 1025) {
// 		$('body').prepend('<div class="menu-backdrop"></div>');
// 	}

// 	menuToggle.on('click', function() {
// 		console.log('click');
// 		if (
// 			$(this)
// 				.parents('.header')
// 				.hasClass('is-opened')
// 		) {
// 			$('body').css('overflow', 'initial');
// 			$(this)
// 				.parents('.header')
// 				.removeClass('is-opened');
// 			$('.menu').removeClass('is-opened');
// 			if ($(window).width() > 480 && $(window).width() < 1025) {
// 				$('.menu-backdrop').fadeOut();
// 			}
// 		} else {
// 			if ($(window).width() > 480 && $(window).width() < 1025) {
// 				$('.menu-backdrop').fadeIn();
// 			}
// 			$('body').css('overflow', 'hidden');
// 			$(this)
// 				.parents('.header')
// 				.addClass('is-opened');
// 			$('.menu').addClass('is-opened');
// 		}
// 	});

// 	$('.menu [data-toggle="modal"]').on('click', function() {
// 		if (menuToggle.parents('.header').hasClass('is-opened')) {
// 			if ($(window).width() > 480 && $(window).width() < 1025) {
// 				$('.menu-backdrop').fadeOut();
// 			}
// 			menuToggle.parents('.header').removeClass('is-opened');
// 			$('.menu').removeClass('is-opened');
// 			$('body').css('overflow', 'initial');
// 		}
// 	});

// 	$('.menu-backdrop').on('click', function() {
// 		if ($(window).width() > 480 && $(window).width() < 1025) {
// 			$(this).fadeOut();
// 		}
// 		menuToggle.parents('.header').removeClass('is-opened');
// 		$('.menu').removeClass('is-opened');
// 		$('body').css('overflow', 'initial');
// 	});

// 	var dropdownsMobile = (function() {
// 		var dropdowns = $('.nav--primary .dropdown-menu'),
// 			btnArrow = $('.btn--arrow');

// 		var makeHeightArray = (function() {
// 			for (var i = 0; i < dropdowns.length; i++) {
// 				$(dropdowns[i]).attr(
// 					'data-height',
// 					'' + $(dropdowns[i]).height() + ''
// 				);
// 				$(dropdowns[i]).css('height', String($(dropdowns[i]).height()));
// 			}
// 			dropdowns.css('height', '0');
// 		})();

// 		btnArrow.on('click', function() {
// 			if ($(this).hasClass('is-opened')) {
// 				$(this).removeClass('is-opened');
// 				$(this)
// 					.parent()
// 					.find('.dropdown-menu')
// 					.css('height', '0');
// 			} else {
// 				btnArrow.removeClass('is-opened');
// 				dropdowns.css('height', '0');
// 				$(this).addClass('is-opened');
// 				$(this)
// 					.parent()
// 					.find('.dropdown-menu')
// 					.css(
// 						'height',
// 						String(
// 							$(this)
// 								.parent()
// 								.find('.dropdown-menu')
// 								.data('height')
// 						)
// 					);
// 			}
// 		});
// 	})();
// };

// var dommanipulate = function() {
// 	$('.winner-1').insertBefore($('.winner-2'));
// 	for (var i = 0; i < $('.people-item .people-image').length; i++) {
// 		$($('.people-item .people-image')[i]).prependTo(
// 			$($('.people-item .people-image')[i]).parents('.people-item')
// 		);
// 		$($('.people-item .people-image')[i])
// 			.parents('.people-item')
// 			.find('.people-meta')
// 			.appendTo(
// 				$($('.people-item .people-image')[i])
// 					.parents('.people-item')
// 					.find('.people-body')
// 			);
// 	}
// };

// if ($(window).width() >= 320 && $(window).width() < 1025) {
// 	menuMobile();
// 	dommanipulate();
// }

// if ($('#map').length) {
// 	var map, myIcon, myDivIcon;

// 	DG.then(function() {
// 		map = DG.map('map', {
// 			center: [43.233774, 76.976138],
// 			zoom: 15,
// 			minZoom: 12,
// 			maxZoom: 18,
// 			geoclicker: false,
// 			zoomControl: false,
// 			fullscreenControl: false,
// 			dragging: false,
// 			touchZoom: false,
// 			scrollWheelZoom: false,
// 			doubleClickZoom: false
// 		});
// 		myIcon = DG.icon({
// 			iconUrl: '/images/icons/iconmap.png',
// 			iconSize: [46, 77],
// 			iconAnchor: [23, 77]
// 		});
// 		DG.marker([43.233774, 76.976138], {
// 			icon: myIcon
// 		}).addTo(map);
// 	});
// }

// $('#gallery-about').lightSlider({
// 	gallery: true,
// 	item: 1,
// 	thumbWidth: 50,
// 	thumbItem: 7,
// 	thumbMargin: 4,
// 	slideMargin: 0,
// 	loop: true
// });

// var juryClickable = function() {
//   var juryItem = $('.people-item');
//   for (var i = 0; i < juryItem.length; i++) {
//     if ($(juryItem[i]).find('.link--overlay').attr('href') === '#') {
//       $(juryItem[i]).find('.link--overlay').remove();
//       $(juryItem[i]).find('.people-body').css('pointer-events','none');
//     }
//   }
// };

// if ($(window).width() > 481) {
//   juryClickable();
// }

// //intl-tel-input
// if ($('#intphone').length) {
// 	var input = document.querySelector('#intphone');
// 	window.intlTelInput(input, {
// 		// any initialisation options go here
// 		initialCountry: 'auto',
// 		geoIpLookup: function(success, failure) {
// 			$.get('https://ipinfo.io', function() {}, 'jsonp').always(function(
// 				resp
// 			) {
// 				var countryCode = resp && resp.country ? resp.country : '';
// 				success(countryCode);
// 			});
// 		},
// 		utilsScipt: 'node_modules/intl-tel-input/build/js/utils.js'
// 	});
// }
// //intl-tel-input

// send report to rocketfirm.com
// 	$('.error-page .btn').on('click', function() {
// 		$(this).hide();
// 		var url = '/mainpage/default/error/';
// 		var errorMessage = $('.error-page .error-message').text();
// 		var location = document.location.href;
// 		var csrf = $('.error-page .error-message').data('csrf');

// 		$.ajax({
// 			type: 'POST',
// 			url: url,
// 			data: {
// 				errorMessage: errorMessage,
// 				location: location,
// 				_csrf: csrf
// 			},
// 			success: function(data) {
// 				if (data == 1) {
// 					$('.error-page .alert-info').removeClass('hidden');
// 				} else {
// 					$('.error-page .alert-danger').removeClass('hidden');
// 				}
// 			}
// 		});
// 	});
// });

// var socialCallback2 = function(data) {
// 	$('.btn--sharing.fb').click(function() {
// 		window.open(data.facebook.link);
// 	});
// 	$('.btn--sharing.fb .number').html(data.facebook.count);

// 	$('.btn--sharing.twi').click(function() {
// 		window.open(data.twitter.link);
// 	});
// 	$('.btn--sharing.twi .number').html(data.twitter.count);

// 	$('.btn--sharing.vk').click(function() {
// 		window.open(data.vkontakte.link);
// 	});
// 	$('.btn--sharing.vk .number').html(data.vkontakte.count);

// 	$('.btn--sharing.wh').attr('href', data.watsup.link);
// 	$('.btn--sharing.wh').click(function() {
// 		ajax.doit(
// 			'/totalsharing/index/',
// 			{ sn: 'wu', link: data.watsup._url },
// 			'GET'
// 		);
// 	});
// 	$('.btn--sharing.wh .number').html(data.watsup.count);
// };
