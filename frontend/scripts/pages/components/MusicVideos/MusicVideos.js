import React, {Component} from 'react'
import {observer} from 'mobx-react'

@observer
class MusicVideos extends Component{

    render() {
        const {handleChange} = this.props.store
        return(
            <div>
                <label> Вас зовут
                    <input 
                        type="text"
                        name="fullName"
                        placeholder="ФИО"
                        onChange={handleChange}
                    />
                </label>
                <div className="form-fl">
                    <label> Телефон
                        <input 
                            type="tel"
                            id="intphone"
                            name="phone"
                            onChange={handleChange}
                        />
                    </label>
                    <label> Почта
                        <input 
                            type="email"
                            name="mail"
                            placeholder="example@gmail.com"
                            onChange={handleChange}
                        />
                    </label>
                    <label> Исполнитель
                        <input 
                            type="text"
                            name="author"
                            placeholder="Имя или псевдоним"
                            onChange={handleChange}
                        />
                    </label>
                    <label> Название песни
                        <input 
                            type="text"
                            name="songName"
                            placeholder="Название песни"
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <label> Клип
                    <input 
                        type="text"
                        name="songMovie"
                        placeholder="Вставьте ссылку на клип"
                        onChange={handleChange}
                    />
                </label>
            </div>
        )
    }
}

export default MusicVideos