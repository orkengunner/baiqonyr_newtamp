import React, {Component} from 'react'

class MusicVideosMembers extends Component {
    render() {
        const {
            handleAddInput,
            handleAddNewInput,
            fieldsCount,
            fields,
        } = this.props.store
        console.log(fieldsCount)
        return(
            <div>
                <div className="members">
                    <h4>Укажите участиков команды</h4>
                    <div className="form-fl">
                        <label>Режиссер
                            <input 
                                type="text" 
                                name="member" 
                                placeholder="Введите фамилию и имя"
                                onChange={handleAddNewInput}
                            />
                        </label>
                        <label className="file">
                            <input className="file__chot" onClick={handleAddInput} type="button" name="file" />
                        </label>
                    </div>
                </div>
            </div>
        )
    }
}

export default MusicVideosMembers