import React, {Component} from 'react'

class InputMembers extends Component {

	constructor(props) {
		super(props)

		this.state = {
			count: 0
		}
	}

	handleAddInp = (e) => {
		e.preventDefault()

		this.setState({
			count: this.state.count + 1
		})
	}

	render() {
		const {addMember} = this.props.store

		let fields = []

		for(let i = 0; i < this.state.count; i++) {
			fields.push(
				<label>
					<input
						type="text"
						name={this.props.name + i}
						placeholder="Введите фамилию и имя"
						onChange={addMember}
					/>
				</label>
			)
		}

		return (
			<div>
					<div className="form-fl">
						<label>{this.props.label}
							<input
								type="text"
								name={this.props.name}
								placeholder="Введите фамилию и имя"
								onChange={addMember}
							/>
						</label>
						<label className="file">
							<input className="file__chot" type="button" onClick={this.handleAddInp} name="+" />
						</label>
						{fields}
					</div>
				</div>
		)
	}
}

export default InputMembers