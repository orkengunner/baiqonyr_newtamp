import React, {Component} from 'react'

class CheckboxInputs extends Component {

	render() {
		const {handleCheckbox} = this.props.store
		return(
			<div>
				<label htmlFor="check1" className="checkbox">
					<input type="checkbox" onChange={handleCheckbox} name="regulations" id="check1"/>
						<span className="checkmark"/>
						<p> Я ознакомлен(а) и принимаю условия, <br/> описанные в <a href="#">Регламенте
							кинофестиваля</a>
						</p>
				</label>

				<label htmlFor="check2" className="checkbox">
					<input
						type="checkbox"
						onChange={handleCheckbox}
						name="situation"
						id="check2"
					/>
						<span className="checkmark"/>
						<p> Я ознакомлен(а) и принимаю условия, <br/> описанные в <a href="#">Положение
							кинофестиваля</a>
						</p>
				</label>

				<label htmlFor="check3" className="checkbox">
					<input onChange={handleCheckbox} type="checkbox" name="dataСollection" id="check3" />
						<span className="checkmark"/>
						<p> Я предоставляю согласие Республиканскому <br/> фестивалю короткометражного кино на
							сбор, <br/> накопление, хранение и обработку моих <br/> персональных данных</p>
				</label>
			</div>
		)
	}

}

export default CheckboxInputs