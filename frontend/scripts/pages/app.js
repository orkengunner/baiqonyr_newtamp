/* eslint-disable no-plusplus */
/* eslint-disable prefer-template */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable one-var */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
import { HelloRocket } from '../plugins/HelloRocket';
import './Testform'

HelloRocket();

var menuMobile = function() {
	var menuToggle = $('.js-menu-toggle'),
		searchToggleBtn = $('.form--secondary .icon--search');

	if ($(window).width() > 480 && $(window).width() < 1025) {
		$('body').prepend('<div class="menu-backdrop"></div>');
	}

	menuToggle.on('click', function() {
		console.log('click');
		if (
			$(this)
				.parents('.header')
				.hasClass('is-opened')
		) {
			$('body').css('overflow', 'initial');
			$(this)
				.parents('.header')
				.removeClass('is-opened');
			$('.menu').removeClass('is-opened');
			if ($(window).width() > 480 && $(window).width() < 1025) {
				$('.menu-backdrop').fadeOut();
			}
		} else {
			if ($(window).width() > 480 && $(window).width() < 1025) {
				$('.menu-backdrop').fadeIn();
			}
			$('body').css('overflow', 'hidden');
			$(this)
				.parents('.header')
				.addClass('is-opened');
			$('.menu').addClass('is-opened');
		}
	});

	$('.menu [data-toggle="modal"]').on('click', function() {
		if (menuToggle.parents('.header').hasClass('is-opened')) {
			if ($(window).width() > 480 && $(window).width() < 1025) {
				$('.menu-backdrop').fadeOut();
			}
			menuToggle.parents('.header').removeClass('is-opened');
			$('.menu').removeClass('is-opened');
			$('body').css('overflow', 'initial');
		}
	});

	$('.menu-backdrop').on('click', function() {
		if ($(window).width() > 480 && $(window).width() < 1025) {
			$(this).fadeOut();
		}
		menuToggle.parents('.header').removeClass('is-opened');
		$('.menu').removeClass('is-opened');
		$('body').css('overflow', 'initial');
	});

	var dropdownsMobile = (function() {
		var dropdowns = $('.nav--primary .dropdown-menu'),
			btnArrow = $('.btn--arrow');

		var makeHeightArray = (function() {
			for (var i = 0; i < dropdowns.length; i++) {
				$(dropdowns[i]).attr('data-height', '' + $(dropdowns[i]).height() + '');
				$(dropdowns[i]).css('height', String($(dropdowns[i]).height()));
			}
			dropdowns.css('height', '0');
		})();

		btnArrow.on('click', function() {
			if ($(this).hasClass('is-opened')) {
				$(this).removeClass('is-opened');
				$(this)
					.parent()
					.find('.dropdown-menu')
					.css('height', '0');
			} else {
				btnArrow.removeClass('is-opened');
				dropdowns.css('height', '0');
				$(this).addClass('is-opened');
				$(this)
					.parent()
					.find('.dropdown-menu')
					.css(
						'height',
						String(
							$(this)
								.parent()
								.find('.dropdown-menu')
								.data('height')
						)
					);
			}
		});
	})();
};

var dommanipulate = function() {
	$('.winner-1').insertBefore($('.winner-2'));
	for (var i = 0; i < $('.people-item .people-image').length; i++) {
		$($('.people-item .people-image')[i]).prependTo(
			$($('.people-item .people-image')[i]).parents('.people-item')
		);
		$($('.people-item .people-image')[i])
			.parents('.people-item')
			.find('.people-meta')
			.appendTo(
				$($('.people-item .people-image')[i])
					.parents('.people-item')
					.find('.people-body')
			);
	}
};

if ($(window).width() >= 320 && $(window).width() < 1025) {
	menuMobile();
	dommanipulate();
}
