import React, {Component} from 'react'
import ReactDOM from 'react-dom'

import {Provider} from 'mobx-react'
import {store} from './store/store'

import MusicVideos from './components/MusicVideos/MusicVideos'
import MusicVideosMembers from './components/MusicVideos/MusicVideosMembers'
import InputMembers from './components/MusicVideos/InputMembers';
import InputPersonal from './components/MusicVideos/InputPersonal'
import CheckboxInputs from './components/MusicVideos/CheckboxInputs';


class Testform extends Component{

    render() {
      const {checkStore} = this.props.store
        return(
            <div>
                <MusicVideos store={store} />
                {/*<MusicVideosMembers store={store} />*/}
                {/*<InputPersonal />*/}
                <div className="members">
                  <h4>Укажите участиков команды</h4>
                  <div className="form-fl">
                    <InputMembers name="producer" label="Режиссер" store={store}/>
                    <InputMembers name="screenwriter" label="Сценарист" store={store}/>
                    <InputMembers name="operator" label="Оператор" store={store}/>
                    <InputMembers name="artist" label="Художник" store={store}/>
                    <InputMembers name="filmEditor" label="Режиссер монтажа" store={store}/>
                    <InputMembers name="drawing" label="Графика" store={store}/>
                  </div>
                </div>

              <CheckboxInputs store={store}/>
              <button className="submit" onClick={checkStore}>Отправить заявку</button>
            </div>
        )
    }
}

export default Testform

const wrapper = document.getElementById("music-video-form");
wrapper ? ReactDOM.render(
    <Provider>
        <Testform store={store}/>
    </Provider>
    , wrapper) : false;