/* eslint-disable no-undef */
/* eslint-disable one-var */
/* eslint-disable no-unused-vars */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
if ($('#map').length) {
	var map, myIcon, myDivIcon;

	DG.then(function() {
		map = DG.map('map', {
			center: [43.233774, 76.976138],
			zoom: 15,
			minZoom: 12,
			maxZoom: 18,
			geoclicker: false,
			zoomControl: false,
			fullscreenControl: false,
			dragging: false,
			touchZoom: false,
			scrollWheelZoom: false,
			doubleClickZoom: false
		});
		myIcon = DG.icon({
			iconUrl: '/images/icons/iconmap.png',
			iconSize: [46, 77],
			iconAnchor: [23, 77]
		});
		DG.marker([43.233774, 76.976138], {
			icon: myIcon
		}).addTo(map);
	});
}
