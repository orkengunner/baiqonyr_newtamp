import { observable, action } from 'mobx';

class Store {

    @observable personal = {
        fullName: '',
        phone: '',
        mail: '',
        author: '',
        songName: '',
        songMovie: ''
    }

    @observable members = {
        producer: '',
        screenwriter: '',
        operator: '',
        artist: '',
        filmEditor: '',
        drawing: ''
    }

    @observable rulesCheckbox = {
        regulations: false,
        situation: false,
        dataСollection: false
    }

    // внесение личных данных в personal{}
    @action.bound handleChange = (e) => {
        const name = e.target.name
        this.personal[name] = e.target.value
    }

    // внесение данных о участниках команды в members{}
    @action.bound addMember = (e) => {
        const name = e.target.name
        this.members[name] = e.target.value
    }

    // изменение состояния чекбоксов rulesCheckbox{}
    @action.bound handleCheckbox = (e) => {
        const name = e.target.name
        this.rulesCheckbox[name] = !this.rulesCheckbox[name]
    }

    // кнопка "отправить заявку"{}
    @action.bound checkStore = (e) => {
        e.preventDefault()
        console.log('personal: ', this.personal)
        console.log('members: ', this.members)
        console.log('rulesCheckbox: ', this.rulesCheckbox)
    }

}

export const store = new Store()
