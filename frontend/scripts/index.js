const entry = {
	program: './pages/program.js',
	app: './pages/app.js',
	main: './pages/main.js'
};

module.exports = entry;
