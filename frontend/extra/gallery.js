/* eslint-disable no-plusplus */
/* eslint-disable no-undef */
/* eslint-disable one-var */
/* eslint-disable object-shorthand */
/* eslint-disable no-unused-vars */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
console.log('hi from gallery');

$('#gallery-about').lightSlider({
	gallery: true,
	item: 1,
	thumbWidth: 50,
	thumbItem: 7,
	thumbMargin: 4,
	slideMargin: 0,
	loop: true
});

var articlesGallery = function(gal) {
	var articleGallerySlider = gal.find('.article-slider').lightSlider({
		gallery: true,
		item: 1,
		loop: true,
		thumbItem: $(window).width() < 481 ? 2 : 5,
		thumbMargin: 10,
		slideMargin: 0,
		enableDrag: false,
		currentPagerPosition: 'left',
		onSliderLoad: function(el) {
			el.lightGallery({
				selector: '.article-slider .lslide'
			});
		},
		onAfterSlide: function() {
			var activeSlide = gal.find('.article-slider').find('.active'),
				slideDescriptionData = activeSlide.data('description'),
				slideDescription = gal.find('.article-gallery-description');

			slideDescription.text(slideDescriptionData);
		}
	});
};

var articlesGalleryLoop = function() {
	for (var i = 0; i < $('.article-gallery').length; i++) {
		articlesGallery($($('.article-gallery')[i]));
	}
};

if ($('.article-gallery').length > 0) {
	articlesGalleryLoop();
}
