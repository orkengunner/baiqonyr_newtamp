CKEDITOR.plugins.add('photodesc', {
  icons: 'photodesc',
  init: function (editor) {
    editor.ui.addButton('Photodesc', {
      label: 'Вставить подпись к фото',
      command: 'photodescDialog',
      toolbar: 'insert'
    });

    editor.addCommand('photodescDialog', new CKEDITOR.dialogCommand('photodescDialog'));

    CKEDITOR.dialog.add('photodescDialog', this.path + 'dialogs/dialog.js');
  }
});
