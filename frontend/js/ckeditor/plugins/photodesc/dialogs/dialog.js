CKEDITOR.dialog.add('photodescDialog', function (editor) {
  return {
    title: 'Введите подпись к фото',
    minWidth: 360,
    minHeight: 120,

    contents: [
      {
        id: 'photodesc_dialog',
        elements: [
          {
            type: 'textarea',
            id: 'desc_text',
            label: 'Текст подписи. Каждый пункт подписи с новой строки',
            style: 'width:350px;',
            validate: CKEDITOR.dialog.validate.notEmpty("Введите текст")
          }
        ]
      }
    ],
    onOk: function () {
      var dialog = this;
      var desc_text = dialog.getValueOf('photodesc_dialog', 'desc_text');
      var items = desc_text.split("\n");
      var content;
      content = '<ul class="list-inline text-center">';
      for (var ind = 0; ind < items.length; ind++)
        content += '<li>' + items[ind] + '</li>';
      content += '</ul>';
      editor.insertHtml(content);
    },
    onCancel: function () {
    }
  };
});
