CKEDITOR.dialog.add('inquoteDialog', function (editor) {
  return {
    title: 'Введите текст для врезки',
    minWidth: 250,
    minHeight: 50,

    contents: [
      {
        id: 'inquote_dialog',
        elements: [
          {
            type: 'textarea',
            id: 'quote_text',
            label: 'Текст врезки',
            style: 'width:240px;',
            validate: CKEDITOR.dialog.validate.notEmpty("Введите текст")
          }
        ]
      }
    ],

    onOk: function () {
      var dialog = this;
      var quote_text = dialog.getValueOf('inquote_dialog', 'quote_text');
      editor.insertHtml('<hr><h2 class="text-center">' + quote_text + '</h2><hr>');
    },
    onCancel: function () {
    }
  };
});
