CKEDITOR.plugins.add('inquote', {
  icons: 'inquote',
  init: function (editor) {
    editor.ui.addButton('Inquote', {
      label: 'Вставить врезку',
      command: 'inquoteDialog',
      toolbar: 'insert'
    });

    editor.addCommand('inquoteDialog', new CKEDITOR.dialogCommand('inquoteDialog'));

    CKEDITOR.dialog.add('inquoteDialog', this.path + 'dialogs/dialog.js');
  }
});
