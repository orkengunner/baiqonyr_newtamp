<?php
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
setlocale(LC_ALL, 'ru_RU.UTF-8');
setlocale(LC_NUMERIC, 'ru_RU.UTF-8');
date_default_timezone_set('Asia/Almaty');

$languages = array(
    1=>'ru',
    2=>'kz',
    3=>'en'
);
$url = explode('/', trim(str_replace('?' . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']), '/'))[0];
$current_url = $url;
if(!$url){
    $current_lang = 1;
    $url = 'ru';
}else{
    if(!$current_lang = array_search($url, $languages)){
        $current_lang = 1;
        $url = 'ru';
    }
}

$app = dirname(__DIR__);
$vendorPath = $app . '/vendor';
$result = [
    'yiiPath' => $vendorPath . '/yiisoft/yii2/Yii.php',
    'yiiDebug' => false,
    'aliases' => array(
        'vendor' => $vendorPath,
        'root' => $app,
        //'runtime' => $app . "/runtime",
        'web' => $app . '/web',
        'media' => $app . '/web/media',
        'tests' => $app . '/tests',
        'console' => $app . '/console',
    ),
    'classMap' => [
        'yii\helpers\StringHelper' => $app . '/helpers/StringHelper.php',
        'yii\helpers\FileHelper' => $app . '/helpers/FileHelper.php',
        'yii\helpers\ArrayHelper' => $app . '/helpers/ArrayHelper.php',
        'yii\helpers\Url' => $app . '/helpers/Url.php'
    ],
    'web' => array(
        'basePath' => dirname(__DIR__),
        'name' => 'raimbek',
        'id' => 'raimbek',
        'language' => $url,
        'sourceLanguage' => 'en',
        'bootstrap' => ['log'],
        'timeZone' => 'Asia/Almaty',
        'defaultRoute' => 'mainpage/default/index',
        'modules' => [
            'mainpage' => [
                'class' => 'app\modules\mainpage\Module',
            ],
            'config' => [
                'class' => 'app\modules\config\Module',
            ],
            'pages' => [
                'class' => 'app\modules\pages\Module'
            ],
            'posts' => [
                'class' => 'app\modules\posts\Module'
            ],
            'comments' => [
                'class' => 'app\modules\comments\Module'
            ],
            'gallery' => [
                'class' => 'app\modules\gallery\Module'
            ],
            'users' => [
                'class' => 'app\modules\users\Module'
            ],
            'languages' => [
                'class' => 'app\modules\languages\Module'
            ],
            'categories' => [
                'class' => 'app\modules\categories\Module'
            ],
            'menu' => [
                'class' => 'app\modules\menu\Module'
            ],
            'banners' => [
                'class' => 'app\modules\banners\Module'
            ],
            'search' => [
                'class' => 'app\modules\search\Module',
            ],
            'translate' => [
                'class' => 'app\modules\translate\Module'
            ],
            'tags' => [
                'class' => 'app\modules\tags\Module'
            ],

            'festivals' => [
                'class' => 'app\modules\festivals\Module'
            ],

            'nominations' => [
                'class' => 'app\modules\nominations\Module'
            ],

            'jobs' => [
                'class' => 'app\modules\jobs\Module'
            ],

            'lookupposts' => [
                'class' => 'app\modules\lookupposts\Module'
            ],

            'participants' => [
                'class' => 'app\modules\participants\Module'
            ],
            'juri' => [
                'class' => 'app\modules\juri\Module'
            ],
            'winners' => [
                'class' => 'app\modules\winners\Module'
            ],
            'sponsors' => [
                'class' => 'app\modules\sponsors\Module'
            ],
            'leading' => [
                'class' => 'app\modules\leading\Module'
            ],
            'news' => [
                'class' => 'app\modules\news\Module'
            ],
            'emailtemplates' => [
                'class' => 'app\modules\emailtemplates\Module'
            ],

            'debug' => [
                'class' => 'yii\debug\Module',
                'allowedIPs' => ['*']
            ]
        ],
        'components' => [
            'request' => [
                'enableCsrfValidation' => true,
                'cookieValidationKey' => 'sdfepioDzxqwf3246dfgkljdsa'
            ],
            'cache' => [
                'class' => 'yii\caching\FileCache'
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
            ],
            'user' => [
                'identityClass' => 'app\modules\users\models\UserIdentity',
                'enableAutoLogin' => true,
            ],
            'errorHandler' => [
                'errorAction' => 'mainpage/default/error',
            ],
            'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                'useFileTransport' => false,
                'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => 'smtp.mailgun.org',
                    'username' => 'postmaster@mg.baiqonyr.kz',
                    'password' => '50bf15fd186ede77cf7fcd7a61b71af4-f135b0f1-83fdea21',
                    'port' => '465',
                    'encryption' => 'SSL',
                ]
            ],
            'log' => [
                'traceLevel' => 3,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'formatter' => [
                'defaultTimeZone' => 'Asia/Almaty',
                'timeZone' => 'Asia/Almaty',
            ],
            'db' => [
                'class' => 'yii\db\Connection',
                'charset' => 'utf8',
            ],
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'yii\i18n\DbMessageSource'
                    ],
                ],
            ],
            'eauth' => [
                'class' => 'nodge\eauth\EAuth',
                'popup' => true, // Use the popup window instead of redirecting.
                'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
                'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
                'httpClient' => [],
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
                'services' => [
                    'twitter' => [
                        // register your app here: https://dev.twitter.com/apps/new
                        'class' => 'app\components\eauth\TwitterOAuth1Service',
                        'key' => 'NEmRyybS9VWCUHIkUM0y5Q',
                        'secret' => 'rwfGzRHzcVVuwY3ImdYf5mBTIbr4DWwvdRUZMjnoY',
                    ],
                    'facebook' => [
                        // register your app here: https://developers.facebook.com/apps/
                        'class' => 'app\components\eauth\FacebookOAuth2Service',
                        'clientId' => '139918989492028',
                        'clientSecret' => '7ab782fea8ec6c2e6f51b64da793e8d4',
                    ],
                    'vkontakte' => [
                        // register your app here: https://vk.com/editapp?act=create&site=1
                        'class' => 'app\components\eauth\VKontakteOAuth2Service',
                        'clientId' => '3436742',
                        'clientSecret' => 'gRldb1QT3QCxds7jmhhU',
                    ],
                ]
            ],
            'devicedetect' => [
                'class' => 'alexandernst\devicedetect\DeviceDetect'
            ],
        ],
        'params' => [
            'ftp_dir' => '/media/jobs/user_uploads',
            'adminEmail' => 'info@raimbekgroup.kz',
            'wMail' => [
                'domain' => 'baikonur.brod.kz',
                'system_mail' => 'info@brod.kz',
                'easySend' => false,
                'smtp' => [
                    'smtp_host' => 'smtp.yandex.ru',
                    'smtp_username' => 'baykonur.brod@yandex.ru',
                    'smtp_password' => 'wf4FZ17M',
                    'smtp_port' => 465,
                    'smtp_secure' => 'ssl',
                    'smtp_from_email' => 'baykonur.brod@yandex.ru',
                    'smtp_from_name' => 'Администрация Brod.kz',

                ],
                'mails' => [
                    'bug-report' => [
                        'subject' => 'Баг репорт baikonur.brod.kz',
                        'developers' => [
                            //'fackinmail@mail.ru',
                            'support@rocketfirm.com'
                        ]
                    ],
                    'invite-form' => [
                        'subject' => 'Приглашение в группу'
                    ],
                    'forgot-form' => [
                        'subject' => 'Востановление пароля'
                    ],
                ]
            ],
            'uniSender' => [
                'key' => '5euk6tysqbcdyicrunfceeyurqrxm7gfbg754sxe',
                'digestList' => '7012554',
                'senderName' => 'Администрация Brod.kz',
                'senderEmail' => 'prokhonenkov@gmail.com',

            ],
            'users' => [
                'avatar' => [
                    'sizes' => [
                        [49, 48],
                        [40, 41],
                        [128, 128],
                    ],
                ]
            ],
            'pages' => [
                'logo' => [
                    'sizes' => [
                        [725, null],
                    ],
                ]
            ],
            'posts' => [
                'logo' => [
                    'sizes' => [
                        [205, 110],
                        [640, 250],
                        [618, null],
                    ],
                ]
            ],
            'jobs' => [
                'logo' => [
                    'sizes' => [
                        [200, 290],
                        [220, 313],
                        [167, 239],
                    ],
                ],
                'brod' => [
                    'sizes' => [
                        [150, 235],
                        [150, 210],
                        [167, 239],
                        [200, 290],
                        [235, 336],
                        [50, 71],
                    ],
                ]
            ],
			'nominations' => [
                'logo' => [
                    'sizes' => [
                        [269, 396],
                    ],
                ],
            ],
            'juri' => [
                'logo' => [
                    'sizes' => [
                        [128, 128],
                    ],
                ]
            ],

            'leading' => [
                'logo' => [
                    'sizes' => [
                        [220, 129],
                    ],
                ]
            ],
            'gallery' => [
                'image' => [
                    'sizes' => [
                        [114, 69],
                        [620, 500]
                    ]
                ],
            ],
        ],
    ),
    'console' => array(
        'id' => 'basic-console',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log'],
        'controllerNamespace' => 'app\console\controllers',
        'components' => [
            'log' => [
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
        ],
        'params' => [
        ]
    ),
];
return $result;
