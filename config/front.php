<?php
$result = [
    'web' => array(
        'modules' => array(),
        'components' => [
            'assetManager' => [
                'appendTimestamp' => true,
                'bundles' => [
                    'app\assets\AppAsset' => [
                        'packages' => ['main', 'vendor']
                    ],
                ],
            ],
            'request' => [
                'class' => 'app\components\rocket\RFRequest'
            ],
            'user' => [
                'loginUrl' => ['/users/default/login'],
            ],
            'urlManager' => [
                'class' => '\app\components\rocket\RFUrlManager',
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'suffix' => '/',
                'rules' => [

                    'login/' => 'users/default/login',
                    'logout/' => 'users/default/logout',
                    '/' => 'mainpage/default/index',
                    'search' => 'search/default/index',

                    'winners' => 'jobs/default/winners',

                    'jury' => 'juri/default/index',
                    'participants' => 'jobs/default/index',
                    'participants/page-<page:[0-9]{1,}>' => 'jobs/default/index',
                    'participants/<type:applicant|out-of-competition|active>' => 'jobs/default/index',
                    'participants/<type:applicant|out-of-competition|active>/page-<page:[0-9]{1,}>' => 'jobs/default/index',
                    'participants/<job_slug>' => 'jobs/default/view',

                    'statuette' => 'pages/default/statuette',
                    'ticket' => 'pages/default/ticket',
                    'request' => 'pages/default/request',
                    'request/<step:1|2|3>' => 'pages/default/request',
                    'program' => 'pages/default/program',
                    'program/<program_slug>' => 'pages/default/program',
                    'rules' => 'pages/default/rules',
                    'about' => 'pages/default/about',
                    'team' => 'pages/default/team',

					'history/<year:\d{4}>' => 'pages/default/history',

					'history/<year:\d{4}>/<page_slug:jury>' => 'pages/default/history-jury',
					'history/<year:\d{4}>/<page_slug:program>' => 'pages/default/history-program',
					'history/<year:\d{4}>/<page_slug:winners>' => 'pages/default/history-winners',
					'history/<year:\d{4}>/<page_slug:participants>' => 'pages/default/history-participants',
					'history/<year:\d{4}>/<page_slug:participants>/page-<page:[0-9]{1,}>' => 'pages/default/history-participants',
					'history/<year:\d{4}>/<page_slug:participants>/<type:applicant|out-of-competition|active>' => 'pages/default/history-participants',
					'history/<year:\d{4}>/<page_slug:participants>/<type:applicant|out-of-competition|active>/page-<page:[0-9]{1,}>' => 'pages/default/history-participants',

					'history' => 'pages/default/history',
                    'history/<page_slug:jury>' => 'pages/default/history-jury',
                    'history/<page_slug:program>' => 'pages/default/history-program',
                    'history/<page_slug:winners>' => 'pages/default/history-winners',
                    'history/<page_slug:participants>' => 'pages/default/history-participants',
                    'history/<page_slug:participants>/page-<page:[0-9]{1,}>' => 'pages/default/history-participants',
                    'history/<page_slug:participants>/<type:applicant|out-of-competition|active>' => 'pages/default/history-participants',
                    'history/<page_slug:participants>/<type:applicant|out-of-competition|active>/page-<page:[0-9]{1,}>' => 'pages/default/history-participants',




                    'contact-us' => 'pages/default/contacts',
                    '<page_slug>' => 'pages/default/index',

                    '<module>/<action>' => '<module>/default/<action>',
                    '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                ]
            ],
            'mailchimp' => [
                'class' => 'sammaye\mailchimp\Mailchimp',
                'apikey' => '71591265d4306101f25e75e2c0a0f25b-us8'
            ],
            'sphinx' => [
                'class' => 'yii\sphinx\Connection',
                'dsn' => 'mysql:host=127.0.0.1;port=9306;',
            ],
            'errorHandler' => [
                'errorAction' => 'mainpage/default/error',
            ],
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'yii\i18n\DbMessageSource',
                        'on missingTranslation' => [
                            'app\modules\translate\events\MissingTranslationHandler',
                            'handleMissingTranslation'
                        ],
                        'forceTranslation' => false
                    ],
                ],
            ],
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
            ],
            'feed' => [
                'class' => 'yii\feed\FeedDriver',
            ],
        ],
        'params' => [
            'yiiEnd' => 'front'
        ],
    ),
];
return $result;
