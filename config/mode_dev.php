<?php
return [
    'yiiDebug' => true,
    'yiiEnv' => 'dev',
    'web' => [
        'components' => [
            'db' => [
                'dsn' => 'mysql:host=localhost;dbname=admin_baiqonyr_test',
                'username' => 'baikonur_user',
                'password' => 'Fvv87j7?',
                'enableSchemaCache' => true,
                'schemaCacheDuration' => 86400
            ],
            'dbBrod' => [
                'class' => '\yii\db\Connection',
                'emulatePrepare' => true,
                'dsn' => 'mysql:host=localhost;dbname=admin_brod_test',
                'username' => 'brod_user',
                'password' => 'Tqhk3$36',
                'charset' => 'UTF8'
            ],
            'cache' => [
                'class' => 'yii\caching\MemCache',
                'useMemcached' => true,
                'keyPrefix' => 'baykonur',
                'servers' => [
                    [
                        'host' => 'localhost',
                        'port' => 11211,
                        'weight' => 60,
                    ]
                ],
            ],
        ],

    ],
    'console' => [
        'components' => [
            'db' => [
                'class' => '\yii\db\Connection',
                'dsn' => 'mysql:host=localhost;dbname=db_raimbek',
                'username' => 'db_raimbek',
                'password' => 'm2F8V9AwbDG6pG64',
                'enableSchemaCache' => true,
                'schemaCacheDuration' => 86400
            ],
        ],
    ]
];
