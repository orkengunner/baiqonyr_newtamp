<?php
return [
    'yiiDebug' => true,
    'yiiEnv' => 'test',
    'web' => [
        'components' => [
            'db' => [
                'dsn' => 'mysql:host=localhost:8889;dbname=raimbek',
                'username' => 'root',
                'password' => 'root',
                'enableSchemaCache' => true,
                'schemaCacheDuration' => 86400
            ],
        ],
    ],
    'console' => [
        'components' => [
            'db' => [
                'class' => '\yii\db\Connection',
                'dsn' => 'mysql:host=localhost:8889;dbname=raimbek',
                'username' => 'root',
                'password' => 'root',
                'enableSchemaCache' => true,
                'schemaCacheDuration' => 86400
            ],
        ],
    ]
];
