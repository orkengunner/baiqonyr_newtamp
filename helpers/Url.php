<?php
namespace yii\helpers;
class Url extends BaseUrl
{

    public static function isRoute($c, $a = null)
    {
        if (is_array($c)) {
            foreach ($c as $parts) {
                if (is_array($parts))
                {
                    $isRoute=self::isRoute($parts[0], $parts[1]);
                } else {
                    $isRoute=self::isRoute($parts);
                }
                if ($isRoute)
                    return true;
            }
            return false;
        }
        return \Yii::$app->controller->id == $c && ($a == null || \Yii::$app->controller->action->id == $a);
    }

    public static function host($scheme = true)
    {
        return ($scheme ? 'http://' : '') . \Yii::$app->params['host'];
    }

    public static function imgUrl($image, $table,  $size = [], $isFile = true, $defaultExt = 'png')
    {
        $image = pathinfo($image);
        $sizes = (
        $size
            ? '-' . (
            $size[0]
                ? $size[0]
                : ''
            ) . 'x' . (
            empty($size[1])
                ? ''
                : $size[1]
            )
            : ''
        );

        if (!$image || empty($image['dirname']) || ($isFile && !is_file($_SERVER['DOCUMENT_ROOT'].'/media/' . $table .  '/' . $image['dirname'] . '/' . $image['filename'] . $sizes . '.' . $image['extension'])))
            return '/images/default/' . $table . $sizes . '.' . $defaultExt;

        return '/media/' . $table .  '/' . $image['dirname'] . '/' . $image['filename'] . $sizes . '.' . $image['extension'];
    }

    public static function parse_url($url = null)
    {
        if (!$url) $url = parse_url($_SERVER['REQUEST_URI']);
        else $url = parse_url($url);
        $query = [];
        if(isset($url['query'])) foreach (explode('&', $url['query']) as $v) {
            $_v = explode('=', $v);
            $query[$_v[0]] = $_v[1];
        }

        return [
            'path' => $url['path'],
            'params' => $query
        ];
    }
}