<?php
namespace yii\helpers;

use yii\base\Exception;
use yii\web\UploadedFile;

class FileHelper extends BaseFileHelper
{
    public static $storageDir = 'media';

    /**
     * Import sql file to database
     * @param string $file
     * @param \PDO $pdo
     * @throws \Exception
     */
    public static function importSql($file, $pdo)
    {
        if (file_exists($file)) {
            $sqlStream = file_get_contents($file);
            $sqlStream = rtrim($sqlStream);
            $newStream = preg_replace_callback("/\((.*)\)/",
                create_function('$matches', 'return str_replace(";"," $$$ ",$matches[0]);'), $sqlStream);
            $sqlArray = explode(";", $newStream);
            foreach ($sqlArray as $value) {
                if (!empty($value)) {
                    $sql = str_replace(" $$$ ", ";", $value) . ";";
                    $pdo->exec($sql);
                }
            }
        } else {
            throw new \Exception("file not found");
        }
    }

    public static function download($url, $path, $tryCount = 1)
    {
        $content = @file_get_contents($url);
        if ($content === false) {
            $tryCount--;
            if ($tryCount) {
                return self::download($url, $path, $tryCount);
            }
            return false;
        }
        file_put_contents($path, $content);
        return true;
    }

    public static function generateName($ext, $suffix = null)
    {
        if (!$suffix) {
            return time() . StringHelper::random(5, 5) . '.' . $ext;
        }
        return time() . StringHelper::random(5, 5) . '-' . $suffix . '.' . $ext;
    }

    public static function generateSubdir($path, $check = true)
    {
        if ($check && !is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $subdir = StringHelper::random(mt_rand(2,6), 6);

        if (is_dir($path . '/' . $subdir)){
            return self::generateSubdir($path, $check);
        }

        if ($path && $check && !is_dir($path . '/' . $subdir)) {
            mkdir($path . '/' . $subdir);
        }
        return $subdir;
    }

    /**
     * @param UploadedFile $file
     * @param string $path
     * @param bool $subdir
     * @return string new name
     */
    public static function saveUploaded($file, $path, $subdir = true)
    {
        $path = static::getStoragePath(true, $path);
        if ($subdir) {
            $subdir = static::generateSubdir($path);
        } else {
            $subdir = '';
            if (!is_dir($path)) {
                if (!is_dir(dirname($path))) {
                    mkdir(dirname($path));
                }
                mkdir($path);
            }
        }
        $newName = static::generateName($file->extension);
        $result = $subdir ? $subdir . '/' . $newName : $newName;
        $saved = $file->saveAs($path . '/' . $result);
        if (!$saved) {
            throw new \Exception('Could not save uploaded file: ' . $file->error . ' to path ' . $path . '/' . $result);
        }
        return $result;
    }

    /**
     * @param UploadedFile $file
     * @param string $path
     * @param bool $subdir
     * @return string new name
     */
    public static function saveFromUrl($url, $path, $subdir = true)
    {
        $path = static::getStoragePath(true, $path);
        if ($subdir) {
            $subdir = static::generateSubdir($path);
        } else {
            $subdir = '';
            if (!is_dir($path)) {
                if (!is_dir(dirname($path))) {
                    mkdir(dirname($path));
                }
                mkdir($path);
            }
        }
        $rawExt = pathinfo($url, PATHINFO_EXTENSION);
        $extension = '';
        if ($rawExt) {
            $extension = $rawExt;
        }

        $newName = static::generateName($extension);

        $result = $subdir ? $subdir . '/' . $newName : $newName;

        $content = @file_get_contents($url);
        if ($content === false) {
            throw new Exception('Could not load file from url: ' . $url);
        }

        $saved = file_put_contents($path . '/' . $result, $content);

        if (!$saved) {
            throw new Exception('Could not save file from url:' . $path . '/' . $result);
        }

        return $result;
    }

    public static function getStoragePath($abs = false, $name = null)
    {
        if ($abs) {
            $path = \Yii::getAlias('@' . static::$storageDir);
        } else {
            $path = '/' . self::$storageDir;
        }
        if (!$name) {
            return $path;
        } else {
            return $path . '/' . trim($name, '/');
        }
    }

    public static function getImageSize($path, $makeAbs = false)
    {
        $fullPath = $makeAbs ? (\Yii::getAlias('@webroot') . $path) : $path;
        if (!file_exists($fullPath)) {
            return null;
        } else {
            $size = getimagesize($fullPath);
            return [$size[0], $size[1]];
        }
    }

    public static function rmdirContent($dir)
    {
        if (!$dh = opendir($dir)) {
            return;
        }
        while (false !== ($obj = readdir($dh))) {
            if ($obj == '.' || $obj == '..') {
                continue;
            }
            if (is_dir($dir . '/' . $obj)) {
                self::rmdirContent($dir . '/' . $obj, true);
            } else {
                unlink($dir . '/' . $obj);
            }
        }
        closedir($dh);
    }

    public static function forceDirectories($path)
    {
        if (strlen($path) == 0)
            return false;
        if (strlen($path) < 3)
            return true;
        else
            if (is_dir($path))
                return true;
            else
                if (dirname($path) == $path)
                    return true;
        return (self::forceDirectories(dirname($path)) and mkdir($path, 0775) and chmod($path, 0775));
    }

    public static function deleteDirectories($path)
    {
        if (!is_dir($path)) return false;
        $dir = opendir($path);
        while (($file = readdir($dir)) !== false)
        {
            if ($file != '.' && $file != '..')
            {
                $item = $path . (substr($path, -1) == '/' ? '' : '/') . $file;
                if (is_dir($item)) self::deleteDirectories($item);
                else if(is_file($item)) unlink($item);
            }
        }
        closedir($dir);
        try{@rmdir($path); } catch (exception $e){}

    }

    public static function downloadFile($file, array $params = array())
    {
        if (!file_exists($file)) return false;

        if (!isset($params['size']))
            $params['size'] = filesize($file);
        if (!isset($params['mime']))
            $params['mime'] = mime_content_type($file);
        if (!isset($params['filename']))
            $params['filename'] = basename($file);

        // required for IE, otherwise Content-disposition is ignored
        if (ini_get('zlib.output_compression')) ini_set('zlib.output_compression', 'Off');
        header('Pragma: public'); // required
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false); // required for certain browsers
        header('Content-Type: ' . $params['mime']);
        header('Content-Disposition: attachment; filename="' . $params['filename'] . '";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . $params['size']);
        readfile($file);
        exit;
    }

    public static function saveImageFromBase64($base64, $savePath, $postfix = '')
    {
        preg_match('/data:image\/(.*?);base64/', $base64, $ext);
        if(!$ext) return false;
        $data = str_replace('data:image/' . $ext[1] . ';base64,', '', $base64);
        $data = str_replace(' ', '+', $data);
        $filename = md5(microtime()) . $postfix;
        $basename = $filename . '.' . $ext[1];
        $savePath = rtrim($savePath, '/');
        $subDir = FileHelper::generateSubdir($savePath);
        FileHelper::forceDirectories($savePath . '/' . $subDir);
        file_put_contents($savePath . '/' . $subDir . '/' . $basename, base64_decode($data));
        return [
            'filename' => $filename,
            'basename' => $basename,
            'subdir' => $subDir,
            'dirname' => $savePath . '/' . $subDir,
            'extension' => $ext[1]
        ];
    }
}
