/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable object-shorthand */
/* eslint-disable no-var */
// intl-tel-input

var input = document.querySelector('#intphone');
window.intlTelInput(input, {
	// any initialisation options go here
	initialCountry: 'KZ',
	geoIpLookup: function(success, failure) {
		$.get('https://ipinfo.io', function() {}, 'jsonp').always(function(resp) {
			var countryCode = resp && resp.country ? resp.country : '';
			success(countryCode);
		});
	}
	// utilsScipt: 'extra/utils.js'
});

// intl-tel-input
