var wAjax=function(){var _options={cursor:!0,timeOut:0,abort:!0},xhr;this.doit=function(s,t,o,e){this.setOptions(e),_options.abort&&xhr&&4!=xhr.readyState&&0!=xhr.readyState&&(xhr.abort(),this.setCursor("default")),_options.timeOut?setTimeout(this.post(s,t,o),_options.timeOut):this.post(s,t,o)},this.post=function(action,data,method){!1!==_options.cursor?this.setCursor("wait"):_options.cursor=!0,xhr=jQuery.ajax({type:"POST"==method?method:"GET",data:data,url:"/"+action.replace(/^\/+|$/gm,""),cache:!1,success:function(response){if(void 0!==response&&""!=response&&(response=JSON.parse(response),void 0!==response.callback&&""!=response.callback))switch(response.callback){case"redirect":window.location.assign(void 0!==response.data?response.data:"");break;case"update":$.pjax.reload({container:response.data.container});break;default:eval(response.callback)(response.data)}if(!1!==_options.cursor){var a=new wAjax;a.setCursor("default")}}})},this.setCursor=function(s){jQuery("body").css("cursor",s)},this.setOptions=function(s){void 0!==s&&jQuery.each(_options,function(t,o){void 0!==s[t]&&(_options[t]=s[t])})}};
var wAjax = function () {
  var _options = {
    cursor: true,
    timeOut: 0,
    abort: true,
  };
  var xhr;
  this.doit = function (action, data, method, options) {
    this.setOptions(options);

    if (_options.abort && xhr && xhr.readyState != 4 && xhr.readyState != 0) {
      xhr.abort();
      this.setCursor('default');
    }
    if (_options.timeOut) {
      setTimeout(this.post(action, data, method), _options.timeOut);
    } else {
      this.post(action, data, method);
    }
  };
  this.post = function (action, data, method) {
    if (_options.cursor !== false) this.setCursor('wait');
    else _options.cursor = true;
    xhr = jQuery.ajax({
      type: method == 'POST' ? method : 'GET',
      data: data,
      url: '/' + action.replace(/^\/+|$/gm, ''),
      cache: false,

      success: function (response) {
        if (typeof response != 'undefined' && response != '') {
          response = JSON.parse(response);
          if (
            typeof response.callback != 'undefined' &&
            response.callback != ''
          ) {
            switch (response.callback) {
              case 'redirect':
                window.location.assign(
                  typeof response.data != 'undefined' ? response.data : ''
                );
                break;
              case 'update':
                $.pjax.reload({ container: response.data.container }); //Reload GridView
                break;
              default:
                eval(response.callback)(response.data);
            }
          }
        }
        if (_options.cursor !== false) {
          var a = new wAjax();
          a.setCursor('default');
        }
      },
    });
  };
  this.setCursor = function (cursor) {
    jQuery('body').css('cursor', cursor);
  };
  this.setOptions = function (options) {
    if (typeof options == 'undefined') return;
    jQuery.each(_options, function (k, v) {
      if (typeof options[k] != 'undefined') _options[k] = options[k];
    });
  };
};
