function declension(e,a,s,c){if(e<0)return!1;var r,n,t=(e=e.toString()).length;if(t<1)var i=parseInt(e[t-2]+e[t-1]);else i=parseInt(e);switch(r=i>=10&&i<=20?0:e>19?e[t-1]:e,parseInt(r)){case 1:n=a;break;case 2:case 3:case 4:n=s;break;case 0:case 5:case 6:case 7:case 8:case 9:n=c}return n}
/**
 * Created by Vitaliy Prokhonenkov on 04.01.2016.
 */
function declension(count, _1, _2_4, _5_0)
{
    if(count < 0) return false;
    count = count.toString();
    var _count;
    var len = count.length;

    if(len < 1) {
        var exclusion = parseInt(count[len-2] + count[len-1])
    } else {
        var exclusion = parseInt(count)
    }

    if(exclusion >= 10 && exclusion <= 20) _count = 0;
    else if (count > 19) _count = count[len-1];
    else _count = count;

    var str;

    switch(parseInt(_count)) {
        case 1:
            str = _1;
            break;
        case 2:
        case 3:
        case 4:
            str = _2_4;
            break;
        case 0:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            str = _5_0;
            break;
    }

    return str;
}