<?php

use yii\db\Migration;

/**
 * Handles adding position to table `sponsors`.
 */
class m190612_045017_add_position_column_to_sponsors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('sponsors', 'position', $this->tinyInteger()->defaultValue(0)->notNull());
    	$this->createIndex('idx-position', 'sponsors', 'position');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
