<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nominations_files_logo`.
 */
class m190808_033949_create_nominations_files_logo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('nominations_files_logo', [
            'nomination_id' => $this->primaryKey(10)->unsigned(),
			'subdir' => $this->string(255),
			'basename' => $this->string(255)
        ]);

        $this->addForeignKey(
        	'FK-nominations_files_logo-nominations',
			'nominations_files_logo',
			'nomination_id',
			'nominations',
			'id',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('nominations_files_logo');
    }
}
