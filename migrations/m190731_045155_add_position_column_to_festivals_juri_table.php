<?php

use yii\db\Migration;

/**
 * Handles adding position to table `festivals_juri`.
 */
class m190731_045155_add_position_column_to_festivals_juri_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('festivals_juri', 'position', $this->integer()->defaultValue(0));
    	$this->createIndex('idx-position', 'festivals_juri', 'position');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
