<?php

use yii\db\Migration;

/**
 * Handles adding country to table `jobs`.
 */
class m190528_035631_add_country_column_to_jobs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('jobs', 'country', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
