<?php

use yii\db\Migration;

/**
 * Handles adding type to table `juty`.
 */
class m190730_032316_add_type_column_to_juty_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->addColumn('festivals_juri', 'type', $this->tinyInteger(1)->unsigned()->defaultValue(1));
    	$this->createIndex('idx-type', 'festivals_juri', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
