<?php

use yii\db\Migration;

/**
 * Handles adding type to table `nominations`.
 */
class m190808_045040_add_type_column_to_nominations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('nominations', 'type', $this->tinyInteger(1)->unsigned()->defaultValue(1));
		$this->createIndex('idx-type', 'nominations', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
