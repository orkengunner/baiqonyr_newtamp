<?php
use yii\helpers\Html;
use yii\helpers\Url;
use \app\modules\languages\models\Languages;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="fixed-left">
<?php $this->beginBody() ?>
<script>
    var resizefunc = [];
</script>
<div id="wrapper">
    <div class="content-page">
        <!-- ============================================================== -->
        <!-- Start Content here -->
        <!-- ============================================================== -->
        <div class="content">
            <!-- Start info box -->
            <?= $content ?>
            <!-- Footer Start -->
            <footer>
                Rocket Firm &copy; 2014
            </footer>
            <!-- Footer End -->
        </div>
        <!-- ============================================================== -->
        <!-- End content here -->
        <!-- ============================================================== -->

    </div>
    <!-- End right content -->

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
