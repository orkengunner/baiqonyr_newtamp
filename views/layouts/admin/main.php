<?php
use yii\helpers\Html;
use yii\helpers\Url;
use \app\modules\languages\models\Languages;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="fixed-left">
<?php $this->beginBody() ?>
<script>
    var resizefunc = [];
    function setCookie (name, value, expires, path, domain, secure) {
        document.cookie = name + "=" + escape(value) +
            ((expires) ? "; expires=" + expires : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure" : "");
    }
</script>
<div id="wrapper" class="forced <?= isset($_COOKIE['panel']) && $_COOKIE['panel'] == 'hide' ? 'enlarged' : ''?>">
    <!-- Top Bar Start -->
    <div class="topbar">
        <div class="topbar-left">
            <div class="logo">
                <h1>Baikonur</h1>
            </div>
            <div class="logo logo-rocket">
                <h1><a href="#"><img src="/images/admin/rocket_logo.png" alt="Logo"></a></h1>
            </div>
            <button class="button-menu-mobile open-left">
                <i class="fa fa-bars"></i>
            </button>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-collapse2">
                    <ul class="nav navbar-nav hidden-xs language-bar">
                        <li class="language_bar dropdown hidden-xs">
                            <a href="#" class="dropdown-toggle"
                               data-toggle="dropdown"><?= Languages::getAdminCurrent()->title ?>
                                (<?= mb_strtoupper(Languages::getAdminCurrent()->locale) ?>) <i
                                    class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu pull-right">
                                <?php foreach (Languages::getDropdownList() as $id => $language) {
                                    /*if ($id == Languages::getAdminCurrent()->id || !in_array($id,
                                            Yii::$app->controller->user->languages)
                                    ) {
                                        continue;
                                    }*/
                                    ?>
                                    <li><a href="<?= Url::to([
                                            '/languages/default/set-language',
                                            'id' => $id
                                        ]) ?>"><?= $language ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right top-navbar">
                        <li class="dropdown iconify hide-phone"><a href="#" onclick="javascript:toggle_fullscreen()"><i
                                    class="icon-resize-full-2"></i></a></li>
                        <li class="dropdown topbar-profile">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?= Yii::$app->getUser()->getIdentity()->username ?> <i
                                    class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= Url::to(['/users/default/update', 'id' => Yii::$app->user->id]) ?>">Мой
                                        профиль</a></li>
                                <li class="divider"></li>
                                <li><a class="md-trigger" data-modal="logout-modal"
                                       href="<?= Url::to(['/users/default/logout']) ?>"><i class="icon-logout-1"></i>
                                        Выйти</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->
    <!-- Left Sidebar Start -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!-- Search form -->
            <div class="clearfix"></div>
            <!--- Profile -->
            <div class="profile-info">
                <div class="col-xs-8">
                    <div class="profile-text">Welcome <b><?= Yii::$app->getUser()->getIdentity()->username ?></b></div>
                    <div class="profile-buttons">
                        <a href="mailto:evgeniy@rocketfirm.com"><i class="fa fa-envelope-o pulse"></i></a>
                        <a href="<?= Url::to(['/users/default/logout']) ?>" title="Выйти"><i
                                class="fa fa-power-off text-red-1"></i></a>
                    </div>
                </div>
            </div>
            <!--- Divider -->
            <div class="clearfix"></div>
            <hr class="divider"/>
            <div class="clearfix"></div>
            <!--- Divider -->
            <div id="sidebar-menu">
                <?php if (!Yii::$app->getUser()->getIsGuest() && (Yii::$app->getUser()->getIdentity()->isAdmin() || \Yii::$app->user->getIdentity()->getPermissions())) {

                    echo \app\components\admin\RFAMenuNav::widget([
                        'items' => [
                            /*[
                                'label' => 'Главная',
                                'url' => ['/mainpage/default/index'],
                                'icon' => 'icon-home-3',
                                'controller' => 'mainpage'
                            ],*/
                            [
                                'label' => 'Категории и контент',
                                'icon' => 'icon-list-nested',
                                'items' => [
                                    /*[
                                        'label' => 'Категории',
                                        'icon' => 'icon-list-nested',
                                        'url' => ['/categories/default/index'],
                                        //'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin', 'editor']),
                                        'controller' => 'categories/default'
                                    ],*/
                                    [
                                        'label' => 'Фестивали',
                                        'url' => ['/festivals/default/index'],
                                        'icon' => 'icon-article',
                                        'controllers' => [
                                            'festivals/default'
                                        ]
                                    ],
                                    [
                                        'label' => 'Работы',
                                        'url' => ['/jobs/default/index'],
                                        'icon' => 'icon-article',
                                        'controllers' => [
                                            'jobs/default',
                                            'jobs/request',
                                        ]
                                    ],
                                    [
                                        'label' => 'Творческая группа',
                                        'url' => ['/participants/default/index'],
                                        'icon' => 'icon-article',
                                        'controllers' => [
                                            'participants/default',
                                        ]
                                    ],
                                    [
                                        'label' => 'Победители',
                                        'url' => ['/winners/default/index'],
                                        'icon' => 'icon-article',
                                        'controllers' => [
                                            'winners/default',
                                        ]
                                    ],
                                    [
                                        'label' => 'Жюри',
                                        'url' => ['/juri/default/index'],
                                        'icon' => 'icon-article',
                                        'controllers' => [
                                            'juri/default',
                                        ]
                                    ],
                                    [
                                        'label' => 'Номинации',
                                        'url' => ['/nominations/default/index'],
                                        'icon' => 'icon-article',
                                        'controllers' => [
                                            'nominations/default'
                                        ]
                                    ],
                                    [
                                        'label' => 'Спонсоры',
                                        'url' => ['/sponsors/default/index'],
                                        'icon' => 'icon-article',
                                        'controllers' => [
                                            'sponsors/default'
                                        ]
                                    ],
                                    [
                                        'label' => 'Ведущие',
                                        'url' => ['/leading/default/index'],
                                        'icon' => 'icon-article',
                                        'controllers' => [
                                            'leading/default'
                                        ]
                                    ],
                                    [
                                        'label' => 'Текстовые страницы',
                                        'icon' => 'icon-newspaper-1',
                                        'url' => ['/pages/default/index'],
                                        //'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin', 'editor']),
                                        'controllers' => [
                                            'pages/default'
                                        ]
                                    ],
                                    [
                                        'label'=>'Галереи',
                                        'icon'=>'glyphicon glyphicon-picture',
                                        'url'=>['/gallery/default/index'],
                                        //'visible'=>Yii::$app->getUser()->getIdentity()->checkRole(['admin', 'editor']),
                                        'controllers' => [
                                            'gallery/default'
                                        ]
                                    ],
                                ]
                            ],
                            [
                                'label' => 'Модули',
                                'icon' => 'icon-layers',
                                'items' => [

                                    [
                                        'label' => 'Меню',
                                        'url' => ['/menu/default/index'],
                                        'icon' => 'icon-menu-1',
                                        'controllers' => [
                                            'menu/default',
                                            'menu/menu-items'
                                        ]
                                    ],
                                    [
                                        'label' => 'Банеры',
                                        'url' => ['/banners/default/index'],
                                        'icon' => 'icon-menu-1',
                                        'controllers' => [
                                            'banners/default',
                                        ]
                                    ],
                                ],
                            ],
                            [
                                'label' => 'Настройки',
                                'icon' => 'icon-cog',
                                'items' => [
                                    /*[
                                        'label' => 'Настройки системы',
                                        'url' => ['/config/default/index'],
                                        'icon' => 'icon-cog',
                                        //'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin']),

                                    ],*/
                                    [
                                        'label' => 'Языки',
                                        'url' => ['/languages/default/index'],
                                        'icon' => 'icon-language',
                                        'controller' => 'languages',
                                        //'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin']),

                                    ],
                                    [
                                        'label' => 'Переводы',
                                        'url' => ['/translate/default/index/?language='.Languages::getAdminCurrent()->locale],
                                        'icon' => 'icon-globe-alt',
                                        'controller' => 'translate',
                                        //'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin']),

                                    ],
                                    /*[
                                        'label' => 'Логи',
                                        'url' => ['/logs/default/index'],
                                        'icon' => 'icon-tape',
                                        'controller' => 'logs',
                                        //'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin']),

                                    ],*/
                                    [
                                        'label' => 'Сбросить кэш',
                                        'url' => ['/mainpage/default/cache-flush'],
                                        'icon' => 'glyphicon glyphicon-floppy-remove',
                                        'controller' => 'logs',
                                    ]
                                ]
                            ],
                        ],
                    ]);

                }
                ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- Left Sidebar End -->            <!-- Right Sidebar Start -->
    <!-- Right Sidebar End -->
    <!-- Start right content -->
    <div class="content-page">
        <!-- ============================================================== -->
        <!-- Start Content here -->
        <!-- ============================================================== -->
        <div class="content">
            <!-- Start info box -->
            <?= $content ?>
            <!-- Footer Start -->
            <footer>
                Rocket Firm &copy;
            </footer>
            <!-- Footer End -->
        </div>
        <!-- ============================================================== -->
        <!-- End content here -->
        <!-- ============================================================== -->

    </div>
    <!-- End right content -->

</div>

<?php $this->endBody() ?>
<script>
    var mAjax = new wAjax();

    $(function () {
          $(".open-left").click(function(){
              setTimeout(function(){
                
                  if($('#wrapper').hasClass('enlarged')) {
                      setCookie('panel', 'hide', "Mon, 01-Jan-2020 00:00:00 GMT", "/");
                  } else {
                      setCookie('panel', 'open', "Mon, 01-Jan-2020 00:00:00 GMT", "/");
                  }
              }, 200)
          });
    });



</script>
</body>
</html>
<?php $this->endPage() ?>
