<?php
/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!doctype html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ru-RU">
<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon" />
    <link rel="icon" type="image/png" sizes="192x192" href="/images/favicon.png">
    <title><?= $this->title ?></title>
        <?= \yii\helpers\Html::csrfMetaTags() ?>
    <?php $this->head(); ?>
    <!--[if lt IE 9]>
    <script src="bower_components/respond/src/respond.js"></script>
    <![endif]-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-79725244-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body<?= \Yii::$app->controller->isMain ? '' : ' class="body-inner"'?>>
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter38090145 = new Ya.Metrika({ id:38090145, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/38090145" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
<!--[if lt IE 10]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<?php $this->beginBody() ?>
<?php if(\Yii::$app->user->isGuest) {
    echo \app\modules\users\front\components\AuthPopUp::widget(['type' => 'login']);
    echo \app\modules\users\front\components\AuthPopUp::widget(['type' => 'reg']);
    echo \app\modules\users\front\components\AuthPopUp::widget(['type' => 'recover']);
    if(\Yii::$app->user->isGuest && isset($_GET['login'])) {
        $this->registerJs('
            $(\'#loginPopup\').modal()
    ');
    }
}
?>

<div class="modal fade modal--result" id="modalResult" tabindex="-1" role="dialog" aria-labelledby="modalResultLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close hidden" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon icon--close"></span></button>
                <div class="modal-title">Спасибо за заявку!</div>
            </div>
            <div class="modal-body">
                <p>Ваша работа скоро появится на сайте.</p>
            </div><!-- end .modal-body -->
            <div class="modal-footer">
                <p><a href="/" class="link link--form">Вернуться на главную страницу</a></p>
            </div><!-- end .modal-footer -->
        </div><!-- end .modal-content -->
    </div><!-- end .modal-dialog -->
</div><!-- end .modal -->

<header class="header<?= \Yii::$app->controller->isHidden ? ' hidden' : ''; ?> ">
    <div class="container clearfix">
        <nav class="nav nav--user pull-right">
            <?php if(\Yii::$app->user->isGuest): ?>
                <a href="#" class="link link--primary link--login" data-toggle="modal" data-target="#loginPopup">
                    <?= \Yii::t('front-main', 'Войти'); ?> <span class="fa fa-sign-in"></span>
                </a>
            <?php else: ?>
                <a href="/logout/" class="link link--primary link--login">
                    <?= \Yii::t('front-main', 'Выйти'); ?> <span class="fa fa-sign-out"></span>
                </a>
            <?php endif; ?>
            <a href="<?= \app\modules\languages\models\Languages::currentLink(\app\modules\languages\models\Languages::$current->code == 'ru' ? 'kz' : ''); ?>" class="link link--primary link--lang"><?= \app\modules\languages\models\Languages::$current->code == 'ru' ? 'Ққ': 'Ру'?></a>
        </nav>
        <a class="btn btn--primary pull-right hidden" onclick="ticketon.openEvent(3741, 'almaty', null)" href="javascript:;">Купить билет</a>
        <figure class="logo pull-left"><a href="/"><img src="/images/logo.png" width="190" height="52" alt="Фестиваль кино Байконур"></a></figure>

        <?php echo \app\modules\menu\front\components\Menu::widget([
            'menuId' => 5,
            'tag' => 'nav',
            'viewFile' => 'menu',
            'itemCssClass' => 'link link--primary',
            'cssClass' => 'nav nav--default nav--mini',
            'itemTagContainer' => null,
        ]); ?>
        <?php
        echo \app\modules\menu\front\components\Menu::widget([
            'menuId' => 1,
            'tag' => 'nav',
            'viewFile' => 'menu',
            'itemCssClass' => 'link link--primary',
            'cssClass' => 'nav--default nav--primary pull-left',
            'itemTagContainer' => null,
        ]); ?>
    </div><!-- end .container -->
</header>

<header class="header header--mobile">
    <div class="container clearfix">
        <button type="button" class="btn--menu js-menu-toggle"></button>
        <figure class="logo">
            <a href="/"><img src="/images/logo_1.png" alt="Фестиваль кино Байконур"></a>
        </figure>
    </div><!-- end .container -->
</header>

<div class="menu">
    <div class="menu-wrapper">

        <?php
        echo \app\modules\menu\front\components\Menu::widget([
            'menuId' => 1,
            'tag' => 'nav',
            'viewFile' => 'menu',
            'itemCssClass' => 'link link--primary',
            'cssClass' => 'nav--default nav--primary',
            'itemTagContainer' => null,
        ]); ?>
		<?php echo \app\modules\menu\front\components\Menu::widget([
			'menuId' => 5,
			'tag' => 'nav',
			'viewFile' => 'menu',
			'itemCssClass' => 'link link--primary',
			'cssClass' => 'nav--default nav--primary',
			'itemTagContainer' => null,
		]); ?>
    </div><!-- end .menu-wrapper -->
</div><!-- end .menu -->


<?php if(Yii::$app->controller->cssClass == 'main-page') : ?> 
    <section class="section section--first section-bg-placeholder"></section>
<?php endif; ?>

<main class="main main--secondary <?= \Yii::$app->controller->cssClass; ?>">
    <?= $content; ?>
</main>

<footer class="footer<?= \Yii::$app->controller->cssClass == 'main-page' ? ' footer--main' : ''; ?>">
    <div class="container">


        <?php if(Yii::$app->controller->cssClass == 'main-page') {
            echo \app\modules\sponsors\front\components\SponsorsWidget::widget();
        }?>


        <div class="footer--primary clearfix">
            <!--       <ul class="social-list pull-right">
                    <li class="social-list-item"><a href="#" class="link--social"><span class="icon icon--social fb"></span>Фейсбук</a></li>
                    <li class="social-list-item"><a href="#" class="link--social"><span class="icon icon--social insta"></span>Инстаграм</a></li>
                    <li class="social-list-item"><a href="#" class="link--social"><span class="icon icon--social vk"></span>Вконтакте</a></li>
                  </ul>
                  <figure class="logo--secondary"><a href="main.html" class="link--image"><img src="images/logo.svg" width="202" height="54" alt="Фестиваль кино Байконур"></a></figure>
                  <p class="brand-description">Байконур — фестиваль студенческих короткометражных фильмов. Это возможность для студентов любого казахстанского ВУЗа продемонстрировать свое творение публике.</p> -->

            <!--?php echo \app\modules\menu\front\components\Menu::widget([
                'menuId' => 2,
                'viewFile' => 'menu',
                'cssClass' => 'nav--default nav--footer',
                'itemContainerCssClass' => 'nav-item',
                'itemCssClass' => 'link link--primary',
                'activateItems' => false,
            ]); ?-->

            <?php  $menu = \app\modules\menu\models\Menus::findOne(2)->getItems();?>
            <div class="nav--default nav--footer">
                <div class="col">
                    <?php $i = 0; foreach($menu as $k => $v){
                        if($i && $i%2==0) {
                            echo '</div><div class="col">';
                        }
                        echo '<div class="nav-item"><a class="link link--primary active" href="' . \yii\helpers\Url::to($v['url']) . '">' . $v['label'] . '</a></div>';
                        $i++;
                    }

                    ?>
                </div>
                

            </div>

        </div>

        <div class="footer--secondary clearfix">
            <a class="rocket-link pull-right" href="http://rocketfirm.com" target="_blank"><svg class="rocket-logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 105 14.47"><title>Rocket Firm logo</title><path class="logo-letter" d="M5,11.67L3.28,8.51H1.89v3.16H0V2.78H4.17A3.44,3.44,0,0,1,5.41,3,2.6,2.6,0,0,1,6.95,4.51a3.08,3.08,0,0,1,.21,1.14,3,3,0,0,1-.17,1,2.51,2.51,0,0,1-.45.78A2.54,2.54,0,0,1,5.91,8a2.39,2.39,0,0,1-.71.29l2,3.38H5ZM3.88,6.85a1.43,1.43,0,0,0,1-.32,1.09,1.09,0,0,0,.38-0.88,1.09,1.09,0,0,0-.38-0.88,1.43,1.43,0,0,0-1-.32h-2v2.4h2Z"/><path class="logo-letter" d="M13.11,11.83a5,5,0,0,1-1.85-.34A4.29,4.29,0,0,1,8.82,9.08a4.82,4.82,0,0,1-.35-1.85,4.82,4.82,0,0,1,.35-1.85A4.29,4.29,0,0,1,11.26,3,5.25,5.25,0,0,1,15,3a4.29,4.29,0,0,1,2.44,2.41,4.82,4.82,0,0,1,.35,1.85,4.82,4.82,0,0,1-.35,1.85A4.29,4.29,0,0,1,15,11.49,5,5,0,0,1,13.11,11.83Zm0-1.68a2.67,2.67,0,0,0,1.11-.23,2.6,2.6,0,0,0,.85-0.61,2.73,2.73,0,0,0,.55-0.92,3.58,3.58,0,0,0,0-2.32,2.8,2.8,0,0,0-.55-0.93,2.46,2.46,0,0,0-.85-0.61,2.93,2.93,0,0,0-2.23,0,2.39,2.39,0,0,0-.85.61,2.85,2.85,0,0,0-.54.93,3.57,3.57,0,0,0,0,2.32,2.77,2.77,0,0,0,.54.92,2.52,2.52,0,0,0,.85.61A2.67,2.67,0,0,0,13.11,10.15Z"/><path class="logo-letter" d="M23.67,11.83a5.15,5.15,0,0,1-1.87-.33,4.35,4.35,0,0,1-2.5-2.39,4.66,4.66,0,0,1-.37-1.87,4.66,4.66,0,0,1,.37-1.87A4.35,4.35,0,0,1,21.8,3a5.15,5.15,0,0,1,1.87-.33A4.34,4.34,0,0,1,25,2.82a4.14,4.14,0,0,1,1,.49,3.68,3.68,0,0,1,.79.69,5.6,5.6,0,0,1,.57.81l-1.63.8a2.43,2.43,0,0,0-.84-0.92,2.21,2.21,0,0,0-1.25-.37,2.8,2.8,0,0,0-1.11.22,2.63,2.63,0,0,0-.89.61,2.79,2.79,0,0,0-.58.93,3.19,3.19,0,0,0-.21,1.16,3.23,3.23,0,0,0,.21,1.17,2.71,2.71,0,0,0,.58.93,2.68,2.68,0,0,0,.89.61,2.8,2.8,0,0,0,1.11.22,2.24,2.24,0,0,0,1.25-.37,2.36,2.36,0,0,0,.84-0.93l1.63,0.79a5.54,5.54,0,0,1-.57.81,3.87,3.87,0,0,1-.79.7,4,4,0,0,1-1,.49A4.34,4.34,0,0,1,23.67,11.83Z"/><path class="logo-letter" d="M34.53,11.67L31.84,8.11l-0.69.83v2.73H29.25V2.78h1.89v4l3.15-4h2.33L33.09,7l3.77,4.72H34.53Z"/><path class="logo-letter" d="M38.53,11.67V2.78h6.29V4.45h-4.4V6.33h4.3V8h-4.3v2h4.4v1.67H38.53Z"/><path class="logo-letter" d="M49.21,11.67V4.45h-2.6V2.78h7.08V4.45H51.11v7.22H49.21Z"/><path class="logo-letter" d="M73.56,11.67V2.78h6.29V4.45h-4.4V6.33h4.3V8h-4.3v3.68H73.56Z"/><path class="logo-letter" d="M82,11.67V2.78h1.89v8.89H82Z"/><path class="logo-letter" d="M91.18,11.67L89.43,8.51H88v3.16H86.15V2.78h4.17A3.44,3.44,0,0,1,91.57,3,2.6,2.6,0,0,1,93.1,4.51a3.08,3.08,0,0,1,.21,1.14,3,3,0,0,1-.17,1,2.51,2.51,0,0,1-.45.78,2.54,2.54,0,0,1-.63.53,2.39,2.39,0,0,1-.71.29l2,3.38H91.18ZM90,6.85a1.43,1.43,0,0,0,1-.32,1.09,1.09,0,0,0,.38-0.88A1.09,1.09,0,0,0,91,4.77a1.43,1.43,0,0,0-1-.32H88v2.4h2Z"/><path class="logo-letter" d="M102.92,11.67V5.45l-2.51,6.22H99.59L97.08,5.45v6.22H95.19V2.78h2.65L100,8.16l2.16-5.37h2.66v8.89h-1.89Z"/><path class="logo-icon" d="M70.57,7.23a7.24,7.24,0,1,0-7.24,7.24A7.23,7.23,0,0,0,70.57,7.23ZM61.75,12c-0.36.07-.32-0.1-0.21-0.25l0.15-.17c0.77-.87,1.37-1.19.37-1.19s-1.32,0-1.59-.28h0a2.62,2.62,0,0,1-.28-1.59c0-1-.32-0.4-1.2.37L58.81,9c-0.15.11-.32,0.15-0.25-0.21A5.23,5.23,0,0,1,60,6.14a12,12,0,0,1,2.38-.89c2.29-1.65,4.48-2.31,4.95-2s-0.32,2.65-2,4.95a12,12,0,0,1-.89,2.38A5.23,5.23,0,0,1,61.75,12Z"/><path class="logo-icon" d="M65.08,6.78A1.24,1.24,0,0,1,63.79,8,1.25,1.25,0,1,1,65.08,6.78Z"/></svg><span class="logo-year"><?= date('Y'); ?></span></a>

            <p class="copyright">&copy; 2016–<?= date('Y'); ?> <?= \Yii::t('front-main', 'Байконур — проект');?> <a class="link link--primary" target="_blanc" href="https://brod.kz/"><?= \Yii::t('front-main', 'Бродвея');?></a></p>
        </div>
    </div>
</footer>
<script type="text/javascript" src="https://static.ticketon.kz/widget/consumer.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
