<noindex>
<section class="auth-soc">
    <p class="auth-soc-title">или</p>
    <ul class="auth-soc-list">
        <li class="auth-soc-list-item"><a rel="nofollow" href="/login/?service=facebook&redirect=/" class="link--social fb"><span class="icon icon--social fb"></span>через Фейсбук</a></li>
        <li class="auth-soc-list-item"><a rel="nofollow" href="/login/?service=twitter&redirect=/" class="link--social twi"><span class="icon icon--social twi"></span>через Твиттер</a></li>
    </ul>
    <ul class="auth-soc-list">
        <li class="auth-soc-list-item"><a rel="nofollow" href="/login/?service=vkontakte&redirect=/" class="link--social vk"><span class="icon icon--social vk"></span>через Вконтакте</a></li>
        <!--li class="auth-soc-list-item"><a href="#" class="link--social google"><span class="icon icon--social google"></span>через Гугл плюс</a></li-->
    </ul>
</section>
</noindex>