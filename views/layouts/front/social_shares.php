<?php
/* @var $this \yii\web\View */
/* @var $content string */
if(!isset($ajax)) $ajax = true;
if($js) {
    $this->registerJs('
$(\'.btn--sharing\').click(function(){
  $(this).find(\'span.number\').html(
    parseInt($(this).find(\'span.number\').html())+1
  )
})
');
    if($ajax)
    $this->registerJs('
        var ajaxsoc = new wAjax();
        ajaxsoc.doit("/mainpage/default/social-share/", {
        url:window.location.href,
        title: $("meta[property=\'og:title\']").attr(\'content\'),
        description: $("meta[property=\'og:description\']").attr(\'content\'),
        image: $("meta[property=\'og:image\']").attr(\'content\'),
        });
');
}
?>
<ul class="sharing">
    <?php if(isset($is_cinema) && $is_cinema): ?>
    <!--li class="sharing-item">
        <button class="btn btn--sharing">Скопировать ссылку</button>
    </li-->
    <?php endif; ?>
    <li class="sharing-item">
        <button class="btn btn--sharing fb">
            <span class="icon icon--social fb <?= isset($is_cinema) && $is_cinema ? '': 'icon--color'; ?>"></span>
            <?= \Yii::t('front', 'Поделиться')?>
            <span class="number">0</span>
        </button>
    </li>
    <li class="sharing-item">
        <button class="btn btn--sharing vk">
            <span class="icon icon--social vk <?= isset($is_cinema) && $is_cinema ? '': 'icon--color'; ?>"></span>
            <?= \Yii::t('front', 'Поделиться')?>
            <span class="number vk_share_count">0</span>
        </button>
    </li>
    <li class="sharing-item">
        <button class="btn btn--sharing twi">
            <span class="icon icon--social twi <?= isset($is_cinema) && $is_cinema ? '': 'icon--color'; ?>  "></span>
            <?= \Yii::t('front', 'Поделиться')?>
            <span class="number tw_share_count">0</span>
        </button>
    </li>
    <?php if(\Yii::$app->controller->isMobile || \Yii::$app->devicedetect->isTablet()): ?>
        <li class="sharing-item">
            <a data-action="share/whatsapp/share" class="btn btn--sharing wh">
                <span class="icon icon--social wh"></span>
                Отправить
                <span class="number">0</span>
            </a>
        </li>
    <?php endif; ?>
</ul>
