<?php
/**
 * Created by PhpStorm.
 * User: Vital
 * Date: 04.03.2015
 * Time: 13:17
 */
namespace app\components;

use app\modules\config\models\Config;
use app\modules\emailtemplates\models\EmailTemplates;
use yii\helpers\ArrayHelper;

class wMail
{

    /**
     * Отправка почты
     *
     * @param $subject
     * @param $to
     * @param string $view
     * @param array $viewParams
     *
     * @return bool
     */
    private function smtpSend($subject, $to, $view = '', $viewParams = [], $attach = null)
    {
        $params = \Yii::$app->params['wMail']['smtp'];
        //$config = Config::findOne(1);
        $mailerConfig = \Yii::$app->components['mailer'];
        $secures = [
            0 => null,
            1 => 'ssl',
            2 => 'tls'
        ];

        //$to = explode(',', $to);

        $transportConfig = [
            'class' => 'Swift_SmtpTransport',
            'host' => $params['smtp_host'],
            'username' => $params['smtp_username'],
            'password' => $params['smtp_password'],
            'port' => $params['smtp_port'],
            'encryption' => $params['smtp_secure']
        ];
//print_r($transportConfig);exit;
        $mailerConfig['transport'] = $transportConfig;
        \Yii::$app->setComponents(['mailer' => $mailerConfig]);
        $result = \Yii::$app->getMailer()
            ->compose($view, $viewParams)
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom([$params['smtp_from_email'] => $params['smtp_from_name']]);
        if ($attach) {
            $result->attach($attach);
        }
        $result->send();

        return $result;
    }

    protected $params;

    public static function send($template, $emails = [], $replacement = [], $subject = null, $attach = null)
    {
        $params = \Yii::$app->params['wMail'];

        if (is_numeric($template)) {
            if(\Yii::$app->controller->_user && \Yii::$app->controller->_user->id && \Yii::$app->controller->_user && in_array($template, \Yii::$app->controller->_user->unsubscribe)) return true;

            $tpl = EmailTemplates::findOne($template);
            $replacement = ArrayHelper::merge($replacement, [
                '%domain%' => $params['domain'],
                '%system_mail%' => $params['system_mail'],
                '%cancel_subscription%' => 'http://' . $params['domain'] . '/profile/#unsubscribe',
            ]);
            $content = strtr($tpl->text, $replacement);
            $subject = strtr($tpl->title, $replacement);
        }
        else {
            $content = strtr(\Yii::$app->getView()->render('@app/mail/layouts/' . $template), $replacement);
        }

        //print_r($content);exit;

        //$content = strtr(\Yii::$app->getView()->render('@app/mail/layouts/' . $template), $replacement);


        if (!$subject) {
            $subject = $params['mails'][$template]['subject'];
        }

        if (isset($params['easySend']) && $params['easySend'])
            return self::easySend($subject, $emails, 'layouts/html', ['content' => $content], $attach);
        else
            return self::smtpSend($subject, $emails, 'layouts/html', ['content' => $content], $attach);

    }

    private static function easySend($subject, $to, $view = '', $viewParams = [], $attach = null)
    {
        return \Yii::$app->mailer->compose($view, $viewParams)
            ->setFrom(\Yii::$app->params['adminEmail'])
            ->setTo($to)
            ->setSubject($subject)
            ->send();
    }

    public static function sendBugReport($emails = [], $replacements = [], $subject = null)
    {
        self::send('bug-report', $emails, $replacements, $subject);
    }

    public static function sendForgotPassword($emails = [], $replacements = [], $subject = null)
    {
        self::sendEmail('forgot-form', $emails, $replacements, $subject);
    }

    public static function sendRegistrationPassword($emails = [], $replacements = [], $subject = null)
    {
        self::sendEmail('registration', $emails, $replacements, $subject);
    }

    public static function sendInvite($emails = [], $replacements = [], $subject = null, $attach = null)
    {
        self::sendEmail('invite-form', $emails, $replacements, $subject, $attach);
    }
}