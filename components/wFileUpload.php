<?php
/**
 * Created by PhpStorm.
 * User: Vitaly Prokhonenkov
 * Date: 04.03.2015
 * Time: 13:17
 * Email: prokhonenkov@gmail.com
 */
namespace app\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class wFileUpload extends Component
{
    public $attribute = null;
    public $model = null;
    public $property = null;
    protected $tmpFile = null;
    protected $currentFile = null;
    protected $defaultImage = null;
    protected $baseUrl = null;

    public function __construct($model, $attribute, $property, $baseUrl = null)
    {
        $this->attribute = $attribute;
        $this->model = $model;
        $this->property = $property;
        $this->tmpFile = $this->model->getAsyncTempFile($this->attribute);
        $this->currentFile = $this->model->getFilePath($this->property, false);
        $this->baseUrl = $baseUrl;
    }

    public function fileInput($multiple = false, $limit = null)
    {
        return Html::activeFileInput($this->model, $this->attribute, [
            'data-url' => \yii\helpers\Url::to([
                ($this->baseUrl ? $this->baseUrl : '') .
                'upload-async', 'attribute' => $this->attribute,
                'id' => (int)$this->model->id,
                'multiple' => (int)$multiple,
                'multiple_template' => '@app/modules/files/front/views/default/_multiple_list'
            ]),
            'data-attribute' => $this->attribute,
            'multiple' => $multiple,
        ]);
    }

    protected function getFile()
    {
        if ($this->tmpFile) return $this->tmpFile;
        elseif ($this->currentFile) return $this->currentFile;
        else return false;
    }

    protected function getFiles()
    {
        $files = [];

        if($this->currentFile) {
            if(is_array($this->currentFile))foreach($this->currentFile as $k => $v) {
                $files['current'][$k] = $v;
            } else {
                $files['current'][$this->model->{$this->property}->id] = $this->currentFile;
            }
        }

        if($this->tmpFile) {
            if(!is_array($this->tmpFile)) $this->tmpFile = [$this->tmpFile];
            foreach($this->tmpFile as $k => $v) {
                $files['temp'][$k] = $v;
            }
        }
        return $files;
    }

    public function img($defaultImage = null, $options = [])
    {
        $this->defaultImage = $defaultImage;
        $file = $this->getFile();

        $src = null;
        if ($file) $src = $file;
        elseif ($defaultImage) $src = $defaultImage;
        else $options['style'] = isset($options['style']) ? $options['style'] . ';display:none;' : ';display:none;';
        return Html::img($src, $options);
    }

    public function multipleListTemplate($template, $params)
    {
        $options = [
            'class' => 'multiple_list' . (isset($params['containerClass']) ? ' ' . $params['containerClass'] : ''),
            'id' => 'sortable'
        ];

        $files = $this->getFiles();

        $cnt = '';
        if(isset($files['current'])){

            foreach($this->model->{$this->property} as $k => $v) {
                $cnt .= \Yii::$app->view->render($template, [
                    'src' => $files['current'][$v->id],
                    'key' => $k . '_' . $v->id,
                    'attribute' => $this->attribute,
                    'recAttribute' => $this->property,
                    'model' => $v,
                    'model_id' => $this->model->id
                ]);
            }
        }

        if(isset($files['temp']))foreach($files['temp'] as $k => $v) {
            $cnt .= \Yii::$app->view->render($template, [
                'src' => $v,
                'key' => $k,
                'attribute' => $this->attribute,
                'recAttribute' => $this->property,
                'model' => $params['model'],
                'model_id' => $this->model->id
            ]);
        }

        return Html::tag('div', $cnt, $options);
    }
    public function multipleList($options = [], $content = [])
    {
        $options = ArrayHelper::merge([
            'class' => 'multiple_list',
            'id' => 'sortable'
        ], $options);
        $files = $this->getFiles();

        $cnt = [];
        $_cnt = [];

        if(\Yii::$app->request->post('multiple_list'))
            $post = \Yii::$app->request->post('multiple_list');
        else
            $post = [0];
        foreach($post as $post_v) {
            $post_v = pathinfo($post_v);
            foreach($files as $key => $type) {
                foreach($type as $k => $v) {
                    if($post_v && strpos($v, $post_v['filename']) === false) continue;
                    foreach($content as $_k => $_v) {
                        if ($_k == 'img') {
                            $_cnt[] = Html::img($v, $_v);
                        }
                        if ($_k == 'remove') {
                            $_cnt[] = $this->remove($key == 'temp' ? null : $k, ArrayHelper::merge($_v['options'], [
                                'html' => [
                                    'tag' => $_v['tag'],
                                    'content' => $_v['content']
                                ],
                            ]),
                                $key == 'temp' ? $k : null,
                                true);
                        }
                    }

                    $file = pathinfo($v);
                    $_file = explode('-', $file['filename']);
                    $file = $_file[0] . '.' . $file['extension'];

                    $cnt[] = implode('', $_cnt) . Html::hiddenInput('multiple_list[]', $file);
                    $_cnt = [];
                }
            }
        }
        return Html::ul($cnt, $options);
    }

    public function error()
    {
        return HTML::label(null, null, [
            'class' => 'help-block error-text wHint', 'style'=> 'color:#a94442;'
        ]);
    }

    public function getDataUrl($file_id, $item)
    {
        return \yii\helpers\Url::to([
            ($this->baseUrl ? $this->baseUrl : '') .
            'remove-file',
            'attribute' => $this->attribute,
            'id' => $this->model->id,
            'file_id' => $file_id,
            'item' => $item,
        ]);
    }

    public function remove($file_id = null, $options = [], $item = null, $multiple = false)
    {
        if (!$this->getFile()) $options['style'] = isset($options['style']) ? $options['style'] . ';display:none;' : ';display:none;';
        if(isset($options['class'])) $options['class'] .= ' remove-img' . (!$multiple ? ' wHide' : '');
        else $options['class'] = ' remove-img wHide';

        if($this->defaultImage)
            $options['data-default'] = $this->defaultImage;
        $options['data-url'] = $this->getDataUrl($file_id, $item);
        if (isset($options['html'])) {
            $tag = $options['html']['tag'];
            $content = $options['html']['content'];
            unset($options['html']);
            return Html::tag($tag, $content, $options);
        }
        return Html::a('Remove', 'javascript:;', $options);
    }
}