<?php
/**
 * Created by PhpStorm.
 * User: Vital
 * Date: 18.03.2015
 * Time: 12:20
 */
namespace app\components;
class wUniSender
{
    protected $apiKey = null;
    protected $apiUrl = 'http://api.unisender.com/ru/api';

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function request($method, $params = [])
    {
        return $this->wCURL($method, $params);
    }

    protected function wCURL($method, $params = [])
    {
        $params['api_key'] = $this->apiKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '/' . $method . '?format=json');
        $result = curl_exec($ch);
        if ($result) {
            $jsonObj = json_decode($result);
            if(null===$jsonObj) {
                return "Invalid JSON";
            }
            return $jsonObj;
        } else {
            return "API access error";
        }
    }
}