<?php
namespace app\components\behaviors;

use app\components\ActiveRecord;
use app\modules\socialnetworks\models\LookupSocNets;
use app\modules\socialnetworks\models\SocNetsPosts;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\validators\Validator;

/**
 */
class CrossPosting extends Behavior
{
    public $socialPages = [];
    public $timeToPost = null;
    public $is_publish_to_soc_nets = null;
    public function attach($owner)
    {
        parent::attach($owner);
        $stringValidator = Validator::createValidator('string', $this->owner,
            ['timeToPost'], ['max' => 255]);

        $safeValidator = Validator::createValidator('safe', $this->owner,
            ['socialPages']);


        $this->owner->validators[] = $stringValidator;
        $this->owner->validators[] = $safeValidator;

    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }
    public function afterSave()
    {
        if($this->owner->status_id != ActiveRecord::STATUS_ACTIVE || !array_filter($this->owner->socialPages)) return false;
        foreach(LookupSocNets::getEauth() as $k => $v) {


            if($this->owner->timeToPost) {
                $data = [
                    'published' => 'false',
                    'scheduled_publish_time' => \DateTime::createFromFormat('d/m/Y H:i', $this->owner->timeToPost)->getTimestamp()
                ];
            }
            $data['message'] = StringHelper::removHtmTags($this->owner->short_text);
            if($this->owner->logo) {
                $data['url'] =  $this->owner->getFilePath('logo', false);
            }

            $token = explode('_', $this->owner->socialPages[$k]);
            if($id = $v->postToPage($token[0], $token[1], $data)) {
                $posts = new SocNetsPosts();
                $posts->object_id = $this->owner->id;
                $posts->object_type = \Yii::$app->controller->module->id;
                $posts->soc_net_id = $k;
                $posts->soc_post_id = $id;
                $posts->save();
            }

        }

        return true;

    }

    public function afterDelete()
    {
        SocNetsPosts::deleteAll([
            'object_id' => $this->owner->id,
            'object_type' => \Yii::$app->controller->module->id
        ]);
    }

    public function afterValidate()
    {
    }
}
