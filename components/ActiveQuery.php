<?php

namespace app\components;


use app\modules\languages\models\Languages;
use yii\db\ActiveQueryTrait;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class ActiveQuery extends \yii\db\ActiveQuery
{
    use ActiveQueryTrait;

    protected $model;

    public function init()
    {
        $this->model = new $this->modelClass;
    }

    public function populate($rows)
    {
        if (empty($rows)) {
            return [];
        }

        $models = $this->createModels($rows);

        if (!empty($this->with)) {
            $this->findWith($this->with, $models);
        }
        if (!$this->asArray) {
            foreach ($models as $model) {
                $model->afterFind();
            }
        }

        return $models;
    }

    public function isActive($alias)
    {
        $this->andWhere([$alias . '.status_id' => ActiveRecord::STATUS_ACTIVE]);

        return $this;
    }

    public function isNoDeleted($alias)
    {
        $this->andWhere([$alias . '.delete_by' => null]);
        return $this;
    }

    public function isPublish($alias)
    {
        $this->andWhere([
            'AND',
            ['<',$alias . '.publish_date', new Expression('NOW()')],
            [$alias . '.status_id' => ActiveRecord::STATUS_ACTIVE]
        ]);
        return $this;
    }

    public function setLanguage()
    {
        if (\Yii::$app->params['yiiEnd'] == 'admin') {
            $this->andWhere([$this->model->tableName() . '.lang_id' => Languages::getAdminCurrent()->id]);
        } else {
            $this->andWhere([$this->model->tableName(). '.lang_id' => Languages::getCurrent()->id]);
        }

        return $this;
    }

    public function withUsers($alias)
    {
        return $this->innerJoin('users AS u', 'u.id = ' . $alias . '.user_id');
    }

    public function withComments($object_id, $type_id)
    {
        return $this->leftJoin('comments AS c', 'c.object_id = ' . $object_id . ' AND type_id = ' . $type_id);
    }

    public function withFilesLogo($table, $key, $alias = null)
    {
        return $this->leftJoin($table . '_files_logo', $table . '_files_logo.' . $key . ' = ' . ($alias ? $alias : $table) . '.id')
            ->addSelect(['CONCAT_WS("/", ' . $table . '_files_logo.subdir, ' . $table . '_files_logo.basename) fa_logo']);
    }

    public function withFilesVideo($table, $key)
    {
        return $this->leftJoin($table . '_files_video', $table . '_files_video.' . $key . ' = ' . $table . '.id')
            ->addSelect(['CONCAT_WS("/", ' . $table . '_files_video.subdir, ' . $table . '_files_video.basename) fa_video']);
    }

    public function withFilesMetaImage($table, $key)
    {
        return $this->leftJoin($table . '_files_meta_image', $table . '_files_meta_image.' . $key . ' = ' . $table . '.id')
            ->addSelect(['CONCAT_WS("/", ' . $table . '_files_meta_image.subdir, ' . $table . '_files_meta_image.basename) fa_meta_image']);
    }

    public function getLangField($field, $alias = null)
    {
        $splitField = explode('.', $field);
        return $this->addSelect([
            'IF(' . $field . \Yii::$app->controller->langPostfix
            . ' IS NOT NULL AND '
            . $field . \Yii::$app->controller->langPostfix .  ' <> "", '
            . $field . \Yii::$app->controller->langPostfix
            . ', ' . $field . ') AS ' . ($alias ? $alias : end($splitField))
        ]);
    }
}
