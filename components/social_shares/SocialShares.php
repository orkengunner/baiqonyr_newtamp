<?php

/**
 * Created by PhpStorm.
 * User: Prokhonenkov Vitaly
 * Email: prokhonenkov@gmail.com
 * Date: 10.08.2015
 * Time: 11:20
 */

namespace app\components\social_shares;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class SocialShares
{
    public $params;
    protected $url;

    public function __construct($url = null, $params = [])
    {
        $this->params = $params;
        $this->url = $url ? $url : $this->getProtocol() . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    public function facebook()
    {
        return new ShareFacebook($this->url, $this->params);
    }

    public function watsup($text = null)
    {
        return new ShareWatsUp($this->url, ArrayHelper::merge($this->params, ['text' => $text]));
    }

    public function twitter($text = null)
    {
        return new ShareTwitter($this->url, ArrayHelper::merge($this->params, ['text' => $text]));
    }

    public function vkontakte($text = null, $title = null, $image = null)
    {
        $params = [];
        if($text) $params['description'] = $text;
        if($title) $params['title'] = $title;
        if($image) $params['image'] = $image;
        return new ShareVkontakte($this->url, ArrayHelper::merge($this->params, $params));
    }

    private function getProtocol()
    {
        return stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
    }
}

