<?php

/**
 * Created by PhpStorm.
 * User: Prokhonenkov Vitaly
 * Email: prokhonenkov@gmail.com
 * Date: 10.08.2015
 * Time: 11:20
 */

namespace app\components\social_shares;

use yii\helpers\ArrayHelper;

class ShareVkontakte extends Social
{
    public function __construct($url, $params)
    {
        parent::__construct($url, $params);
        $this->count = $this->getCount();
        $this->link = $this->getLink();

    }

    public function getLink()
    {
        /*
         * array elements: url, title, description, image(http://dom.com/images/img.jpg)
         * */
        return $this->build_url('https://vk.com/share.php', ArrayHelper::merge([
            'url' => $this->url,
        ], $this->params));
    }

    private function getCount()
    {
        $str = $this->curl('http://vk.com/share.php?act=count&index=1&url=' . $this->url);
        preg_match('/VK.Share.count\(1, (.*?)\)/', $str, $count);
        return isset($count[1]) ? $count[1] : 0;
    }
}