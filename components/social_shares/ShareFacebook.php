<?php

/**
 * Created by PhpStorm.
 * User: Prokhonenkov Vitaly
 * Email: prokhonenkov@gmail.com
 * Date: 10.08.2015
 * Time: 11:20
 */

namespace app\components\social_shares;

use yii\helpers\ArrayHelper;

class ShareFacebook extends Social
{
    public function __construct($url, $params)
    {
        parent::__construct($url, $params);
        $this->count = $this->getCount();
        $this->link = $this->getLink();

    }

    public function getLink()
    {
        /*
         * array elements: u(url), title, description, image(http://dom.com/images/img.jpg)
         * */
        return $this->build_url('https://www.facebook.com/sharer/sharer.php', ArrayHelper::merge([
            'u' => $this->url,
            'src' => 'sp'
        ], $this->params));
    }

    private function getCount()
    {
        $str = $this->curl('https://graph.facebook.com/?id=' . $this->url);
        preg_match('/\"shares\"\:(.*?)\}/', $str, $count);
        return isset($count[1]) ? $count[1] : 0;
    }
}