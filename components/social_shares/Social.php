<?php

/**
 * Created by PhpStorm.
 * User: Prokhonenkov Vitaly
 * Email: prokhonenkov@gmail.com
 * Date: 10.08.2015
 * Time: 11:20
*/

namespace app\components\social_shares;

class Social
{
    protected $url;
    protected $params;

    public $link;
    public $count;

    public function __construct($url, $params)
    {
        $this->url = $url;
        $this->params = $params;
    }

    protected function curl( $url )
    {
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        $response = curl_exec( $ch );
        curl_close( $ch );
        return $response;
    }

    protected function build_url($url, $params=[])
    {
        $p = [];

        foreach($params as $k => $v) {
            $p[] = $k . '=' . $v;
        }
        return $url . '?' . implode('&', $p);
    }

    public function getUrl()
    {
        return $this->url;
    }
}