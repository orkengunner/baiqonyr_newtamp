<?php

/**
 * Created by PhpStorm.
 * User: Prokhonenkov Vitaly
 * Email: prokhonenkov@gmail.com
 * Date: 10.08.2015
 * Time: 11:20
 */

namespace app\components\social_shares;

use app\modules\totalsharing\models\TotalSharing;
use yii\helpers\ArrayHelper;

class ShareWatsUp extends Social
{
    public $_url = null;
    public function __construct($url, $params)
    {
        parent::__construct($url, $params);
        $this->count = $this->getCount();
        $this->link = $this->getLink();
        $this->_url = $this->getUrl();
    }

    public function getLink()
    {
        /*
         * array elements: url, title, description, image(http://dom.com/images/img.jpg)
         * */
        return $this->build_url('whatsapp://send', ArrayHelper::merge([
            'text' => $this->url
        ], array_filter($this->params)));
    }

    private function getCount()
    {
        $count = TotalSharing::find()->andWhere([
            'url' => $this->url,
            'soc_net' => 'wu'
        ])->one();

        return $count ? $count->total : 0;
    }
}