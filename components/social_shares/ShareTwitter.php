<?php

/**
 * Created by PhpStorm.
 * User: Prokhonenkov Vitaly
 * Email: prokhonenkov@gmail.com
 * Date: 10.08.2015
 * Time: 11:20
 */

namespace app\components\social_shares;

use yii\helpers\ArrayHelper;

class ShareTwitter extends Social
{
    private $limitSymbols = 140;
    private $countSymbolsLink = 23;

    public function __construct($url, $params)
    {
        parent::__construct($url, $params);
        $this->count = $this->getCount();
        $this->link = $this->getLink();

    }

    public function getLink()
    {
        /*
         * array elements: url, text
         * */

        //$countSymbolsLink = mb_strlen(preg_replace('/\:\/\/.*?\..*?\/(.*?)$/', '', $this->url) . '://');
        $countSymbolsText = mb_strlen($this->params['text']);

        if ($this->countSymbolsLink + $countSymbolsText > $this->limitSymbols) {
            $this->params['text'] = mb_substr($this->params['text'], 0, $this->limitSymbols - $this->countSymbolsLink);
        }

        return $this->build_url('https://twitter.com/intent/tweet', ArrayHelper::merge([
            'original_referer' => $this->url,
            'text' => $this->params['text'],
            'url' => $this->url,
            'tw_p' => 'tweetbutton',
        ], $this->params));
    }

    private function getCount()
    {
        $str = $this->curl('http://urls.api.twitter.com/1/urls/count.json?url=' . $this->url);
        preg_match('/\{\"count\"\:(.*?)\,/', $str, $count);
        return isset($count[1]) ? $count[1] : 0;
    }
}