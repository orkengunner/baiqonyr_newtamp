<?php
namespace app\components;

use app\modules\files\models\Files;
use app\modules\pages\models\PagesCategories;
use Imagine\Image\Box;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\imagine\Image;

class ActiveRecord extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 3;
    const STATUS_MODERATION = 2;
    const STATUS_ACTIVE = 1;
    const STATUS_MERGE_SN = 5;
    public static $statuses = [
        self::STATUS_INACTIVE => 'Черновик',
        self::STATUS_ACTIVE => 'Опубликован',
        self::STATUS_MODERATION => 'На утверждении'
    ];
    protected $allValidationRules = [];
    protected $allAttributeLabels = [];

	const TYPE_NATIONAL = 1;
	const TYPE_WORLD = 2;

    protected function createPreview($attribute, $path = null, $dist = null)
    {
        $sizes = $this->getParam($attribute . '.sizes');

        foreach ($sizes as $size) {
            if (!$size[0] && !$size[1]) break;
            $mode = '-' . ($size[0] ? $size[0] : '') . 'x' . ($size[1] ? $size[1] : '');
            if ($path) {
                $pathinfo = pathinfo($path);
                $newPath =  ($dist ? $dist : $pathinfo['dirname']) . "/" . $pathinfo['filename'] . $mode . "." . $pathinfo['extension'];
            }
            $this->getThumbnail(
                $path ? $path : $this->getFilePath($attribute, 1),
                $path ? $newPath : $this->getFilePath($attribute, 1, $mode, $dist),
                $size[0],
                $size[1]
            );
        }
    }

    public function getThumbnail($oldPath, $newPath, $width, $height)
    {
        Image::$driver = [Image::DRIVER_GD2, Image::DRIVER_IMAGICK];
        if (!$height || !$width)
        {
            $img = Image::getImagine()->open($oldPath);
            if (!$height) $height = $width*$img->getSize()->getHeight()/$img->getSize()->getWidth();
            if (!$width) $width = $height*$img->getSize()->getWidth()/$img->getSize()->getHeight();
            $box = new Box($width, $height);
            $img->resize($box)->save($newPath);
        } else Image::thumbnail(
            $oldPath,
            $width, $height
        )->save($newPath);
    }

    public static function getDropdownList($title = 'title', $key = 'id', $where = [])
    {
        $list = ArrayHelper::map(static::find()
			->select([$key, $title])
			->andFilterWhere($where)->asArray()
			->orderBy([$title => SORT_ASC])
			->all(), $key, $title);
        return $list;
    }

    public function getParam($param)
    {
        if (isset(\Yii::$app->params[$this->tableName()]))
            return ArrayHelper::valueFromPath($param, \Yii::$app->params[$this->tableName()]);
    }

    public static function getMenuDropdown($title = 'title', $key = 'id')
    {
        $data = static::find()->select([$key, $title])->asArray()->all();

        $items = [];
        foreach ($data as $item) {
            $items[] = [
                'label' => $item[$title],
                'url' => Url::to(['/' . \Yii::$app->requestedRoute, 'lang' => $item[$key]])
            ];
        }

        return $items;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'name' => 'Название',
            'short_text' => 'Краткое описание',
            'text' => 'Полное описани',
            'slug' => 'Постоянная ссылка',
            'publish_date' => 'Дата публикации',
            'create_date' => 'Дата добавления',
            'update_date' => 'Дата редактирования',
            'status_id' => 'Статус',
            'user_id' => 'Пользователь',
            'delete_date' => 'Дата удаления',
            'delete_by' => 'Кем удалено',
            'views' => 'Просмотры',
            'meta_title' => 'Заголовок страницы',
            'meta_description' => 'Описание страницы',
            'meta_keywords' => 'Ключевые слова',
            'lang_id' => 'Язык',
            'is_active' => 'Активный',
            'tagNames' => 'Теги',
            'phones' => 'Телефоны',
            'phone' => 'Телефон',
            'site' => 'Сайт',
            'parent_id' => 'Родитель',
            'description' => 'Описание',
            'email' => 'E-mail',
            'username' => 'Логин',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'timeToPost' => 'Дата публикации',
            'is_publish_to_soc_nets' => 'Публикация в соц сети',
            'date' => 'Дата',
            'sex' => 'Пол',
            'link' => 'Ссылка',
            'year' => 'Год',
            'second_name' => 'Отчество',
            'position' => 'Позиция',
            'category_id' => 'Категория',

            'short_text_kz' => 'Краткое описание (каз)',
            'text_kz' => 'Полное описание (каз)',
            'meta_title_kz' => 'Заголовок страницы (каз)',
            'meta_description_kz' => 'Описание страницы (каз)',
            'meta_keywords_kz' => 'Ключевые слова (каз)',
            'title_kz' => 'Заголовок (каз)',
            'first_name_kz' => 'Имя (каз)',
            'last_name_kz' => 'Фамилия (каз)',
            'second_name_kz' => 'Отчество (каз)',
        ];
    }

    //=============

    /*public function behaviors()
    {
        $behaviors = [];
        if (property_exists($this, 'create_date'))
            $behaviors[] = [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    self::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ];
        return $behaviors;
    }*/

    protected function getTimestampBehavior()
    {
        return [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                self::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                self::EVENT_BEFORE_UPDATE => ['update_date']
            ],
            'value' => new Expression('NOW()'),
        ];
    }

    protected function getTimestampBehaviorUpdate()
    {
        return [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                self::EVENT_BEFORE_UPDATE => ['update_date']
            ],
            'value' => new Expression('NOW()'),
        ];
    }

    protected function getTimestampBehaviorCreate()
    {
        return [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                self::EVENT_BEFORE_INSERT => ['create_date'],
            ],
            'value' => new Expression('NOW()'),
        ];
    }

    protected function getTimestampBehaviorDelete()
    {
        return [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                self::EVENT_BEFORE_DELETE => ['delete_date'],
            ],
            'value' => new Expression('NOW()'),
        ];
    }

    protected function getTimestampBehaviorDeleteBy()
    {
        return [
            'class' => AttributeBehavior::className(),
            'attributes' => [
                self::EVENT_BEFORE_DELETE => ['delete_by'],
            ],
            'value' => \Yii::$app->user->id,
        ];
    }

    protected function getUserIdBehavior()
    {
        return [
            'class' => AttributeBehavior::className(),
            'attributes' => [
                self::EVENT_BEFORE_INSERT => ['user_id'],
            ],
            'value' => \Yii::$app->user->id,
        ];
    }

    public $isNew = null;

    private $fieldAliases = [];
    private static $aliasPrefix = 'fa_';

    protected static function isAlias($name)
    {
        return strpos($name, self::$aliasPrefix) === 0;
    }

    public static function populateRecord($record, $row)
    {
        foreach ($row as $name => $value) {
            if (self::isAlias($name))
                $record->$name = $value;
        }
        parent::populateRecord($record, $row);
    }

    public function __set($name, $value)
    {
        if (self::isAlias($name)) {
            $this->fieldAliases[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    public function __get($name)
    {
        if (self::isAlias($name)) {
            return $this->fieldAliases[$name];
        } else {
            return parent::__get($name);
        }
    }

    public function getLogo()
    {
        $class = $this->className() . 'FilesLogo';
        return $this->hasOne($class, [$class::$keyField => 'id']);
    }

    public function getVideo()
    {
        $class = $this->className() . 'FilesVideo';
        return $this->hasOne($class, [$class::$keyField => 'id']);
    }

    public function getImage()
    {
        $class = $this->className() . 'FilesImage';
        return $this->hasOne($class, [$class::$keyField => 'id']);
    }

    public function getMetaImage()
    {
        $class = $this->className() . 'FilesMetaImage';
        return $this->hasOne($class, [$class::$keyField => 'id']);
    }

    public function getIdentitiyLogo()
    {
        return $this->logo;
    }

    public function setFile($attribute, $data, $thumbnails = false, $multiple = false, $relationData = [])
    {
        $_file = null;
        if ($multiple) {
            /*print_r($data);
            print_r('<br><br>');
            print_r($relationData);
*/
            if($relationData) {
                $i = 0;
                foreach($relationData as $k => $v) {
                    $i++;
                    if(isset($data[$v['key']])) {
                        $relationData[$k] = ArrayHelper::merge($v, $data[$v['key']]);
                    }
                    $relationData[$k]['position'] = $i;
                }
                $data = $relationData;
            }
            //exit;
            foreach($data as $v) {
                $_file = $this->_setFile($attribute, $v, $thumbnails, true);
            }
        } else {
            $_file = $this->_setFile($attribute, $data, $thumbnails);
        }
        return $_file;
    }

    private function _setFile($attribute, $data, $thumbnails, $multiple = false)
    {
        $class = $this->className() . 'Files' . ucfirst($attribute);
        $file = null;
        if(!$multiple) {
            $dfile = $class::findOne([$class::$keyField => $this->id]);
            if ($dfile && trim(str_replace('/', '', $dfile->subdir))) {
                FileHelper::deleteDirectories(
                    implode('/', [
                        FileHelper::getStoragePath(true, \Yii::$app->controller->module->id),
                        $dfile->subdir
                    ])
                );
                $dfile->delete();
            }
        } else {
            $file = $class::findOne($data['id']);
        }

        if(!$file) $file = new $class();
        $file->load([$data], 0);
        $file->{$class::$keyField} = $this->id;
        $file->validate();
        $file->save();


        if ($thumbnails) {
            $sizes = $this->getParam($attribute . '.sizes');
            $thumb = new wImage(implode('/',[
                FileHelper::getStoragePath(true, $this->tableName()),
                $file->subdir,
                $file->basename
            ]));
            foreach($sizes as $v) $thumb->createThumbnail($v[0], $v[1]);
        }
        return $file;
    }

    public function deleteFile($attribute, $id)
    {
        $class = $this->className() . 'Files' . ucfirst($attribute);
        $file = $class::findOne($id);
        if ($file) {

            FileHelper::removeDirectory(FileHelper::getStoragePath(true, $this->tableName()) . '/' . $file->subdir);
            $file->delete();
        }
    }

    public static function query($sql = null, $db = 'db')
    {
        $connection = \Yii::$app->{$db};
        return $connection->createCommand($sql);
    }

    public function beforeSave($insert)
    {
        $this->isNew = $this->isNewRecord;
        return parent::beforeSave($insert);
    }

    public function remove()
    {
        $this->delete_date = new Expression('NOW()');
        $this->delete_by = \Yii::$app->user->id;
        $this->save(false);
    }

    public function saveCategories($idTitle, $class, $category_id_title = 'category_id')
    {
        if (!empty($this->listCategories) && is_array($this->listCategories)) {
            $class::deleteAll(([$idTitle => $this->id]));

            foreach ($this->listCategories as $category) {
                if ($category <= 0) {
                    continue;
                }
                $model = new $class();
                $model->$idTitle = $this->id;
                $model->$category_id_title = $category;
                $model->save();
            }
        }
    }

    protected $deleteDir = [];

    public function beforeDelete()
    {
        if(isset($this->file_attributes)) {
            foreach($this->file_attributes as $v) {
                $files = $this->getFilePath($v, true);
                if(is_array($files)) {
                    foreach($files as $file) {
                        $this->deleteDir[] = dirname($file);
                    }
                } else {
                    $this->deleteDir[] = dirname($files);
                }
            }
        }
        return parent::beforeDelete();
    }

    public function afterDelete()
    {
        if($this->deleteDir) foreach($this->deleteDir as $v) {
            FileHelper::removeDirectory($v);
        }

        return parent::afterDelete();
    }

    public function saveRelations($idKey, $relationKey, $table, $id, $relationIDs = [])
    {
        $data = [];
        $this->query()->delete($table, $idKey . ' = :id', [':id' => $id])->execute();
        if($relationIDs){
            foreach ($relationIDs as $v) $data[] = [$v, $id];
            $this->query()->batchInsert($table, [$relationKey, $idKey, ], $data)->execute();
        }
    }

    public function getMetaTitle($titleField = 'title')
    {
        return $this->meta_title ? $this->meta_title : Html::encode(strip_tags($this->$titleField));
    }

    public function getMetaDescription($titleField = 'short_text')
    {
        return $this->meta_description ? $this->meta_description : (strip_tags($this->$titleField));
    }

    public function getMetaKeywords($titleField = 'meta_keywords')
    {
        return $this->meta_keywords ? $this->meta_keywords : Html::encode(strip_tags($this->$titleField));
    }

    public function getJoinMetaImage($titleField = 'fa_logo')
    {
        return $this->fa_meta_image ? Url::imgUrl($this->fa_meta_image, $this->tableName()) : ($this->$titleField ? Url::imgUrl($this->$titleField, $this->tableName()) : null);
    }


    public static function festID()
    {
        return \Yii::$app->controller->festID;
    }

    public static function fest()
    {
        return \Yii::$app->controller->fest;
    }
}
