<?php
/**
 * Created by PhpStorm.
 * User: Vital
 * Date: 04.03.2015
 * Time: 13:17
 */
namespace app\components;

use yii\web\NotFoundHttpException;

class Pagination extends \yii\data\Pagination
{
    public function init()
    {
        if ($this->getPageCount() < \Yii::$app->request->get($this->pageParam))
            throw new NotFoundHttpException();
    }
}