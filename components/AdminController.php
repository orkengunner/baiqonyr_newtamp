<?php

namespace app\components;

use app\assets\AppAsset;
use app\components\actions\CropImage;
use app\components\actions\RemoveFile;
use app\components\actions\UploadAsync;
use app\components\grid\AddMenuItemAction;
use app\modules\festivals\models\Festivals;
use app\modules\lookupusersroles\models\LookupUsersRoles;
use app\modules\users\models\Users;
use dosamigos\grid\actions\ToggleAction;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\View;

class AdminController extends MainController
{
    public $layout = '//admin/main';
    protected $_modelName = null;


    public $allowedRoles = [];

    public $festID = null;

    public function init()
    {
        parent::init();

        $fest = Festivals::findOne(['status_id' => Festivals::STATUS_MODERATION]);
        if(!$fest) {
            $fest = Festivals::findOne(['status_id' => Festivals::STATUS_ACTIVE]);
        }
        if($fest) $this->festID = $fest->id;

        $this->getView()->on(View::EVENT_BEFORE_RENDER, function () {
            AppAsset::register($this->getView());
        });
    }

    public function behaviors()
    {
        return [
            [
                'class' => AccessControl::className(),
                'except' => ['login', 'error', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            if (!\Yii::$app->getUser()->getIsGuest()) {
                                if (empty(\Yii::$app->controller->allowedRoles) || !is_array(\Yii::$app->controller->allowedRoles)) {

                                    $user = null;
                                    if (in_array(Users::PERMISSION_BAYKONUR, \Yii::$app->user->getIdentity()->getPermissions())) {
                                        $user = 'user';
                                    }

                                    //if (!$user && !\Yii::$app->user->getIdentity()->checkRole('admin') && \Yii::$app->user->getIdentity()->getPermissions())
                                        //return $this->redirect('/admin.php/'. Users::$permsUrl[\Yii::$app->user->getIdentity()->getPermissions()[0]]);
                                    return \Yii::$app->user->getIdentity()->checkRole(['admin', 'author' , $user]);
                                }
                                return \Yii::$app->getUser()->getIdentity()->checkRole(\Yii::$app->controller->allowedRoles);
                            }
                            return false;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'crop-image' => [
                'class' => CropImage::className()
            ],
            'upload-async' => [
                'class' => UploadAsync::className()
            ],
            'toggle' => [
                'class' => ToggleAction::className(),
                'modelClass' => $this->getModelName(),
                'onValue' => 1,
                'offValue' => 0
            ],
            'add-menu-item' => [
                'class' => AddMenuItemAction::className()
            ],
            'remove-file' => [
                'class' => RemoveFile::className()
            ],
        ];
    }

    public function actionRemove($id)
    {
        $this->findModel($id)->remove();

        return $this->redirect(['index']);
    }
}
