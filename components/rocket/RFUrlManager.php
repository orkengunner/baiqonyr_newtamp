<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 9/19/14
 * Time: 15:46
 */

namespace app\components\rocket;


use app\modules\languages\models\Languages;
use app\modules\news\models\TypeNews;
use app\modules\shops\models\ShopsTypes;
use yii\caching\DbDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UrlManager;

class RFUrlManager extends UrlManager
{
    public $url_items = [];
    public $string = [];

    public function createUrl($params)
    {
        if (isset($params['lang_id'])) {
            //Если указан идентификатор языка, то делаем попытку найти язык в БД,
            //иначе работаем с языком по умолчанию
            $lang = Languages::findOne($params['lang_id']);

            if ($lang === null) {
                $lang = Languages::getDefaultLang();
            }
            //unset($params['lang_id']);
        } else {
            //Если не указан параметр языка, то работаем с текущим языком
            $lang = Languages::getCurrent();
        }

        //Получаем сформированный URL(без префикса идентификатора языка)
        $url = parent::createUrl($params);
        //Добавляем к URL префикс - буквенный идентификатор языка
        if ($lang->code != 'ru') {
            if ($url == '/') {
                return '/' . $lang->code;
            } else {
                return '/' . $lang->code . $url;
            }
        }

        return $url;

    }

    public function init() {
        $newsTypes = TypeNews::findTypes();

        $rules = [
            '/news/' => 'news/default/index',
            '/news/<content_slug:text|video|photo>' => 'news/default/index-content',
            '/news/page-<page:[0-9]{1,}>' => 'news/default/index',
            '/news/<content_slug:text|video|photo>/page-<page:[0-9]{1,}>' => 'news/default/index-content',
            '/news/<news_types_slug:' . $this->arrayToString($newsTypes) . '>' => 'news/default/index-category',
            '/news/<news_types_slug:' . $this->arrayToString($newsTypes) . '>/<content_slug:text|video|photo>' => 'news/default/index-category-content',
            '/news/<news_types_slug:' . $this->arrayToString($newsTypes) . '>/page-<page:[0-9]{1,}>' => 'news/default/index-category',
            '/news/<news_types_slug:' . $this->arrayToString($newsTypes) . '>/<content_slug:text|video|photo>/page-<page:[0-9]{1,}>' => 'news/default/index-category-content',
            '/news/<news_slug>' => 'news/default/view',
        ];

        $this->rules = ArrayHelper::merge($rules, $this->rules);
        parent::init();
    }

    private function arrayToString($array, $is_object = true, $separator = '|', $slug = 'url')
    {
        $_array = [];
        if($is_object) foreach($array as $v) {
            $_array[] = $v->{$slug};
        }
        else return implode($separator, $array);
        return implode($separator, $_array);
    }

    public function generateUrl($array, $count, $n, $separator = '/')
    {
        for($i=$n;$i<$count;$i++)
        {
            $this->url_items[$n] = $array[$i];
            if($i < $count-1)
                $this->generateUrl($array, $count, $i+1);
            $this->string[] = implode($separator, $this->url_items);
            if(!$n) return $this->string;
            unset($this->url_items[$n]);
        }
    }
}
