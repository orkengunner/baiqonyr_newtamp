<?php
namespace app\components\traits;

use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 *
 */
trait UploadableAsync
{
    use Uploadable;

    public static $asyncTempDir = 'temp';

    protected $uploaded1sAsync = [];

    public function getTempMediaDirectory($abs = false)
    {
        $path = $this->getStorageDir($abs);
        return $path . '/' . self::$asyncTempDir;
    }

    private function getFiles()
    {
        return \Yii::$app->session->get($this->tableName());
    }

    private function setFiles($data)
    {
        \Yii::$app->session->set($this->tableName(), $data);
    }

    public function getAsyncFileFromSession($attribute, $index = null)
    {
        $data = $this->getFiles();

        if (is_numeric($index)) {
            if (isset($data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute][$index]) && $data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute][$index])
                return $data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute][$index];
        }
        else {
            if (isset($data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute]) && $data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute])
                return $data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute];
        }
        return false;
    }

    public function addAsyncFileToSession($attribute, $file, $index = null, $multiple = false)
    {
        $data = $this->getFiles();
        if (!$multiple)
            $data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute] = $file;
        elseif(is_numeric($index))
            $data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute][$index] = $file;
        else
        {
            if (!isset($data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute]) || !is_array($data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute]))
                $data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute] = [];
            $data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute][] = $file;
        }

        $this->setFiles($data);
    }

    public function removeAsyncFileFromSession($attribute, $index = null)
    {
        $data = $this->getFiles();
//print_r($data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute][$index]);
        if (is_numeric($index)) {
            if (isset($data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute][$index]))
                unset($data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute][$index]);
        }
        else {
            if (isset($data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute]) && $data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute])
                unset($data['__FILES__'][($this->isNew ? 0 : (int)$this->id)][$attribute]);
        }
        $this->setFiles($data);
    }

    public function getAsyncTempFiles($attributeName, $abs = true, $ensure = false)
    {
        $file = $this->getAsyncFileFromSession($attributeName);

        if (!$file)
            return false;
        $result = [];
        foreach ($file as $i => $value) {
            $file = $this->getAsyncTempFile($attributeName, $abs, $ensure, $i);
            if ($file)
                $result[$i] = $file;
        }
        return $result;
    }


    public function getAsyncTempFile($attributeName, $index = null, $abs = false)
    {
        $data = $this->getAsyncFileFromSession($attributeName, $index);

        if (!$data) return null;
        if (!is_array($data))
            $path = $this->getTempMediaDirectory($abs) . '/' . $data;
        else {
            foreach ($data as $k => $v)
                $path[$k] = $this->getTempMediaDirectory($abs) . '/' . $v;
        }
        return $path;
    }

    public function hasAsyncTempFile($attributeName, $index = null)
    {
        return (bool)$this->getAsyncFileFromSession($attributeName, $index);
    }

    public function validateAndSaveAsyncFile($attribute, $saveToSession = true, $removeOld = true, $multiple = false)
    {
        if (!($this->{$attribute} instanceof UploadedFile)) {
            $this->$attribute = UploadedFile::getInstance($this, $attribute);
        }
        if (!$this->$attribute || $this->$attribute->hasError)
            return false;
        $valid = $this->validate([$attribute]);

        if (!$valid)
            return false;
        $path = $this->saveAsyncTempFile($attribute, $saveToSession, $removeOld, $multiple);
        return $path;
    }

    public function saveAsyncTempFile($attributeName, $saveToSession = true, $removeOld = true, $multiple = false)
    {
        /**
         * @var $file UploadedFile
         */
        $file = $this->$attributeName;
        $path = $this->tableName() . '/' . self::$asyncTempDir;
        $name = FileHelper::saveUploaded($file, $path, false);
        if ($saveToSession) {
            if ($removeOld)
                $this->removeAsyncTempFile($attributeName);
            $this->addAsyncFileToSession($attributeName, $name, null, $multiple);
        }
        return $name;
    }

    public function saveAsyncFile($attribute, $index = null, $removeTemporaryFiles = true)
    {
        $name = $this->getAsyncFileFromSession($attribute);

        if (!$name) return [];
        /*if (!$name)
            throw new \Exception('No such file: ' . $attribute);*/

        $dir = $this->getStorageDir(true);

        if (is_array($name)) {
            foreach ($name as $k => $v) {
                $subDir = FileHelper::generateSubdir($dir);
                $path = $dir . '/' . $subDir . "/" . $v;
                if($removeTemporaryFiles) {
                    rename($this->getTempMediaDirectory(true) . '/' . $v, $path);
                    $this->removeAsyncFileFromSession($attribute, $k);
                } else {
                    copy($this->getTempMediaDirectory(true) . '/' . $v, $path);
                }

                $data[$k] = [
                    'basename' => $v,
                    'mime' => mime_content_type($path),
                    'size' => filesize($path),
                    'subdir' => $subDir
                ];
            }
        } else {
            $subDir = FileHelper::generateSubdir($dir);
            $path = $dir . '/' . $subDir . "/" . $name;
            if($removeTemporaryFiles) {
                rename($this->getTempMediaDirectory(true) . '/' . $name, $path);
                $this->removeAsyncFileFromSession($attribute, $index);
            } else {
                copy($this->getTempMediaDirectory(true) . '/' . $name, $path);
            }
//print_r($removeTemporaryFiles);
            $data = [
                'basename' => $name,
                'mime' => mime_content_type($path),
                'size' => filesize($path),
                'subdir' => $subDir
            ];
        }
        return $data;
    }



    public function removeAsyncTempFile($attribute, $index = null)
    {
        $path = $this->getAsyncTempFile($attribute, $index);

        if (file_exists($path)) unlink($path);
        $this->removeAsyncFileFromSession($attribute, $index);
    }

    public function removeAsyncTempFiles($removeFromSession = true)
    {
        $files = $this->getAsyncFileFromSession(null);
        if (!$files)
            return;
        foreach ($files as $attr => $file) {
            $this->removeAsyncTempFile($attr, $removeFromSession);
        }
    }

    public function validateRequiredAsyncFile($attribute)
    {
        if (!$this->isNewRecord)
            return;
        if ($this->$attribute)
            return;
        $file = $this->getAsyncTempFile($attribute);
        if (!$file) {
            $this->addError($attribute, 'Необходимо загрузить файл');
        }
    }
}
