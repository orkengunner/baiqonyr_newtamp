<?php
namespace app\components\traits;

use app\components\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

trait Uploadable
{

    /**
     * @param UploadedFile $file
     * @param $attribute
     * @param bool $removeOld
     * @return string
     */
    public function saveFile(UploadedFile $file, $attribute, $removeOld = true)
    {
        if ($removeOld) {
            $this->deleteFile($attribute);
        }
        $newName = FileHelper::saveUploaded($file, $this->tableName());
        $this->$attribute = $newName;
        return $newName;
    }

    /**
     * @param string $attribute
     * @param bool $abs
     * @param string $mode
     * @return string filename or false if there's no image
     */
    public function getFilePath($attribute, $abs = false, $size = [], $path = null)
    {
        if (!$this->$attribute) {
            return false;
        }
        if(!$path) $path = $this->getStorageDir($abs);

        $data = null;
        if (is_array($this->$attribute)) {
            $data = [];
            foreach ($this->$attribute as $k => $v) {
                if(trim(str_replace('/', '', $v->subdir))) {
                    $data[$v->id] = $this->_getFilePath($v, $path, $size);
                }
            }
        } elseif(trim(str_replace('/', '', $this->$attribute->subdir))) {
            $data = $this->_getFilePath($this->$attribute, $path, $size);
        }

        return $data;
    }

    private function _getFilePath($attribute, $path, $size)
    {
        $pathinfo = pathinfo($attribute->basename);

        if ($size && ($size[0] || $size[1])) {
            $sizes = (
            $size
                ? '-' . (
                $size[0]
                    ? $size[0]
                    : ''
                ) . 'x' . (
                empty($size[1])
                    ? ''
                    : $size[1]
                )
                : ''
            );
        } else $sizes = '';

        return implode('/',[
            $path,
            $attribute->subdir,
            $pathinfo['filename'] . $sizes . "." . $pathinfo['extension']
        ]);
    }


    /**public function deleteFile($attribute, $mode = null)
    {
        $path = $this->getFilePath($attribute, true, $mode);
        if ($path && file_exists($path)) {
            FileHelper::deleteDirectories(dirname($path));
        }
    }*/

    /**
     * @param bool $abs
     * @return string path
     */
    public function getStorageDir($abs = false)
    {
        return FileHelper::getStoragePath($abs, $this->tableName());
    }

}