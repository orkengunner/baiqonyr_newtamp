<div class="widget">
    <div class="widget-header">
        <h2>Опубликовать</h2>
    </div>
    <div class="widget-content padding">
        <?= in_array('publish_date', $exclude) ? '' : $form->field($model, 'publish_date', [
            'template' => '{label}{input}',
            //'options' => ['class' => 'form-group form-post-date-section'],
            //'labelOptions' => ['class' => 'control-label title-form-section']
        ])->widget(\janisto\timepicker\TimePicker::className(), [
            //'language' => 'fi',
            'mode' => 'datetime',
            'addon' => '',
            'clientOptions'=>[
                'dateFormat' => 'dd/mm/yy',
                'timeFormat' => 'HH:mm',
            ]
        ])->textInput(['value' => $model->publish_date ? date('d/m/Y H:i', strtotime($model->publish_date)) : date('d/m/Y H:i')]);?>

        <?= $form->field($model,
            'status_id')->dropDownList($model::$statuses) ?>

        <?= in_array('slug', $exclude) ? '' : $form->field($model, 'slug')->textInput(['maxlength' => 255, 'placeholder' => 'Генерируется автоматически']) ?>

        <?= $isSubmit ? \yii\helpers\Html::submitButton(Yii::t('posts', 'Сохранить'), [
            'class' => 'btn btn-primary  side-bar',
            'id' => 'submit-page'
        ]) : \yii\helpers\Html::button(Yii::t('posts', 'Сохранить'), [
            'class' => 'btn btn-primary  side-bar',
            'id' => 'submit-page'
        ]) ?>

    </div>
</div>