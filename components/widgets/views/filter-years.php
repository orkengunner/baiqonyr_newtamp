<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.05.19
 * Time 15:06
 */

?>

<nav>
	<?php
		foreach ($fests as $k => $fest):
			$active = '';
			$year = date('Y', strtotime($fest->date));
			if(\Yii::$app->request->get('year') == $year || !\Yii::$app->request->get('year') && !$k) {
				$active = 'active';
			}

			if(!$k) {
				$url = \yii\helpers\Url::to(['/pages/default/history']);
			} else {
				$url = \yii\helpers\Url::to(['/pages/default/history', 'year' => $year]);
			}
		?>
		<a href="<?= $url?>" class="title-section--year <?= $active; ?>"><?= $year; ?></a>
	<?php endforeach; ?>
</nav>

