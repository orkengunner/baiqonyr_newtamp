<?php
$this->registerJs('



$( "#af-' . $attribute . ' input[type=text]" ).autocomplete({
			source: "' . \yii\helpers\Url::to(['/jobs/search-af']) . '",
			autoFill: true,
            minLength: 2,
			html: true,
			select: function( event, ui ) {
    $("#ui-id-1").addClass(\'hidden\');

        setTimeout(function(){
            $("#winners-_job_id").val(ui.item.label)
        }, 50);
       $("#winners-job_id").val(ui.item.value);
    //console.log(ui.item.value);
            },
            search: function( event ) {
                $("#ui-id-1").removeClass(\'hidden\');
            },
            response: function( event, data ) {

            },
            close: function( event, data ) {

                $("#ui-id-1").addClass(\'hidden\');
            },
		});


');
?>
<div id="af-<?= $attribute; ?>">
<?= $form->field($model, '_' . $attribute)->textInput() ?>

<?= \yii\helpers\Html::activeHiddenInput($model, $attribute)?>
</div>