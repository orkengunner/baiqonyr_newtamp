<?php
/**
 * @var $form \yii\bootstrap\ActiveForm
 * @var $model \app\components\behaviors\WithMetaTags|\app\components\traits\UploadableAsync|\yii\db\ActiveRecord
 * @var $this \yii\web\View
 */
?>
<div class="widget">
    <div class="widget-header"><h2 class="widget-toggle">Публикация в социальные сети</h2>

        <div class="additional-btn"><a class="widget-toggle" href="#"><i class="icon-down-open-2"></i></a></div>
    </div>


        <div class="widget-content padding" style="display: none;">

            <?php $displayTime = false; foreach($socialNetworks as $v): $soc_net_id = $v->id?>
                <?php if(isset($pages[$v->id])): $displayTime = true; ?>
                    <div class="form-group field-posts-pages">
                        <label class="control-label"><?= $v->title; ?></label>
                        <?= $form->field($model,'socialPages[' . $v->id . ']', ['template' => '{input}'])->radioList($pages[$v->id],['item' => function ($index, $label, $name, $checked, $value) use ($model, $soc_net_id) {

                            $_value = explode('_', $value);
                            if($model->socialPages) {
                                $_socialPages = explode('_', $model->socialPages[$soc_net_id]);
                                if(!$checked && $_value[0] == $_socialPages[0]) {
                                    $checked = true;
                                }
                            }

                            $cnt = '
                                <div class="radio-item">
                                    ' . \yii\helpers\Html::radio($name, $checked, ['value' => $value]) . '
                                    <label class="control-label" for="id_' . $value . '">' . $label . '</label>
                                </div>';
                            return $cnt;
                        }])?>
                    </div>
                <?php else:?>
                    <div>Необходимо получить <a href="<?= \yii\helpers\Url::to([
                            '/users/soc-net-login',
                            'redirect' => $_SERVER['REQUEST_URI'],
                            'service' => $v->slug
                        ])?>">токен</a></div>
                <?php endif; ?>
            <?php endforeach; ?>


            <?= $displayTime ? $form->field($model, 'timeToPost', [
                'template' => '{label}{input}',
                //'options' => ['class' => 'form-group form-post-date-section'],
                //'labelOptions' => ['class' => 'control-label title-form-section']
            ])->widget(\janisto\timepicker\TimePicker::className(), [
                //'language' => 'fi',
                'mode' => 'datetime',
                'addon' => '',
                'clientOptions'=>[
                    'dateFormat' => 'dd/mm/yy',
                    'timeFormat' => 'HH:mm',
                ]
            ])->textInput(['placeholder' => 'Если дата не указана, пост опуликуется сразу']) : '';?>

        </div>

</div>
