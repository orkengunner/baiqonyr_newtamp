<?php
/**

 * @var $form \yii\widgets\ActiveForm
 * @var $model \yii\db\ActiveRecord
 * @var $attribute string
 */

    $this->registerJs("
        var getGalleryLink = function(gallery_id, title) {
        window.open('/admin.php/gallery/' +
            (parseInt($(gallery_id).val()) ? 'update?id=' +
            $(gallery_id).val() + '&model_id={$model->id}' : 'create?model_id=0&title=' +
            $(title).val())
        , '_blank')
    }

    var setGallery = function(gallery_id) {
            $('#" . \Yii::$app->controller->module->id  . "-gallery_id').val(gallery_id);
        var params = window.location.search.replace('?','').split('&');
        params[params.length] = '$attribute='+gallery_id;
        window.history.pushState(null, null, window.location.pathname+'?'+params.join('&'));
        $.pjax.reload({container:'#$container'});
    }
    ", \yii\web\View::POS_END);
?>
<div class="widget">
    <div class="widget-header">
        <h2 class="widget-toggle"><?= $title ? $title : 'Галерея';?></h2>
        <div class="additional-btn">
            <a class="widget-toggle" href="#">
                <i class="icon-down-open-2"></i>
            </a>
        </div>
    </div>
    <div class="widget-content padding"  style="display: none;">
        <?php \yii\widgets\Pjax::begin(['id' => $container, 'options' => ['style' => 'padding-top: 20px;']]); ?>
        <table>
            <tr>
                <?php
                $data = [];
                if($model->$attribute) {
                    $data = $model->$attribute->images;
                    $model->gallery_id = $model->$attribute->id;
                }
                elseif(\Yii::$app->request->get($attribute) || $model->gallery_id)
                    $data = \app\modules\gallery\models\Gallery::findOne(\Yii::$app->request->get($attribute) ? \Yii::$app->request->get($attribute) : $model->gallery_id)->images;
                foreach($data as $k => $v): ?>
                    <?php if($k && $k%4==0) echo '</tr><tr>';?>
                    <td>
                        <div class="col-md-3 gallery-image">
                            <div class="thumbnail">
                                <img src="<?= \yii\helpers\Url::imgUrl($v->subdir . '/' . $v->basename, 'gallery'); ?>">
                            </div>
                        </div>
                    </td>
                <?php endforeach; ?>
            </tr>
        </table>
        <?php \yii\widgets\Pjax::end(); ?>
        <?= $form->field($model, 'gallery_id', ['template' => '{input}'])->hiddenInput(); ?>

        <a class="btn btn-info btn-add-image gallery" href="javascript:;"
           onclick="getGalleryLink('#<?= \Yii::$app->controller->module->id; ?>-gallery_id', '#<?= \Yii::$app->controller->module->id; ?>-title')">Редактировать галерею</a>


    </div>
</div>