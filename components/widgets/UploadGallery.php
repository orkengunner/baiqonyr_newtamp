<?php
namespace app\components\widgets;

class UploadGallery extends \yii\base\Widget
{

    public $title;
    public $model;
    public $form;
    public $attribute;
    public $container;

    public function run()
    {

        return $this->render('uploadGallery', [
            'form'=>$this->form,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'container' => $this->container,
            'title' => $this->title
        ]);
    }
}
