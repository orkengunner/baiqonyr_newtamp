<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.05.19
 * Time 15:03
 */

namespace app\components\widgets;


use app\modules\festivals\models\Festivals;
use yii\base\Widget;

class FilterYears extends Widget
{
	public function run()
	{
		$fests = Festivals::getCacheOldFests();

		return $this->render('filter-years', [
			'fests' => $fests
		]);
	}
}