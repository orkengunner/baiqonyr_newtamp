<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LinkPager extends \yii\widgets\LinkPager
{
    public $pageParam = 'page';
    public $anchor = '';
    public $mode_rewrite = true;
    public $linkClass = true;
    public $tag = null;
    public $tagClass = null;
    public $noIndex = true;

    /**
     * Renders a page button.
     * You may override this method to customize the generation of page buttons.
     * @param string $label the text label for the button
     * @param integer $page the page number
     * @param string $class the CSS class for the page button.
     * @param boolean $disabled whether this page button is disabled
     * @param boolean $active whether this page button is active
     * @return string the rendering result
     */

    protected function renderPageButtons()
    {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            return '';
        }

        $buttons = [];
        $currentPage = $this->pagination->getPage();

       /* // first page
        if ($this->firstPageLabel !== false) {
            $buttons[] = $this->renderPageButton($this->firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false);
        }*/

        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $currentPage - 1) < 0) {
                $page = 0;
            }
            $buttonsInternal[] = $this->renderPageButton($this->prevPageLabel, $page, $this->prevPageCssClass, $currentPage <= 0, false, $this->tag, $this->tagClass);
        }

        // internal pages
        list($beginPage, $endPage) = $this->getPageRange();
        for ($i = $beginPage; $i <= $endPage; ++$i) {
            $buttonsInternal[] = $this->renderPageButton($i + 1, $i, $this->linkClass, false, $i == $currentPage, $this->tag, $this->tagClass);
        }

        if ($i != $pageCount)
        {
            $buttonsInternal[] = '...';
            $buttonsInternal[] = $this->renderPageButton($pageCount, $pageCount-1, $this->linkClass, false, false, $this->tag, $this->tagClass);
        }

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $currentPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }
            $buttonsInternal[] = $this->renderPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false, $this->tag, $this->tagClass);
        }


       /* // last page
        if ($this->lastPageLabel !== false) {
            $buttons[] = $this->renderPageButton($this->lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);
        }*/

        $nav = Html::tag('nav', ('<ul>' . implode("\n", $buttonsInternal) . '</ul>'), $this->options);;
        if($this->noIndex) {
            $nav = '<noindex>' . $nav . '</noindex>';
        }
        return $nav;
    }


    protected function renderPageButton($label, $page, $class, $disabled, $active, $tag = null, $tagClass = '')
    {
        $options = ['class' => $class === '' ? null : $class];
        if($this->noIndex) $options = ArrayHelper::merge($options, ['rel' => 'nofollow']);
        $tagOptions = ['class' => $tagClass === '' ? null : $tagClass];

        if ($active) {
            Html::addCssClass($options, $this->activePageCssClass);
            if($tag)
            Html::addCssClass($tagOptions, $this->activePageCssClass);
        }
        if ($disabled) {
            Html::addCssClass($options, $this->disabledPageCssClass);

            return Html::tag($tag, Html::tag('a', $label, $options), $tagOptions);
        }
        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;
        #print_r($_SERVER['REQUEST_URI']);
        if($tag)
            return Html::tag($tag, Html::a($label, $this->createUrl($page), $options), $tagOptions);
        else {
            Html::a($label, $this->createUrl($page));
        }
    }

    protected function createUrl($page)
    {
        if($this->mode_rewrite) {
            $query_params = [];
            $url = parse_url($_SERVER['REQUEST_URI']);
            if(isset($url['query']))foreach(explode('&', $url['query']) as $v) {
                $_v = explode('=', $v);
                $query_params[$_v[0]] = $_v[1];
            }
            $query_url = explode('/', trim($url['path'], '/'));
            //print_r($this->pagination->getPage());exit;

            if (\Yii::$app->request->getQueryParam('page')) {
                array_pop($query_url);
            }
            return '/' . implode('/', $query_url) . '/' . $this->pageParam . '-' . ($page+1) . '/' . (isset($url['query']) ? '?' . $url['query'] : '') . ($this->anchor ? '#' . $this->anchor : '');
        }
        $request = explode('?', $_SERVER['REQUEST_URI']);
        $get = Yii::$app->request->get();
        $url = [];
        foreach ($get as $k => $v){
            if ($k == 'url' || $k == $this->pageParam) continue;
            $url[] = $k . '=' . $v;
        }
        $url[] = $this->pageParam . '=' . ($page+1);
        return $request[0] . ($url ? '?' . implode('&', $url) : '') . ($this->anchor ? '#' . $this->anchor : '');
    }
}
