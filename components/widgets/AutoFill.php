<?php
namespace app\components\widgets;

class AutoFill extends \yii\base\Widget
{
    public $model = null;
    public $form = null;
    public $attribute = null;

    public function run()
    {
        return $this->render('autoFill', [
            'model' => $this->model,
            'form' => $this->form,
            'attribute' => $this->attribute,
        ]);
    }
}
