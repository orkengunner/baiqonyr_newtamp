<?php
namespace app\components\widgets;

class PublishData extends \yii\base\Widget
{
    public $model = null;
    public $form = null;
    public $exclude = [];
    public $isSubmit = true;

    public function run()
    {
        return $this->render('publishData', [
            'model' => $this->model,
            'form' => $this->form,
            'isSubmit' => $this->isSubmit,
            'exclude' => $this->exclude
        ]);
    }
}
