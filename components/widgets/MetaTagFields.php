<?php
namespace app\components\widgets;

use app\components\ActiveRecord;
use app\components\behaviors\WithMetaTags;
use app\components\traits\UploadableAsync;
use app\modules\languages\models\Languages;

class MetaTagFields extends \yii\base\Widget
{
    /**
     * @var \app\components\behaviors\WithMetaTags|ActiveRecord|UploadableAsync
     */
    public $model;
    public $isLang = false;

    /**
     * @var \yii\widgets\ActiveForm
     */
    public $form;

    public function run()
    {
        $langs = [];
        if($this->isLang) {
            $langs = Languages::find()->select('code')->andWhere(['<>', 'id', 1])->column();
            $attrs = ['meta_title', 'meta_description', 'meta_keywords', 'metaImageFile'];
            foreach ($langs as $lang) {
                $attrs[] = 'meta_title_' . $lang;
                $attrs[] = 'meta_description_' . $lang;
                $attrs[] = 'meta_keywords_' . $lang;
            }
        } else {
            $attrs = ['meta_title', 'meta_description', 'meta_keywords', 'metaImageFile'];
        }
        $hasErrors = false;
        foreach ($attrs as $attr) {
            if ($this->model->hasErrors($attr)) {
                $hasErrors = true;
                break;
            }
        }
        return $this->render('metaTagFields', [
            'model' => $this->model,
            'form' => $this->form,
            'hasErrors' => $hasErrors,
            'id' => $this->id,
            'imageSize' => WithMetaTags::$metaImageSize,
            'langs' => $langs
        ]);
    }
}
