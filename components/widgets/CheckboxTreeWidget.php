<?php
/**
 * Created by PhpStorm.
 * User: yevgeniy
 * Date: 10/24/14
 * Time: 11:33 AM
 */

namespace app\components\widgets;


use app\modules\languages\models\Languages;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\Html;

class CheckboxTreeWidget extends Widget
{
    /**
     * Имя модели, для полей
     * @var string
     */
    public $model;

    /**
     * Имя модели, с которой брать список для построения дерева
     * @var string
     */
    public $treeModel;

    public $query = null;
    public $where = [];

    /**
     * Имя поля, для формы
     * @var string
     */
    public $attributeName;

    public function run()
    {
        if (empty($this->treeModel)) {
            throw new InvalidParamException('Не установлен параметр $treeModel');
        }

        if (empty($this->model)) {
            throw new InvalidParamException('Не установлен параметр $model');
        }
        /**
         * @var \yii\db\ActiveRecord $model
         */
        $model = $this->treeModel;

        $query = $this->query ? $this->query : $model::find();

        $categories = $query->andWhere([
                'lang_id' => Languages::getAdminCurrent()->id,
                'is_active' => 1
            ])
            ->andFilterWhere($this->where)
            ->addOrderBy('root, lft')->all();


        return $this->render('checkboxTree',
            ['categories' => $categories, 'model' => $this->model, 'attributeName' => $this->attributeName]);
    }
}
