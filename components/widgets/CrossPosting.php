<?php
namespace app\components\widgets;

use app\components\ActiveRecord;
use app\components\behaviors\WithMetaTags;
use app\components\traits\UploadableAsync;
use app\modules\socialnetworks\models\LookupSocNets;
use app\modules\socialnetworks\models\SocNetsTokens;

class CrossPosting extends \yii\base\Widget
{
    /**
     * @var \app\components\behaviors\WithMetaTags|ActiveRecord|UploadableAsync
     */
    public $model;
    public $user_id = null;
    /**
     * @var \yii\widgets\ActiveForm
     */
    public $form;

    public function run()
    {
        if(\Yii::$app->request->isAjax) {
            return false;
        }
        $token = SocNetsTokens::find()
            ->andWhere([
                'user_id' => $this->user_id ? $this->user_id : \Yii::$app->user->id,
            ])
            ->indexBy('soc_net_id')
            ->asArray()
            ->all();


        $pages = [];
        foreach(LookupSocNets::getEauth() as $k => $v) {

            if($token[$k]['token_expires'] && strtotime($token[$k]['token_expires']) <= time() || !$v->checkToken($token[$k]['token'])) {
                $pages[$k] = null;
                continue;
            }

            $_pages = $v->getPages($token[$k]['soc_user_id'], $token[$k]['token']);
            foreach($_pages as $page) {
                $pages[$k][$page['id'] . '_' . $page['access_token']] = $page['name'];
            }
        }

        return $this->render('crossPosting', [
            'model' => $this->model,
            'form' => $this->form,
            'id' => $this->id,
            'socialNetworks' => LookupSocNets::find()->all(),
            'pages' => $pages
        ]);
    }
}
