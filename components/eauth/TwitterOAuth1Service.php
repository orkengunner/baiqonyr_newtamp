<?php
namespace app\components\eauth;


use nodge\eauth\ErrorException;

class TwitterOAuth1Service extends \nodge\eauth\services\TwitterOAuth1Service
{
    protected function fetchAttributes()
    {
        $info = $this->makeSignedRequest('account/verify_credentials.json');

        $this->attributes['socUserID'] = $info['id'].'';
        $this->attributes['username'] = 'id'.$info['id'];
        $this->attributes['url'] = 'http://twitter.com/' . $info['screen_name'];
        $this->attributes['firstName'] = $info['name'];
        $this->attributes['twProfile'] = $info['screen_name'];
        $this->attributes['photo'] = $info['profile_image_url'];
		$this->attributes['service'] = 'twitter';

        //$this->attributes['language'] = $info['lang'];
        //$this->attributes['timezone'] = timezone_name_from_abbr('', $info['utc_offset'], date('I'));

        return true;
    }

    public function authenticate()
    {
        if (isset($_GET['denied'])) {
            $this->cancel();
        }
        try {
            $proxy = $this->getProxy();

            if (!empty($_GET['oauth_token'])) {
                $token = $proxy->retrieveAccessToken();

                // This was a callback request, get the token now
                $proxy->requestAccessToken($_GET['oauth_token'], $_GET['oauth_verifier'], $token->getRequestTokenSecret());

                $this->authenticated = true;
            } else if ($proxy->hasValidAccessToken()) {
                $this->authenticated = true;
            } else {
                // extra request needed for oauth1 to request a request token :-)
                $token = $proxy->requestRequestToken();
                /** @var $url Uri */
                $url = $proxy->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));
                \Yii::$app->getResponse()->redirect($url->getAbsoluteUri(), 301)->send();
            }
        } catch (\OAuthException $e) {
            throw new ErrorException($e->getMessage(), $e->getCode(), 1, $e->getFile(), $e->getLine(), $e);
        }

        return $this->getIsAuthenticated();
    }
}