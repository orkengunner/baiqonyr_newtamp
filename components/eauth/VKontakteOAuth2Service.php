<?php

namespace app\components\eauth;

use nodge\eauth\ErrorException;

class VKontakteOAuth2Service extends \nodge\eauth\services\VKontakteOAuth2Service
{
    protected $providerOptions = array(
        'authorize' => 'https://oauth.vk.com/authorize',
        'access_token' => 'https://api.vk.com/oauth/access_token',
    );
    protected function fetchAttributes()
    {
        $tokenData = $this->getAccessTokenData();
		$info = $this->makeSignedRequest('users.get.json', array(
			'query' => array(
				'uids' => $tokenData['params']['user_id'],
				//'fields' => '', // uid, first_name and last_name is always available
				'fields' => 'nickname, sex, bdate, city, country, timezone, photo, photo_medium, photo_big, photo_rec',
				'v' => self::API_VERSION
			),
		));

        $info = $info['response'][0];
        $this->attributes['socUserID'] = $info['id'].'';
        $this->attributes['username'] = 'id'.$info['id'];
        $this->attributes['url'] = 'http://vk.com/id' . $info['id'];
        $this->attributes['firstName'] = $info['first_name'];
        $this->attributes['lastName'] = $info['last_name'];
        if (isset($info['bdate']))
            $this->attributes['birthDay'] = date('Y-m-d', strtotime($info['bdate']));

        $this->attributes['vkProfile'] = 'id'.$info['id'];

        //2 - Муж
        $this->attributes['sex'] = $info['sex'] == 2 ? 1 : 0;
        $this->attributes['photo'] = $info['photo'];
		$this->attributes['service'] = 'vkontakte';

        /*$this->attributes['photo_medium'] = $info['photo_medium'];
        $this->attributes['country'] = $info['country'];
        $this->attributes['timezone'] = timezone_name_from_abbr('', $info['timezone']*3600, date('I'));;
        $this->attributes['photo_big'] = $info['photo_big'];
        $this->attributes['photo_rec'] = $info['photo_rec'];*/


        return true;
    }

    public function authenticate()
    {
        if (!$this->checkError()) {
            return false;
        }

        try {
            $proxy = $this->getProxy();

            if (!empty($_GET['code'])) {
                // This was a callback request from a service, get the token
                $proxy->requestAccessToken($_GET['code']);
                $this->authenticated = true;
            } else if ($proxy->hasValidAccessToken()) {
                $this->authenticated = true;
            } else {
                /** @var $url Uri */
                $url = $proxy->getAuthorizationUri();
                \Yii::$app->getResponse()->redirect($url->getAbsoluteUri(), 301)->send();
            }
        } catch (\OAuthException $e) {
            throw new ErrorException($e->getMessage(), $e->getCode(), 1, $e->getFile(), $e->getLine(), $e);
        }

        return $this->getIsAuthenticated();
    }

}