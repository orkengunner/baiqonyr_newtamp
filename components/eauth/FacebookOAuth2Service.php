<?php

namespace app\components\eauth;


use app\modules\logs\models\Logs;
use nodge\eauth\ErrorException;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

class FacebookOAuth2Service extends \nodge\eauth\services\FacebookOAuth2Service
{
    //const SCOPE_USER_PHOTOS = 'user_photos';
    //const SCOPE_READ_STREAM = 'read_stream';
   // const SCOPE_MANAGE_PAGE = 'manage_pages';
    //const SCOPE_PUBLISH_PAGE = 'publish_pages';
    //const SCOPE_PUBLISH_ACTIONS = 'publish_actions';
    //const SCOPE_READ_INSIGHTS = 'read_insights';
    //const SCOPE_USER_GROUPS = 'user_groups';
    //const SCOPE_USER_MANGED_GROUPS = 'user_managed_groups';
    //const SCOPE_USER_FRIENDS = 'user_friends';
    //const SCOPE_USER_ABOUT_ME = 'user_about_me';

    public $count = 70;
    /*protected $scopes = array(
        self::SCOPE_PUBLISH_ACTIONS,
        self::SCOPE_MANAGE_PAGE,
        self::SCOPE_PUBLISH_PAGE,
    );*/
    public $pagination = null;
    protected $baseApiUrl = 'https://graph.facebook.com/v2.6/';
    protected function fetchAttributes()
    {
        /*$tokenData = $this->makeRequest('', [
        'query' => [
            'client_id' => '429519783896575',
            'client_secret' => '37aec4d512b018636b6f16c6214f17a0',
            'scope' => 'manage_pages,read_stream',
            'response_type' => 'token',
        ]
    ]);
        print_r($tokenData);exit;*/
/*$tokenData = $this->makeRequest('oauth/access_token', [
        'query' => [
            'client_id' => '429519783896575',
            'client_secret' => '37aec4d512b018636b6f16c6214f17a0',
            'grant_type' => 'fb_exchange_token',
            'fb_exchange_token' => '429519783896575|uhMgJeJ6jLDwQMOkewAXgOnCAlo',
        ]
    ]);
*/




//print_r($this->getAccessTokenData());exit;

        //date_default_timezone_set('UTC');


        $info = $this->makeSignedRequest('me', [
            'query' => [
                'fields' => 'first_name,last_name,link'
            ]
        ]);
//print_r($info);exit;
        $this->attributes['soc_id'] = $info['id'].'';
        $this->attributes['username'] = 'id'.$info['id'];
        $this->attributes['url'] = $info['link'];
        if (isset($info['birthday']) && $info['birthday'])
            $this->attributes['birth_day'] = date('Y-m-d H:i:s', strtotime($info['birthday']));
        $this->attributes['first_name'] = $info['first_name'];
        $this->attributes['last_name'] = $info['last_name'];
        $this->attributes['profile'] = $info['id'];
        $this->attributes['photo'] = 'https://graph.facebook.com/'.$info['id'].'/picture?type=large';
        $this->attributes['network'] = 'facebook';
        $tokenData = $this->getAccessTokenData();
        $this->attributes['access_token'] = $tokenData['access_token'];
        $this->attributes['token_expires'] = $tokenData['expires'];


        //print_r($info);exit;
        $this->attributes['socUserID'] = $info['id'].'';
        $this->attributes['username'] = 'id'.$info['id'];
        $this->attributes['url'] = $info['link'];
        if (isset($info['birthday'])) $this->attributes['birthDay'] = date('Y-m-d H:i:s', strtotime($info['birthday']));
        $this->attributes['firstName'] = $info['first_name'];
        $this->attributes['lastName'] = $info['last_name'];
        $this->attributes['fbProfile'] = $info['id'];
        $this->attributes['photo'] = 'https://graph.facebook.com/'.$info['id'].'/picture?type=large';
        $this->attributes['service'] = 'facebook';

        /*$tokenData = $this->makeSignedRequest('oauth/access_token', [
            'query' => [
                'client_id' => '429519783896575',
                'client_secret' => '37aec4d512b018636b6f16c6214f17a0',
                'grant_type' => 'fb_exchange_token',
                'fb_exchange_token' => $tokenData['access_token'],
            ]
        ]);

        $this->attributes['access_token'] = $tokenData['access_token'];
        $this->attributes['token_expires'] = $tokenData['expires'];
        print_r($tokenData);exit;*/

        return true;
    }

    public function getPages($acc_id, $access_token)
    {/*$tokenData = $this->makeRequest('oauth/access_token', [
        'query' => [
            'client_id' => '429519783896575',
            'client_secret' => '37aec4d512b018636b6f16c6214f17a0',
            'grant_type' => 'fb_exchange_token',
            'fb_exchange_token' => 'CAAGGpV4kpf8BAGEJygQwhazoHPiV6jwvGqeBxZCZCgi83reaauJMgF8XZCCLGYzf38AyFJFKGmeBJknHudJDgBoy1PJtELjNW1ScbPEIihu3pdE72e38XhPWaF4AV22YfhWmGLisNyuEM57kxxQvVOTZCdE2jzuLBIj4ZCewdLNRBdsW3ZBXEIPDJEU6BIpPIdMOPS9VFuOJIHURSG3MV7',
        ]
    ]);
        print_r($tokenData);exit;*/
        try{
            $data = $this->makeRequest($acc_id . '/accounts', [
                    'query' => [
                        'access_token' => $access_token,
                        'fields' => 'about,cover,name,access_token'
                    ]
                ]
            );
        } catch(\Exception $e) {
            return [];
        }

        if (isset($data['data']) && is_array($data['data']))
            return $data['data'];
        return false;
    }

    public function getGroups ($acc_id, $access_token)
    {
        try{
            $data = $this->makeRequest($acc_id . '/groups', [
                    'query' => [
                        'access_token' => $access_token
                    ]
                ]
            );
        } catch(\Exception $e) {
            return [];
        }
        if (isset($data['data']) && is_array($data['data']))
            return $data['data'];
        return false;
    }

    protected function parseResponseInternal($response)
    {
        if (preg_match('/^access_token=(.*)expires=(.*)$/', $response, $token)) {
            return [
                'access_token' => $token[1],
                'expires' => $token[2]
            ];
        }
        //print_r($response);exit;
        try {
            $result = $this->parseResponse($response);
            if (!isset($result)) {
                throw new ErrorException(\Yii::t('eauth', 'Invalid response format.'), 500);
            }

            $error = $this->fetchResponseError($result);
            if (isset($error) && !empty($error['message'])) {
                throw new ErrorException($error['message'], $error['code']);
            }

            return $result;
        } catch (\Exception $e) {
            throw new ErrorException($e->getMessage(), $e->getCode());
        }
    }

    public $pageFeed = [];
    public function getPageFeed($page_id, $access_token, $from = null, $to = null)
    {
        if($this->pagination){
            $data = json_decode($this->uploadFile($this->pagination), true);
        } else
        $data = $this->makeRequest($page_id . '/posts', [
                'query' => [
                    'access_token' => $access_token,
                    'fields' => 'object_id,likes.summary(true),comments.summary(true),shares,message,created_time,photos,attachments',
                    'limit' => $this->count,
                    //'offset' => $offset * $this->count,
                    'since' => $from,
                    'until' => $to,
                ],
            ]
        );
        //print_r($data);exit;
//print_r($this->baseApiUrl . 'fql/?access_token=' . $access_token . '&q=SELECT created_time, comment_info, like_info, share_count,message, actor_id, attachment  FROM stream WHERE source_id = 468553803256132 AND created_time = 1441965780');exit;
        //$data = $this->uploadFile($this->baseApiUrl . 'fql/?access_token=' . $access_token . '&q=SELECT created_time, comment_info, like_info, share_count,message, actor_id, attachment  FROM stream WHERE source_id = 468553803256132 AND created_time = 1441965780');
        //print_r($data);exit;
        if (isset($data['data']) && is_array($data['data'])) {
            if(isset($data['paging']['next'])) $this->pagination = $data['paging']['next'];
            else $this->pagination = null;
            return $data['data'];
        }
        return false;
    }

    public function getPosts($page_id, $access_token, $countPage = 0, $from = null, $to = null)
    {
        if(!isset($this->pageFeed[$countPage])) {
            $this->pageFeed[$countPage] = $this->getPageFeed($page_id, $access_token, $countPage, $from, $to);
        }
        return $this->pageFeed[$countPage];
    }

    public $pagesInfo = [];

    public function getPageInfo($page_id, $access_token)
    {
        $data = $this->makeRequest($page_id, [
                'query' => [
                    'access_token' => $access_token,
                    'fields' => 'likes,cover,name,username,id'
                ],
            ]
        );
        if (is_array($data))
            return $data;
        return false;
    }

    public function getCountMembers($page_id, $access_token)
    {
        if(!$this->pagesInfo) {
            $this->pagesInfo = $this->getPageInfo($page_id, $access_token);
        }
        return isset($this->pagesInfo['likes']) ? $this->pagesInfo['likes'] : 0;
    }

    public function getGroupName($page_id, $access_token)
    {
        if(!$this->pagesInfo) {
            $this->pagesInfo = $this->getPageInfo($page_id, $access_token);
        }
        return $this->pagesInfo['name'];
    }

    public function getGroupIdByName($page_id, $access_token)
    {
        if(!$this->pagesInfo) {
            $this->pagesInfo = $this->getPageInfo($page_id, $access_token);
        }
        return $this->pagesInfo['id'];
    }

    public function postMessage ($page_id, $access_token, $data, $post_id, $user_id = null, $company_id = null)
    {

        # $data = [
        #     'picture' => 'http://....',
        #     'type' => 'photo',
        #     'description' => 'image description',
        #     'name' => 'imate title',
        #     'link' => 'image url',
        #     'message' => 'message',
        # ];

        //$data['access_token'] = $access_token;
        //$data['action'] = 'post';
        //print_r($data);exit;
        try {
            $data = $this->makeRequest($page_id . '/feed', [
                    'query' => [
                        'access_token' => $access_token,
                    ],
                    'data' => $data
                ]
            );
        } catch (\Exception $e) {
            //print_r($e);
            $_data = [];
            if ($user_id || $company_id){
                $_data['user_id'] = $user_id;
                $_data['company_id'] = $company_id;
            }
            Logs::log(9, ArrayHelper::merge([
                'post_id' => $post_id,
                'data' => $e
            ], $_data));
        }

        if (isset($data['id']))
            return $data['id'];
        return false;
    }

    public function editPost ($post_id, $access_token, $data)
    {

        # $data = [
        #     'picture' => 'http://....',
        #     'type' => 'photo',
        #     'description' => 'image description',
        #     'name' => 'imate title',
        #     'link' => 'image url',
        #     'message' => 'message',
        # ];

        //$data['access_token'] = $access_token;
        //$data['action'] = 'post';
        //print_r($data);exit;
        $data = $this->makeRequest($post_id, [
                'query' => [
                    'access_token' => $access_token,
                ],
                'data' => $data
            ]
        );


        return $post_id;
    }

    public function parseAccessTokenResponse($response)
    {

        // Facebook gives us a query string or json
        if ($response[0] === '{') {
            $data = json_decode($response, true);
        }
        else {
            parse_str($response, $data);
        }
        if (!isset($data['expires'])) {
            $data['expires'] = time() + 3600*24*365;
        }

        return $data;
    }


    public function postToPage($page_id, $access_token, $data)
    {

        if(isset($data['url'])) {
            $data['url'] = 'http://' . $_SERVER['HTTP_HOST'] . $data['url'];
            $data['caption'] = $data['message'];
            unset($data['message']);
            try {
                $data = $this->makeRequest($page_id . '/photos', [
                        'query' => [
                            'access_token' => $access_token,
                        ],
                        'data' => $data
                    ]
                );
            } catch (\Exception $e) {
                $this->setLog($e, 'postPhoto');
            }
            if (isset($data['post_id']))
                return $data['post_id'];
            else if (isset($data['id']))
                return $page_id . '_' . $data['id'];
        } else {
            try {
                $data = $this->makeRequest($page_id . '/feed', [
                        'query' => [
                            'access_token' => $access_token,
                        ],
                        'data' => $data
                    ]
                );
            } catch (\Exception $e) {
                $this->setLog($e, 'postMessage');
            }
            if (isset($data['id']))
                return $data['id'];
        }


        return false;
    }

    public function postPhotos ($page_id, $access_token, $data, $post_id)
    {
        $files = [];
        foreach($data['files'] as $k => $v) {
            $files['file_' . $k] = '@' . $v;
        }
        $data = ArrayHelper::merge(['access_token' => $access_token], [
            'batch' => json_encode([
                [
                    "method" 			=> "POST",
                    "relative_url" 		=> $page_id . "/photos",
                    "body" 			=> "message=" . $data['message'],
                    "attached_files"  	=> 'file_'.array_keys($data['files'])[0]
                ],
                [
                    "method" 			=> "POST",
                    "relative_url" 		=> $page_id . "/photos",
                    "body" 			=> "message=" . $data['message'],
                    "attached_files"  	=> 'file_'.array_keys($data['files'])[1]
                ]
            ])
        ], $files);
//print_r($data);exit;
        try {
            $this->uploadFile($this->baseApiUrl, $data);
            /*$data = $this->makeRequest($page_id . '/photos', [
                    'query' => [
                        'access_token' => $access_token,
                    ],
                    'data' => $data
                ]
            );*/
        } catch (\Exception $e) {
print_r($e);exit;
            Logs::log(9, [
                'post_id' => $post_id,
                'data' => $e
            ]);
        }

        if (isset($data['post_id']))
            return $data['post_id'];
        else if (isset($data['id']))
            return $page_id . '_' . $data['id'];
        return false;
    }

    private function uploadFile( $url, $post = null )
    {
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        if($post) {
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $post );
        }
        $response = curl_exec( $ch );
        curl_close( $ch );
        //print_r(($response));
        return $response;
    }

    public function getUrlsStats($urls)
    {
        $stats = $this->uploadFile('https://api.facebook.com/restserver.php?' .
            'method=links.getStats&' .
            'urls=' . $urls . '&' .
            'format=json'
        );
    }

    public function postVideo ($page_id, $access_token, $data)
    {
        try {
            $data = $this->makeRequest($page_id . '/videos', [
                    'query' => [
                        'access_token' => $access_token,
                    ],
                    'data' => $data
                ]
            );
        } catch (\Exception $e) {

            print_r($e);
        }
        print_r($data);
exit;
        if (isset($data['post_id']))
            return $data['post_id'];
        else if (isset($data['id']))
            return $page_id . '_' . $data['id'];
        return false;
    }

    public function getAlbums ($page_id, $access_token, $post_id)
    {
        try {
            $data = $this->makeRequest($page_id . '/albums', [
                    'query' => [
                        'access_token' => $access_token,
                    ],
                ]
            );
        } catch (\Exception $e) {

            Logs::log(9, [
                'post_id' => $post_id,
                'data' => $e
            ]);
        }

        if (isset($data['data']))
            return $data['data'];
        return false;
    }

    public function createAlbum ($page_id, $access_token, $data, $post_id)
    {
        try {
            $data = $this->makeRequest($page_id . '/albums', [
                    'query' => [
                        'access_token' => $access_token,
                    ],
                    'data' => $data
                ]
            );
        } catch (\Exception $e) {
            Logs::log(9, [
                'post_id' => $post_id,
                'data' => $e
            ]);
        }

        if (isset($data['id']))
            return $data['id'];
        return false;
    }

    public function parseFacebookPage($url, $is_fan_page = true)
    {
        $response = $this->uploadFile($url);
        if($is_fan_page) {
            //if(strpos($response, 'FAN_PAGE') !== false) {
                preg_match('/<span itemprop="name">(.*?)<\/span>/', $response, $name);
                preg_match('/<h2 class="_58gi"><span>(.*?)<\/span>/', $response, $name2);
                preg_match('/<img class="profilePic img".*?src="(.*?)"/', $response, $img);
                preg_match('/https:\/\/www.facebook.com\/r.php\?fbpage_id=(.*?)\&amp\;r/', $response, $id);
                preg_match('/favorite_status.php?fbpage_id=(.*?)&/', $response, $id2);

        if(!isset($id[1]) && !isset($id2[1])) return [
                'error' => 1
            ];
        }

        return [
            'title' => isset($name[1]) ? $name[1] : (isset($name2) ? $name2 : null),
            'img' => isset($img[1]) ? trim(urldecode($img[1])) : null,
            'id' => isset($id[1]) ? $id[1] : (isset($id2) ? $id2 : null)
        ];
    }

    public function checkToken($token, $isBool = true)
    {
        $response = $this->uploadFile($this->baseApiUrl .
            'me/'.
            '?access_token=' . $token
        );
        if(isset(json_decode($response, true)['id'])) {
            return $isBool ? true : $token;
        }
        return false;
    }

    public function setLog($log, $action = null)
    {
        \Yii::info(\yii\helpers\Json::encode($log), 'facebook' . ($action ? '-' . $action  : ''));
    }
}
