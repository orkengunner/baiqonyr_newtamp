<?php
/**
 * Created by PhpStorm.
 * User: Vital
 * Date: 20.02.2015
 * Time: 12:38
 */

namespace app\components;

use Imagine\Image\Box;
use Imagine\Image\Color;
use Imagine\Image\Point;
use yii\base\InvalidParamException;
use yii\imagine\BaseImage;

class wImage extends BaseImage
{
    protected $drivers = [
        self::DRIVER_GD2,
        self::DRIVER_IMAGICK
    ];

    protected $image;

    protected $path;

    public function __construct($path)
    {
        $this->path = $path;
        self::$driver = $this->drivers;
    }

    public function createThumbnail($width, $height, $dist = null)
    {
        if (!$width || !$height) $img = $this->resizeImg($width, $height);
        else $img = self::thumbnail($this->path, $width, $height);
        if ($dist) {
            return $img->save($dist);
        } else {
            $path = pathinfo($this->path);
            return $img->save(
                $path['dirname']
                    . '/' . $path['filename']
                    . '-'
                    . ($width ? $width : '')
                    . 'x'
                    . ($height ? $height : '')
                    . '.'
                    . $path['extension']
            );
        }

    }

    public function showThumbnail($width, $height, $zoom = false, $print = false)
    {
        $ext = strtolower(pathinfo($this->path, PATHINFO_EXTENSION));
        if (!$width || !$height) $img = $this->resizeImg($width, $height, $zoom);
        else $img = self::thumbnail($this->path, $width, $height);
        return 'data:image/' . $ext . ';base64,' . base64_encode($img->{$print ? 'show' : 'get'}($ext));
    }

    public function resizeImg($width, $height, $zoom = false)
    {
        $img = $this->getImageObject($this->path);
        $w = $img->getSize()->getWidth();
        $h = $img->getSize()->getHeight();
        if (!$height) {
            if (!$zoom && $width >= $w) return $img;
            $box = new Box($width, $width * $h/$w);
        } elseif (!$width) {
            if (!$zoom && $height >= $h) return $img;
            $box = new Box($height * $w/$h, $height);
        } else {
            $box = new Box($width, $height);
        }
        return $img->resize($box);
    }

    public function combineThumbnail($combinePath, $dist = null, $start = [0, 0],  $action = 'save')
    {
        // print_r($dist);exit;

        $ext = strtolower(pathinfo($this->path, PATHINFO_EXTENSION));
        if ($action == 'save') {
            $img = $this->watermark($this->path, $combinePath, $start)->save($dist ? $dist : $this->path);
        } else {
            $img = 'data:image/' . $ext . ';base64,' . base64_encode($this->watermark($this->path, $combinePath, $start)->{$action}($ext));
            //print_r($img);exit;
        }
        return $img;
    }

    public function getImageObject($path = null)
    {
        return static::getImagine()->open(\Yii::getAlias($path ? $path : $this->path));
    }

    public function drawRectangle($side = [], $color = '000' , $path = null)
    {
        $a = new Point($side['a'][0], $side['a'][1]);
        $b = new Point($side['b'][0], $side['b'][1]);
        $c = new Point($side['c'][0], $side['c'][1]);
        $d = new Point($side['d'][0], $side['d'][1]);
        $img = $this->getImageObject($path);

        $img->draw()->polygon([
            $a, $d, $b, $c,
            $a, $b, $d, $c
        ],
            new Color($color, 0),
            true
        );
        return $img;
    }

    public function setWatermark($filename, $watermarkFilename, array $start = [0, 0])
    {
        if (!isset($start[0], $start[1])) {
            throw new InvalidParamException('$start must be an array of two elements.');
        }

        $img = $this->getImageObject(\Yii::getAlias($filename));
        if(is_string($watermarkFilename)) {
            $watermark = $this->getImageObject(\Yii::getAlias($watermarkFilename));
        } else {
            $watermark = $watermarkFilename;
        }
        $img->paste($watermark, new Point($start[0], $start[1]));

        return $img;
    }

}