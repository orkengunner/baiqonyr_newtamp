<?php
namespace app\components;

use app\modules\bugreport\models\BugReport;
use app\modules\config\models\Config;
use app\modules\users\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

class MainController extends \yii\web\Controller
{
    public $_user = null;
    public function afterAction($action, $result)
    {
        //BugReport::setError();
        return parent::afterAction($action, $result);
    }

    public function init()
    {
        //\Yii::$app->db->createCommand('SET GLOBAL time_zone = "+6:00"')->query();
        parent::init();

        if (!\Yii::$app->user->isGuest) {
            $this->_user = \Yii::$app->user->getIdentity()->userAttributes();
        } else {
            $this->_user = new Users();
        }
    }

    public function actionAjaxValidate()
    {
        $post = array_keys(\Yii::$app->request->post());

        //$class = '\app\modules\\' . strtolower($post[0]) . '\models\\' . $post[0];
        $class = $this->getModelName();
        if (isset(\Yii::$app->request->post($post[0])['id']) && \Yii::$app->request->post($post[0])['id'])
            $model = $class::findOne(\Yii::$app->request->post($post[0])['id']);
        else
            $model = new $class();
        foreach ($model->getValidators() as $v) {
            if ($v->except == 'ajax-validate' || $v->on == 'ajax-validate') {
                $model->scenario = 'ajax-validate';
                break;
            }
        }

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }
    }

    public function getModelName()
    {
        if ($this->_modelName === null) {
            $this->_modelName = ucfirst($this->id);
        }

        return $this->_modelName;
    }

    public function setMeta($title, $description = null, $keywords = null, $image = null, $url = null)
    {
        if (\Yii::$app->request->isAjax) {
            return;
        }

        $this->getView()->title = $title . ' - ' . Config::getParamValue('sitename');

        if ($url !== false && empty($url)) {
            $url = Url::canonical();
        }

        if ($url) {
            $this->getView()->registerLinkTag([
                'href' => $url,
                'rel' => 'canonical'
            ], 'canonical');
        }

        $properties = [
            'og:title' => $this->getView()->title,
            'og:description' => $description,
            'og:image' => $image,
            'og:url' => $url,
        ];

        if (empty($description)) {
            $description = Config::getParamValue('meta_description', '');
        }

        if (empty($keywords)) {
            $keywords = Config::getParamValue('meta_keywords', '');
        }

        $seoMeta = ['description' => $description, 'keywords' => $keywords];

        foreach ($seoMeta as $seoName => $seoValue) {
            if (!$seoValue) {
                continue;
            }

            $this->getView()->registerMetaTag([
                'name' => $seoName,
                'content' => $seoValue
            ]);
        }

        foreach ($properties as $prop => $value) {
            if (!$value) {
                continue;
            }
            $this->getView()->registerMetaTag([
                'property' => $prop,
                'content' => $value
            ]);
        }
    }
}
