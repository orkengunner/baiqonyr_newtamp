<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 7/16/14
 * Time: 11:34
 */

namespace app\components;

use app\assets\AppAsset;
use app\components\actions\RemoveFile;
use app\components\actions\UploadAsync;
use app\modules\festivals\models\Festivals;
use app\modules\jobs\models\RequestForm;
use app\modules\languages\models\Languages;
use app\modules\lookupusersroles\models\LookupUsersRoles;
use app\modules\posts\models\Posts;
use yii\base\View;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class FrontController extends MainController
{
    protected $meta;
    public $layout = '//front/main';
    public $cssClass = '';
    public $routeUrl;
    public $assets = [];
    public $banners = [];
    public $festID;
    public $isAbout = false;
    public $isMain = false;
    public $isMobile = false;
    public $isHidden = false;
    public $fest = null;
    public $langPostfix = null;

    public $popularPosts = [];

    protected $allowedRoles = [];

    public function init()
    {
        parent::init();

        $this->isMobile = \Yii::$app->devicedetect->isMobile();
        $this->fest = \Yii::$app->getCache()->get('bkr_global_active_fest');
        if ($this->fest === false) {
            $this->fest = Festivals::findOne(['status_id' => Festivals::STATUS_ACTIVE]);
            \Yii::$app->getCache()->set('bkr_global_active_fest', $this->fest, 3600*24*7);
        }

        if(Languages::$current->code != 'ru') {
            $this->langPostfix =  '_' . Languages::$current->code;
        }


        if($this->fest) $this->festID = $this->fest->id;

        if (\Yii::$app->user->isGuest) {

            if(!\Yii::$app->request->getIsAjax() && strpos($_SERVER['REQUEST_URI'], 'login') === false && strpos($_SERVER['REQUEST_URI'], 'styles') === false && strpos($_SERVER['REQUEST_URI'], '=reg') === false && strpos($_SERVER['REQUEST_URI'], 'images') === false && strpos($_SERVER['REQUEST_URI'], 'get-terms') === false && strpos($_SERVER['REQUEST_URI'], 'pay.php') === false)
            {
                $url = Url::parse_url($_SERVER['REQUEST_URI']);

                \Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => 'current_url',
                    'value' => str_replace('//', '/', Url::to(ArrayHelper::merge([$url['path']], $url['params'], ['action' => 'phone']))),
                    'expire' => time()+3600,
                    'path' => '/'
                ]));
            }
        }

        $this->getView()->on(View::EVENT_BEFORE_RENDER, function () {
            AppAsset::register($this->getView());
        });
    }

    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }


    public function behaviors()
    {
        return [
            'xssXlean' => [
                'class' => XssFilter::className()
            ],
            [
                'class' => 'yii\filters\HttpCache',
                'lastModified' => function ($action, $params) {
                    return time() + 60;
                },
                'cacheControlHeader' => 'public, max-age=1'
            ],
        ];
    }

    public function setBanner($zone_id, $object_type, $object_id = null)
    {
        $this->banners[$zone_id] = [
            'zone_id' => $zone_id,
            'object_type' => $object_type,
            'object_id' => $object_id
        ];
    }

    protected function allowActions()
    {
        if ($this->_user->role_id == LookupUsersRoles::ROLE_ADMIN) return true;
        $module = \Yii::$app->controller->module->id;
        $action = \Yii::$app->controller->action->id;

        $sadc = [
            LookupUsersRoles::ROLE_SMM_MANAGER,
            LookupUsersRoles::ROLE_ACCOUNT_MANAGER,
            LookupUsersRoles::ROLE_DESIGNER,
            LookupUsersRoles::ROLE_COPYWRITER,
        ];
        $sadcc = [
            LookupUsersRoles::ROLE_SMM_MANAGER,
            LookupUsersRoles::ROLE_ACCOUNT_MANAGER,
            LookupUsersRoles::ROLE_DESIGNER,
            LookupUsersRoles::ROLE_COPYWRITER,
            LookupUsersRoles::ROLE_CLIENT,
            //LookupUsersRoles::ROLE_ADMIN,
        ];
        $sa = [
            LookupUsersRoles::ROLE_SMM_MANAGER,
            LookupUsersRoles::ROLE_ACCOUNT_MANAGER,
            //LookupUsersRoles::ROLE_ADMIN,
        ];
        $actions = [
            'companies' => [
                'update' => [
                    'companies' => $sa,
                    'users' => $sa
                ],
                'save-company' => [
                    'companies' => $sa,
                    'users' => $sa
                ],
                'remove-cmpu' => [
                    'companies' => $sa,
                ],
                'recover-cmpu' => [
                    'companies' => $sa,
                ],
                'cmp-login' => [
                    'companies' => $sa,
                ],
                'sn-unset' => [
                    'companies' => $sa,
                ],
                'set-social-group' => [
                    'companies' => $sa,
                ],
                'set-plan' => [
                    'companies' => $sa,
                ],
                'remove-file' => [
                    'companies' => $sadcc,
                ],
            ],
            'posts' => [
                'index' => [
                    'companies' => $sadcc
                ],
                'create' => [
                    'companies' => $sadc
                ],
                'send-moderation' => [
                    'companies' => $sadc
                ],
                'send-approve-notif' => [
                    'companies' => [LookupUsersRoles::ROLE_CLIENT]
                ],
                'remove' => [
                    'companies' => $sadc
                ],
                'remove-file' => [
                    'users' => $sadc,
                ],
                'set-status' => [
                    'companies' => $sadcc
                ],
                'get-stats' => [
                    'companies' => $sadcc
                ]
            ],
            'comments' => [
                'set-comments' => [
                    'companies' => $sadcc
                ],
                'remove-comments' => [
                    'companies' => $sadcc
                ],
                'recover-comments' => [
                    'companies' => $sadcc
                ],
                'unset-viewed-comments' => [
                    'companies' => $sadcc
                ]
            ],
            'users' => [
                'af-search' => [
                    'companies' => $sa,
                ],
                'send-invite' => [
                    'companies' => $sa,
                ],
                're-send-invite' => [
                    'companies' => $sa,
                ],
                'update' => [
                    'users' => $sadcc,
                ],
                'remove-file' => [
                    'users' => $sadcc,
                ],
                'profile' => [
                    'users' => $sadcc,
                ],
                'set-photo' => [
                    'users' => $sadcc,
                ],
                'sn-unset' => [
                    'users' => $sadcc,
                ]
            ],
            'logs' => [
                'index' => [
                    'companies' => $sadcc,
                ],
            ]
        ];
//print_r($action);
        if(isset($actions[$module][$action]['companies']) && in_array($this->_user->c_role_id, $actions[$module][$action]['companies'])) {
            return true;
        } elseif(isset($actions[$module][$action]['users']) && in_array($this->_user->role_id, $actions[$module][$action]['users'])) {
            return true;
        }


        return false;
    }

    public function actions()
    {
        return [
            'upload-async' => [
                'class' => UploadAsync::className()
            ],
            'remove-file' => [
                'class' => RemoveFile::className()
            ],
        ];
    }

}
