<?php
namespace app\components\actions;


use app\modules\files\models\Files;
use Yii;
use yii\base\Action;
use yii\helpers\FileHelper;


class RemoveFile extends Action
{
    public function run($model_id, $recAttribute, $attribute, $id, $is_temp = null)
    {
        $class = $this->controller->modelName;
        $model = $class::findOne($model_id);
        if(!$model) $model = new $class();
        if (!$is_temp) {
            $model->deleteFile($recAttribute, $id);
        } else {
            $model->removeAsyncTempFile($attribute, $id);
        }
    }
}
