<?php
/**
 * Created by PhpStorm.
 * User: yevgeniy
 * Date: 10/17/14
 * Time: 4:00 PM
 */

namespace app\components\actions;


use yii\base\Action;

class NoEmptyImage extends Action
{

    public function run()
    {
        list($width, $height) = explode('x', $_GET['wh']);
        $fill = '00a2ff';
        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = \Yii::$app->response->headers;
        $headers->add('Content-Type', 'image/svg+xml');
        header('Content-Type: image/svg+xml'); ?>
        <svg viewBox="0 0 <?= $width ?> <?= $height ?>" xmlns="http://www.w3.org/2000/svg">
            <g>
                <rect id="svg_1" height="<?= $height ?>" width="<?= $width ?>" y="0" x="0" fill="#<?= $fill ?>"/>
            </g>
        </svg>
        <?php
    }
}
