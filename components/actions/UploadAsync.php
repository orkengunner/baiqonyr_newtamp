<?php
namespace app\components\actions;

use app\components\ActiveRecord;
use app\components\traits\UploadableAsync;
use yii\base\Action;
use Yii;
use yii\helpers\FileHelper;
use yii\web\HttpException;
use yii\web\Response;

class UploadAsync extends Action
{
    public function run($attribute, $id = 0, $multiple = 0)
    {
        /** @var UploadableAsync|ActiveRecord $model */
        $model = new $this->controller->modelName;
        if ($id)
            $model = $model::findOne($id);
        $model->setScenario('upload');
        $model->load(Yii::$app->getRequest()->post(), false);
        $name = $model->validateAndSaveAsyncFile($attribute, true, !$multiple ? true : false, (bool)$multiple);

        $file = $model->$attribute;
        if (!$name) {
            if (!$model->$attribute) {
                throw new HttpException(400);
            }
            $response['success'] = false;
            //$response['rules'] = $model->rules();
            $response['file'] = [
                'name' => $file->name,
                'size' => $file->size,
                'attriubute' => $attribute,
                'error' => $model->getFirstError($attribute)
            ];
        } else {
            $response['success'] = true;
            if (is_array($model->getAsyncFileFromSession($attribute)))
                $response['index'] = array_search($name, $model->getAsyncFileFromSession($attribute));
            $response['multiple'] = (bool)$multiple;
            $response['object_id'] = $id;
            $path = $model->getTempMediaDirectory() . '/' . $name;
            $size = FileHelper::getImageSize($path, true);
            $response['file'] = [
                'name' => $file->name,
                'size' => $file->size,
                'width' => $size[0],
                'height' => $size[1],
                'url' => $path,
            ];
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }
}
