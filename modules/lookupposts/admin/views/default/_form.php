<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\lookupposts\models\LookupPosts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lookup-posts-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="widget">
        <div class="widget-content padding">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'title_kz')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
    <div class="widget">
        <div class="widget-content padding">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('lookupposts', 'Создать') : Yii::t('lookupposts',
                'Изменить'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                'id' => 'submit-page'
            ]) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
