<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\lookupposts\models\LookupPosts */

$this->title = 'Редактировать должность: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Lookup Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lookup-posts-update">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
