<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\lookupposts\models\LookupPosts */

$this->title = 'Добавить должность';
$this->params['breadcrumbs'][] = ['label' => 'Lookup Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lookup-posts-create">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
