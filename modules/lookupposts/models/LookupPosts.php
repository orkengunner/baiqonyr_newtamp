<?php

namespace app\modules\lookupposts\models;

use app\components\ActiveRecord;
use app\components\LanguageActiveRecord;
use Yii;

/**
 * This is the model class for table "lookup_posts".
 *
 * @property string $id
 * @property string $title
 * @property integer $lang_id
 *
 * @property JobsParticipants[] $jobsParticipants
 * @property Languages $lang
 * @property Participants[] $participants
 */
class LookupPosts extends ActiveRecord
{
    const POST_PRODUCER = 2; // Режиссер
    const POST_OPERATOR = 1;
    const POST_SCENARIST = 3;
    const POST_AUTHOR = 4;
    const POST_ACTOR = 5;
    const POST_PRODUCER_SECOND = 6; // Продюсер
    const POST_DESIGN_DIRECTOR = 7;
    const POST_PRODUCNTION_DIRECTOR = 8;
    const POST_WORK_SHOP = 9;
    const POST_SOUND_ENGINEER = 10;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'lang_id'], 'required'],
            [['lang_id'], 'integer'],
            [['title', 'title_kz'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'lang_id' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsParticipants()
    {
        return $this->hasMany(JobsParticipants::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants()
    {
        return $this->hasMany(Participants::className(), ['post_id' => 'id']);
    }
}
