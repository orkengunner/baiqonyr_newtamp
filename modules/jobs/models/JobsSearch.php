<?php

namespace app\modules\jobs\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\jobs\models\Jobs;

/**
 * JobsSearch represents the model behind the search form about `app\modules\jobs\models\Jobs`.
 */
class JobsSearch extends Jobs
{
    public $isRequest = false;
    public $outCompetition = false;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'festival_id', 'user_id', 'views', 'delete_by', 'lang_id', 'out_competition'], 'integer'],
            [['title', 'short_text', 'text', 'publish_date', 'slug', 'meta_title', 'meta_keywords', 'meta_description', 'create_date', 'update_date', 'delete_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = Model::scenarios();
        $scenarios['search'] = ['title', 'status_id', 'out_competition'];
        return $scenarios;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Jobs::find()->andWhere(['register_id' => null]);
//print_r($query->asArray()->all());exit;
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'festival_id' => $this->festival_id,
            'user_id' => $this->user_id,
            'publish_date' => $this->publish_date,
            'views' => $this->views,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_date' => $this->delete_date,
            'delete_by' => $this->delete_by,
            'status_id' => $this->status_id,
            'out_competition' => $this->out_competition,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'short_text', $this->short_text])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description]);

        if($this->isRequest) {
            $query->andWhere(['is_participant' => 0]);
        } else{
            $query->andWhere(['is_participant' => ($this->outCompetition ? $this->outCompetition : Jobs::IS_PARTICIPANT)]);
        }

        return $dataProvider;
    }
}
