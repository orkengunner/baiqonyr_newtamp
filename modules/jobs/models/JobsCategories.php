<?php

namespace app\modules\jobs\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "jobs_categories".
 *
 * @property string $id
 * @property string $title
 * @property string $title_kz
 *
 * @property Jobs[] $jobs
 * @property Jobs[] $jobs0
 */
class JobsCategories extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'title_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'title_kz' => 'Title Kz',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Jobs::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs0()
    {
        return $this->hasMany(Jobs::className(), ['category_id' => 'id']);
    }
}
