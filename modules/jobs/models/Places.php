<?php

namespace app\modules\jobs\models;

use app\components\LanguageActiveRecord;
use Yii;

/**
 * This is the model class for table "places".
 *
 * @property string $placeID
 * @property string $title
 * @property string $phones
 * @property string $address
 * @property string $addressMap
 * @property string $previewText
 * @property string $text
 * @property string $workTime
 * @property string $site
 * @property integer $statusID
 * @property string $url
 * @property string $titleLink
 * @property string $metaTitle
 * @property string $metaKeywords
 * @property string $metaDescription
 * @property string $created
 * @property string $pTypeID
 * @property string $services
 * @property integer $onHome
 * @property integer $langID
 * @property integer $listView
 * @property string $title_kz
 * @property string $previewText_kz
 * @property string $text_kz
 * @property string $titleLink_kz
 * @property string $metaTitle_kz
 * @property string $metaKeywords_kz
 * @property string $metaDescription_kz
 * @property string $services_kz
 * @property string $workTime_kz
 * @property integer $inKz
 */
class Places extends LanguageActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'places';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'addressMap', 'previewText', 'text', 'workTime', 'site', 'statusID', 'url', 'metaDescription', 'created'], 'required'],
            [['previewText', 'text', 'workTime', 'metaKeywords', 'metaDescription', 'previewText_kz', 'text_kz', 'metaDescription_kz', 'workTime_kz'], 'string'],
            [['statusID', 'pTypeID', 'onHome', 'langID', 'listView', 'inKz'], 'integer'],
            [['created'], 'safe'],
            [['title', 'phones', 'address', 'addressMap', 'site', 'url', 'titleLink', 'metaTitle', 'services', 'title_kz', 'titleLink_kz', 'metaTitle_kz', 'metaKeywords_kz', 'services_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'placeID' => 'Place ID',
            'title' => 'Title',
            'phones' => 'Phones',
            'address' => 'Address',
            'addressMap' => 'Address Map',
            'previewText' => 'Preview Text',
            'text' => 'Text',
            'workTime' => 'Work Time',
            'site' => 'Site',
            'statusID' => 'Status ID',
            'url' => 'Url',
            'titleLink' => 'Title Link',
            'metaTitle' => 'Meta Title',
            'metaKeywords' => 'Meta Keywords',
            'metaDescription' => 'Meta Description',
            'created' => 'Created',
            'pTypeID' => 'P Type ID',
            'services' => 'Services',
            'onHome' => 'On Home',
            'langID' => 'Lang ID',
            'listView' => 'List View',
            'title_kz' => 'Title Kz',
            'previewText_kz' => 'Preview Text Kz',
            'text_kz' => 'Text Kz',
            'titleLink_kz' => 'Title Link Kz',
            'metaTitle_kz' => 'Meta Title Kz',
            'metaKeywords_kz' => 'Meta Keywords Kz',
            'metaDescription_kz' => 'Meta Description Kz',
            'services_kz' => 'Services Kz',
            'workTime_kz' => 'Work Time Kz',
            'inKz' => 'In Kz',
        ];
    }
}
