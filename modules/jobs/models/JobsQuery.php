<?php
namespace app\modules\jobs\models;

use app\components\ActiveQuery;

class JobsQuery extends ActiveQuery
{
    public function withGenres()
    {
        return $this
            ->innerJoin('jobs_genries AS gc', 'gc.job_id = jobs.id');
    }
}
