<?php

namespace app\modules\jobs\models;


use app\components\ActiveRecord;
use yii\db\ActiveQuery;
use yii\db\Expression;

class OnlineQuery extends ActiveQuery
{
    public function isActive()
    {
        $this->andWhere(['online.statusID' => ActiveRecord::STATUS_ACTIVE]);

        return $this;
    }

}
