<?php

namespace app\modules\jobs\models;

use Yii;

/**
 * This is the model class for table "jobs_genries".
 *
 * @property string $job_id
 * @property string $genre_id
 *
 * @property Jobs $job
 */
class JobsGenries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs_genries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'genre_id'], 'required'],
            [['job_id', 'genre_id'], 'integer'],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jobs::className(), 'targetAttribute' => ['job_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'genre_id' => 'Genre ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Jobs::className(), ['id' => 'job_id']);
    }

    public function getOldJob()
    {
        $job = new Jobs();
        $job::$oldFest = true;
        return $this->hasOne($job::className(), ['id' => 'job_id']);
    }

    public static function findGenresIDs($typeJobs, $rel = 'job', $fest_id = null)
    {
        return self::find()
            ->innerJoinWith($rel)
            ->addSelect(['genre_id'])
            ->andFilterWhere([
                'jobs.is_participant' => $typeJobs,
                'jobs.festival_id' => $fest_id
            ])
            ->column();
    }
}
