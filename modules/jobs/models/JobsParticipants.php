<?php

namespace app\modules\jobs\models;

use app\modules\lookupposts\models\LookupPosts;
use app\modules\participants\models\Participants;
use Yii;

/**
 * This is the model class for table "jobs_participants".
 *
 * @property string $job_id
 * @property string $participant_id
 * @property string $post_id
 * @property string $position
 *
 * @property LookupPosts $post
 * @property Jobs $job
 * @property Participants $participant
 */
class JobsParticipants extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs_participants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'participant_id', 'post_id'], 'required'],
            [['job_id', 'participant_id', 'post_id', 'position'], 'integer'],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => LookupPosts::className(), 'targetAttribute' => ['post_id' => 'id']],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jobs::className(), 'targetAttribute' => ['job_id' => 'id']],
            [['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Participants::className(), 'targetAttribute' => ['participant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'participant_id' => 'Participant ID',
            'post_id' => 'Post ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(LookupPosts::className(), ['id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Jobs::className(), ['id' => 'job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant()
    {
        return $this->hasOne(Participants::className(), ['id' => 'participant_id']);
    }
}
