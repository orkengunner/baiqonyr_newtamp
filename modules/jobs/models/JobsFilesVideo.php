<?php

namespace app\modules\jobs\models;

use Yii;

/**
 * This is the model class for table "jobs_files_video".
 *
 * @property string $job_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Jobs $job
 */
class JobsFilesVideo extends \yii\db\ActiveRecord
{
    public static $keyField = 'job_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs_files_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'subdir', 'basename'], 'required'],
            [['job_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jobs::className(), 'targetAttribute' => ['job_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Jobs::className(), ['id' => 'job_id']);
    }
}
