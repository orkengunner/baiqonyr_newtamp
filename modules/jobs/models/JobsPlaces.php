<?php

namespace app\modules\jobs\models;

use Yii;

/**
 * This is the model class for table "jobs_places".
 *
 * @property string $job_id
 * @property string $place_id
 */
class JobsPlaces extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs_places';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'place_id'], 'required'],
            [['job_id', 'place_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'place_id' => 'Place ID',
        ];
    }
}
