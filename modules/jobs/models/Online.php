<?php

namespace app\modules\jobs\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "online".
 *
 * @property string $onlineID
 * @property string $title
 * @property integer $price
 * @property string $filmID
 * @property integer $discount
 * @property string $note
 * @property integer $statusID
 * @property string $created
 * @property integer $langID
 * @property string $text
 * @property integer $onHome
 * @property integer $countSeasons
 * @property integer $typeID
 * @property string $rightholder
 * @property integer $is_not_auth
 * @property string $title_kz
 * @property string $text_kz
 * @property integer $inKz
 * @property string $note_kz
 */
class Online extends ActiveRecord
{
    const LANG_KZ = 31;
    const LANG_RU = 33;

    const ONLY_FEST = 1;
    const BROD_FEST = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'online';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'filmID', 'note', 'statusID', 'created', 'text', 'typeID'], 'required'],
            [['price', 'filmID', 'discount', 'statusID', 'langID', 'onHome', 'countSeasons', 'typeID', 'rightholder', 'is_not_auth', 'inKz'], 'integer'],
            [['note', 'text', 'text_kz', 'note_kz'], 'string'],
            [['created'], 'safe'],
            [['title', 'title_kz'], 'string', 'max' => 255],
            [['title_kz'], 'unique'],
        ];
    }
    public static function find()
    {
        $query = new OnlineQuery(get_called_class());


        return $query->isActive();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'onlineID' => 'Online ID',
            'title' => 'Title',
            'price' => 'Price',
            'filmID' => 'Film ID',
            'discount' => 'Discount',
            'note' => 'Note',
            'statusID' => 'Status ID',
            'created' => 'Created',
            'langID' => 'Lang ID',
            'text' => 'Text',
            'onHome' => 'On Home',
            'countSeasons' => 'Count Seasons',
            'typeID' => 'Type ID',
            'rightholder' => 'Rightholder',
            'is_not_auth' => 'Is Not Auth',
            'title_kz' => 'Title Kz',
            'text_kz' => 'Text Kz',
            'inKz' => 'In Kz',
            'note_kz' => 'Note Kz',
        ];
    }

    public static function getFilesByID($id, $lang_id)
    {
        return self::query('
            SELECT i.* FROM images AS i
            INNER JOIN online AS o ON o.onlineID = i.objectID AND i.oTypeID IN (' . self::LANG_KZ . ', ' . self::LANG_RU . ')
            WHERE o.onlineID = ' . $id . ' AND lang_id = ' . $lang_id
        , 'dbBrod')->queryOne();
    }
}
