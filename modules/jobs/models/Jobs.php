<?php

namespace app\modules\jobs\models;

use app\components\ActiveRecord;
use app\components\behaviors\WithMetaTags;
use app\components\LanguageActiveRecord;
use app\components\traits\UploadableAsync;
use app\modules\gallery\models\Gallery;
use app\modules\languages\models\Languages;
use app\modules\nominations\models\Nominations;
use app\modules\participants\models\Participants;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "jobs".
 *
 * @property string $id
 * @property string $festival_id
 * @property string $title
 * @property string $link
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $sex
 * @property string $user_id
 * @property string $brod_online_id
 * @property string $short_text
 * @property string $text
 * @property string $embed
 * @property string $publish_date
 * @property integer $status_id
 * @property integer $request_num
 * @property integer $request_pending
 * @property string $views
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $create_date
 * @property string $update_date
 * @property string $delete_date
 * @property integer $delete_by
 * @property integer $lang_id
 * @property integer $year
 * @property string $premier
 * @property string $duration
 * @property string $original_title
 * @property string $first_name
 * @property string $second_name
 * @property string $last_name
 * @property string $language
 * @property string $chronometry
 * @property string $out_competition
 * @property string $is_participant
 * @property string $category_id
 * @property string $register_id
 * @property string $country
 *
 * @property JobsCategories $category
 * @property Languages $lang
 * @property JobsFilesLogo $jobsFilesLogo
 * @property JobsFilesMetaImage $jobsFilesMetaImage
 * @property JobsFilesVideo $jobsFilesVideo
 * @property JobsGenries[] $jobsGenries
 * @property JobsNominations[] $jobsNominations
 * @property Nominations[] $nominations
 * @property JobsParticipants[] $jobsParticipants
 * @property Winners[] $winners
 */
class Jobs extends ActiveRecord
{
    use UploadableAsync;

    public $participant;
    public $profession;
    public $_genres;
    public $_places;
    public $upload_logo = null;
    public $upload_video = null;
    public $upload_video_ftp = null;

    const LANGUAGE_RU = 1;
    const LANGUAGE_KZ = 2;
    const LANGUAGE_ENG = 3;

    public static $languages = [
        self::LANGUAGE_KZ => 'Казахский',
        self::LANGUAGE_RU => 'Русский',
        self::LANGUAGE_ENG => 'Английский',
    ];

    public $file_attributes = [
        'logo',
        'metaImage'
    ];

    public $gallery_type_photo = Gallery::TYPE_JOB_PHOTO;

    public $gallery_id = null;
    public $old_status_id = null;
    public static $oldFest = false;

    const IS_PARTICIPANT = 1;
    const IS_REQUESTER = 2;
    const IS_OUT_COMPETITION = 3;

    public static $jobsTypesUrl = [
        self::IS_PARTICIPANT => 'active',
        self::IS_REQUESTER => 'applicant',
        self::IS_OUT_COMPETITION => 'out-of-competition'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['out_competition', 'brod_online_id', 'language', 'festival_id', 'user_id', 'views', 'delete_by', 'lang_id', 'status_id', 'gallery_id', 'year', 'sex', 'request_num', 'request_pending', 'is_participant', 'category_id'], 'integer'],
            [['short_text', 'text', 'meta_description', 'embed', 'duration', 'premier', 'original_title', 'phone', 'chronometry', 'first_name_2', 'second_name_2', 'last_name_2'], 'string'],
            [['short_text_kz', 'text_kz', 'meta_description_kz', 'first_name_2_kz', 'second_name_2_kz', 'last_name_2_kz'], 'string'],
            [['publish_date', 'create_date', 'update_date', 'delete_date', '_genres', '_places', 'upload_video_ftp'], 'safe'],
            [['email', 'title', 'title_kz', 'slug', 'meta_title', 'meta_title_kz', 'meta_keywords', 'meta_keywords_kz', 'name', 'link', 'last_name', 'first_name_kz', 'second_name', 'second_name_kz', 'country'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['register_id'], 'string', 'max' => 40],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [
                ['upload_logo'],
                'image',/* 'minWidth' => 620, 'minHeight'=>400,*/
                'on' => 'upload'
            ],
        ];

        if(!Yii::$app->params['yiiEnd'] == 'admin' && Languages::$current->code == 'kz') {
            $rules[] = [['title_kz', 'text_kz', 'publish_date', 'first_name_kz', 'last_name_kz'], 'required'];
        } else {
            $rules[] = [['title', 'text', 'publish_date', 'first_name', 'last_name'], 'required', 'except' => 'frontValid'];
        }
        return $rules;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'embed' => 'Embed код',
            'country' => 'Страна',
            'genres' => 'Жанры',
            '_genres' => 'Жанры',
            'places' => 'Производство',
            '_places' => 'Производство',
            'name' => 'Имя участника',
            'duration' => 'Продолжительность',
            'premier' => 'Премьера',
            'original_title' => 'Оригинальное название',
            'last_name_2_kz' => 'Фамилия второго автора (каз)',
            'last_name_2' => 'Фамилия второго автора',
            'second_name_2_kz' => 'Отчество второго автора (каз)',
            'second_name_2' => 'Отчество второго автора',
            'first_name_2_kz' => 'Имя второго автора (каз)',
            'first_name_2' => 'Имя второго автора',
            'language' => 'Язык',
            'brod_online_id' => 'Онлайно просмотр на brod.kz',
            'is_participant' => 'Тип работы',
        ]);
    }

    public function getCategory()
    {
        return $this->hasOne(JobsCategories::className(), ['id' => 'category_id']);
    }

    public static function find()
    {
        $query = new JobsQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query->isNoDeleted(self::tableName())
                ->andWhere(['festival_id' => self::festID()]);
        } elseif (Yii::$app->params['yiiEnd'] == 'request') {
            return $query->andWhere(['festival_id' => self::festID()]);
        }

        if(!self::$oldFest) {
            $query->andWhere(['festival_id' => self::festID()]);
        }

        return $query->isActive(self::tableName())->isNoDeleted(self::tableName());
    }

    public function afterFind()
    {
        parent::afterFind();

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            $this->_genres = ArrayHelper::map($this->genres, 'genreID', 'genreID');
            $this->_places = ArrayHelper::map($this->places, 'placeID', 'placeID');
            $this->old_status_id = $this->status_id;
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsFilesLogos()
    {
        return $this->hasMany(JobsFilesLogo::className(), ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsGenries()
    {
        return $this->hasMany(JobsGenries::className(), ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsFilesMetaImages()
    {
        return $this->hasMany(JobsFilesMetaImage::className(), ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsNominations()
    {
        return $this->hasMany(JobsNominations::className(), ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNominations()
    {
        return $this->hasMany(Nominations::className(), ['id' => 'nomination_id'])->viaTable('jobs_nominations', ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsParticipants()
    {
        return $this->hasMany(JobsParticipants::className(), ['job_id' => 'id'])
            ->joinWith('post', false)
            ->joinWith('participant', false)
            ->orderBy(['jobs_participants.position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants()
    {
        return $this->hasMany(Participants::className(), ['id' => 'participant_id'])->viaTable('jobs_participants', ['job_id' => 'id']);
    }

    public function getJobPhotosGallery()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id'])
            ->viaTable('gallery_objects', ['object_id' => 'id', 'type_id' => 'gallery_type_photo']);
    }


    public function getGenres()
    {
        return $this->hasMany(Genries::className(), ['genreID' => 'genre_id'])->viaTable('jobs_genries', ['job_id' => 'id']);
    }

    public function getPlaces()
    {
        return $this->hasMany(Places::className(), ['placeID' => 'place_id'])->viaTable('jobs_places', ['job_id' => 'id']);
    }

    public function beforeValidate()
    {
        $this->festival_id = $this->festID();
        if ($this->getScenario() != 'search') {
            $this->user_id = Yii::$app->getUser()->id;
            /*if (!$this->slug || ($this->old_status_id != self::STATUS_ACTIVE && $this->status_id == self::STATUS_ACTIVE)) {
                if($this->slug != StringHelper::url($this->title)) {
                    $this->slug = $this->slug;
                } else {
                    $this->slug = StringHelper::url($this->title);
                }
            }*/
            if (!$this->slug) {
                $this->slug = StringHelper::url($this->title ? $this->title : $this->title_kz);
            }
        }
        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if($this->publish_date && $dt = \DateTime::createFromFormat('d/m/Y H:i', $this->publish_date)) {
            $this->publish_date = $dt->format('Y-m-d H:i');
        }

        if($this->status_id == self::STATUS_ACTIVE) {
            $this->request_pending = 0;
        }

        return parent::beforeSave($insert);
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior,
            [
                'class' => WithMetaTags::className()
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
       // echo basename($this->upload_video_ftp);exit;
        if (Yii::$app->params['yiiEnd'] == 'admin') {
            $this->saveRelations('job_id', 'genre_id', JobsGenries::tableName(), $this->id, $this->_genres);
            $this->saveRelations('job_id', 'place_id', JobsPlaces::tableName(), $this->id, $this->_places);

            $data = [];
            $this->query()->delete(JobsParticipants::tableName(), 'job_id = :id', [':id' => $this->id])->execute();
            if(isset(\Yii::$app->request->post('Jobs')['participants'])) {
                foreach (\Yii::$app->request->post('Jobs')['participants'] as $k => $v) $data[] = [$this->id, $v, \Yii::$app->request->post('Jobs')['professions'][$k], $k+1];
                if($data)$this->query()->batchInsert(JobsParticipants::tableName(), ['job_id', 'participant_id', 'post_id', 'position'], $data)->execute();
            }
        }

        if ($this->hasAsyncTempFile('upload_logo')) {
            $logo = $this->setFile('logo', $this->saveAsyncFile('upload_logo'), true);
        }

        if($this->upload_video_ftp) {
            $this->removeAsyncFileFromSession('upload_video');

            $path = FileHelper::getStoragePath(true, 'jobs');
            $subdir = FileHelper::generateSubdir($path);
            $basename = basename($this->upload_video_ftp);
            if(rename($this->upload_video_ftp, $path . '/' . $subdir . '/' . $basename)){
                if($this->video) {
                    FileHelper::deleteDirectories(dirname($this->getFilePath('video', true)));
                    $file = JobsFilesVideo::findOne($this->id);
                    if($file) {
                        $file->delete();
                    }
                }
                $file = new JobsFilesVideo();
                $file->job_id = $this->id;
                $file->subdir = $subdir;
                $file->basename = $basename;
                $file->save();
            }
        }
        if ($this->hasAsyncTempFile('upload_video')) {
            $video = $this->setFile('video', $this->saveAsyncFile('upload_video'));
        }

        if($this->gallery_id) {
            Gallery::setGalleryObject($this->gallery_id, $this->id, $this->gallery_type_photo);
        }

        parent::afterSave($insert, $changedAttributes);
        return true;

    }

    public static function findWinners($nomination = null)
    {
        return self::findLastJobs(3, true, ['n.position' => SORT_ASC, 'place' => SORT_ASC])
            ->innerJoin('winners AS w', 'w.job_id = jobs.id')
            ->innerJoin('nominations AS n', 'n.id = w.nomination_id')
            ->addSelect([
                'n.title AS nominationTitle'
            ])
            ->andFilterWhere(['n.slug' => $nomination])
            ->asArray()
            ->all();
    }

    public static function _findWinners($nomination = null, $fest_id = null)
    {
        $nominations =  Nominations::find();
        if(!$fest_id) {
            $nominations->innerJoinWith('festivalsNominations', false);
        }
            //->leftJoin('winners AS w', 'w.nomination_id = nominations.id')
        $nominations = $nominations->leftJoin('(
                SELECT 
                    jobs.*, 
                    w.nomination_id, 
                    w.place, 
                    GROUP_CONCAT(DISTINCT jg.genre_id SEPARATOR ";") AS genres, CONCAT_WS("/", jfl.subdir, jfl.basename) AS fa_logo,
                    GROUP_CONCAT(DISTINCT CONCAT_WS(":", jobs_participants.post_id, CONCAT_WS(" ", participants.name, participants.second_name, participants.last_name)) ORDER BY jobs_participants.position SEPARATOR ";") AS partic 
                FROM jobs
                LEFT JOIN `jobs_participants` ON `jobs`.`id` = `jobs_participants`.`job_id`
                 LEFT JOIN `lookup_posts` ON `jobs_participants`.`post_id` = `lookup_posts`.`id` 
                 LEFT JOIN `participants` ON `jobs_participants`.`participant_id` = `participants`.`id` 
                 
                INNER JOIN winners AS w ON jobs.id = w.job_id AND jobs.delete_by IS NULL AND jobs.status_id = ' . self::STATUS_ACTIVE . '
                LEFT JOIN jobs_genries AS jg ON jg.job_id = jobs.id
                LEFT JOIN jobs_files_logo AS jfl ON jfl.job_id = jobs.id
                WHERE jobs.festival_id = ' . ($fest_id ? $fest_id : self::festID()) . '
                GROUP BY jobs.id, w.id
                ) AS j', 'j.nomination_id = nominations.id')
            ->addSelect([
                'nominations.id',
                'j.id AS job_id',
                //'w.job_id AS wjob_id',
                'j.title',
                'j.slug',
                'j.year',
                'j.name',
                'j.first_name',
                'j.first_name_2',
                'j.second_name',
                'j.second_name_2',
                'j.last_name',
                'j.last_name_2',
                'j.views',
                'j.is_participant',
                'j.publish_date',
                'j.genres',
                'j.fa_logo',
                'j.partic',
                //'(SELECT GROUP_CONCAT(genre_id SEPARATOR ";") FROM jobs_genries WHERE jobs_genries.job_id = j.id)/*GROUP_CONCAT(gc.genre_id SEPARATOR ";")*/ AS genres',
                'nominations.title AS nominationTitle'
            ])
            ->andWhere([
                'j.festival_id' => ($fest_id ? $fest_id : self::festID())
            ])
            ->andFilterWhere(['nominations.slug' => $nomination])
            ->orderBy(['nominations.position' => SORT_ASC, 'j.place' => SORT_ASC])

            ->asArray()
            ->all();
        return $nominations;
    }

    public static function findLastJobs($limit = 5, $isQuery =  false, $orderBy = ['publish_date' => SORT_DESC], $where = [], $icCount = false)
    {
        $query = self::find()
            ->joinWith('jobsParticipants', false)
            ->withGenres()
            ->withFilesLogo('jobs', 'job_id')
            ->getLangField('jobs.title')
            ->getLangField('jobs.first_name')
            ->getLangField('jobs.first_name_2')
            ->getLangField('jobs.second_name')
            ->getLangField('jobs.second_name_2')
            ->getLangField('jobs.last_name')
            ->getLangField('jobs.last_name_2')
            ->addSelect([
                'jobs.id',
                'jobs.slug',
                'jobs.year',
                'jobs.name',
                'jobs.views',
                'jobs.is_participant',
                'jobs.publish_date',
                'GROUP_CONCAT(DISTINCT gc.genre_id SEPARATOR ";") AS genres',
                'GROUP_CONCAT(DISTINCT CONCAT_WS(":", jobs_participants.post_id, CONCAT_WS(" ", participants.name, participants.second_name, participants.last_name)) ORDER BY jobs_participants.position SEPARATOR ";") AS partic',
            ])
            ->andWhere($where)
            ->orderBy($orderBy)
            ->groupBy('jobs.id');
        if($limit) $query->limit($limit);
        if($isQuery) return $query;
        if($icCount) return $query->count();

        return $query->asArray()->all();
    }

    public static function getBySlug($slug, $metaImage = false)
    {
        $query = self::find()
            ->withFilesLogo(self::tableName(), 'job_id')
            ->withFilesVideo(self::tableName(), 'job_id')
            ->getLangField('jobs.title')
            ->getLangField('jobs.first_name')
            ->getLangField('jobs.first_name_2')
            ->getLangField('jobs.second_name')
            ->getLangField('jobs.second_name_2')
            ->getLangField('jobs.last_name')
            ->getLangField('jobs.last_name_2')
            ->getLangField('jobs.short_text')
            ->getLangField('jobs.text')
            ->addSelect([
                'jobs.id',
                'jobs.original_title',
                'jobs.slug',
                'jobs.views',
                'jobs.publish_date',
                'jobs.year',
                'jobs.premier',
                'jobs.language',
                'jobs.duration',
                'jobs.brod_online_id',
                'GROUP_CONCAT(gc.genre_id SEPARATOR ";") AS fa_genres'
            ])
            ->withGenres()
            ->andWhere(['jobs.slug' => $slug])
            ->groupBy(['jobs.id']);

        if($metaImage) {
            $query->withFilesMetaImage(self::tableName(), 'job_id');
        }
        return $query->one();
    }

}
