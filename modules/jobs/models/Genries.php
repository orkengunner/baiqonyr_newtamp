<?php

namespace app\modules\jobs\models;

use app\components\LanguageActiveRecord;
use app\modules\languages\models\Languages;
use Yii;

/**
 * This is the model class for table "genries".
 *
 * @property string $genreID
 * @property string $name
 * @property integer $langID
 * @property string $decline
 * @property string $name_kz
 * @property string $decline_kz
 */
class Genries extends LanguageActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'genries';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['langID'], 'integer'],
            [['name', 'decline', 'name_kz', 'decline_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'genreID' => 'Genre ID',
            'name' => 'Name',
            'langID' => 'Lang ID',
            'decline' => 'Decline',
            'name_kz' => 'Name Kz',
            'decline_kz' => 'Decline Kz',
        ];
    }

    public static function findAllDD()
    {
        $genres = \Yii::$app->getCache()->get('all_genres_dd_' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($genres === false) {
            $genres = self::getDropdownList('name' . (Languages::getCurrent()->id != 1 ? '_' . Languages::getCurrent()->code : ''), 'genreID');
            \Yii::$app->getCache()->set('all_genres_dd_' . Languages::getCurrent()->id . \Yii::$app->language, $genres, 3600*24*7);
        }
        return $genres;
    }
}
