<?php

namespace app\modules\jobs\models;

use app\components\wMail;
use app\modules\lookupposts\models\LookupPosts;
use app\modules\participants\models\Participants;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;


class RequestForm extends Model
{

    public $step = null;
    public $phone;
    public $country;
    public $link;
    public $text;
    public $image;
    public $title;
    public $year;
    public $genres;
    public $contact_name;
    public $first_name;
    public $second_name;
    public $last_name;
    public $operator_first_name = [];
    public $operator_second_name = [];
    public $operator_last_name = [];
    public $scenarist_first_name = [];
    public $scenarist_second_name = [];
    public $scenarist_last_name = [];
    public $producer_first_name = [];
    public $producer_second_name = [];
    public $producer_last_name = [];
    public $producer_second_first_name = [];
    public $producer__second_second_name = [];
    public $producer_second_last_name = [];
    public $author_first_name = [];
    public $author_second_name = [];
    public $author_last_name = [];

    public $author_name = [];
    public $director_name = [];
    public $operator_name = [];
    public $scenarist_name = [];
    public $producer_name = [];
    public $actor_name = [];
    public $production_director_name = [];
    public $design_director_name = [];
    public $workshop_name = [];
    public $sound_engineer_name = [];

    public $language;
    public $terms;
    public $perms;
    public $duration;
    public $email;
    public $category_id;
    public $user_data_agree;
    public $user_reglament_read;
    public $register_id;

    public $isRequest = false;



    /**
     * @return array the validation rules.
     */
    /*public function rules()
    {
        $required = [];
        if($this->isRequest) {
            $required = ['first_name', 'last_name', 'terms', 'perms', 'language', 'duration', 'phone', 'email', 'image'];
        }
        return [
            // username and password are both required
            [ArrayHelper::merge(['text', 'link', 'text', 'title', 'year', 'genres'], $required), 'required'],
            [['phone', 'link', 'year', 'last_name', 'first_name', 'second_name', 'language', 'title', 'producer_last_name', 'producer_first_name', 'producer_second_name', 'operator_last_name', 'operator_first_name', 'operator_second_name', 'scenarist_last_name', 'scenarist_first_name', 'scenarist_second_name' ], 'string', 'max' => 255],
            [['text', 'image'], 'string'],
            [['terms', 'perms'], 'integer'],
            [['genres'], 'safe']
        ];
    }*/

    public function rules()
    {
        $required = [];
        $required1 = [['register_id', ], 'required'];
        $required2 = [['register_id', ], 'required'];
        switch($this->step) {
            case 1:
                $required = ['title', 'category_id', 'language', 'duration', 'link', 'year', 'text', 'image'];
            break;
            case 2:
                $required = ['author_name', 'director_name'];
            break;
            case 3:
                $required = ['register_id', 'contact_name', 'phone', 'email'];
                $required1 = [['user_reglament_read'], 'required', 'message' => 'Ознакомтесь с регламентом фестиваля'];
                $required2 = [['user_data_agree'], 'required', 'message' => 'Требуется согласе на предоставление информации Республиканскому фестивалю'];
            break;
            //$required = ['first_name', 'last_name', 'terms', 'perms', 'language', 'duration', 'phone', 'email', 'image'];
        }
        return [
            // username and password are both required
            [$required, 'required'],
            $required1,
            $required2,
            [['country', 'phone', 'link', 'year', 'last_name', 'first_name', 'second_name', 'language', 'title', 'producer_last_name', 'producer_first_name', 'producer_second_name', 'operator_last_name', 'operator_first_name', 'operator_second_name', 'scenarist_last_name', 'scenarist_first_name', 'scenarist_second_name'], 'string', 'max' => 255],
            [['text', 'image'], 'string'],
            [['register_id'], 'string', 'max' => 40],
            [['terms', 'perms', 'category_id'], 'integer'],
            [['duration'], 'integer', 'max' => 999],
            [['email'], 'email'],
            [['genres', 'director_name', 'author_name', 'operator_name', 'scenarist_name', 'producer_name', 'actor_name', 'workshop_name', 'production_director_name', 'design_director_name', 'sound_engineer_name'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('request-form', 'Как вас зовут'),
            'phone' => Yii::t('request-form', 'Телефон'),
            'country' => Yii::t('request-form', 'Страна'),
            'link' => Yii::t('request-form', 'Ссылка на скачивание'),
            'text' => Yii::t('request-form', 'Краткий синопсис к фильму'),
            'title' => Yii::t('request-form', 'Оригинальное название фильма'),
            'year' => Yii::t('request-form', 'Год выпуска'),
            'first_name' => Yii::t('request-form', 'Полное имя'),
            'last_name' => Yii::t('request-form', 'Фамилия'),
            'second_name' => Yii::t('request-form', 'Отчество'),
            'operator_first_name' => Yii::t('request-form', 'Полное имя'),
            'operator_last_name' => Yii::t('request-form', 'Фамилия'),
            'operator_second_name' => Yii::t('request-form', 'Отчество'),
            'scenarist_first_name' => Yii::t('request-form', 'Полное имя'),
            'scenarist_last_name' => Yii::t('request-form', 'Фамилия'),
            'scenarist_second_name' => Yii::t('request-form', 'Отчество'),
            'producer_first_name' => Yii::t('request-form', 'Полное имя'),
            'producer_last_name' => Yii::t('request-form', 'Фамилия'),
            'producer_second_name' => Yii::t('request-form', 'Отчество'),
            'author_first_name' => Yii::t('request-form', 'Полное имя'),
            'author_last_name' => Yii::t('request-form', 'Фамилия'),
            'author_second_name' => Yii::t('request-form', 'Отчество'),
            'duration' => Yii::t('request-form', 'Длительность'),
            'language' => Yii::t('request-form', 'Язык фильма'),
            'genres' => Yii::t('request-form', 'Жанр'),
            'image' => Yii::t('request-form', 'Постер'),
            'category_id' => Yii::t('request-form', 'Категория'),
            'contact_name' => Yii::t('request-form', 'Фамилия имя отчествоj'),
        ];
    }

    public static function attributeLabelsData()
    {
        return [
            'add_button' => Yii::t('request-form', 'Добавить'),
            'remove_button' => Yii::t('request-form', 'Удалить'),
            'director_name' => Yii::t('request-form', 'Режиссер'),
            'author_name' => Yii::t('request-form', 'Авторр'),
            'link' => Yii::t('request-form', 'Ссылка на скачивание'),
            'text' => Yii::t('request-form', 'Краткий синопсис к фильму'),
            'title' => Yii::t('request-form', 'Оригинальное название фильма'),
            'year' => Yii::t('request-form', 'Год выпуска'),

            'form_title' => Yii::t('request-form', 'Форма заявки'),
            'workshop_name' => Yii::t('request-form', 'Мастерская (опционально)'),
            'sound_engineer_name' => Yii::t('request-form', 'Звукорежиссер'),
            'production_director_name' => Yii::t('request-form', 'Режиссер-постановщик (опционально)'),
            'operator_name' => Yii::t('request-form', 'Оператор (опционально)'),
            'design_director_name' => Yii::t('request-form', 'Художник-постановщик (опционально)'),
            'scenarist_name' => Yii::t('request-form', 'Сценарист (опционально)'),
            'producer_name' => Yii::t('request-form', 'Продюсер (опционально)'),
            'actor_name' => Yii::t('request-form', 'Актеры (опционально)'),
            'duration' => Yii::t('request-form', 'Длительность в минутах'),
            'language' => Yii::t('request-form', 'Язык фильма'),
            'genres' => Yii::t('request-form', 'Жанр'),
            'image' => Yii::t('request-form', 'Добавить постер'),
            'category_id' => Yii::t('request-form', 'Категория'),
			'country' => Yii::t('request-form', 'Страна'),
            'contact_name' => Yii::t('request-form', 'Фамилия имя отчество'),
            'email' => Yii::t('request-form', 'Электронная почта'),
            'submit-general' => Yii::t('request-form', 'Подтвердить сведения'),
            'submit-team' => Yii::t('request-form', 'Подтвердить команду'),
            'submit-contacts' => Yii::t('request-form', 'Подать заявку на конкурс'),
            'tab-general' => Yii::t('request-form', 'Общие сведения'),
            'tab-contacts' => Yii::t('request-form', 'Контакты'),
            'tab-team' => Yii::t('request-form', 'Команда'),
            'terms-begin' => Yii::t('request-form', 'Я ознакомлен(а) и принимаю условия, описанные в'),
            'terms-end' => Yii::t('request-form', 'Регламенте кинофестиваля'),
            'access' => Yii::t('request-form', 'Я предоставляю согласие Республиканскому фестивалю короткометражного кино на сбор, накопление, хранение и обработку моих персональных данных'),
            'error-check-field' => Yii::t('request-form', 'Проверьте поле'),
            'success-header' => Yii::t('request-form', 'Ваша заявка принята!'),
            'success-message' => Yii::t('request-form', 'Мы получили вашу заявку, спасибо. После того, как жюри рассмотрит все работы, мы свяжемся с вами и сообщим результаты.'),
            'lang_rus' => Yii::t('request-form', 'Русский'),
            'lang_kaz' => Yii::t('request-form', 'Казахский'),
            'lang_eng' => Yii::t('request-form', 'Английский'),

        ];
    }

    public static function attributePlaceholdersData()
    {
        return [
            'director_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'author_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'link' => Yii::t('request-form', 'Укажите ссылку на фильм'),
            'text' => Yii::t('request-form', 'Расскажите про свой фильм'),
            'title' => Yii::t('request-form', 'Введите название фильма'),
            'year' => Yii::t('request-form', 'гггг'),

            'production_director_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'design_director_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'sound_engineer_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'workshop_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'operator_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'scenarist_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'producer_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'actor_name' => Yii::t('request-form', 'Введите имя и фамилию'),
            'duration' => Yii::t('request-form', 'мм'),
            'language' => Yii::t('request-form', 'Язык фильма'),
            'genres' => Yii::t('request-form', 'Выберите жанр'),
            'image' => Yii::t('request-form', 'Постер'),
            'category_id' => Yii::t('request-form', 'Выберите категорию'),
            'contact_name' => Yii::t('request-form', 'Введите имя'),
			'country' => Yii::t('request-form', 'Введите страну'),
            'email' => Yii::t('request-form', 'Электронная почта'),
        ];
    }

    public static function imageNotice()
    {
        return [
            Yii::t('request-form', 'Требования к постеру:'),
            Yii::t('request-form', '- минимальный размер постера: 200х290 пикселей;'),
            Yii::t('request-form', '- обязательное размещение название фильма;'),
            Yii::t('request-form', '- на постере должна быть перечислина команда фильма;'),
            Yii::t('request-form', '- текст должен контрастировать с задним фоном.'),
            Yii::t('request-form', 'Рекомендации к постеру:'),
            Yii::t('request-form', '- ориентация постера должна быть вертикальная;'),
            Yii::t('request-form', '- постер должен нести смысловую нагрузку;'),
            Yii::t('request-form', '- желательно разместить на постере слоган фильма.'),
        ];
    }

    public function send()
    {
        $refer = Url::parse_url(\Yii::$app->request->referrer);

        //$this->genres = [1,2];
        //$this->category_id = 1;
//print_r(\Yii::$app->request->post());exit;

        if($this->user_data_agree && $this->user_data_agree == 'false') $this->user_data_agree = '';
        if($this->user_reglament_read && $this->user_reglament_read == 'false') $this->user_reglament_read = '';
        if (!$this->validate() || $refer['path'] != Url::to(['/pages/default/request'])) {
            return $this->errors;
        }
        Yii::$app->params['yiiEnd'] = 'request';
        $max = Jobs::find()->max('request_num');

        if(!$jobs = Jobs::findOne(['register_id' => $this->register_id])) {
            $jobs = new Jobs();
        }

        $contact_name = [];
        if($this->contact_name) {
            $contact_name = explode(' ', $this->contact_name);
        }

        $jobs->scenario = 'frontValid';
        $jobs->title = $jobs->title ?: 'Заявка на участие №' . ($max + 1) . '. ' . $this->title;
        $jobs->title_kz = $jobs->title_kz ?: $jobs->title_kz ? 'Заявка на участие №' . ($max + 1) . '. ' . $this->title_kz : '';
        $jobs->text = $jobs->text ?: $this->text;
        $jobs->link = $jobs->link ?: $this->link;
        $jobs->name = $jobs->name ?: $this->title;
        $jobs->year = $jobs->year ?: $this->year;
        $jobs->phone = $jobs->phone ?: $this->phone;
        $jobs->country = $jobs->country ?: $this->country;
        $jobs->request_num = $jobs->request_num ?: $max + 1;;
        $jobs->request_pending = $jobs->request_pending ?: 1;
        $jobs->short_text = $jobs->short_text ?: $this->text;
        $jobs->short_text_kz = $jobs->short_text_kz ?: $this->text;
        $jobs->language = $jobs->language ?: $this->language;
        $jobs->first_name = $jobs->first_name ?: ArrayHelper::getValue($contact_name, 1);
        $jobs->first_name_kz = $jobs->first_name_kz ?: ArrayHelper::getValue($contact_name, 1);
        $jobs->second_name = $jobs->second_name ?: ArrayHelper::getValue($contact_name, 2);
        $jobs->second_name_kz = $jobs->second_name_kz ?: ArrayHelper::getValue($contact_name, 2);
        $jobs->last_name = $jobs->last_name  ?: ArrayHelper::getValue($contact_name, 0);
        $jobs->last_name_kz = $jobs->last_name_kz ?: ArrayHelper::getValue($contact_name, 0);
        $jobs->duration = $jobs->duration ?: $this->duration;
        $jobs->email = $jobs->email ?: $this->email;
        $jobs->publish_date = $jobs->publish_date ?: date('d/m/Y H:i');
        $jobs->status_id = $jobs->status_id ?: $jobs::STATUS_MODERATION;
        $jobs->category_id = $jobs->category_id ?:$this->category_id;
        $jobs->register_id = $this->step == 3 ? null : $this->register_id;
        $jobs->validate();



      /*  $this->operator_first_name = trim($this->operator_first_name);
        $this->operator_last_name = trim($this->operator_last_name);
        $this->operator_second_name = trim($this->operator_second_name);

        $this->scenarist_first_name = trim($this->scenarist_first_name);
        $this->scenarist_last_name = trim($this->scenarist_last_name);
        $this->scenarist_second_name = trim($this->scenarist_second_name);

        $this->producer_first_name = trim($this->producer_first_name);
        $this->producer_last_name = trim($this->producer_last_name);
        $this->producer_second_name = trim($this->producer_second_name);

        $this->author_first_name = trim($this->author_first_name);
        $this->author_last_name = trim($this->author_last_name);
        $this->author_second_name = trim($this->author_second_name);
*/

      //print_r(\Yii::$app->request->post());exit;
        if($jobs->save()) {

            if($this->image) {
                $file = FileHelper::saveImageFromBase64($this->image, \Yii::getAlias('@media') . '/' . 'jobs');
                if($file) {
                    $jobs->setFile('logo', $file, true);
                }
            }

            if($this->step == 2) {
                $w = [];
                $team = [];

                if(is_array($this->operator_name)) {
                    foreach ($this->operator_name as $k => $v) {
                        $operator_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $operator_name[0]],
                            ['last_name' => isset($operator_name[1]) ? $operator_name[1] : null],
                            //['second_name' => $this->operator_second_name[$k]]
                        ];

                        $team[] = [
                            'name' => $operator_name[0],
                            'last_name' => isset($operator_name[1]) ? $operator_name[1] : null,
                            //'second_name' => $this->operator_second_name[$k],
                            'type' => LookupPosts::POST_OPERATOR
                        ];
                    }
                }

                if(is_array($this->scenarist_name)) {
                    foreach ($this->scenarist_name as $k => $v) {
                        $scenarist_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $scenarist_name[0]],
                            ['last_name' => isset($scenarist_name[1]) ? $scenarist_name[1] : null],
                            //['second_name' => $this->scenarist_first_name[$k]]
                        ];
                        $team[] = [
                            'name' => $scenarist_name[0],
                            'last_name' => isset($scenarist_name[1]) ? $scenarist_name[1] : null,
                            //'second_name' => $this->scenarist_second_name[$k],
                            'type' => LookupPosts::POST_SCENARIST
                        ];
                    }
                }

                if($this->director_name) {
                    foreach ($this->director_name as $k => $v) {
                        $director_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $director_name[0]],
                            ['last_name' => isset($director_name[1]) ? $director_name[1] : null],
                            //['second_name' => $this->producer_first_name[$k]]
                        ];

                        $team[] = [
                            'name' => $director_name[0],
                            'last_name' => isset($director_name[1]) ? $director_name[1] : null,
                            //'second_name' => $this->producer_second_name[$k],
                            'type' => LookupPosts::POST_PRODUCER
                        ];
                    }
                }

                if(is_array($this->producer_name)) {
                    foreach ($this->producer_name as $k => $v) {
                        $producer_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $producer_name[0]],
                            ['last_name' => isset($producer_name[1]) ? $producer_name[1] : null],
                            //['second_name' => $this->producer_second_first_name[$k]]
                        ];

                        $team[] = [
                            'name' => $producer_name[0],
                            'last_name' => isset($producer_name[1]) ? $producer_name[1] : null,
                            //'second_name' => $this->producer_second_second_name[$k],
                            'type' => LookupPosts::POST_PRODUCER_SECOND
                        ];
                    }
                }


                if($this->author_name) {
                    foreach ($this->author_name as $k => $v) {
                        $author_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $author_name[0]],
                            ['last_name' => isset($author_name[1]) ? $author_name[1] : null],
                            //['second_name' => $this->author_second_name[$k]]
                        ];
                        $team[] = [
                            'name' => $author_name[0],
                            'last_name' => isset($author_name[1]) ? $author_name[1] : null,
                            //'second_name' => trim($this->author_second_name[$k]),
                            'type' => LookupPosts::POST_AUTHOR
                        ];
                    }
                }


                if($this->actor_name) {
                    foreach ($this->actor_name as $k => $v) {
                        $actor_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $actor_name[0]],
                            ['last_name' => isset($actor_name[1]) ? $actor_name[1] : null],
                            //['second_name' => $this->author_second_name[$k]]
                        ];
                        $team[] = [
                            'name' => $actor_name[0],
                            'last_name' => isset($actor_name[1]) ? $actor_name[1] : null,
                            //'second_name' => trim($this->author_second_name[$k]),
                            'type' => LookupPosts::POST_ACTOR
                        ];
                    }
                }

                if($this->design_director_name) {
                    foreach ($this->design_director_name as $k => $v) {
                        $design_director_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $design_director_name[0]],
                            ['last_name' => isset($design_director_name[1]) ? $design_director_name[1] : null],
                            //['second_name' => $this->author_second_name[$k]]
                        ];
                        $team[] = [
                            'name' => $design_director_name[0],
                            'last_name' => isset($design_director_name[1]) ? $design_director_name[1] : null,
                            //'second_name' => trim($this->author_second_name[$k]),
                            'type' => LookupPosts::POST_DESIGN_DIRECTOR
                        ];
                    }
                }

                if($this->production_director_name) {
                    foreach ($this->production_director_name as $k => $v) {
                        $production_director_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $production_director_name[0]],
                            ['last_name' => isset($production_director_name[1]) ? $production_director_name[1] : null],
                            //['second_name' => $this->author_second_name[$k]]
                        ];
                        $team[] = [
                            'name' => $production_director_name[0],
                            'last_name' => isset($production_director_name[1]) ? $production_director_name[1] : null,
                            //'second_name' => trim($this->author_second_name[$k]),
                            'type' => LookupPosts::POST_PRODUCNTION_DIRECTOR
                        ];
                    }
                }

                if($this->workshop_name) {
                    foreach ($this->workshop_name as $k => $v) {
                        $workshop_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $workshop_name[0]],
                            ['last_name' => isset($workshop_name[1]) ? $workshop_name[1] : null],
                            //['second_name' => $this->author_second_name[$k]]
                        ];
                        $team[] = [
                            'name' => $workshop_name[0],
                            'last_name' => isset($workshop_name[1]) ? $workshop_name[1] : null,
                            //'second_name' => trim($this->author_second_name[$k]),
                            'type' => LookupPosts::POST_WORK_SHOP
                        ];
                    }
                }

                if($this->sound_engineer_name) {
                    foreach ($this->sound_engineer_name as $k => $v) {
                        $sound_engineer_name = explode(' ', $v);
                        $w[] = ['AND',
                            ['name' => $sound_engineer_name[0]],
                            ['last_name' => isset($sound_engineer_name[1]) ? $sound_engineer_name[1] : null],
                            //['second_name' => $this->author_second_name[$k]]
                        ];
                        $team[] = [
                            'name' => $sound_engineer_name[0],
                            'last_name' => isset($sound_engineer_name[1]) ? $sound_engineer_name[1] : null,
                            //'second_name' => trim($this->author_second_name[$k]),
                            'type' => LookupPosts::POST_SOUND_ENGINEER
                        ];
                    }
                }

                //print_r($w);exit;
                $persons = Participants::find()
                    ->andWhere(ArrayHelper::merge(['OR'], $w))
                    ->all();

                /*$team = [
                    [
                        'name' => $this->operator_first_name,
                        'last_name' => $this->operator_last_name,
                        'second_name' => $this->operator_second_name,
                        'type' => LookupPosts::POST_OPERATOR
                    ],
                    [
                        'name' => $this->scenarist_first_name,
                        'last_name' => $this->scenarist_last_name,
                        'second_name' => $this->scenarist_second_name,
                        'type' => LookupPosts::POST_SCENARIST
                    ],
                    [
                        'name' => $this->producer_first_name,
                        'last_name' => $this->producer_last_name,
                        'second_name' => $this->producer_second_name,
                        'type' => LookupPosts::POST_PRODUCER
                    ],
                    [
                        'name' => $this->author_first_name,
                        'last_name' => $this->author_last_name,
                        'second_name' => $this->author_second_name,
                        'type' => LookupPosts::POST_AUTHOR
                    ]
                ];*/


                foreach($team as $k => $v) {
                    $participant_id = null;
                    foreach($persons as $_v) {
                        if($v['name'] == $_v->name && $v['last_name'] == $_v->last_name /*&& $v['second_name'] == $_v->second_name*/) {
                            $participant_id = $_v->id;
                            break;
                        }
                    }

                    if(!$participant_id) {
                        $participant = new Participants();
                        $participant->name = $v['name'];
                        //$participant->second_name = $v['second_name'];
                        $participant->last_name = $v['last_name'];
                        $participant->save();
                        $participant_id = $participant->id;
                    }
                    $participant = new JobsParticipants();
                    $participant->position = $k+1;
                    $participant->post_id = $v['type'];
                    $participant->job_id = $jobs->id;
                    $participant->participant_id = $participant_id;
                    $participant->save();
                }


            }
            if(is_array($this->genres) && $this->step == 1) {
                //echo 'ok';
                (new Genries())->saveRelations('job_id', 'genre_id', JobsGenries::tableName(), $jobs->id, $this->genres);
            }

            return true;
        } else {
            return false;
        }
    }

}
