<?php

namespace app\modules\jobs\models;

use Yii;

/**
 * This is the model class for table "jobs_nominations".
 *
 * @property string $job_id
 * @property string $nomination_id
 *
 * @property Nominations $nomination
 * @property Jobs $job
 */
class JobsNominations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jobs_nominations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_id', 'nomination_id'], 'required'],
            [['job_id', 'nomination_id'], 'integer'],
            [['nomination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nominations::className(), 'targetAttribute' => ['nomination_id' => 'id']],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jobs::className(), 'targetAttribute' => ['job_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'nomination_id' => 'Nomination ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomination()
    {
        return $this->hasOne(Nominations::className(), ['id' => 'nomination_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Jobs::className(), ['id' => 'job_id']);
    }
}
