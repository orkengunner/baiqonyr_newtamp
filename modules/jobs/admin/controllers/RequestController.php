<?php

namespace app\modules\jobs\admin\controllers;

use app\components\AdminController;
use Yii;
use app\modules\jobs\models\Jobs;
use app\modules\jobs\models\JobsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DefaultController implements the CRUD actions for Jobs model.
 */
class RequestController extends AdminController
{
    protected $_modelName = 'app\modules\jobs\models\Jobs';
    /**
     * Lists all Jobs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobsSearch();
        $searchModel->isRequest = true;
        $searchModel->setScenario('search');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jobs model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Jobs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Jobs();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Jobs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Jobs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Jobs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Jobs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jobs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSearchAf($term)
    {

        $data = [];
        $jobs = Jobs::find()
            ->andWhere(['like', 'title',  $term])
            ->addSelect([
                'id',
                'title'
            ])
            ->limit(10)
            ->all();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        foreach($jobs as $v) {
            $data[] = [
                'label' => $v->title,
                'value' => $v->id,
            ];
        }
        return $data;
    }

    public function actionFileManager()
    {
        $this->layout = '//admin/empty';
        $dir = $_SERVER['DOCUMENT_ROOT'] . \Yii::$app->params['ftp_dir'] . \Yii::$app->request->get('object') . '/';
        $scan = scandir($dir);

        array_shift($scan);
        array_shift($scan);

        return $this->render('@app/modules/jobs/admin/views/default/file_manager', ['dir' => $dir, 'scan' => $scan]);
    }
}
