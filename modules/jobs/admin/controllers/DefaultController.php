<?php

namespace app\modules\jobs\admin\controllers;

use app\components\AdminController;
use app\modules\films\models\Films;
use app\modules\films\models\Online;
use app\modules\films\models\RelationFilmsGenries;
use app\modules\films\models\RelationFilmsPersons;
use app\modules\images\models\Images;
use app\modules\persons\models\Persons;
use app\modules\persons\models\PersonsTypes;
use app\modules\persons\models\RelationPersonsTypes;
use Yii;
use app\modules\jobs\models\Jobs;
use app\modules\jobs\models\JobsSearch;
use yii\db\Expression;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DefaultController implements the CRUD actions for Jobs model.
 */
class DefaultController extends AdminController
{
    protected $_modelName = 'app\modules\jobs\models\Jobs';
    /**
     * Lists all Jobs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobsSearch();
        $searchModel->setScenario('search');
        $searchModel->outCompetition = \Yii::$app->request->get('type');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jobs model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Jobs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Jobs();

        $is_participant = $model->is_participant;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Jobs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $is_participant = $model->is_participant;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index' . ($is_participant != $model::IS_PARTICIPANT ? '?type=' . $is_participant : '')]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Jobs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Jobs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Jobs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jobs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSearchAf($term)
    {

        $data = [];
        $jobs = Jobs::find()
            ->andWhere(['like', 'title',  $term])
            ->addSelect([
                'id',
                'title'
            ])
            ->limit(10)
            ->all();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        foreach($jobs as $v) {
            $data[] = [
                'label' => $v->title,
                'value' => $v->id,
            ];
        }
        return $data;
    }

    public function actionFileManager()
    {
        $this->layout = '//admin/empty';
        $dir = $_SERVER['DOCUMENT_ROOT'] . \Yii::$app->params['ftp_dir'] . \Yii::$app->request->get('object') . '/';
        $scan = scandir($dir);

        array_shift($scan);
        array_shift($scan);

        return $this->render('file_manager', ['dir' => $dir, 'scan' => $scan]);
    }


    public function actionCopyToBrod($id)
    {
        $model = $this->findModel($id);
        $genres = $model->jobsGenries;
        $participants = $model->jobsParticipants;
        $cover = $model->logo;
        $video = $model->video;
        $exists = false;

        if(!$brodFilms = Films::findOne(['title' => $model->title])) {
            $brodFilms = new Films();
        } else {
            $exists = true;
        }

        $brodFilms->title = $model->title;
        $brodFilms->previewText = $model->short_text;
        $brodFilms->text = $model->text;
        $brodFilms->premier = $model->premier;
        $brodFilms->url = 'baikonur-' . $model->slug;
        $brodFilms->duration = $model->duration;
        $brodFilms->year = $model->year;
        $brodFilms->datePublish = new Expression('NOW()');
        $brodFilms->statusID = 2;
        $brodFilms->langID = 1;
        $brodFilms->created = new Expression('NOW()');
        $brodFilms->isDelete = 0;

        $brodFilms->validate();
        echo 'Films = '; print_r($brodFilms->errors);echo "\n\n";

        if($brodFilms->save() ) {
            if($cover)  {

                if(!$brodCover = Images::findOne(['objectID' => $brodFilms->filmID, 'oTypeID' => Images::IMAGE_FILM_LOGO])) {
                    $brodCover = new Images();
                    $subdir = 'bkr_' . $cover->subdir;
                }  else {
                    $subdir = dirname($cover->subdir);
                }

                $brodCover->name = $subdir . '/' . $cover->basename;
                $brodCover->oTypeID = Images::IMAGE_FILM_LOGO;
                $brodCover->objectID = $brodFilms->filmID;
                $brodCover->lang_id = 1;
                $brodCover->created = new Expression('NOW()');
                $brodCover->save();

                $filePath = $model->getFilePath('logo', true);
                $newDir = $_SERVER['DOCUMENT_ROOT'] . '/../../html/web/media/films/' . $subdir;
                //$newDir = $_SERVER['DOCUMENT_ROOT'] . '/../../brod.dev/web/media/films/' . $subdir;
                FileHelper::forceDirectories($newDir);
                copy($filePath, $newDir . '/' . $cover->basename);

                $sizes = $model->getParam('brod.sizes');

                foreach ($sizes as $size) {
                    if (!$size[0] && !$size[1]) break;
                    $mode = '-' . ($size[0] ? $size[0] : '') . 'x' . ($size[1] ? $size[1] : '');
                    $pathinfo = pathinfo($newDir . '/' . $cover->basename);

                    $model->getThumbnail(
                        $newDir . '/' . $cover->basename,
                        $pathinfo['dirname'] . "/" . $pathinfo['filename'] . $mode . "." . $pathinfo['extension'],
                        $size[0],
                        $size[1]
                    );
                }

                echo 'Images cover = '; print_r($brodCover->errors);echo "\n\n";
            }

            if( !$exists && $video) {
                $brodOnline = new Online();
                $brodOnline->title = $model->title;
                $brodOnline->filmID = $brodFilms->filmID;
                $brodOnline->statusID = 2;
                $brodOnline->text = $brodFilms->text;
                $brodOnline->typeID = 2;
                $brodOnline->created = new Expression('NOW()');
                $brodOnline->save();

                echo 'Online = '; print_r($brodOnline->errors);echo "\n\n";

                $brodVideo = new Images();
                $brodVideo->name = 'bkr_' . $video->subdir . '/' . $video->basename;
                $brodVideo->oTypeID = Images::IMAGE_ONLINE_VIDEO;
                $brodVideo->objectID = $brodOnline->onlineID;
                $brodVideo->lang_id = 1;
                $brodVideo->created = new Expression('NOW()');
                $brodVideo->save();

                $filePath = $model->getFilePath('video', true);
                echo 'qwe';
                if(is_file($filePath) && !is_link(dirname($filePath))) {
                    $subdir = 'bkr_' . $video->subdir;
                    $newDir = $_SERVER['DOCUMENT_ROOT'] . '/../../html/web/media/online/' . $subdir;
                    //$newDir = $_SERVER['DOCUMENT_ROOT'] . '/../../brod.dev/web/media/online/' . $subdir;
                    echo $newDir . '/' . $video->basename;
                    FileHelper::forceDirectories($newDir);
                    copy($filePath, $newDir . '/' . $video->basename);
                    rename($filePath, $newDir . '/' . $video->basename);
                    FileHelper::deleteDirectories(dirname($filePath));
                    exec('ln -s ' .  $newDir . ' ' . dirname($filePath));
                }


                echo 'Images video = '; print_r($brodVideo->errors);echo "\n\n";
            }

            if(!$exists && $genres) {
                foreach($genres as $v) {
                    $brodRelGenres = new RelationFilmsGenries();
                    $brodRelGenres->genreID = $v->genre_id;
                    $brodRelGenres->filmID = $brodFilms->filmID;
                    $brodRelGenres->save();

                    echo 'RelationFilmsGenries = '; print_r($brodRelGenres->errors);echo "\n\n";
                }
            }

            if(!$exists && $model->first_name) {
                $this->setPerson($model->first_name, $model->last_name, $brodFilms->filmID, 'Режиссер');
            }

            if(!$exists && $model->first_name_2) {
                $this->setPerson($model->first_name2, $model->last_name2, $brodFilms->filmID, 'Режиссер');
            }

            if(!$exists && $participants) {
                foreach($participants as $v) {
                    $this->setPerson($v->participant->name, $v->participant->last_name, $brodFilms->filmID, $v->post->title);
                }
            }
        }

        return $this->redirect(['index']);
    }

    public function setPerson($first_name, $last_name, $filmID, $_profession)
    {
        $profession = PersonsTypes::findOne(['title' => trim($_profession)]);
        if(!$profession) {
            $profession = new PersonsTypes();
            $profession->title = $_profession;
            $profession->url = StringHelper::url($profession);
            $profession->sort = 0;
            $profession->save();

            echo 'PersonsTypes = '; print_r($profession->errors);echo "\n\n";
        }
        $brodPerson = Persons::findOne(['firstName' => $first_name, 'lastName' => $last_name]);
        $brodRelProfessions = null;
        if(!$brodPerson) {
            $brodPerson = new Persons();
            $brodPerson->firstName = $first_name;
            $brodPerson->lastName = $last_name;
            $brodPerson->url = StringHelper::url($first_name . '-' . $last_name);
            $brodPerson->statusID = 2;
            $brodPerson->created = new Expression('NOW()');
            $brodPerson->save();
            echo 'Persons = '; print_r($brodPerson->errors);echo "\n\n";
        } else {
            $brodRelProfessions = RelationPersonsTypes::findOne([
                'personID' => $brodPerson->personID,
                'pTypeID' => $profession->pTypeID
            ]);
            //$brodProfession = PersonsTypes::findOne(['personID' => $brodPerson->personID]);
        }

        if(!$brodRelProfessions) {
            $brodRelProfessions = new RelationPersonsTypes();
            $brodRelProfessions->personID = $brodPerson->personID;
            $brodRelProfessions->pTypeID = $profession->pTypeID;
            $brodRelProfessions->save();

            echo 'RelationPersonsTypes = '; print_r($brodRelProfessions->errors);echo "\n\n";
        }

        $relPersonsFilms = new RelationFilmsPersons();
        $relPersonsFilms->personID = $brodPerson->personID;
        $relPersonsFilms->filmID = $filmID;
        $relPersonsFilms->pTypeID = $profession->pTypeID;
        $relPersonsFilms->save();

        echo 'RelationFilmsPersons = '; print_r($relPersonsFilms->errors);echo "\n\n";

        return $brodPerson->personID;
    }
}