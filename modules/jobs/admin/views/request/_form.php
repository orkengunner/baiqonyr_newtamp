<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\jobs\models\Jobs */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('
    setInterval(function(){
        $(".jcrop-tracker").click(function(){
            return false;
        });
    }, 500);

    $("#jobs-participant").keypress(function( e ){
        if (e.keyCode == 13) {
            mAjax.doit("/admin.php/participants/add", {name:$("#jobs-participant").val(), post_id:$("#jobs-profession").val()});
        }
    });


    $("#submit-page").click(function(){
        $("form").submit();
    });

$( "#jobs-participant" ).autocomplete({
			source: "' . \yii\helpers\Url::to(['/participants/search']) . '?job_id=' . \Yii::$app->request->get('id') . '",
			autoFill: true,
            minLength: 2,
			html: true,
			select: function( event, ui ) {
    $("#ui-id-1").addClass(\'hidden\');

       setTimeout(\'$("#jobs-participant").val("")\', 50);
       $("#tbl-participants tbody").append("" +
            "<tr>" +
                "<td class=\"td_left\">"+
                    ui.item.label+
                    "<input type=\"hidden\" value=\""+ui.item.value+"\" name=\"Jobs[participants][]\">" +
                "</td>" +
                "<td class=\"td_right\">"+
                    $("#jobs-profession :selected").text()+
                    "<input type=\"hidden\" value=\""+ $("#jobs-profession").val()+"\" name=\"Jobs[professions][]\">" +
                "</td>" +
                "<td><a onclick=\"$(this).closest(\'tr\').remove()\" href=\"javascript:;\" class=\"remove\">Удалить</a></td>" +
            "</tr>" +
        "");
    //console.log(ui.item.label);
            },
            search: function( event ) {
                $("#ui-id-1").removeClass(\'hidden\');
            },
            response: function( event, data ) {
                if (!data.content.length) {
                    $("#ui-id-1").addClass(\'hidden\');
                    $("#unknownMember").removeClass("hidden");
                } else {
                    $("#unknownMember").addClass("hidden");
                }
            },
            close: function( event, data ) {

                $("#ui-id-1").addClass(\'hidden\');
            },
		});

		$( "#tbl-participants tbody" ).sortable();
');

?>
<style>
    table {
        display: inline-table;
        margin-top: 10px!important;
        width: 100%;
    }
    td {
        border: 1px solid;
        padding: 10px;
    }
    .td_left{width: 70%}
    .td_right{width: 29%;}


    #jobs-_places, #jobs-_genres {
        height: 200px;
        overflow-y: scroll;
    }
    #jobs-_places > label, #jobs-_genres > label {
        width: 100%;
    }
</style>
<div class="jobs-form">

    <?php $form = ActiveForm::begin(['enableClientScript' => false]); ?>

    <div class="row">
        <div class="col-md-9">
            <div class="widget">
                <div class="widget-content padding">

                    <div class="form-group field-jobs-is_participant">

                        <label><input type="checkbox" value="<?= $model::IS_REQUESTER?>" name="Jobs[is_participant]" id="jobs-is_participant"> Заявитель</label>

                        <div class="help-block"></div>
                    </div>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'duration')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'premier')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'language')->dropDownList($model::$languages) ?>
					<?= $form->field($model, 'country')->textInput() ?>

                    <!--?= $form->field($model, 'short_text')->textarea(['rows' => 6]) ?-->

                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>


                    <?= $form->field($model, 'embed')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, '_genres')->checkboxList(\app\modules\jobs\models\Genries::getDropdownList('name', 'genreID'))?>
                    <!--div>
                        <?= $form->field($model, '_places')->checkboxList(\app\modules\jobs\models\Places::getDropdownList('title', 'placeID'))?>
                    </div-->

                </div>
            </div>


            <div class="widget">
                <div class="widget-header">
                    <h2>Автор</h2>
                </div>
                <div class="widget-content padding">

                    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'second_name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>


                    <?= $form->field($model, 'first_name_2')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'second_name_2')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'last_name_2')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'sex')->dropDownList(['Мужской', 'Женский']) ?>


                </div>
            </div>


            <div class="widget">
                <div class="widget-content padding">

                    <div class="form-group field-jobs-participant">
                        <label style="" class="control-label" for="jobs-participant">Творческая группа</label><br>
                        <?= Html::activeTextInput($model, 'participant', [
                            'style' => 'width:70%;float:left',
                            'class' => 'form-control'
                        ])?>

                        <?= Html::activeDropDownList($model, 'profession', \app\modules\lookupposts\models\LookupPosts::getDropdownList(), [
                            'style' => 'width:29%;float:left;margin-left:10px',
                            'class' => 'form-control'
                        ])?>
                        <br />
                    </div>

                    <table id="tbl-participants">
                        <tbody>
                        <?php foreach((array)\app\modules\jobs\models\JobsParticipants::find()
                            ->andWhere(['job_id' => $model->id])
                            ->orderBy(['position' => SORT_ASC])
                            ->all() as $v): ?>
                            <tr>
                                <td>
                                    <?= $v->participant->name . ' ' . $v->participant->second_name . ' ' . $v->participant->last_name; ?>
                                    <input type="hidden" name="Jobs[participants][]" value="<?= $v->participant_id; ?>">
                                </td>
                                <td>
                                    <?= $v->post->title; ?>
                                    <input type="hidden" name="Jobs[professions][]" value="<?= $v->post_id; ?>">
                                </td>
                                <td><a onclick="$(this).closest('tr').remove()" href="javascript:;" class="remove">Удалить</a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>


                </div>
            </div>

            <?= \app\components\widgets\UploadGallery::widget([
                'model' => $model,
                'form' => $form,
                'attribute' => 'jobPhotosGallery',
                'container' => 'gallery_photo'
            ])?>

            <?= \app\components\widgets\MetaTagFields::widget([
                'model' => $model,
                'form' => $form
            ]) ?>


        </div>
        <div class="col-md-3">
            <?= \app\components\widgets\PublishData::widget([
                'model' => $model,
                'form' => $form,
                'isSubmit' => false,
            ])?>

            <div class="widget">
                <div class="widget-header">
                    <h2>Миниатюра записи</h2>
                </div>
                <div class="widget-content padding gallery-wrap">
                    <?php
                    echo \app\components\widgets\ImageUploadAsync::widget([
                        'model' => $model,
                        'attribute' => 'upload_logo',
                        'form' => $form,
                        //'hint' => 'Рекомендуемый размер в пропорции 1,5',
                        'croppable' => false,
                        'previewPath' => $model->getFilePath('logo', false),
                    ]); ?>
                </div>
            </div>

            <div class="widget">
                <div class="widget-header">
                    <h2>Видео файл</h2>
                </div>

                <div class="widget-content padding gallery-wrap">
                    <?= $form->field($model, 'brod_online_id')->dropDownList(\app\modules\jobs\models\Online::getDropdownList('title', 'onlineID'), ['prompt' => 'Выберите фильм'])?>
                    <?php
                    echo \app\components\widgets\ImageUploadAsync::widget([
                        'model' => $model,
                        'attribute' => 'upload_video',
                        'form' => $form,
                        //'hint' => 'Рекомендуемый размер в пропорции 1,5',
                        'croppable' => false,
                        'previewPath' => $model->getFilePath('video', false),
                        'update_container' => false
                    ]); ?>

                    <a href="javascript:;" id="upload_videoFtpLink" data-note="#jobFile .success-msg" data-disable="#jobs-upload_video" onclick="openFileManager('upload_video')">Загрузить с FTP</a><br>
                    <input type="hidden" id="upload_video_ftp" name="Jobs[upload_video_ftp]">

                    <div class="help-block">
                        <p id="jobFile" class="text-info">
                            <em class="success-msg hidden"><?php echo $model->video ? 'Загружен файл: ' . $model->getFilePath('video', false) : '';?></em>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="/js/ckeditor/ckeditor.js?<?= time() ?>"></script>
<script>
    var EDITOR_GALLERIES = <?=\yii\helpers\Json::encode(\app\modules\gallery\models\Gallery::getDropdownList())?>;

    CKEDITOR.replace('jobs-text', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('jobs-short_text', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName === 'table') {
            var infoTab = dialogDefinition.getContents('info');
            var border = infoTab.get('txtBorder');
            border['default'] = "0";
        }
    });

    var participantsCallback = function(data) {
        $("#tbl-participants tbody").append("" +
            "<tr>" +
            "<td class=\"td_left\">"+
            $("#jobs-participant").val()+
            "<input type=\"hidden\" value=\""+data.id+"\" name=\"Jobs[participants][]\">" +
            "</td>" +
            "<td class=\"td_right\">"+
            $("#jobs-profession :selected").text()+
            "<input tyиpe=\"hidden\" value=\""+data.post_id+"\" name=\"Jobs[professions][]\">" +
            "</td>" +
            "<td><a onclick='$(this).closest('tr').remove()' href='javascript:;' class='remove'>Удалить</a></td>" +
            "</tr>" +
            "");
    }

    var serverWin = null;
    var selectFile = function (path, attribute) {
        serverWin.close();
        $('#'+attribute+'_ftp').val(path);

        $($('#'+attribute+'FtpLink').data('disable')).attr('disabled', true);
        $($('#'+attribute+'FtpLink').data('note')).removeClass('hidden').text('Файл выбран');
    }

    var openFileManager = function(attribute) {
        serverWin = window.open("/admin.php/jobs/file-manager?attribute="+attribute,
            "_blank"
        )
    }
</script>