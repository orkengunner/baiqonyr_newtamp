<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\jobs\models\JobsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Работы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jobs-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <p>
        <?= Html::a(Yii::t('jobs', '<i class="fa fa-align-justify"></i> Участники', [
            'modelClass' => 'Jobs',
        ]), ['/jobs'], ['class' => 'btn btn-default']) ?>

        <a href="/admin.php/jobs?type=<?= \app\modules\jobs\models\Jobs::IS_OUT_COMPETITION?>" class="btn btn-default"><i class="fa fa-align-justify"></i> Внеконкурсные работы</a>&nbsp;
        <a href="/admin.php/jobs?type=<?= \app\modules\jobs\models\Jobs::IS_REQUESTER?>" class="btn btn-default"><i class="fa fa-align-justify"></i> Заявители</a>&nbsp;
    </p>


    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'pager' => [
                    'lastPageLabel' => 'Последняя',
                    'firstPageLabel' => 'Первая'
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'title',
                    [
                        'attribute' => 'status_id',
                        'value' => function ($data) {
                            return \app\modules\pages\models\Pages::$statuses[$data->status_id];
                        },
                        'filter' => \app\modules\pages\models\Pages::$statuses
                    ],
                    // 'short_text',
                    // 'text:ntext',
                    // 'publish_date',
                    // 'views',
                    // 'slug',
                    // 'meta_title',
                    // 'meta_keywords',
                    // 'meta_description:ntext',
                    // 'create_date',
                    // 'update_date',
                    // 'delete_date',
                    // 'delete_by',
                    // 'lang_id',

                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{update} {remove}'
                    ],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
