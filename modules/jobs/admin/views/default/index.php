<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\jobs\models\JobsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Работы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jobs-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <p>
        <?= Html::a(Yii::t('jobs', '<i class="icon-list-add"></i> Добавить работу', [
            'modelClass' => 'Jobs',
        ]), ['create'], ['class' => 'btn btn-success']) ?>

        <?php if(\Yii::$app->request->get('type') && \Yii::$app->request->get('type') != \app\modules\jobs\models\Jobs::IS_PARTICIPANT): ?>
            <a href="/admin.php/jobs?type=<?= \app\modules\jobs\models\Jobs::IS_PARTICIPANT?>" class="btn btn-default"><i class="fa fa-align-justify"></i> Участники</a>&nbsp;
        <?php endif; ?>
        <a href="/admin.php/jobs?type=<?= \app\modules\jobs\models\Jobs::IS_OUT_COMPETITION?>" class="btn btn-default"><i class="fa fa-align-justify"></i> Внеконкурсные работы</a>&nbsp;
        <a href="/admin.php/jobs?type=<?= \app\modules\jobs\models\Jobs::IS_REQUESTER?>" class="btn btn-default"><i class="fa fa-align-justify"></i> Заявители</a>&nbsp;

        <a href="/admin.php/jobs/request/" class="btn btn-default"><i class="fa fa-align-justify"></i> Заявки</a>
    </p>


    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'pager' => [
                    'lastPageLabel' => 'Последняя',
                    'firstPageLabel' => 'Первая'
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'title',
                    [
                        'attribute' => 'status_id',
                        'value' => function ($data) {
                            return \app\modules\pages\models\Pages::$statuses[$data->status_id];
                        },
                        'filter' => \app\modules\pages\models\Pages::$statuses
                    ],
                    // 'short_text',
                    // 'text:ntext',
                    // 'publish_date',
                    // 'views',
                    // 'slug',
                    // 'meta_title',
                    // 'meta_keywords',
                    // 'meta_description:ntext',
                    // 'create_date',
                    // 'update_date',
                    // 'delete_date',
                    // 'delete_by',
                    // 'lang_id',

                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{copy-to-brod}{update} {remove}',
                        'buttons' => [
                            'copy-to-brod' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-cloud-download"></span>', $url, [
                                    'title' => \Yii::t('yii', 'Копировать на Брод'),
                                    'data-pjax' => '0',
                                    'class' => 'btn btn-success'
                                ]);
                            }

                        ],
                    ],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
