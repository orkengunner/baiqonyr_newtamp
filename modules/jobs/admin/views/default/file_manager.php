<?php
/**
 * Created by PhpStorm.
 * User: Vitaly Prokhonenkov
 * Date: 23.04.2015
 * Time: 9:55
 */
$this->registerCSS('
    table {width:100%}
');
$cnt = [];

foreach ($scan as $k => $v) {
    if (is_dir($dir . $v)) {
        $link = 'href="?attribute=' . \Yii::$app->request->get('attribute') . '&object=' . (\Yii::$app->request->get('object') . '/' . $v) . '"';
        $note = 'Каталог';
    }
    else {
        $link = 'href="javascript:;" onclick="window.opener.selectFile(\'' . $dir . $v . '\', \'' . \Yii::$app->request->get('attribute') . '\')"';
        $note = 'Файл';
    }
    $cnt[] = '
        <td><a ' . $link . '>' . $v . '</a></td>
        <td>' . $note . '</td>';
}
?>
<div class="pages-form">
    <div class="row">
        <div class="col-md-9">
            <div class="widget">
                <div class="widget-content padding">
                    <table>
                        <tr>
                            <td colspan="2">ftp://<?= ltrim(\Yii::$app->request->get('object'), '/'); ?><br><br></td>
                        </tr>
                        <tr>
                            <?= implode('</tr><tr>', $cnt); ?>
                        </tr>
                    </table>
                </div>
        </div>
    </div>
</div>
