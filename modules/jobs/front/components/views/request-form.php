<section class="form-application<?= $isInside ? ' form-application--inner"' : ''?>" id="form--add-movie">
    <div class="container">
        <?php
            if(!\Yii::$app->controller->fest->registration_close):
                if(!$isSend):
                $form = \yii\widgets\ActiveForm::begin([
                    'options' => [
                        'class' => 'form form--profile'
                    ],
                    'action' => !$isRequest ? \yii\helpers\Url::to(['/pages/default/request']) : $_SERVER['REQUEST_URI']
                ]);
                    $form->field($model, 'genres')->textInput(['id' => 'movieType'])->render();
                ?>
                    <div class="grid">
                        <div class="grid-item form-group--text form-wrapper">
                            <h3 class="form-title">Подать свой фильм на фестиваль</h3>
                            <div class="grid">
                                <div class="grid-item grid-item--form">
                                    <?= $form->field($model, 'title')->textInput([
                                        'placeholder' => 'Введите название фильма',
                                        'onfocus' => "if(this.placeholder  == 'Введите название фильма') {this.placeholder = '';}",
                                        'onblur' => "if(this.value == '') { this.placeholder = 'Введите название фильма'; }",
                                        'class' => 'form-control'
                                    ])->hint('Полное название фильма', ['class' => 'info small']); ?>
                                </div>
                                <div class="grid-item grid-item--form grid-item--year">
                                    <?= $form->field($model, 'year')->dropDownList([
                                        // date('Y', strtotime('- 2 Year')) => date('Y', strtotime('- 2 Year')),
                                        date('Y', strtotime('- 1 Year')) => date('Y', strtotime('- 1 Year')),
                                        date('Y') => date('Y'),
                                    ])?>
                                </div>
                            </div>

                            <div class="form-group field-movieType">
                                <label for="movieType" class="control-label">Жанр</label>
                                <div class="select-wrapper">
                                    <?= \yii\helpers\Html::activeDropDownList($model, 'genres', \app\modules\jobs\models\Genries::getDropdownList('name', 'genreID'), [
                                        'class' => 'form-control',
                                        'multiple' => 'multiple',
                                        'id' => 'movieType'
                                    ])?>


                                </div>
                                <?= \yii\helpers\Html::error($model, 'genres', ['class' => 'help-block'])?>
                            </div>
                            <?= $form->field($model, 'link')->textInput([
                                'placeholder' => 'Укажите ссылку на свой фильм',
                                'onfocus' => "if(this.placeholder  == 'Укажите ссылку на свой фильм') {this.placeholder = '';}",
                                'onblur' => "if(this.value == '') { this.placeholder = 'Укажите ссылку на свой фильм'; }",
                                'class' => 'form-control'
                            ])->hint('Drive, DropBox, Яндекс.Диск', ['class' => 'info small']); ?>

                            <?= $form->field($model, 'text')->textarea([
                                'placeholder' => 'Введите сообщение',
                                'onfocus' => "if(this.placeholder  == 'Введите сообщение') {this.placeholder = '';}",
                                'onblur' => "if(this.value == '') { this.placeholder = 'Введите сообщение'; }",
                                'cols' => 30,
                                'rows' => 10,
                                'class' => 'form-control textarea'
                            ])->hint('История создания, творческий путь режиссера и т. д.', ['class' => 'info small']); ?>

                            <div class="form-footer">
                                <?= \yii\helpers\Html::submitInput(\Yii::t('front', 'Подать заявку'), [
                                    'class' => 'btn btn--primary',
                                    'onclick' => "$('#requestform-image').val($('.cropper').cropit('export'))"
                                ]) ?>
                                <!-- НА СТРАНИЦЕ ЗАЯВКИ -->
                                <!-- <button type="button" class="btn btn--primary">Отправить заявку</button> -->
                            </div>
                        </div>
                        <!-- <div class="form-group--image">
                                <div class="cropper cropper-post-cover">
                                <button type="button" class="btn-cropper link js-upload-trigger">Постер фильма</button>
                                <div class="cropit-preview"></div>
                                <input type="file" class="cropit-image-input" name="user_cover" id="userCover" />
                                <input type="hidden" class="cropit-image-input-hidden" name="user_cover_url" id="userCoverUrl" value="" />
                                <button type="button" class="btn btn-remove hidden" title="Удалить фото"><span class="icon icon--remove link--image"></span></button>
                                <button type="button" class="btn btn-reupload link js-upload-trigger hidden">Заменить фото</button>
                              </div>
                            </div> -->

                        <div class="form-scheme">
                            <ul class="form-scheme-list">
                                <li class="form-scheme-item">
                                    <h3 class="form-scheme-item-title">Подайте заявку</h3>
                                    <p class="form-scheme-item-description">Расскажите про заявку, почему решили снимать в этом жанре и тд</p>
                                </li>

                                <li class="form-scheme-item">
                                    <h3 class="form-scheme-item-title">Время для модерации</h3>
                                    <p class="form-scheme-item-description">Модератор рассмотрит Вашу работу. В случае одобрения Ваша работа появится на сайте</p>
                                </li>

                                <li class="form-scheme-item">
                                    <h3 class="form-scheme-item-title">Участвуйте в конкурсе</h3>
                                    <p class="form-scheme-item-description">Теперь Вы являетесь официальным участником конкурса!</p>
                                </li>
                            </ul>
                        </div><!-- end .form-scheme -->

                    </div>
                <?= \yii\helpers\Html::activeHiddenInput($model, 'image'); ?>
                <?php \yii\widgets\ActiveForm::end(); endif; ?>
        <?php else: ?>
                <div class="application-completed">
                    <h3 class="form-title">ПОДАЧА ЗАЯВОК ЗАВЕРШЕНА</h3>
                    <p><?= \Yii::$app->controller->fest->registration_close_text; ?></p>
                </div>
        <?php endif; ?>
    </div>
</section>