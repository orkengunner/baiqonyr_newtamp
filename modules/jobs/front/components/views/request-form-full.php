<?php
$this->registerJs('
    $("#requestform-phone").mask("999 999 99 99");
');
?>
<section class="form-application">
    <?php
    if(!$isSend):
    $form = \yii\widgets\ActiveForm::begin([
        'options' => [
            'class' => 'form form--profile'
        ],
        'action' => !$isRequest ? \yii\helpers\Url::to(['/pages/default/request']) : $_SERVER['REQUEST_URI']
    ]);

        $genres = \app\modules\jobs\models\Genries::getDropdownList('name', 'genreID');
        $form->field($model, 'title')->render();
        $form->field($model, 'genres')->dropDownList($genres, ['id' => 'movieType'])->render();
        $form->field($model, 'duration')->render();
        $form->field($model, 'link')->render();
        $form->field($model, 'first_name')->render();
        $form->field($model, 'last_name')->render();
        $form->field($model, 'second_name')->render();

        $form->field($model, 'operator_first_name')->render();
        $form->field($model, 'operator_last_name')->render();
        $form->field($model, 'operator_second_name')->render();

        $form->field($model, 'scenarist_first_name')->render();
        $form->field($model, 'scenarist_last_name')->render();
        $form->field($model, 'scenarist_second_name')->render();

        $form->field($model, 'producer_first_name')->render();
        $form->field($model, 'producer_last_name')->render();
        $form->field($model, 'producer_second_name')->render();
        $form->field($model, 'phone')->render();
        $form->field($model, 'email')->render();
        $form->field($model, 'text')->render();
        $form->field($model, 'image')->render();
        $form->field($model, 'terms')->textInput(['id' => 'uesrReglamentRead'])->render();
        $form->field($model, 'perms')->textInput(['id' => 'userDataAgree'])->render();

    ?>
        <div class="form-group--text form-wrapper">
            <div class="form-section">
                <h3 class="form-title">Подать свой фильм на фестиваль</h3>
                <div class="form-row">
                    <div class="form-group form-group--movie-name field-requestform-title">
                        <label for="movieName" class="control-label">Оригинальное название фильма</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'title',[
                            'placeholder' => 'Введите название фильма',
                            'onfocus' => "if(this.placeholder  == 'Введите название фильма') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Введите название фильма'; }",
                            'class' => 'form-control'
                        ])?>
                        <p class="info small">Полное название фильма</p>
                        <?= \yii\helpers\Html::error($model, 'title', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--genre field-movieType">
                        <label for="movieType" class="control-label">Жанр</label>
                        <div class="select-wrapper">
                            <?= \yii\helpers\Html::activeDropDownList($model, 'genres', $genres, [
                                'class' => 'form-control',
                                'multiple' => 'multiple',
                                'id' => 'movieType'
                            ])?>
                        </div>
                        <p class="info small">Например: мелодрама, ужасы, триллер</p>
                        <?= \yii\helpers\Html::error($model, 'genres', ['class' => 'help-block'])?>
                    </div>
                </div><!-- end .row -->
                <div class="form-row">
                    <?= $form->field($model, 'year')->dropDownList([
                        // date('Y', strtotime('- 2 Year')) => date('Y', strtotime('- 2 Year')),
                        date('Y', strtotime('- 1 Year')) => date('Y', strtotime('- 1 Year')),
                        date('Y') => date('Y'),
                    ])?>
                    <?= $form->field($model, 'language')->dropDownList(\app\modules\jobs\models\Jobs::$languages)?>
                    <div class="form-group form-group--duration field-requestform-duration">

                        <label for="movieDuration" class="control-label">Длительность</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'duration',[
                            'placeholder' => '__мм __сс',
                            'class' => 'form-control js-masked'
                        ])?>
                        <?= \yii\helpers\Html::error($model, 'duration', ['class' => 'help-block'])?>
                    </div>
                </div><!-- end .row -->
                <div class="form-row">
                    <div class="form-group form-group--link field-requestform-link">
                        <label for="movieLink" class="control-label">Ссылка на скачивание</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'link',[
                            'placeholder' => 'Укажите ссылку на фильм',
                            'onfocus' => "if(this.placeholder  == 'Укажите ссылку на фильм') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Укажите ссылку на фильм'; }",
                            'class' => 'form-control'
                        ])?>
                        <p class="info small">Google Drive, DropBox, Яндекс.Диск</p>
                        <?= \yii\helpers\Html::error($model, 'title', ['class' => 'help-block'])?>
                    </div>
                </div><!-- end .row -->
            </div><!-- end .form-section -->

            <div class="form-section">
                <h3 class="form-title">ОПИСАНИЕ ФИЛЬМА И ПОСТЕР</h3>
                <div class="form-row">
                    <div class="form-group form-group--description field-requestform-text">
                        <label for="movieDescription" class="control-label">Краткий синопсис к фильму</label>
                        <?= \yii\helpers\Html::activeTextarea($model, 'text',[
                            'placeholder' => 'Расскажите про свой фильм',
                            'onfocus' => "if(this.placeholder  == 'Расскажите про свой фильм') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Расскажите про свой фильм'; }",
                            'class' => 'form-control textarea'
                        ])?>
                        <?= \yii\helpers\Html::error($model, 'text', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--image field-requestform-image">
                        <div class="cropper cropper-post-cover">
                            <button type="button" class="btn--cropper link js-upload-trigger">Добавить фото для постера</button>
                            <div class="cropit-preview"></div>
                            <input type="file" class="cropit-image-input" name="movie_cover" id="movieCover" />
                            <input type="hidden" class="cropit-image-input-hidden" name="movie_cover_url" id="movieCoverUrl" value="" />
                            <button type="button" class="btn btn--remove hidden" title="Удалить фото"><span class="icon icon--remove link--image"></span></button>
                            <button type="button" class="btn btn--reupload link js-upload-trigger hidden">Заменить постер</button>
                            <?= \yii\helpers\Html::error($model, 'image', ['class' => 'help-block'])?>
                        </div><!-- end .cropper -->
                        <p class="tip">Рекомендуемый размер&nbsp;постера 230x340пкс</p>
                    </div>
                </div><!-- end .form-row -->
            </div><!-- end .form-section -->

            <div class="form-section">
                <h3 class="form-title">КОНТАКТНАЯ ИНФОРМАЦИЯ</h3>
                <div class="form-row">
                    <div class="form-group form-group--name field-requestform-first_name">
                        <label for="userName" class="control-label">Полное имя</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'first_name',[
                            'placeholder' => 'Имя',
                            'onfocus' => "if(this.placeholder  == 'Имя') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Имя'; }",
                            'class' => 'form-control'
                        ])?>

                        <?= \yii\helpers\Html::error($model, 'first_name', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--name field-requestform-last_name">
                        <label for="userSecondName" class="control-label">Фамилия</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'last_name',[
                            'placeholder' => 'Фамилия',
                            'onfocus' => "if(this.placeholder  == 'Фамилия') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Фамилия'; }",
                            'class' => 'form-control'
                        ])?>

                        <?= \yii\helpers\Html::error($model, 'last_name', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--name field-requestform-second_name">
                        <label for="userFathersName" class="control-label">Отчество</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'second_name',[
                            'placeholder' => 'Отчество',
                            'onfocus' => "if(this.placeholder  == 'Отчество') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Отчество'; }",
                            'class' => 'form-control'
                        ])?>

                        <?= \yii\helpers\Html::error($model, 'second_name', ['class' => 'help-block'])?>
                    </div>
                </div><!-- end .form-row -->
                <div class="form-row">
                    <div class="form-group form-group--phone field-requestform-phone">
                        <label for="userPhone" class="control-label">Телефон</label>
                        <div class="phone-group clearfix">
                            <span class="phone-helper pull-left">+7</span>
                            <?= \yii\helpers\Html::activeTextInput($model, 'phone',[
                                'placeholder' => '___ ___ __ __',
                                'onfocus' => "if(this.placeholder  == '___ ___ __ __') {this.placeholder = '';}",
                                'onblur' => "if(this.value == '') { this.placeholder = '___ ___ __ __'; }",
                                'class' => 'form-control pull-right'
                            ])?>

                            <?= \yii\helpers\Html::error($model, 'phone', ['class' => 'help-block'])?>
                        </div>
                    </div>
                    <div class="form-group form-group--email field-requestform-email">
                        <label for="userEmail" class="control-label">E-mail</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'email',[
                            'placeholder' => 'Электронная почта',
                            'onfocus' => "if(this.placeholder  == 'Электронная почта') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Электронная почта'; }",
                            'class' => 'form-control'
                        ])?>

                        <?= \yii\helpers\Html::error($model, 'email', ['class' => 'help-block'])?>
                    </div>
                </div><!-- end .form-row -->
            </div><!-- end .form-section -->

            <div class="form-section">
                <h3 class="form-title">Режиссер</h3>
                <div class="form-row">
                    <div class="form-group form-group--name field-requestform-producer_first_name">
                        <label for="userName" class="control-label">Полное имя</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'producer_first_name',[
                            'placeholder' => 'Имя',
                            'onfocus' => "if(this.placeholder  == 'Имя') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Имя'; }",
                            'class' => 'form-control'
                        ])?>

                        <?= \yii\helpers\Html::error($model, 'producer_first_name', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--name field-requestform-producer_last_name">
                        <label for="userSecondName" class="control-label">Фамилия</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'producer_last_name',[
                            'placeholder' => 'Фамилия',
                            'onfocus' => "if(this.placeholder  == 'Фамилия') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Фамилия'; }",
                            'class' => 'form-control'
                        ])?>

                        <?= \yii\helpers\Html::error($model, 'producer_last_name', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--name field-requestform-producer_second_name">
                        <label for="userFathersName" class="control-label">Отчество</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'producer_second_name',[
                            'placeholder' => 'Отчество',
                            'onfocus' => "if(this.placeholder  == 'Отчество') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Отчество'; }",
                            'class' => 'form-control'
                        ])?>
                        <?= \yii\helpers\Html::error($model, 'producer_second_name', ['class' => 'help-block'])?>
                    </div>
                </div><!-- end .form-row -->
            </div><!-- end .form-section -->
            <div class="form-section">
                <h3 class="form-title">Сценарист</h3>
                <div class="form-row">
                    <div class="form-group form-group--name field-requestform-scenarist_first_name">
                        <label for="userName" class="control-label">Полное имя</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'scenarist_first_name',[
                            'placeholder' => 'Имя',
                            'onfocus' => "if(this.placeholder  == 'Имя') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Имя'; }",
                            'class' => 'form-control'
                        ])?>
                        <?= \yii\helpers\Html::error($model, 'scenarist_first_name', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--name field-requestform-scenarist_last_name">
                        <label for="userSecondName" class="control-label">Фамилия</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'scenarist_last_name',[
                            'placeholder' => 'Фамилия',
                            'onfocus' => "if(this.placeholder  == 'Фамилия') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Фамилия'; }",
                            'class' => 'form-control'
                        ])?>
                        <?= \yii\helpers\Html::error($model, 'scenarist_last_name', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--name field-requestform-scenarist_second_name">
                        <label for="userFathersName" class="control-label">Отчество</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'scenarist_second_name',[
                            'placeholder' => 'Отчество',
                            'onfocus' => "if(this.placeholder  == 'Отчество') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Отчество'; }",
                            'class' => 'form-control'
                        ])?>
                        <?= \yii\helpers\Html::error($model, 'scenarist_second_name', ['class' => 'help-block'])?>
                    </div>
                </div><!-- end .form-row -->
            </div><!-- end .form-section -->
            <div class="form-section">
                <h3 class="form-title">Оператор</h3>
                <div class="form-row">
                    <div class="form-group form-group--name field-requestform-operator_first_name">
                        <label for="userName" class="control-label">Полное имя</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'operator_first_name',[
                            'placeholder' => 'Имя',
                            'onfocus' => "if(this.placeholder  == 'Имя') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Имя'; }",
                            'class' => 'form-control'
                        ])?>
                        <?= \yii\helpers\Html::error($model, 'operator_first_name', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--name field-requestform-operator_last_name">
                        <label for="userSecondName" class="control-label">Фамилия</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'operator_last_name',[
                            'placeholder' => 'Фамилия',
                            'onfocus' => "if(this.placeholder  == 'Фамилия') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Фамилия'; }",
                            'class' => 'form-control'
                        ])?>
                        <?= \yii\helpers\Html::error($model, 'operator_last_name', ['class' => 'help-block'])?>
                    </div>
                    <div class="form-group form-group--name field-requestform-operator_second_name">
                        <label for="userFathersName" class="control-label">Отчество</label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'operator_second_name',[
                            'placeholder' => 'Отчество',
                            'onfocus' => "if(this.placeholder  == 'Отчество') {this.placeholder = '';}",
                            'onblur' => "if(this.value == '') { this.placeholder = 'Отчество'; }",
                            'class' => 'form-control'
                        ])?>
                        <?= \yii\helpers\Html::error($model, 'operator_second_name', ['class' => 'help-block'])?>
                    </div>
                </div><!-- end .form-row -->
            </div><!-- end .form-section -->

            <div class="form-section">
                <div class="form-group checkbox field-uesrReglamentRead">
                    <?= \yii\helpers\Html::checkbox('RequestForm[terms]', true, ['id' => 'uesrReglamentRead'])?>

                    <label for="uesrReglamentRead" class="control-label">Я ознакомлен(а) и принимаю условия, описанные в <a href="#" class="link" data-toggle="modal" data-target="#modalReglament">Регламенте кинофестиваля</a></label>

                </div>
                <div class="form-group checkbox field-userDataAgree">
                    <?= \yii\helpers\Html::checkbox('RequestForm[perms]', true, ['id' => 'userDataAgree'])?>
                    <label for="userDataAgree" class="control-label">Я предоставляю согласие Республиканскому фестивалю короткометражного кино на сбор, накопление, хранение и обработку моих персональных данных</label>
                </div>
            </div><!-- end .form-section -->

            <div class="form-footer">
                <?= \yii\helpers\Html::submitInput(\Yii::t('front', 'Подать заявку'), [
                    'class' => 'btn btn--primary',
                    'onclick' => "$('#requestform-image').val($('.cropper').cropit('export'))"
                ]) ?>

            </div>
        </div><!-- end .form-wrapper -->

        <div class="form-scheme">
            <ul class="form-scheme-list">
                <li class="form-scheme-item">
                    <h3 class="form-scheme-item-title">Подайте заявку</h3>
                    <p class="form-scheme-item-description">Расскажите про заявку, почему решили снимать в этом жанре и тд</p>
                </li>

                <li class="form-scheme-item">
                    <h3 class="form-scheme-item-title">Время для модерации</h3>
                    <p class="form-scheme-item-description">Модератор рассмотрит Вашу работу. В случае одобрения Ваша работа появится на сайте</p>
                </li>

                <li class="form-scheme-item">
                    <h3 class="form-scheme-item-title">Участвуйте в конкурсе</h3>
                    <p class="form-scheme-item-description">Теперь Вы являетесь официальным участником конкурса!</p>
                </li>
            </ul>
        </div><!-- end .form-scheme -->

        <?= \yii\helpers\Html::activeHiddenInput($model, 'image'); ?>
        <?php \yii\widgets\ActiveForm::end();
    else:
        $this->registerJs('$(\'#modalResult\').modal(\'show\');');
    endif; ?>
</section><!-- end .form-application -->
<script>
    var send = function() {
        if($('.cropper').cropit('export')) {
            $('#requestform-image').val($('.cropper').cropit('export'))
            $('.form-group--image').removeClass('has-error');
        } else {
            $('.form-group--image').addClass('has-error');
            return false;
        }
    }


</script>