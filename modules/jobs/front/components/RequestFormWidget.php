<?php
/**
 * Created by PhpStorm.
 * User: Vitaliy Prokhonenkov
 * Date: 06.05.2016
 * Time: 14:59
 */

namespace app\modules\jobs\front\components;

use app\modules\jobs\models\RequestForm;
use yii\base\Widget;

class RequestFormWidget extends Widget
{
    public $isRequest = false;
    public $isSend = false;
    public $isInside = true;
    public function run()
    {
        $model = new RequestForm();
        $model->isRequest = $this->isRequest;
        $model->load(\Yii::$app->request->post());
        return $this->render($this->isRequest ? 'request-form-full' : 'request-form',[
            'model' => $model,
            'isRequest' => $this->isRequest,
            'isSend' => $this->isSend,
            'isInside' => $this->isInside
        ]);
    }
}