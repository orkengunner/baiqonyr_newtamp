<?php

namespace app\modules\jobs\front\controllers;

use app\components\FrontController;
use app\components\Pagination;
use app\modules\jobs\models\Genries;
use app\modules\jobs\models\Jobs;
use app\modules\jobs\models\JobsCategories;
use app\modules\jobs\models\JobsGenries;
use app\modules\jobs\models\Online;
use app\modules\jobs\models\RequestForm;
use app\modules\languages\models\Languages;
use app\modules\nominations\models\Nominations;
use app\modules\pages\models\Pages;
use app\modules\participants\models\Participants;
use app\modules\users\models\Users;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class DefaultController extends FrontController
{

        public $count = 20;

    public function actionIndex($category = null, $t = null, $type = 'applicant')
    {
        $model = Pages::findBySlug('participants');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $genres = Genries::findAllDD();
        $where = [];

        $typeID = null;
        if($type) {
            $typeID = array_search($type, Jobs::$jobsTypesUrl);
            $where = ['jobs.is_participant' => $typeID];
        }
        $participants = Jobs::findLastJobs(null, true, ['publish_date' => SORT_DESC], $where);
        /*if($genre) {
            $participants->andWhere([
                'gc.genre_id' => $genre
            ]);
        }*/

        if($category) {
            $participants->andWhere([
                'jobs.category_id' => $category
            ]);
        }
        if($t) {
            $participants->andWhere([
                'like', 'jobs.title', $t
            ]);
        }
        $countQuery = clone $participants;

        $pager = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => $this->count]);

        $participants = $participants
            ->limit($this->count)
            ->offset($pager->offset)
            ->asArray()
            ->all();
        $_genres = [];

        $usedGenres = \Yii::$app->getCache()->get('usedGenres_' . Languages::getCurrent()->id . \Yii::$app->language . $type);
        if ($usedGenres === false) {
            $usedGenres = JobsGenries::findGenresIDs($typeID);
           \Yii::$app->getCache()->set('usedGenres_' . Languages::getCurrent()->id . \Yii::$app->language . $type, $usedGenres, 3600*24*7);
        }
        $usedGenres = JobsGenries::findGenresIDs($typeID);


        foreach($usedGenres as $v) {
            $_genres[$v] = $genres[$v]??null;
        }

        asort($_genres);

        //$categories = JobsCategories::getDropdownList();
        $categories =
            ArrayHelper::map(
                JobsCategories::find()
                    ->innerJoinWith('jobs')
                    ->andWhere(['jobs.is_participant' => $typeID])
                    ->all(),
                'id',
                'title'
            );
//print_r($_genres);exit;
        return $this->render('index', [
            'participants' => $participants,
            'genres' => $_genres,
            'genre' => null,
            'pager' => $pager,
            'type' => $type,
            'categories' => $categories,
            'category' => $category
        ]);
    }

    public function actionWinners()
    {
        $this->cssClass = 'main--movie';
        $model = Pages::findBySlug('winners');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $nominations = Nominations::findNomination();
        $winners = Jobs::_findWinners();

        $_winners = [];
        foreach ($winners as $winner) {
        	$_winners[$winner['id']] = $winner;
		}

        $genres = Genries::findAllDD();
        return $this->render('winners', [
            'winners' => $_winners,
            'nominations' => $nominations,
            'genres' => $genres,
			'model' => $model
        ]);
    }

    public function actionView($job_slug)
    {
        \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'] =
            ArrayHelper::merge(
                \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'],
                ['ajax', 'comments']
            );
        $this->cssClass = 'main--movie';

        $jobs = new Jobs();
        $jobs::$oldFest = true;
        $model = $jobs::getBySlug($job_slug, true);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        $this->setMeta(
            $model->metaTitle,
            $model->metaDescription,
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $genres = Genries::findAllDD();

        $participants = Participants::findByJobID($model->id);

        $_participants = [];
        foreach($participants as $v) {
            $_participants[$v['post_id']][] = $v;
        }

        $salt = null;
        if($model->brod_online_id) {
            $mktime = microtime();
            $salt = $this->getHash($mktime);
            \Yii::$app->session->set('online_view', ['oid' => $mktime]);
        }

        Jobs::updateAllCounters(['views' => 1], 'id = ' . $model->id);

        $isVideoAllow = false;

        if(!\Yii::$app->user->isGuest && (in_array(Users::PERMISSION_BAYKONUR_JURI, $this->_user->permissions) || \Yii::$app->user->id == Users::USER_ADMIN)) {
            $isVideoAllow = true;
        }

        return $this->render('view', [
            'model' => $model,
            'genres' => $genres,
            'participants' => $_participants,
            'salt' => $salt,
            'isVideoAllow' => $isVideoAllow
        ]);
    }

    public function actionBrodView($oid, $lid)
    {

        $session = \Yii::$app->session->get('online_view');
        //print_r($session);exit;
        //if (\Yii::$app->request->get('m') == 1)

        //if ((!isset($session['oid']) || $session['oid'] == '' || $salt != $this->getHash($session['oid'])) && !isset($_SERVER['HTTP_RANGE'])){
        if (
            !isset($_SERVER['HTTP_RANGE']) &&
            (
                (
                    isset($_SERVER['HTTP_ACCEPT_ENCODING']) &&
                    (
                        strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false ||
                        strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'deflate') !== false
                    )
                ) ||
                (
                    isset($_SERVER['HTTP_ACCEPT']) &&
                    strpos($_SERVER['HTTP_ACCEPT'], 'text/html') !== false
                )
            )
        )
        {
            throw new NotFoundHttpException();
        }

        \Yii::$app->session->set('online_view', ['oid' => '']);

        $file = Online::getFilesByID($oid, $lid);

        header('Content-Type: unknown');
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header('Location: http://brod.kz/media/online/' . ($file['oTypeID'] == Online::LANG_RU || $file['oTypeID'] == Online::LANG_KZ ? $file['name'] : $file['file']) /*. ($oid == 16939 || $oid == 17252 ? '' : '.exe')*/, true, 301);
        die();

    }

    public function getHash($microtime)
    {
        $key=base64_encode('4F6vg1');
        $ip = base64_encode($_SERVER['REMOTE_ADDR']);
        $microtime = base64_encode($microtime);
        $ip = md5($ip.$microtime.$key);
        return sha1($ip);
    }

    public function actionGetData()
    {
        $genres = [];
        foreach(Genries::findAllDD() as $k => $v) {
            $genres[] = [
                'id' => $k,
                'title' => $v
            ];
        }

        $categories = [];
        foreach(JobsCategories::getDropdownList() as $k => $v) {
            $categories[] = [
                'id' => $k,
                'title' => $v
            ];
        }


        $labels = RequestForm::attributeLabelsData();
        $placeholders = RequestForm::attributePlaceholdersData();
        $imageNotice = RequestForm::imageNotice();

        $data = [
            'genres' => $genres,
            'categories' => $categories,
            'labels' => $labels,
            'placeholders' => $placeholders,
            'imageNotice' => $imageNotice
        ];

       // print_r($data);exit;
        echo json_encode($data);exit;
    }

    public function actionGetCategories()
    {
        echo json_encode(JobsCategories::getDropdownList());exit;
    }
}
