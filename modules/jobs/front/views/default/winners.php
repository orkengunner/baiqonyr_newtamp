<?php
use \app\modules\nominations\models\Nominations;
?>
<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-winners', 'Победители');?></h1>
        <h3 class="title-section title-section--primary"><?= \Yii::t('front-winners', 'Победители');?></h3>
    </div>
</div>

<div class="main-body">
    <section class="articles">
        <div class="container">

            <div class="movie-tab-description"><?= $model->text; ?></div>

            <h3 class="title--secondary"><?= \Yii::t('front-winners', Nominations::$types[Nominations::TYPE_WORLD])?></h3>
            <section class="winners">
				<?php foreach($nominations as $nomination):
					if($nomination->type != Nominations::TYPE_WORLD) {
						continue;
					}
					$winner = $winners[$nomination->id] ?? null;
					?>
					<?= $this->render('_winner', [
					'winner' => $winner,
					'nomination' => $nomination,
					'genres' => $genres
				]);?>
				<?php endforeach; ?>

            </section>

            <h3 class="title--secondary"><?= \Yii::t('front-winners', Nominations::$types[Nominations::TYPE_NATIONAL])?></h3>
            <section class="winners">
                <?php foreach($nominations as $nomination):
                    if($nomination->type != Nominations::TYPE_NATIONAL) {
                        continue;
                    }
                    $winner = $winners[$nomination->id] ?? null;
                ?>
                    <?= $this->render('_winner', [
                        'winner' => $winner,
                        'nomination' => $nomination,
                        'genres' => $genres
                    ]);?>
                <?php endforeach; ?>

            </section>
        </div><!-- end .container -->
    </section>
</div>