<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 08.08.2019
 * Time 11:25
 */
?>

<div class="winner">
	<div class="film">
		<?php if($winner): ?>
			<a href="<?= $winner['slug'] ? \yii\helpers\Url::to(['/jobs/default/view', 'job_slug' => $winner['slug']]) : 'javascript:;'?>" class="link--overlay"></a>
		<?php endif; ?>

		<figure class="afisha">
                                <span>
                                    <?php if($winner): ?>
										<img src="<?= $winner['fa_logo'] ? \yii\helpers\Url::imgUrl($winner['fa_logo'], 'jobs', [220, 313]) : '/images/1-2.png'?>" alt="">
									<?php else: ?>
										<img src="<?= $nomination->fa_logo ? \yii\helpers\Url::imgUrl($nomination->fa_logo, 'nominations', [269, 396]) : '/images/1-2.png'?>" alt="">
									<?php endif; ?>
                                </span>
			<figcaption><?= $winner['title']; ?></figcaption>
		</figure>

		<span class="label--default label--primary label--news link--label"><?= $nomination->title; ?></span>
		<?php if($winner): ?>
			<?php if($winner['genres']):?>
				<p class="info small"><?= implode(', ', array_map(function($_v) use ($genres) {
						return $genres[$_v];
					}, explode(';', $winner['genres']))); ?>, <?= $winner['year']?></p>
			<?php endif; ?>
			<p class="info">
				<?= $winner['first_name'] . ' ' . $winner['last_name'] . (
				($winner['first_name_2'] && $winner['last_name_2']) ? ', ' . $winner['first_name_2'] . ' ' . $winner['last_name_2'] : ''
				); ?>
			</p>
		<?php endif; ?>

	</div>
</div>
