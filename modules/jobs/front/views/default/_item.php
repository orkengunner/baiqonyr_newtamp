<li class="movies-item">
    <div class="movies-item-body">
        <a href="<?= \yii\helpers\Url::to(['/jobs/default/view', 'job_slug' => $v['slug']])?>" class="link--overlay"></a>
        <figure class="movies-item-cover"><img src="<?= \yii\helpers\Url::imgUrl($v['fa_logo'], 'jobs', [167, 239])?>"></figure>
        <?php if($v['is_participant'] == \app\modules\jobs\models\Jobs::IS_OUT_COMPETITION): ?>
            <span class="label--default label--primary label--news link--label"><?= \Yii::t('front-jobs', 'Внеконкурсная работа'); ?></span>
        <?php endif; ?>
        <h5 class="movies-item-title"><?= $v['title']; ?></h5>
    </div>
    <div class="movies-item-description"><?= implode(', ', array_map(function($v) use ($genres) {
            return isset($genres[$v]) ? $genres[$v] : '';
        }, explode(';', $v['genres']))); ?>, <?=$v['year']?></div>
    <?php
        $authors = [];
        foreach(explode(';', $v['partic']) as $_v){
            $p = explode(':', $_v);
            if($p[0] == \app\modules\lookupposts\models\LookupPosts::POST_PRODUCER) {
                $authors[] = $p[1];
            }
        }
    ?>
    <div class="movies-item-author"><span class="highlight"><?= \Yii::t('front-jobs', count($authors) > 1 ? 'Режиссеры' : 'Режиссер')?></span>: <?= $authors ? implode(', ', $authors) : ($v['first_name'] . ' ' . $v['last_name'] . ($v{'last_name_2'} && $v['first_name_2'] ? ', ' .  $v['first_name_2'] . ' ' . $v['last_name_2'] : '' )); ?></div>
    <div class="movies-meta">
        <span class="movies-meta-item meta-date"><?= \yii\helpers\StringHelper::rudate(['j F,Y', 'j F'], strtotime($v['publish_date']))?></span>
        <span class="movies-meta-item meta-views"><span class="icon icon--views"></span><?= number_format($v['views'], 0, '.', ' ')?></span>
        <!--span class="movies-meta-item meta-comments"><span class="icon icon--comments"></span>542</span-->
    </div>
    <div class="movies-item-rating clearfix">
        <div class="starbox" data-rating="0.72"></div>
        <span class="movies-item-rating-total">7,3</span>
    </div><!-- end .movies-item-rating -->
</li>