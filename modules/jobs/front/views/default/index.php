
<div class="main-head">
    <div class="container">
        <h1><?= Yii::t('front-jobs', 'Фильмы'); ?></h1>
        <h3 class="title-section title-section--primary"><?= Yii::t('front-jobs', 'Фильмы'); ?></h3>
    </div>
</div>

<div class="main-body">
    <div class="container clearfix">
        <?php if($participants/* || $category || (!$participants && $type)*/): ?>
            <form class="form form--secondary form--search pull-right">
                <div class="form-group">
                    <input class="form-control" type="text" name="t" value="<?= \Yii::$app->request->get('t')?>" id="searchWord" placeholder="<?= Yii::t('front-jobs', 'Найти на Байконуре'); ?>">
                    <button type="button" class="icon icon--search" onclick="$('.form--search').submit();">
                        <span class="fa fa-search"></span>
                    </button>
                </div>
            </form>
        <?php endif; ?>

        <?php if(1 || $participants/* || $category || (!$participants && $type)*/): ?>
            <section class="inner-nav inner-nav-participants">
                <ul class="nav-pills nav--default nav--secondary">
                    <li <?= $type == 'applicant' ? 'class="active"' : ''; ?>><a class="link link--primary" href="<?= \yii\helpers\Url::to(['/participants'])?>"><?= Yii::t('front-jobs', 'Заявители'); ?></a></li>
                    <li <?= !$type  || $type == 'active' ? 'class="active"' : ''; ?>><a class="link link--primary"  href="<?= \yii\helpers\Url::to(['/participants/active'])?>"><?= Yii::t('front-jobs', 'Участники'); ?></a></li>
                    <li <?= $type == 'out-of-competition' ? 'class="active"' : ''; ?>><a class="link link--primary" href="<?= \yii\helpers\Url::to(['/participants/out-of-competition'])?>"><?= Yii::t('front-jobs', 'Вне конкурса'); ?></a></li>
                </ul>
            </section>
        <?php endif; ?>

        <?php if($categories): ?>
            <div class="control-bar">
                <div class="control-bar-filters">
                    <div class="filters--primary">
                        <a href="<?= \yii\helpers\Url::to(['/participants/' . $type])?>" class="btn btn--filter<?= !$category ? ' active' : ''; ?>"><?= Yii::t('front-jobs', 'Все категории'); ?></a>
                        <?php foreach($categories as $k => $v): ?>
                            <a href="<?= \yii\helpers\Url::to(['/participants/' . $type, 'category' => $k])?>" class="btn btn--filter<?= $category == $k ? ' active' : ''; ?>"><?= Yii::t('front-jobs', $v); ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div><!-- end .control-bar -->
        <?php endif; ?>

        <div class="movies">
            <?php if($participants): ?>
                <p class="movie-tab-description"><?= Yii::t('front-jobs', 'До участия в конкурсе допускаются режиссеры, чьи фильмы прошли проверку отборочной комиссии, на данном этапе вы являетесь заявителем.  Далее следует проверка основного жюри, и успешно прошедшие являются участниками. А также если вы участник вне конкурса ваша работа помечается флажком в списке.'); ?></p>
            <?php endif;?>
            <?php if($participants): ?>
                <ul class="movies-list">
                    <?php foreach($participants as $v) echo $this->render('_item', ['v' => $v, 'genres' => $genres]) ?>
                </ul>
            <?php else: ?>
                <div class="soon">
                    <p class="text"><?= \Yii::t('front-jobs', 'Фильмы появятся на сайте после отбора заявок.');?></p>
                </div>
            <?php endif; ?>

            <?php echo \app\components\widgets\LinkPager::widget([
                'pagination' => $pager,
                'options' => ['class' => 'pagination--primary pagination--industry'],
                'activePageCssClass' => 'active',
                'linkClass' => 'link pagination-link',
                'tag' => 'li',
                'tagClass' => 'pagination-item',
                'nextPageLabel' => '<span aria-hidden="true">»</span>',
                'prevPageLabel' => '<span aria-hidden="true">«</span>',
                'nextPageCssClass' => 'link pagination-link',
                'prevPageCssClass' => 'link pagination-link',
                'registerLinkTags' => true,
                'maxButtonCount' => 9
            ]);?>
        </div><!-- end .movies -->
    </div><!-- end .container -->
</div>