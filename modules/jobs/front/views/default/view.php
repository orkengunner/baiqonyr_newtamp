<?php

if($isVideoAllow) {
    $this->registerJsFile('/js/uppod-0.7.2.js', ['position' => \yii\web\View::POS_HEAD]);
    $this->registerJsFile('/media/uppod/st/video214-91.js', ['position' => \yii\web\View::POS_HEAD]);

    $this->registerJs('
        setTimeout(function(){
                this.videoplayer = new Uppod({
                    m:"video",comment:"' . $model->title . '",
                    uid:"videoplayer",
                    file:"' . (!$salt ? \yii\helpers\Url::imgUrl($model->fa_video, 'jobs') : '/jobs/default/brod-view/?oid=' . $model->brod_online_id . '&lid=' . $model->language . '&salt=' . $salt) . '",
                    poster:"' . \yii\helpers\Url::imgUrl($model->fa_logo, 'jobs') . '",
                    st:uppodvideo,
                    onReady: function(uppod){
                        uppod.Stop();
                    }
            });
        }, 500);
    ');
}


?>

    <div class="main-head">
        <div class="container">
            <h1><?= $model->title; ?><</h1>
            <h3 class="title-section title-section--primary"><?= $model->title; ?></h3>
        </div>
    </div>

    <div class="main-body">
        <section class="about">
            <div class="container">
                <div class="grid">

                    <div class="grid-item grid-item--content">
                        <?= \app\modules\mainpage\front\components\BreadCrumbs::widget([
                            'data' => [
                                \Yii::t('front-jobs', 'Участники') => ['/jobs/default/index'],
                                $model['title'] => ''
                            ]
                        ])?>
                        <div class="movie">

                            <div class="movie-body clearfix">
                                <div class="movie-hero pull-left">
                                    <figure class="movie-cover"><img src="<?=\yii\helpers\Url::imgUrl($model->fa_logo, 'jobs', [200, 290])?>" alt=""></figure>
                                </div>

                                <p class="info small"><?= implode(', ', array_map(function($v) use($genres) {
                                        return $genres[$v];
                                    }, explode(';', $model->fa_genres)))?>, <?= $model->year; ?></p>
                                <p class="info"><?= $model->name; ?></p>
                                <ul class="movie-info">
                                    <li class="movie-info-item">
                                        <p class="movie-info-title">Автор</p>
                                        <p class="movie-info-description"><?= $model->first_name . ' ' . $model->last_name; ?></p>
                                    </li>
                                    <?php if($model->first_name_2 && $model->last_name_2): ?>
                                        <li class="movie-info-item">
                                            <p class="movie-info-title">&nbsp;</p>
                                            <p class="movie-info-description"><?= $model->first_name_2 . ' ' . $model->last_name_2; ?></p>
                                        </li>
                                    <?php endif; ?>
                                    <?php foreach($participants AS $v): ?>
                                        <li class="movie-info-item">
                                            <p class="movie-info-title"><?= $v[0]['title']?></p>
                                            <p class="movie-info-description">
                                                <?php foreach($v as $_k => $_v) {
                                                    if ($_v['link']) {
                                                        echo '<a target="_blank" href="' . $_v['link'] . '" class="link">' . $_v['name'] . '</a>';
                                                    } else {
                                                        echo $_v['name'] . ' ' . $_v['second_name'] . ' ' . $_v['last_name'];
                                                    }
                                                    if ($_k != count($v) - 1) echo ', ';
                                                }
                                                ?>

                                            </p>
                                        </li>
                                    <?php endforeach; ?>

                                    <?php if($genres): ?>
                                        <li class="movie-info-item">
                                            <p class="movie-info-title"><?= \Yii::t('front-jobs', 'Жанр'); ?></p>
                                            <p class="movie-info-description">
                                                <?= implode(', ', array_map(function($v) use($genres) {
                                                    return '<a href="' . \yii\helpers\Url::to(['/jobs/default/index', 'genre' => $v]) . '" class="link">' .  $genres[$v]. '</a>';
                                                }, explode(';', $model->fa_genres)))?>
                                            </p>
                                        </li>
                                    <?php endif; ?>
                                    <?php if($model->premier): ?>
                                        <li class="movie-info-item">
                                            <p class="movie-info-title"><?= \Yii::t('front-jobs', 'Премьера (KZ)'); ?></p>
                                            <p class="movie-info-description"><?= $model->premier; ?></p>
                                        </li>
                                    <?php endif; ?>
                                    <?php if($model->duration): ?>
                                        <li class="movie-info-item">
                                            <p class="movie-info-title"><?= \Yii::t('front-jobs', 'Продолжительность'); ?></p>
                                            <p class="movie-info-description"><?= $model->duration; ?></p>
                                        </li>
                                    <?php endif; ?>
                                    <?php if($model->places): ?>
                                        <li class="movie-info-item">
                                            <p class="movie-info-title"><?= \Yii::t('front-jobs', 'Производство'); ?></p>
                                            <p class="movie-info-description"><?= implode(', ', array_map(function($v){
                                                    return '<a target="_blank" href="//brod.kz/places/' . $v->url . '/" class="link">' . $v->title . '</a>';
                                                }, $model->places));?></p>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div><!-- end .movie-body -->
                            <div class="movie-info-synopsis">
                                <?= $model->text; ?>
                            </div>

                            <?= $this->render('@app/views/layouts/front/social_shares', ['js' => true]); ?>
                        </div><!-- end .movie -->

                        <div class="movie-watch">
                            <!-- <div class="movie-watch-head clearfix">
                              <h3 class="title title-section--primary">Смотреть онлайн</h3>
                            </div> -->
                            <div class="movie-watch-body">
                                <!-- <ul class="watch-meta">
                                  <li class="dropdown dropdown--dashed watch-meta-item">
                                    <button id="seasonMenu" type="button" class="btn btn--dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <span class="dropdown-status">Трейлер</span>
                                      <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="seasonMenu">
                                      <li class="dropdown-menu-item link active">Трейлер</li>
                                      <li class="dropdown-menu-item link">Трейлер 2</li>
                                    </ul>
                                  </li>
                                  <li class="dropdown dropdown--dashed watch-meta-item">
                                    <button id="seasonMenu" type="button" class="btn btn--dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <span class="dropdown-status">1 сезон</span>
                                      <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="seasonMenu">
                                      <li class="dropdown-menu-item link active">1 сезон</li>
                                      <li class="dropdown-menu-item link">2 сезон</li>
                                      <li class="dropdown-menu-item link">3 сезон</li>
                                      <li class="dropdown-menu-item link">4 сезон</li>
                                      <li class="dropdown-menu-item link">5 сезон</li>
                                    </ul>
                                  </li>
                                  <li class="dropdown dropdown--dashed watch-meta-item">
                                    <button id="seriesMenu" type="button" class="btn btn--dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <span class="dropdown-status">1 серия</span>
                                      <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="seriesMenu">
                                      <li class="dropdown-menu-item link active">1 серия</li>
                                      <li class="dropdown-menu-item link">2 серия</li>
                                      <li class="dropdown-menu-item link">3 серия</li>
                                      <li class="dropdown-menu-item link">4 серия</li>
                                      <li class="dropdown-menu-item link">5 серия</li>
                                    </ul>
                                  </li>
                                </ul> -->
                                <?php if($isVideoAllow && ($model->brod_online_id || $model->fa_video)): ?>
                                    <div class="video-wrapper" id="videoplayer">

                                    </div>
                                <?php endif; ?>

                            </div><!-- end .movie-watch-body -->
                        </div><!-- end .movie-watch -->

                    </div><!-- grid-item -->
                </div><!-- end .grid -->
            </div><!-- end .container -->
        </section>
        <?php echo \app\modules\comments\front\components\Comments::widget([
            'objectID' => $model->id,
            'objectTypeID' => \app\modules\comments\models\Comments::COMMENT_TYPE_BKR_JOBS,
        ]);?>
    </div>
