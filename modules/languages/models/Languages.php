<?php

namespace app\modules\languages\models;

use app\components\ActiveRecord;
use app\modules\pages\models\Pages;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "languages".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property integer $is_active
 *
 * @property Pages[] $pages
 */
class Languages extends ActiveRecord
{
    public static $current = null;
    public static $adminCurrent = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code', 'locale'], 'required'],
            [['is_active'], 'integer'],
            [['title', 'code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('languages', 'ID'),
            'title' => Yii::t('languages', 'Наименование'),
            'code' => Yii::t('languages', 'Код'),
            'locale' => 'Локаль',
            'is_active' => Yii::t('languages', 'Активность'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Pages::className(), ['lang_id' => 'id']);
    }

    public static function getLangIdByCode($code)
    {
        $language = self::find()->where(['code' => $code])->one();

        if ($language === null) {
            throw new \InvalidArgumentException('Не найден язык, с кодом ' . $code);
        }

        return $language->id;
    }

//Получение текущего объекта языка
    public static function getCurrent()
    {
        if (self::$current === null) {
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

//Установка текущего объекта языка и локаль пользователя
    public static function setCurrent($code = null)
    {
        $language = self::getLangByCode($code);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->locale;

    }

//Получения объекта языка по умолчанию
    public static function getDefaultLang()
    {
        return Yii::$app->db->cache(function () {
            return Languages::find()->where(['id' => 1])->one();
        }, 3600);
    }

//Получения объекта языка по буквенному идентификатору
    public static function getLangByCode($code = null)
    {
        if ($code === null) {
            return null;
        } else {
            $language = Languages::find()->where(['code' => $code])->one();

            if ($language === null) {
                return null;
            } else {
                return $language;
            }
        }
    }

    public static function getAdminCurrent()
    {
        if (empty(self::$adminCurrent)) {
            $langId = Yii::$app->getSession()->get('admin-language', 1);
            if (!$langId) $langId = 1;
            self::$adminCurrent = Languages::findOne($langId);

        }

        return self::$adminCurrent;
    }

    public static function currentLink($lang_code) {

        return ($lang_code ? '/' . $lang_code : '') . preg_replace('/^\/' . Languages::$current->code . '\//', '/', rtrim($_SERVER['REQUEST_URI'], '/') . '/');
    }

}
