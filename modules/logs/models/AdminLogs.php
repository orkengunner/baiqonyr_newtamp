<?php

namespace app\modules\logs\models;

use app\components\ActiveRecord;
use app\modules\users\models\Users;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "admin_logs".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $module
 * @property string $action
 * @property integer $item_id
 * @property string $message
 * @property string $create_date
 *
 * @property Users $user
 */
class AdminLogs extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'item_id'], 'integer'],
            [['module', 'action', 'message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'module' => 'Модуль',
            'action' => 'Действие',
            'item_id' => 'ID объекта',
            'message' => 'Сообщение',
            'create_date' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * Добавление сообщения в лог
     *
     * @param $module
     * @param $action
     * @param $item_id
     * @param $message
     */
    public static function log($module, $action, $item_id, $message)
    {
        $model = new AdminLogs;
        $model->module = $module;
        $model->action = $action;
        $model->item_id = $item_id;
        $model->message = $message;
        $model->user_id = Yii::$app->user->id;
        $model->save();
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
