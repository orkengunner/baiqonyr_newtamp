<?php

namespace app\modules\logs\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\logs\models\AdminLogs;

/**
 * AdminLogsSearch represents the model behind the search form about `app\modules\logs\models\AdminLogs`.
 */
class AdminLogsSearch extends AdminLogs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'item_id'], 'integer'],
            [['module', 'action', 'message', 'create_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminLogs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'item_id' => $this->item_id,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'module', $this->module])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
