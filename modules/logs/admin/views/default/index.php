<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\logs\models\AdminLogsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Лог панели управления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-logs-index">

    <div class="page-heading">
        <h1><i class="icon-tape"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="widget">
        <div class="widget-content">
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'user_id',
                        'module',
                        'action',
                        'item_id',
                        'message:html',
                        'create_date',
                        [
                            'class' => \app\components\admin\RFAActionColumn::className(),
                            'template' => '{delete}'
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
