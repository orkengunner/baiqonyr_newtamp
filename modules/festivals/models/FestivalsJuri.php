<?php

namespace app\modules\festivals\models;

use app\components\ActiveRecord;
use app\modules\juri\models\Juri;
use Yii;

/**
 * This is the model class for table "festivals_juri".
 *
 * @property string $festival_id
 * @property string $juri_id
 * @property int $type
 * @property int $position
 *
 * @property Festivals $festival
 * @property Juri $juri
 */
class FestivalsJuri extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'festivals_juri';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['festival_id', 'juri_id'], 'required'],
            [['festival_id', 'juri_id', 'type', 'position'], 'integer'],
            [['festival_id'], 'exist', 'skipOnError' => true, 'targetClass' => Festivals::className(), 'targetAttribute' => ['festival_id' => 'id']],
            [['juri_id'], 'exist', 'skipOnError' => true, 'targetClass' => Juri::className(), 'targetAttribute' => ['juri_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'festival_id' => 'Festival ID',
            'juri_id' => 'Имя',
            'name' => 'Имя',
            'type' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestival()
    {
        return $this->hasOne(Festivals::className(), ['id' => 'festival_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuri()
    {
        return $this->hasOne(Juri::className(), ['id' => 'juri_id']);
    }

    public function beforeValidate()
    {
        $this->festival_id = self::festID();
        return parent::beforeValidate();
    }
}
