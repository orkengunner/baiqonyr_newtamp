<?php

namespace app\modules\festivals\models;

use app\modules\nominations\models\Nominations;
use Yii;

/**
 * This is the model class for table "festivals_nominations".
 *
 * @property string $festival_id
 * @property string $nomination_id
 *
 * @property Nominations $nomination
 * @property Festivals $festival
 */
class FestivalsNominations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'festivals_nominations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['festival_id', 'nomination_id'], 'required'],
            [['festival_id', 'nomination_id'], 'integer'],
            [['nomination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nominations::className(), 'targetAttribute' => ['nomination_id' => 'id']],
            [['festival_id'], 'exist', 'skipOnError' => true, 'targetClass' => Festivals::className(), 'targetAttribute' => ['festival_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'festival_id' => 'Festival ID',
            'nomination_id' => 'Nomination ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomination()
    {
        return $this->hasOne(Nominations::className(), ['id' => 'nomination_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestival()
    {
        return $this->hasOne(Festivals::className(), ['id' => 'festival_id']);
    }
}
