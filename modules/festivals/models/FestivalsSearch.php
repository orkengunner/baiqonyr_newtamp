<?php

namespace app\modules\festivals\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\festivals\models\Festivals;

/**
 * FestivalsSearch represents the model behind the search form about `app\modules\festivals\models\Festivals`.
 */
class FestivalsSearch extends Festivals
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id', 'delete_by', 'lang_id'], 'integer'],
            [['title', 'description', 'date', 'criteria_evaluation', 'requirements', 'rules', 'contacts', 'program', 'way', 'reward_participants', 'delete_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Festivals::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['date' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'status_id' => $this->status_id,
            'delete_date' => $this->delete_date,
            'delete_by' => $this->delete_by,
            'lang_id' => $this->lang_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'criteria_evaluation', $this->criteria_evaluation])
            ->andFilterWhere(['like', 'requirements', $this->requirements])
            ->andFilterWhere(['like', 'rules', $this->rules])
            ->andFilterWhere(['like', 'contacts', $this->contacts])
            ->andFilterWhere(['like', 'program', $this->program])
            ->andFilterWhere(['like', 'way', $this->way])
            ->andFilterWhere(['like', 'reward_participants', $this->reward_participants]);

        return $dataProvider;
    }
}
