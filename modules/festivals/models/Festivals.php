<?php

namespace app\modules\festivals\models;

use app\components\ActiveRecord;
use app\components\LanguageActiveRecord;
use app\modules\languages\models\Languages;
use app\modules\nominations\models\Nominations;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "festivals".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $description_full
 * @property string $date
 * @property string $date_end
 * @property integer $status_id
 * @property string $criteria_evaluation
 * @property string $requirements
 * @property string $rules
 * @property string $contacts
 * @property string $program
 * @property string $way
 * @property string $request
 * @property string $reward_participants
 * @property string $delete_date
 * @property string $delete_by
 * @property integer $lang_id
 * @property integer $registration_close
 * @property string $registration_close_text
 * @property string $team
 * @property string $place
 * @property string $main_page
 * @property string $main_page_kz
 *
 * @property Languages $lang
 * @property FestivalsJuri[] $festivalsJuris
 * @property Juri[] $juris
 * @property FestivalsNominations[] $festivalsNominations
 * @property Nominations[] $nominations
 */
class Festivals extends ActiveRecord
{
    public static $statuses = [
        self::STATUS_INACTIVE => 'Неактивирован',
        self::STATUS_ACTIVE => 'Активирован',
        self::STATUS_MODERATION => 'На утверждении'
    ];

    public static $oldFest = false;

    public $_nominations;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'festivals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_end', 'title', 'description', 'description_full', 'date', 'rules', 'contacts', 'place', 'main_page', 'main_page_kz'], 'required'],
            [['team', 'description', 'registration_close_text', 'criteria_evaluation', 'requirements', 'rules', 'contacts', 'program', 'way', 'reward_participants', 'description_full', 'out_of_competition'], 'string'],
            [['description_kz', 'description_full_kz', 'request_kz', 'contacts_kz', 'rules_kz', 'program_kz', 'team_kz', 'out_of_competition_kz', 'place_kz'], 'string'],
            [['date', 'delete_date', '_nominations', 'date_end'], 'safe'],
            [['status_id', 'delete_by', 'lang_id', 'registration_close'], 'integer'],
            [['title', 'place'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'requirements' => 'Требования к работам',
            'rules' => 'Правила',
            'rules_kz' => 'Правила',
            'request_kz' => 'Запрос',
            'date' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'contacts' => 'Контакты',
            'contacts_kz' => 'Контакты',
            'program' => 'Программа',
            'program_kz' => 'Программа',
            'way' => 'Как проехать',
            'reward_participants' => 'Что получат участники',
            'criteria_evaluation' => 'Критерии оценки',
            'description' => 'О фестивале(на главной)',
            'description_kz' => 'О фестивале(на главной)',
            'description_full' => 'О фестивале(полное описание на отдельной страице)',
            'description_full_kz' => 'О фестивале(полное описание на отдельной страице)',
            'nominations' => 'Номинации',
            '_nominations' => 'Номинации',
            'request' => 'Заявка',
            'registration_close_text' => 'Текст',
            'registration_close' => 'Закрыть',
            'team' => 'Команда фестиваля',
            'team_kz' => 'Команда фестиваля',
            'out_of_competition' => 'Внеконкурсная программа',
            'out_of_competition_kz' => 'Внеконкурсная программа',
            'place' => 'Место проведения',
            'place_kz' => 'Место проведения',
            'main_page' => 'Главная страницы',
            'main_page_kx' => 'Главная страницы',
        ]);
    }

    public static function find()
    {
        $query = new FestivalsQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query->isNoDeleted(self::tableName());
        }

        if(!self::$oldFest) {
            return $query->isActive(self::tableName())->isNoDeleted(self::tableName());
        } else {
            return $query->isNoDeleted(self::tableName());
        }
    }

    public function afterFind()
    {
        parent::afterFind();

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            $this->_nominations = ArrayHelper::map($this->nominations, 'id', 'id');
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestivalsJuris()
    {
        return $this->hasMany(FestivalsJuri::className(), ['festival_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuris()
    {
        return $this->hasMany(Juri::className(), ['id' => 'juri_id'])->viaTable('festivals_juri', ['festival_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestivalsNominations()
    {
        return $this->hasMany(FestivalsNominations::className(), ['festival_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNominations()
    {
        return $this->hasMany(Nominations::className(), ['id' => 'nomination_id'])->viaTable('festivals_nominations', ['festival_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if($this->status_id == self::STATUS_ACTIVE) {
            $this->updateAll(['status_id' => self::STATUS_INACTIVE], ['status_id' => self::STATUS_ACTIVE]);
        } elseif($this->status_id == self::STATUS_MODERATION) {
            $this->updateAll(['status_id' => self::STATUS_INACTIVE], 'status_id = ' . self::STATUS_MODERATION);
        }
        if($this->date && $dt = \DateTime::createFromFormat('d/m/Y H:i', $this->date)) {
            $this->date = $dt->format('Y-m-d H:i');
        }

        if($this->date_end && $dt = \DateTime::createFromFormat('d/m/Y H:i', $this->date_end)) {
            $this->date_end = $dt->format('Y-m-d H:i');
        }

        return parent::beforeSave($insert);
    }


    public function afterSave($insert, $changedAttributes)
    {
        $this->saveRelations('festival_id', 'nomination_id', FestivalsNominations::tableName(), $this->id, $this->_nominations);

        parent::afterSave($insert, $changedAttributes);
        return true;
    }

    public static function getFest($fields = [], $year = null)
    {
        if($fields) {
            $fest = self::fest();
            if($fest && !self::$oldFest) {
                $fest = $fest->attributes;
            } else {
				$fest = self::getCacheFest($year);
            }
            $_fest = [];

            if(count($fields) > 1) {
                foreach($fields as $v) {
                    $_fest[$v] = isset($fest[$v . \Yii::$app->controller->langPostfix]) && $fest[$v . \Yii::$app->controller->langPostfix] ? $fest[$v . \Yii::$app->controller->langPostfix] : $fest[$v];
                }
            } else {
                $_fest = isset($fest[$fields[0] . \Yii::$app->controller->langPostfix]) && $fest[$fields[0] . \Yii::$app->controller->langPostfix] ? $fest[$fields[0] . \Yii::$app->controller->langPostfix] : $fest[$fields[0]];
            }
            return $_fest;
        } else {
            $fest = self::getCacheFest($year);
        }
        return $fest;
    }

    public static function getCacheFest($year = null)
	{
		$fest = Yii::$app->getCache()->get('bkr_getFest' . (int)self::$oldFest . '_' . (int)$year);
		if ($fest === false) {
			$fest = self::find();
			if(self::$oldFest) {
				$fest->andWhere([
					'<', 'YEAR(date)', new Expression('YEAR(NOW())')
				])
					->orderBy(['date' => SORT_DESC]);
			}
			if($year) {
				$fest->andWhere(['YEAR(date)' => $year]);
			}

			$fest = $fest->one();
			Yii::$app->getCache()->set('bkr_getFest' . (int)self::$oldFest . '_' . (int)$year, $fest, 3600*24*7);
		}

		return $fest;
	}

	public static function getCacheOldFests()
	{
		$fest = Yii::$app->getCache()->get('bkr_getFestOldAll');
		if ($fest === false) {
			$fest = self::find();

			$fest->andWhere([
				'<', 'YEAR(date)', new Expression('YEAR(NOW())')
			])
				->orderBy(['date' => SORT_DESC]);


			$fest = $fest->all();
			Yii::$app->getCache()->set('bkr_getFestOldAll', $fest, 3600*24*7);
		}
		return $fest;
	}
}
