<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\festivals\models\FestivalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фестивали';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivals-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <p>
        <?= Html::a(Yii::t('pages', '<i class="icon-list-add"></i> Добавить фестиваль', [
            'modelClass' => 'Pages',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'pager' => [
                    'lastPageLabel' => 'Последняя',
                    'firstPageLabel' => 'Первая'
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'title',
                    'date',
                    [
                        'attribute' => 'status_id',
                        'value' => function ($data) {
                            return \app\modules\festivals\models\Festivals::$statuses[$data->status_id];
                        },
                        'filter' => \app\modules\festivals\models\Festivals::$statuses
                    ],
                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{update} {remove}'
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
