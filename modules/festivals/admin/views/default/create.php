<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\festivals\models\Festivals */

$this->title = 'Добавить фестиваль';
$this->params['breadcrumbs'][] = ['label' => 'Festivals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivals-create">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
