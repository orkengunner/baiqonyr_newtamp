<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\festivals\models\Festivals */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    #festivals-_nominations > label {
        margin-right: 30px;
    }
</style>
<div class="festivals-form">

    <?php $form = ActiveForm::begin(['id' => 'pages-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-9">
            <div class="widget">
                <div class="widget-header">
                    <h2>О фестивале</h2>
                </div>
                <div class="widget-content padding">

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'date', [
                        'template' => '{label}{input}',
                        //'options' => ['class' => 'form-group form-post-date-section'],
                        //'labelOptions' => ['class' => 'control-label title-form-section']
                    ])->widget(\janisto\timepicker\TimePicker::className(), [
                        //'language' => 'fi',
                        'mode' => 'datetime',
                        'addon' => '',
                        'clientOptions'=>[
                            'dateFormat' => 'dd/mm/yy',
                            'timeFormat' => 'HH:mm',
                        ]
                    ])->textInput(['value' => $model->date ? date('d/m/Y H:i', strtotime($model->date)) : date('d/m/Y H:i')]);?>

                    <?= $form->field($model, 'date_end', [
                        'template' => '{label}{input}',
                        //'options' => ['class' => 'form-group form-post-date-section'],
                        //'labelOptions' => ['class' => 'control-label title-form-section']
                    ])->widget(\janisto\timepicker\TimePicker::className(), [
                        //'language' => 'fi',
                        'mode' => 'datetime',
                        'addon' => '',
                        'clientOptions'=>[
                            'dateFormat' => 'dd/mm/yy',
                            'timeFormat' => 'HH:mm',
                        ]
                    ])->textInput(['value' => $model->date_end ? date('d/m/Y H:i', strtotime($model->date_end)) : date('d/m/Y H:i')]);?>

                    <!--?= $form->field($model, '_nominations')->checkboxList(\app\modules\nominations\models\Nominations::getDropdownList()) ?-->
                    <?= $form->field($model, 'main_page')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'description_full')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'request')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'out_of_competition')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'rules')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'contacts')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'team')->textarea(['rows' => 6]) ?>


                </div>
            </div>
            <div class="widget">
                <div class="widget-header">
                    <h2>На казахском языке</h2>
                </div>
                <div class="widget-content padding">

                    <?= $form->field($model, 'place_kz')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'main_page_kz')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'description_kz')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'description_full_kz')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'request_kz')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'out_of_competition_kz')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'rules_kz')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'contacts_kz')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'team_kz')->textarea(['rows' => 6]) ?>


                </div>
            </div>
            <!--div class="widget">
                <div class="widget-header">
                    <h2>Программа и детали</h2>
                </div>
                <div class="widget-content padding">

                    <!--?= $form->field($model, 'program')->textarea(['rows' => 6]) ?-->

                    <!--?= $form->field($model, 'way')->textarea(['rows' => 6]) ?-->

                    <!--?= $form->field($model, 'reward_participants')->textarea(['rows' => 6]) ?>

                </div>
            </div-->

            <div class="widget">
                <div class="widget-header">
                    <h2>Закрыть прием заявок</h2>
                </div>
                <div class="widget-content padding">

                    <!--?= $form->field($model, 'program')->textarea(['rows' => 6]) ?-->

                    <?= $form->field($model, 'registration_close')->checkbox() ?>

                    <?= $form->field($model, 'registration_close_text')->textarea(['rows' => 6]) ?>

                </div>
            </div>

        </div>
        <div class="col-md-3">
            <?= \app\components\widgets\PublishData::widget([
                'model' => $model,
                'form' => $form,
                'exclude' => ['publish_date', 'slug']
            ])?>

        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script src="/js/ckeditor/ckeditor.js?<?= time() ?>"></script>
<script>
    var EDITOR_GALLERIES = <?=\yii\helpers\Json::encode(\app\modules\gallery\models\Gallery::getDropdownList())?>;

    CKEDITOR.replace('festivals-out_of_competition', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-out_of_competition_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-team', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-team_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-description', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-main_page', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-description_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-main_page_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-request', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-request_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-description_full', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-description_full_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    //CKEDITOR.replace('festivals-requirements', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-rules', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-rules_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-contacts', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-contacts_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-program', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-program_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    //CKEDITOR.replace('festivals-way', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-reward_participants', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('festivals-reward_participants_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    //CKEDITOR.replace('festivals-criteria_evaluation', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);


    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName === 'table') {
            var infoTab = dialogDefinition.getContents('info');
            var border = infoTab.get('txtBorder');
            border['default'] = "0";
        }
    });
</script>

