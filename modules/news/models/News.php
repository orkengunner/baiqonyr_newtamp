<?php

namespace app\modules\news\models;

use app\components\ActiveRecord;
use app\components\LanguageActiveRecord;
use app\modules\festivals\models\Festivals;
use Yii;

/**
 * This is the model class for table "news".
 *
 * @property string $newsID
 * @property string $subTitle
 * @property string $title
 * @property string $previewText
 * @property string $text
 * @property string $url
 * @property string $personID
 * @property string $typeID
 * @property string $subTypeID
 * @property string $views
 * @property string $titleLink
 * @property string $metaTitle
 * @property string $metaKeywords
 * @property string $metaDescription
 * @property string $created
 * @property string $publishDate
 * @property string $filmID
 * @property integer $statusID
 * @property integer $langID
 * @property string $userID
 * @property integer $is_article
 * @property integer $listView
 * @property integer $type_content
 * @property string $ru_id
 * @property integer $isFest
 */
class News extends LanguageActiveRecord
{
    const NEWS_ONLY_BROD = 0;
    const NEWS_ONLY_FEST = 1;
    const NEWS_BROD_FEST = 2;

    const TYPE_CONTENT_TEXT = 0;
    const TYPE_CONTENT_VIDEO = 1;
    const TYPE_CONTENT_PHOTO = 2;

    const IMAGE_NEWS = 1;

    public static $types_content = [
        self::TYPE_CONTENT_TEXT => 'Текст',
        self::TYPE_CONTENT_VIDEO => 'Видео',
        self::TYPE_CONTENT_PHOTO => 'Фото',
    ];
    public static $types_content_url = [
        'text' => self::TYPE_CONTENT_TEXT,
        'video' => self::TYPE_CONTENT_VIDEO ,
        'photo' => self::TYPE_CONTENT_PHOTO,
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subTitle', 'previewText', 'text', 'metaDescription'], 'string'],
            [['title', 'previewText', 'text', 'url', 'titleLink', 'metaTitle', 'metaKeywords', 'metaDescription', 'created', 'publishDate'], 'required'],
            [['personID', 'typeID', 'subTypeID', 'views', 'filmID', 'statusID', 'langID', 'userID', 'is_article', 'listView', 'type_content', 'ru_id', 'isFest'], 'integer'],
            [['created', 'publishDate'], 'safe'],
            [['title', 'url', 'titleLink', 'metaTitle', 'metaKeywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newsID' => 'News ID',
            'subTitle' => 'Sub Title',
            'title' => 'Title',
            'previewText' => 'Preview Text',
            'text' => 'Text',
            'url' => 'Url',
            'personID' => 'Person ID',
            'typeID' => 'Type ID',
            'subTypeID' => 'Sub Type ID',
            'views' => 'Views',
            'titleLink' => 'Title Link',
            'metaTitle' => 'Meta Title',
            'metaKeywords' => 'Meta Keywords',
            'metaDescription' => 'Meta Description',
            'created' => 'Created',
            'publishDate' => 'Publish Date',
            'filmID' => 'Film ID',
            'statusID' => 'Status ID',
            'langID' => 'Lang ID',
            'userID' => 'User ID',
            'is_article' => 'Is Article',
            'listView' => 'List View',
            'type_content' => 'Type Content',
            'ru_id' => 'Ru ID',
            'isFest' => 'Is Fest',
        ];
    }

    public function getTypeNews()
    {
        return $this->hasOne(TypeNews::className(), ['typeeID' => 'typeID']);
    }

    public static function find()
    {
        $query = new NewsQuery(get_called_class());
        /*$date = \Yii::$app->getCache()->get('fest_date');
        if ($date === false) {
            $date = Festivals::getFest(['date']);
            \Yii::$app->getCache()->set('fest_date', $date, 3600*24*7);
        }*/
        $query->isActive()->filterByPlace()->isPublished();
        /*if($date) {
            $query->andWhere(['>', 'news.publishDate', $date]);
        }*/
        return $query->setLanguage();
    }

    public static function findLastNews($limit = null, $isQuery = false, $except = null)
    {
        $query = self::find()
            ->withType()
            ->innerJoin('images AS i', 'i.objectID = news.newsID AND i.oTypeID = ' . News::IMAGE_NEWS)
            ->addSelect([
                'news.newsID',
                'news.previewText',
                'news.title',
                'news.url',
                'news.publishDate',
                'tn.name AS tnName',
                'tn.url AS tnUrl',
                'news.type_content',
                'i.name AS img',
            ]);

        if($limit) {
            $query->limit($limit);
        }

        if($except) {
            $query->andWhere(['<>', 'news.newsID', $except]);
        }

        if($isQuery) {
            return $query;
        }

        return $query
            ->orderBy(['publishDate' => SORT_DESC])
            ->asArray()->all();
    }

    public static function findBySlug($news_slug)
    {
        return self::find()
            ->innerJoin('images AS i', 'i.objectID = news.newsID AND i.oTypeID = ' . News::IMAGE_NEWS)
            ->addSelect([
                'news.newsID',
                'news.text',
                'news.title',
                'news.url',
                'news.views',
                'news.publishDate',
                'news.type_content',
                'i.name AS fa_img',
                'metaTitle',
                'metaDescription',
                'metaKeywords',
            ])
            ->andWhere(['news.url' => $news_slug])
            ->one();
    }
}
