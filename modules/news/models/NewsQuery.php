<?php
/**
 * Created by PhpStorm.
 * User: yevgeniy
 * Date: 11/4/14
 * Time: 5:05 PM
 */

namespace app\modules\news\models;


use app\components\ActiveQuery;
use app\modules\comments\models\Comments;
use app\modules\images\models\Images;
use app\modules\languages\models\Languages;
use app\modules\ratings\models\Ratings;
use app\modules\statuses\models\Statuses;
use app\modules\tags\models\Tags;

use yii\db\Expression;

class NewsQuery extends ActiveQuery
{
    public function isActive($alias = null)
    {
        $this->andWhere(['news.statusID' => News::STATUS_ACTIVE]);

        return $this;
    }

    public function isNoDeleted($alias = null)
    {
        if (\Yii::$app->params['yiiEnd'] == 'admin') {
            $this->andWhere(['isDelete' => 0]);
        }

        return $this;
    }

    public function setLanguage()
    {
        if (\Yii::$app->params['yiiEnd'] == 'admin') {
            $this->andWhere(['news.langID' => Languages::getAdminCurrent()->id]);
        } else {
            $this->andWhere(['news.langID' => Languages::getCurrent()->id]);
        }

        return $this;
    }

    public function getLogo($type = Images::IMAGE_NEWS, $alias = '', $object = 'news.newsID')
    {
        $this->leftJoin('images' . ($alias ? ' AS ' . $alias : ''), ($alias ? $alias . '.' : '') . 'objectID = ' . $object . ' AND ' . ($alias ? $alias . '.' : '') . 'oTypeID = ' . $type);
        return $this;
    }

    public function withType($return = false)
    {
        if ($return) return $this;
        $this->leftJoin('type_news AS tn', 'tn.typeID = news.typeID');
        return $this;
    }

    public function withUsers($alias = null)
    {
        $this->innerJoin('users AS u', 'u.id = news.userID');
        return $this;
    }

    public function withSubType()
    {
        $this->leftJoin('sub_type_news AS stn', 'stn.typeID = tn.typeID');
        return $this;
    }

    public function isPublished()
    {
		if (\Yii::$app->user->isGuest || (!\Yii::$app->user->getIdentity()->checkRole('admin'))) {
			$this->andWhere(['<=', 'news.publishDate', new Expression('NOW()')]);
		}
        return $this;
    }

    public function getTitleLink()
    {
        $this->addSelect(['if(news.titleLink, news.titleLink, news.title) AS titleLink']);
        return $this;
    }

    public function withRelNewsFilms()
    {
        $this->innerJoin('relation_news_films AS rnf', 'rnf.newsID = news.newsID');
        return $this;
    }

    public function withRelNewsPersons()
    {
        $this->innerJoin('relation_news_persons AS rnp', 'rnp.newsID = news.newsID');
        return $this;
    }

    public function getMetaFields()
    {
        $this->getLogo(Images::IMAGE_NEWS_META, 'meta');
        $this->addSelect([
            'meta.name AS metaImage',
            'news.metaTitle',
            'news.metaDescription',
            'news.metaKeywords',
        ]);
        return $this;
    }


    public function getCountComments($alias = 'comments')
    {
        $this->addSelect(['(SELECT COUNT(*) FROM comments AS c WHERE c.objectID = news.newsID AND c.typeID = ' . Comments::COMMENT_TYPE_FILM . ') AS ' . $alias]);
        return $this;
    }

    public function withTags()
    {
        $this->innerJoin('relation_tags_objects AS rto', 'rto.objectID = news.newsID AND rto.oTypeID = ' . Tags::TAGS_TYPE_NEWS);
        return $this;
    }

    public function withUsersRating()
    {
        $this->leftJoin('ratings AS rat', 'rat.objectID = rnf.filmID AND rat.userID = news.userID AND rat.typeID = ' . Ratings::RATING_TYPE_FILM);
        return $this;
    }

    public function getUserFullName()
    {
        $this->addSelect(['TRIM(if(LENGTH(u.firstName), CONCAT_WS(" ", u.firstName, u.lastName), u.username)) AS userFullName']);
        return $this;
    }

    public function filterByPlace()
    {
        return $this->andWhere([
            '<>', 'isFest', News::NEWS_ONLY_BROD
        ]);
    }


}
