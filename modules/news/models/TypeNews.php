<?php

namespace app\modules\news\models;

use app\modules\languages\models\Languages;
use Yii;
use yii\caching\DbDependency;

/**
 * This is the model class for table "type_news".
 *
 * @property string $typeID
 * @property string $name
 * @property string $url
 * @property integer $langID
 * @property string $name_kz
 */
class TypeNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_news';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['langID'], 'integer'],
            [['name', 'url', 'name_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'typeID' => 'Type ID',
            'name' => 'Name',
            'url' => 'Url',
            'langID' => 'Lang ID',
            'name_kz' => 'Name Kz',
        ];
    }

    public static function findTypes()
    {
        $types = \Yii::$app->getCache()->get('types_news_rules' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($types === false) {
            $typesDependency = new DbDependency();
            $typesDependency->db = 'dbBrod';
            $typesDependency->sql = 'SELECT COUNT(*) FROM type_news';
            $types = TypeNews::find()->select(['url'])->all();
            \Yii::$app->getCache()->set('types_news_rules' . Languages::getCurrent()->id . \Yii::$app->language, $types, 3600, $typesDependency);
        }
        return $types;
    }
}
