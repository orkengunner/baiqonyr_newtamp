<?php

namespace app\modules\news\front\controllers;

use app\components\FrontController;
use app\components\Pagination;
use app\modules\banners\models\Banners;
use app\modules\languages\models\Languages;
use app\modules\news\models\News;
use app\modules\pages\models\Pages;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;


class DefaultController extends FrontController
{
    public $count = 6;
    public function actionIndex()
    {
        return $this->index();
    }

    public function actionIndexContent($content_slug)
    {
        return $this->index(null, $content_slug);
    }

    public function actionIndexCategory($news_types_slug)
    {
        return $this->index($news_types_slug);
    }

    public function actionIndexCategoryContent($news_types_slug, $content_slug)
    {
        return $this->index($news_types_slug, $content_slug);
    }

    public function index($news_types_slug = null, $content_slug = null)
    {
        $model = Pages::findBySlug('news');

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->metaImage
        );

        $news = News::findLastNews(null, true)
            ->orderBy(['publishDate' => SORT_DESC])
            ->innerJoin('users AS u', 'u.id = news.userID');

        $countQuery = clone $news;

        $pager = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => $this->count]);

        $news = $news
            ->limit($this->count)
            ->offset($pager->offset)
            ->addSelect([
                new Expression('
                    IF(u.firstName OR u.lastName, CONCAT_WS(" ", firstName, lastName), u.username) AS user_name
                ')
            ])
            ->asArray()
            ->all();

        $leftMain = array_shift($news);
        $rightMain = array_shift($news);

        $left = [];
        $right = [];
        $i = 0;
        foreach($news as $k => $v) {
            if($i == 4) $i = 0;
            if($i < 2) {
                $left[] = $v;
            } else {
                $right[] = $v;
            }
            $i++;
        }
//print_r($left);exit;
        return $this->render('index', [
            'leftNews' => $left,
            'rightNews' => $right,
            'leftMain' => $leftMain,
            'rightMain' => $rightMain,
            'contentTypesUrls' => array_flip(News::$types_content_url),
            'news_types_slug' => $news_types_slug,
            'content_slug' => $content_slug,
            'pager' => $pager
        ]);
    }

    public function actionView($news_slug)
    {
        \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'] =
            ArrayHelper::merge(
                \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'],
                ['comments']
            );
        $model = News::findBySlug($news_slug);
        if (!$model) {
            return $this->redirect(['/news']);
            throw new NotFoundHttpException();
        }

        \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'] =
            ArrayHelper::merge(
                \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'],
                ['ajax']
            );

        $this->setMeta(
            $model['metaTitle'] ? $model['metaTitle'] : $model['title'],
            $model['metaDescription'] ? $model['metaDescription'] : strip_tags($model['previewText']),
            $model['metaKeywords'],
            'http://brod.kz/media/news/' . $model['fa_img']
        );
        $model::updateAllCounters(['views' => 1], 'newsID = ' . $model->newsID);
        $this->setBanner(Banners::BANNER_ZONE_SIDEBAR_240x400, Banners::OBJECT_TYPE_NEWS);

        $news = \Yii::$app->getCache()->get('main_page_news_3_except' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($news === false) {
            $news = News::findLastNews(3, false, $model->newsID);
            \Yii::$app->getCache()->set('main_page_news_3_except' . Languages::getCurrent()->id . \Yii::$app->language, $news, 3600*24*7);
        }

        return $this->render('view', [
            'model' => $model,
            'news' => $news
        ]);
    }

}
