
<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-news', 'Новости'); ?></h1>
        <h3 class="title-section title-section--primary"><?= \Yii::t('front-news', 'Новости'); ?></h3>
    </div>
</div>

<div class="main-body">
    <div class="container clearfix">
        <div class="grid grid--wide">
            <div class="grid-item grid-item--primary">
                <?php if($leftMain): ?>
                    <div class="thumb thumb--primary">
                        <a href="<?= \yii\helpers\Url::to(['/news/default/view', 'news_slug' => $leftMain['url']]); ?>" class="link--overlay"></a>
                        <figure class="thumb-image"><img src="http://brod.kz<?= \yii\helpers\Url::imgUrl($leftMain['img'], 'news', [490, 275], false)?>" alt=""></figure>
                        <?php if($leftMain['type_content']): ?>
                            <a href="<?= $news_types_slug
                                ? \yii\helpers\Url::to(['/news/default/index-content', 'content_slug' => $contentTypesUrls[$leftMain['type_content']], 'news_types_slug' => $news_types_slug])
                                : \yii\helpers\Url::to(['/news/default/index-content', 'content_slug' => $contentTypesUrls[$leftMain['type_content']]]);
                            ?>" class="label--default label--article link--image"><span class="icon icon--type <?= $contentTypesUrls[$leftMain['type_content']]; ?>"></span></a>
                        <?php endif; ?>
                        <div class="thumb-info">
                            <p class="thumb-date"><?= \yii\helpers\StringHelper::rudate(['j F, Y', 'j F'], strtotime($leftMain['publishDate']))?></p>
                            <h4 class="thumb-title"><?= $leftMain['title']; ?></h4>
                            <p class="thumb-description"><?= strip_tags($leftMain['previewText'])?></p>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="grid">
                    <?php foreach($leftNews as $v): ?>
                        <div class="grid-item grid-item--secondary">
                            <div class="thumb thumb--article">
                                <?php if($v['type_content']): ?>
                                    <a href="<?= $news_types_slug
                                        ? \yii\helpers\Url::to(['/news/default/index-content', 'content_slug' => $contentTypesUrls[$v['type_content']], 'news_types_slug' => $news_types_slug])
                                        : \yii\helpers\Url::to(['/news/default/index-category-content', 'content_slug' => $contentTypesUrls[$v['type_content']]]);
                                    ?>" class="label--default label--article link--image"><span class="icon icon--type <?= $contentTypesUrls[$v['type_content']]; ?>"></span></a>
                                <?php endif; ?>
                                <div class="thumb-meta">
                                    <figure class="thumb-image"><a href="<?= \yii\helpers\Url::to(['/news/default/view', 'news_slug' => $v['url']]); ?>" class="link--image"><img src="http://brod.kz<?= \yii\helpers\Url::imgUrl($v['img'], 'news', [235, 132], false)?>" alt=""></a></figure>
                                    <p class="thumb-category"><a href="<?= \yii\helpers\Url::to(['/news/default/index-category', 'news_types_slug' => $v['tnUrl']])?>" class="link"><?= $v['tnName']?></a></p>
                                    <p class="thumb-date"><?= \yii\helpers\StringHelper::rudate(['j f, Y', 'j F'], strtotime($v['publishDate']))?></p>
                                </div>
                                <h4 class="thumb-title"><a href="<?= \yii\helpers\Url::to(['/news/default/view', 'news_slug' => $v['url']]); ?>" class="link"><?= $v['title']?></a></h4>
                                <p class="thumb-description"><?= strip_tags($v['previewText'])?></p>
                            </div>
                        </div><!-- end .grid-item -->
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="grid-item grid-item--primary">
                <?php if($rightMain): ?>
                    <div class="thumb thumb--primary">
                        <a href="<?= \yii\helpers\Url::to(['/news/default/view', 'news_slug' => $rightMain['url']]); ?>" class="link--overlay"></a>
                        <figure class="thumb-image"><img src="http://brod.kz<?= \yii\helpers\Url::imgUrl($rightMain['img'], 'news', [490, 275], false)?>" alt=""></figure>
                        <?php if($rightMain['type_content']): ?>
                            <a href="<?= $news_types_slug
                                ? \yii\helpers\Url::to(['/news/default/index-content', 'content_slug' => $contentTypesUrls[$rightMain['type_content']], 'news_types_slug' => $news_types_slug])
                                : \yii\helpers\Url::to(['/news/default/index-content', 'content_slug' => $contentTypesUrls[$rightMain['type_content']]]);
                            ?>" class="label--default label--article link--image"><span class="icon icon--type <?= $contentTypesUrls[$rightMain['type_content']]; ?>"></span></a>
                        <?php endif; ?>
                        <div class="thumb-info">
                            <p class="thumb-date"><?= \yii\helpers\StringHelper::rudate(['j f, Y', 'j F'], strtotime($rightMain['publishDate']))?></p>
                            <h4 class="thumb-title"><?= $rightMain['title']; ?></h4>
                            <p class="thumb-description"><?= strip_tags($rightMain['previewText'])?></p>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="grid">
                    <?php foreach($rightNews as $v): ?>
                        <div class="grid-item grid-item--secondary">
                            <div class="thumb thumb--article">
                                <?php if($v['type_content']): ?>
                                    <a href="<?= $news_types_slug
                                        ? \yii\helpers\Url::to(['/news/default/index-content', 'content_slug' => $contentTypesUrls[$v['type_content']], 'news_types_slug' => $news_types_slug])
                                        : \yii\helpers\Url::to(['/news/default/index-category-content', 'content_slug' => $contentTypesUrls[$v['type_content']]]);
                                    ?>" class="label--default label--article link--image"><span class="icon icon--type <?= $contentTypesUrls[$v['type_content']]; ?>"></span></a>
                                <?php endif; ?>
                                <div class="thumb-meta">
                                    <figure class="thumb-image"><a href="<?= \yii\helpers\Url::to(['/news/default/view', 'news_slug' => $v['url']]); ?>" class="link--image"><img src="http://brod.kz<?= \yii\helpers\Url::imgUrl($v['img'], 'news', [235, 132], false)?>" alt=""></a></figure>
                                    <p class="thumb-category"><a href="<?= \yii\helpers\Url::to(['/news/default/index-category', 'news_types_slug' => $v['tnUrl']])?>" class="link"><?= $v['tnName']?></a></p>
                                    <p class="thumb-date"><?= \yii\helpers\StringHelper::rudate(['j f, Y', 'j F'], strtotime($v['publishDate']))?></p>
                                </div>
                                <h4 class="thumb-title"><a href="<?= \yii\helpers\Url::to(['/news/default/view', 'news_slug' => $v['url']]); ?>" class="link"><?= $v['title']?></a></h4>
                                <p class="thumb-description"><?= strip_tags($v['previewText'])?></p>
                            </div>
                        </div><!-- end .grid-item -->
                    <?php endforeach; ?>
                </div>
            </div>
        </div><!-- end .grid -->
        <?php echo \app\components\widgets\LinkPager::widget([
            'pagination' => $pager,
            'options' => ['class' => 'pagination--primary pagination--industry'],
            'activePageCssClass' => 'active',
            'linkClass' => 'link pagination-link',
            'tag' => 'li',
            'tagClass' => 'pagination-item',
            'nextPageLabel' => '<span aria-hidden="true">»</span>',
            'prevPageLabel' => '<span aria-hidden="true">«</span>',
            'nextPageCssClass' => 'link pagination-link',
            'prevPageCssClass' => 'link pagination-link',
            'registerLinkTags' => true,
            'maxButtonCount' => 9
        ]);?>
    </div><!-- end .container -->
</div><!-- end .main-body -->