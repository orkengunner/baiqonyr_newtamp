
<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-news', 'Новости'); ?></h1>
        <h3 class="title-section title-section--primary">
            <?= \Yii::t('front-news', 'Новости'); ?>
            <?php /* <?= $model->title; ?>*/ ?>
        </h3>
    </div>
</div>
<div class="main-body">
    <section class="articles">
        <div class="container">
            <?php /*<?= \app\modules\mainpage\front\components\BreadCrumbs::widget([
                'data' => [
                    \Yii::t('front-news', 'Новости') => ['/news/default/index'],
                    $model->title => ''
                ]
            ])?>*/ ?>
            <div class="grid">
                <div class="grid-item grid-item--content">
                    <article class="article">
                        <h1><?= $model->title; ?></h1>
                        <h2 class="title--primary"><?= $model->title; ?></h2>
                        <ul class="article-meta">
                            <li class="meta-item meta-date"><?= \yii\helpers\StringHelper::rudate(['j F, Y', 'j F'], strtotime($model->publishDate))?></li>
                            <li class="meta-item meta-views"><span class="icon icon--views icon--lg"></span><?= $model->views?></li>
                        </ul>
                        <!-- <figure class="article-cover"><img src="images/article-cover.jpg" alt=""></figure> -->
                        <figure class="article-cover"><img alt="<?= $model->title; ?>" src="http://brod.kz<?= \yii\helpers\Url::imgUrl($model->fa_img, 'news', [725, null], false)?>"></figure>
                        <?php
                        $model->text = str_replace("\n", '', $model->text);
                        preg_match_all('/<p>\{\{begin_gallery\}\}<\/p>(.*?)<p>\{\{end_gallery\}\}<\/p>/', $model->text, $gallery);

                        foreach($gallery[0] as $k => $v) {
                            $model->text = str_replace($v, $this->render('_gallery', [
                                'gallery' => $gallery[1][$k]
                            ]), $model->text);
                        }
                        // print_r($model->text);exit;
                        preg_match_all('/<p><img.*?alt="(.*?)".*?><\/p>/', $model->text, $alt);
                        preg_match_all('/<p><img.*?src="(.*?)".*?><\/p>/', $model->text, $src);


                        preg_match_all('/<img.*?alt="(.*?)".*?>/', $model->text, $alt2);
                        preg_match_all('/<img.*?src="(.*?)".*?>/', $model->text, $src2);

                        if($alt) {
                            foreach($alt[0] as $k => $v) {
                                //$v = strtr($v, ['<p>' => '', '</p>' => '']);
                                //echo $v;exit;
                                $model->text = str_replace($v,
                                    '<figure class="article-figure">' . str_replace('alt="|', 'alt="', strtr($v, ['<p>' => '', '</p>' => ''])) .
                                    (isset($alt[1][$k][0]) && $alt[1][$k][0] != '|' ? '<figcaption class="article-image-description">' . str_replace(';', '<br>', $alt[1][$k]) . '</figcaption>' : '') . '</figure>', $model->text);
                            }

                        }

                        if($alt2) {
                            foreach($alt2[0] as $k => $v) {
                                if(isset($src[1][$k]) && $src[1][$k] != $src2[1][$k])
                                    $model->text = str_replace($v,
                                        '<figure class="article-figure">' . str_replace('alt="|', 'alt="', $v) .
                                        (isset($alt2[1][$k][0]) && $alt2[1][$k][0] != '|' ? '<figcaption class="article-image-description">' . str_replace(';', '<br>', $alt2[1][$k]) . '</figcaption>' : ''). '</figure>', $model->text);
                            }

                        }
                        $model->text = str_replace('<p>', '<p class="article-text">', $model->text);
                        echo str_replace('/new-uploads/', 'http://brod.kz/new-uploads/', $model->text);

                        ?>

                        <ul class="article-meta">
                            <li class="meta-item meta-date"><?= \yii\helpers\StringHelper::rudate(['j F, Y', 'j F'], strtotime($model->publishDate))?></li>
                            <li class="meta-item meta-views"><span class="icon icon--views icon--lg"></span><?= $model->views?></li>
                        </ul>
                    </article>

                    <?= $this->render('@app/views/layouts/front/social_shares', ['js' => true]); ?>
                </div><!-- grid-item -->

                <div class="grid-item grid-item--sidebar hidden">
                    <?= \app\modules\banners\front\components\BannerWidget::widget([
                        'banner' => \Yii::$app->controller->banners[
                        \app\modules\banners\models\Banners::BANNER_ZONE_SIDEBAR_240x400
                        ],
                    ]);?>

                    <?php foreach($news as $v): ?>
                        <div class="thumb thumb--article is-news">
                            <div class="thumb-meta">
                                <figure class="thumb-image"><a href="<?= \yii\helpers\Url::to(['/news/default/view',
                                        'news_slug' => $v['url']
                                    ]); ?>" class="link--image"><img src="http://brod.kz<?= \yii\helpers\Url::imgUrl($v['img'], 'news', [235, 132], false)?>" alt=""></a></figure>
                                <a href="<?= \yii\helpers\Url::to(['/news/default/index-category',
                                    'news_types_slug' => $v['tnUrl']
                                ]); ?>" class="label--default label--primary label--news link--label"><?= $v['tnName']; ?></a>
                            </div>
                            <p class="thumb-date"><?= \yii\helpers\StringHelper::rudate(['j F, Y', 'j F'], strtotime($v['publishDate']))?></p>
                            <h4 class="thumb-title"><a href="<?= \yii\helpers\Url::to(['/news/default/view',
                                    'news_slug' => $v['url']
                                ]); ?>" class="link"><?= $v['title']; ?></a></h4>
                            <p class="thumb-description"><?= strip_tags($v['previewText']); ?></p>
                        </div>
                    <?php endforeach; ?>
                </div><!-- end .grid-item -->
            </div><!-- end .grid -->
        </div><!-- end .container -->
    </section>
    <?php echo \app\modules\comments\front\components\Comments::widget([
        'objectID' => $model['newsID'],
        'objectTypeID' => $model['is_article'] ? \app\modules\comments\models\Comments::COMMENT_TYPE_ARTICLES: \app\modules\comments\models\Comments::COMMENT_TYPE_NEWS,
    ]);?>
    <?php /*?<?= // \app\modules\sponsors\front\components\SponsorsWidget::widget()?>*/ ?>
</div>