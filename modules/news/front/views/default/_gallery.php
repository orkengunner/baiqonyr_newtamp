<?php
preg_match_all('/<img.*?alt="(.*?)".*?>/', $gallery, $alt);
preg_match_all('/<img.*?src="(.*?)".*?>/', $gallery, $src);

?>
<div class="article-gallery">
    <div class="article-gallery-description"><?= isset($alt[1][0]) ? $alt[1][0] : ''; ?></div>
    <ul class="article-slider">
        <?php foreach($src[1] as $k => $v): ?>
            <li class="article-slider-item" data-thumb="<?= $v; ?>" data-src="<?= $v; ?>" data-description="<?= htmlspecialchars($alt[1][$k])?>">
                <img src="<?= $v; ?>"  class="article-slider-image"/>
            </li>
        <?php endforeach; ?>
    </ul>
</div><!-- end .article-gallery -->