<?php

namespace app\modules\search\front\controllers;

use app\components\RFController;
use app\components\Search;
use app\modules\banners\models\Banners;
use app\modules\languages\models\Languages;
use app\modules\pages\models\Pages;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\sphinx\Query;

class DefaultController extends RFController
{


    public function actionIndex($query = '', $category = 0, $date_from = '', $date_to = '')
    {

        $provider = null;
        $query = Html::encode($query);
        $sphinxQuery = new Query;
        $sphinxQuery->from('vlastkz')/*->match(new Expression(':match',
                ['match' => '@(title,text,category_title) ' . \Yii::$app->sphinx->escapeMatchValue($query)]))*/
        ->orderBy(['pub_date' => SORT_DESC])
            ->snippetOptions([
                'before_match' => '<mark>',
                'after_match' => '</mark>',
            ])
            ->match(\Yii::$app->sphinx->escapeMatchValue($query))
            ->snippetCallback(function ($rows) {
                $result = [];
                foreach ($rows as $row) {
                    $result[] = $row['id'];
                }

                $pages = Pages::find()->select('text')->andWhere(['in', 'id', $result])->asArray()->all();
                $textResult = '';
                foreach ($pages as $page) {
                    $textResult[] = strip_tags($page['text']);
                }

                return $textResult;
            });

        if (!empty($date_from)) {
            $sphinxQuery->andWhere(['>=', 'pub_date', strtotime($date_from . ' 00:00:00')]);
        }

        if (!empty($date_to)) {
            $sphinxQuery->andWhere(['<=', 'pub_date', strtotime($date_to . ' 23:59:59')]);
        }

        if (!empty($category)) {
            $sphinxQuery->andWhere(['category_id' => $category]);
        }

        $sphinxQuery->andWhere(['lang_id' => Languages::getCurrent()->id]);


        $provider = new ActiveDataProvider([
            'query' => $sphinxQuery,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);

        $models = $provider->getModels();

        $this->setMeta('Поиск по сайту', 'Поиск по сайту', null, null, Url::to(['/search/default/index'], true));

        return $this->render('index',
            [
                'query' => $query,
                'models' => $models,
                'provider' => $provider,
                'category' => $category,
                'date_from' => $date_from,
                'date_to' => $date_to
            ]);

    }

}
