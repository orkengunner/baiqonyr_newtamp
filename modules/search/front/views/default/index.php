<section class="page">
    <div class="container-fluid">
        <form class="form-filters" method="GET"
            action="<?= \yii\helpers\Url::to(['/search/default/index']) ?>"
            role="search">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group group-search">
                        <div class="controls">
                            <input class="form-control" type="text" name="query" value="<?= $query ?>">
                            <button type="submit"><i class="icon icon--search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group form-group-sm group-category">
                                <div class="controls">
                                    <?= \yii\helpers\Html::dropDownList(
                                        'category', $category,
                                        \app\modules\categories\models\Categories::getDropdownList(),
                                        ['class' => 'form-control', 'prompt' => 'Выберите категорию']
                                    ) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group form-group-sm group-dates">
                                <label class="sr-only">Date range</label>
                                <div class="controls input-daterange js-datepicker-range">
                                    <?= \yii\helpers\Html::textInput(
                                        'date_from', $date_from,
                                        ['class' => 'form-control']
                                    ) ?>
                                    &mdash;
                                    <?= \yii\helpers\Html::textInput(
                                        'date_to', $date_to,
                                        ['class' => 'form-control']
                                    ) ?>
                                </div>
                            </div>
                            <button class="btn btn-white" type="submit">Применить</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-8">
                <?php if (!empty($models)) { ?>
                    <div class="search-items-list">
                        <div class="block-in">
                            <?php foreach ($models as $data) { ?>
                                <div class="search-item">
                                    <div class="meta search-item-meta clearfix">
                                        <div class="pull-left text-black">
                                            <?= $data['category_title'] ?>
                                        </div>
                                        <div class="pull-right">
                                            <time class="news-date js-date-long"
                                                  datetime="<?= Yii::$app->formatter->asDatetime(
                                                      $data['pub_date'],
                                                      'php:Y-m-d H:i:sP'
                                                  ) ?>">
                                                <?= Yii::$app->formatter->asDatetime(
                                                    $data['pub_date'], 'long'
                                                ) ?>
                                            </time>
                                        </div>
                                    </div>
                                    <div class="search-item-title">
                                        <a href="<?= \yii\helpers\Url::to([
                                            '/pages/default/index',
                                            'sefname' => $data['sefname'],
                                            'category' => $data['category']
                                        ]) ?>"><b><?= $data['title'] ?></b></a>
                                    </div>
                                    <div class="search-item-in">
                                        <?= $data['snippet'] ?>
                                    </div>
                                    <div class="meta search-item-meta">
                                        <a href="#"><?= \yii\helpers\Url::to([
                                                '/pages/default/index',
                                                'sefname' => $data['sefname'],
                                                'category' => $data['category']
                                            ], true) ?></a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <nav class="pagination pagination-light" role="navigation">
                        <?= \yii\widgets\LinkPager::widget([
                            'pagination' => $provider->getPagination(),
                        ]) ?>
                    </nav>
                <?php } else { ?>
                    <div class="search-items-list">
                        <div class="block-in">
                            <p>По Вашему запросу <strong>"<?= $query ?>"</strong> ничего не найдено</p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
