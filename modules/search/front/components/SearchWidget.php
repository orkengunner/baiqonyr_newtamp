<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 8/25/14
 * Time: 10:43
 */

namespace app\modules\search\front\components;

class SearchWidget extends \yii\base\Widget
{

    public function run()
    {
        if (\Yii::$app->controller->module->id == 'search' || \Yii::$app->controller->action->id == 'error') {
            return false;
        }
        return $this->render('searchWidget');
    }
}
