<form class="form form-main" action="<?= \yii\helpers\Url::toRoute(['/search/default/index']) ?>" role="form">
    <div class="input-group input-group-xs">
        <input class="form-control" name="query" type="search">
            <span class="input-group-btn">
              <button class="btn btn-link btn-xs" type="submit">
                  <i class="fa fa-search"></i>
              </button>
            </span>
    </div>
</form>
