<?php

namespace app\modules\posts\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PostsSearch represents the model behind the search form about `app\modules\posts\models\Posts`.
 */
class PostsSearch extends Posts
{
    public $category_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id', 'user_id', 'delete_by', 'views', 'lang_id', 'category_id'], 'integer'],
            [['title', 'short_text', 'text', 'slug', 'publish_date', 'create_date', 'update_date', 'delete_date', 'source_title', 'source_link', 'meta_title', 'meta_description', 'meta_keywords'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = Model::scenarios();
        $scenarios['search'] = ['title', 'publish_date', 'status_id', 'views', 'category_id'];
        return $scenarios;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Posts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['publish_date' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'posts.status_id' => $this->status_id,
            'posts.views' => $this->views,
        ]);

        if (!empty($this->category_id)) {
            $query->joinWith('categories');
            $query->andWhere(['categories.id' => $this->category_id]);
        }

        $query->andFilterWhere(['like', 'posts.title', $this->title])
            ->andFilterWhere(['like', 'posts.publish_date', $this->publish_date]);

        return $dataProvider;
    }
}