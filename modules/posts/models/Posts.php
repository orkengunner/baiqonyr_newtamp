<?php

namespace app\modules\posts\models;

use app\components\behaviors\WithMetaTags;
use app\components\LanguageActiveRecord;
use app\components\rocket\RFTaggable;
use app\components\traits\UploadableAsync;
use app\modules\categories\models\Categories;
use app\modules\comments\models\Comments;
use app\modules\languages\models\Languages;
use app\modules\tags\models\Tags;
use app\modules\users\models\Users;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "posts".
 *
 * @property string $id
 * @property string $title
 * @property string $short_text
 * @property string $text
 * @property string $slug
 * @property string $publish_date
 * @property string $create_date
 * @property string $update_date
 * @property integer $status_id
 * @property string $user_id
 * @property string $delete_date
 * @property string $delete_by
 * @property string $views
 * @property string $source_title
 * @property string $source_link
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $lang_id
 *
 * @property Users $deleteBy
 * @property Languages $lang
 * @property Users $user
 * @property PostsCategories[] $postsCategories
 * @property Categories[] $categories
 * @property PostsFilesLogo[] $postsFilesLogos
 * @property PostsFilesMetaImage[] $postsFilesMetaImages
 * @property PostsTags[] $postsTags
 * @property Tags[] $tags
 */
class Posts extends LanguageActiveRecord
{
    use UploadableAsync;

    public $listCategories;

    public $categorySefname;

    public $upload_logo = null;

    public $file_attributes = [
        'logo',
        'metaImage'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_text', 'text',  'status_id'], 'required'],
            [['short_text', 'text', 'meta_description'], 'string'],
            [['publish_date', 'create_date', 'update_date', 'delete_date', 'listCategories', 'tagNames'], 'safe'],
            [['status_id', 'user_id', 'delete_by', 'views', 'lang_id'], 'integer'],
            [['title', 'slug', 'source_title', 'source_link', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['slug', 'delete_date'], 'unique', 'targetAttribute' => ['slug', 'delete_date'], 'message' => 'The combination of Slug and Delete Date has already been taken.'],
            [['slug'], 'unique'],
            [
                ['upload_logo'],
                'image',/* 'minWidth' => 620, 'minHeight'=>400,*/
                'on' => 'upload'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'source_title' => 'Источник',
            'source_link' => 'Ссылка на источник',
        ]);
    }

    public static function find()
    {
        $query = new PostsQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query->setLanguage()->isNoDeleted('posts');
        }

        return $query->isPublish('posts')->isNoDeleted('posts')->setLanguage();
    }

    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function getDeleteBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'delete_by']);
    }

    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['id' => 'category_id'])->viaTable('posts_categories', ['post_id' => 'id']);
    }

    public function getPostsCategories()
    {
        return $this->hasMany(PostsCategories::className(), ['post_id' => 'id']);
    }

    public function getPostsFilesLogos()
    {
        return $this->hasMany(PostsFilesLogo::className(), ['post_id' => 'id']);
    }

    public function getPostsFilesMetaImages()
    {
        return $this->hasMany(PostsFilesMetaImage::className(), ['post_id' => 'id']);
    }

    public function getPostsTags()
    {
        return $this->hasMany(PostsTags::className(), ['post_id' => 'id']);
    }

    public function getTags()
    {
        return $this->hasMany(Tags::className(), ['id' => 'tag_id'])->viaTable('posts_tags', ['post_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior,
            [
                'class' => WithMetaTags::className()
            ],
            RFTaggable::className(),
        ];
    }

    public function beforeValidate()
    {
        $this->user_id = Yii::$app->getUser()->id;
        if ($this->getScenario() != 'search') {

            if ($this->scenario != 'search' && $this->scenario != 'upload' && (empty($this->listCategories) || !is_array($this->listCategories) || array_sum($this->listCategories) == 0)) {
                $this->addError('listCategories', 'Не выбрана категория');
            }
            if (!$this->slug) {
                $this->slug = StringHelper::url($this->title);
            }
        }



        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if(\Yii::$app->request->post('Posts')){
            $this->publish_date = StringHelper::dateFormat($this->publish_date, 'd/m/Y H:i', 'Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

        if ($this->hasAsyncTempFile('upload_logo')) {
            $logo = $this->setFile('logo', $this->saveAsyncFile('upload_logo'), true);
        }

        $this->saveCategories('post_id', PostsCategories::className());

        parent::afterSave($insert, $changedAttributes);
        return true;

    }

    public function afterFind()
    {
        parent::afterFind();

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            $this->listCategories = ArrayHelper::map($this->categories, 'id', 'id');
        }

        return true;
    }

    public static function getLastPostsByCategoryID($id = null, $image = false, $comments = false, $limit = 6, $order = [])
    {
        $posts = self::find()
            ->withPotsCategories()
            ->addSelect([
                'posts.id',
                'posts.title',
                'posts.slug',
                'posts.short_text',
                'posts.publish_date'
            ])
            ->groupBy(['posts.id']);

        if($id) {
            $posts->andWhere([
                'pc.category_id' => $id
            ]);
        } else {
            $posts->addSelect(['pc.category_id AS fa_category_id']);
        }
        if($order) {
            $posts->orderBy($order);
        } else {
            $posts->orderBy([
                'publish_date' => SORT_DESC
            ]);
        }
        if($limit) $posts->limit($limit);
        if($comments)
            $posts->withComments('posts.id', Comments::POSTS)
                ->addSelect(['COUNT(c.id) AS fa_countComments']);

        if($image)
            $posts->withFilesLogo()
            ->addSelect(['CONCAT_WS("/", subdir, basename) AS fa_img']);

        return $posts->all();
    }

    public static function getBySlug($slug, $metaImage = false)
    {
        $query = self::find()
            ->withFilesLogo()
            ->withTags()
            ->addSelect([
                'posts.id',
                'posts.title',
                'posts.text',
                'posts.slug',
                'posts.views',
                'posts.publish_date',
                'GROUP_CONCAT(CONCAT_WS(":", t.id, t.name) ORDER BY t.frequency SEPARATOR ";") AS fa_tags',
                'CONCAT_WS("/", pfl.subdir, pfl.basename) AS fa_logo'
            ])
            ->andWhere(['posts.slug' => $slug])
            ->groupBy(['posts.id']);

        if($metaImage) {
            $query->withFilesMetaImage()
                ->addSelect(['CONCAT_WS("/", pmi.subdir, pmi.basename) AS fa_meta_image']);
        }
        return $query->one();
    }
}
