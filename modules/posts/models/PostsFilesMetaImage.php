<?php

namespace app\modules\posts\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "posts_files_meta_image".
 *
 * @property string $post_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Posts $post
 */
class PostsFilesMetaImage extends ActiveRecord
{
    public static $keyField = 'post_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts_files_meta_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'subdir', 'basename'], 'required'],
            [['post_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'post_id' => 'Post ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Posts::className(), ['id' => 'post_id']);
    }
}
