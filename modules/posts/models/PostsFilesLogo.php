<?php

namespace app\modules\posts\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "posts_files_logo".
 *
 * @property string $post_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Posts $post
 */
class PostsFilesLogo extends ActiveRecord
{
        public static $keyField = 'post_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts_files_logo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'subdir', 'basename'], 'required'],
            [['post_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'post_id' => 'Page ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Posts::className(), ['id' => 'post_id']);
    }
}
