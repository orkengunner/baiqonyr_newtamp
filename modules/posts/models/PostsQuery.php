<?php
namespace app\modules\posts\models;

use app\components\ActiveQuery;

class PostsQuery extends ActiveQuery
{
    public function withCategories()
    {
        return $this
            ->innerJoin('posts_categories AS pc', 'pc.post_id = posts.id')
            ->innerJoin('categories AS c', 'c.id = pc.category_id');
    }

    public function withPotsCategories()
    {
        return $this
            ->innerJoin('posts_categories AS pc', 'pc.post_id = posts.id');
    }

    public function withFilesLogo()
    {
        return $this->leftJoin('posts_files_logo AS pfl','pfl.post_id = posts.id');
    }

    public function withFilesMetaImage()
    {
        return $this->leftJoin('posts_meta_image AS pmi','pmi.post_id = posts.id');
    }

    public function withTags()
    {
        return $this->leftJoin('posts_tags AS pt', 'pt.post_id = posts.id')
            ->leftJoin('tags AS t', 't.id = pt.tag_id');
    }
}
