<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\posts\models\Posts */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("
    setInterval(function(){
        $(\".jcrop-tracker\").click(function(){
            return false;
        });
    }, 500);

");
?>

<div class="posts-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-9">
            <div class="widget">
                <div class="widget-content padding">

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'short_text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'source_title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'source_link')->textInput(['maxlength' => true]) ?>

                </div>
            </div>

            <?= \app\components\widgets\MetaTagFields::widget([
                'model' => $model,
                'form' => $form
            ]) ?>

            <div class="widget">
                <div class="widget-content padding">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('posts', 'Создать') : Yii::t('posts',
                        'Изменить'), [
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                        'id' => 'submit-page'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget">
                <div class="widget-header">
                    <h2>Опубликовать</h2>
                </div>
                <div class="widget-content padding">
                    <?= $form->field($model, 'publish_date', [
                        'template' => '{label}{input}',
                        //'options' => ['class' => 'form-group form-post-date-section'],
                        //'labelOptions' => ['class' => 'control-label title-form-section']
                    ])->widget(\janisto\timepicker\TimePicker::className(), [
                        //'language' => 'fi',
                        'mode' => 'datetime',
                        'addon' => '',
                        'clientOptions'=>[
                            'dateFormat' => 'dd/mm/yy',
                            // 'timeFormat' => 'HH:mm:ss',
                        ]
                    ])->textInput(['value' => $model->publish_date ? date('d/m/Y H:i', strtotime($model->publish_date)) : date('d/m/Y H:i')]);?>

                    <?= $form->field($model,
                        'status_id')->dropDownList($model::$statuses) ?>

                    <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>


                </div>
            </div>
            <div class="widget">
                <div class="widget-header">
                    <h2>Категории и метки</h2>
                </div>
                <div class="widget-content padding">

                    <?= \app\components\widgets\CheckboxTreeWidget::widget([
                        'treeModel' => '\app\modules\categories\models\Categories',
                        'model' => $model,
                        'attributeName' => 'listCategories'
                    ]); ?>

                    <?= $form->field($model, 'tagNames')->widget(\dosamigos\selectize\SelectizeTextInput::className(), [
                        // calls an action that returns a JSON object with matched
                        // tags
                        'loadUrl' => ['/tags/default/list'],
                        'options' => ['class' => 'form-control'],
                        'clientOptions' => [
                            'plugins' => ['remove_button'],
                            'valueField' => 'name',
                            'labelField' => 'name',
                            'searchField' => ['name'],
                            'create' => true,
                        ],
                    ])->hint('теги разделяются запятой') ?>
                </div>
            </div>

            <div class="widget">
                <div class="widget-header">
                    <h2>Миниатюра записи</h2>
                </div>
                <div class="widget-content padding gallery-wrap">
                    <?php
                    $sizes = $model->getParam('logo.sizes');
                    echo \app\components\widgets\ImageUploadAsync::widget([
                        'model' => $model,
                        'attribute' => 'upload_logo',
                        'form' => $form,
                        //'hint' => 'Рекомендуемый размер в пропорции 1,5',
                        'croppable' => false,
                        'previewPath' => $model->getFilePath('logo', false, [$sizes[0][0], $sizes[0][1]]),
                    ]); ?>
                </div>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="/js/ckeditor/ckeditor.js?<?= time() ?>"></script>
<script>
    var EDITOR_GALLERIES = <?=\yii\helpers\Json::encode(\app\modules\gallery\models\Gallery::getDropdownList())?>;

    CKEDITOR.replace('posts-text', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('posts-short_text', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName === 'table') {
            var infoTab = dialogDefinition.getContents('info');
            var border = infoTab.get('txtBorder');
            border['default'] = "0";
        }
    });
</script>

