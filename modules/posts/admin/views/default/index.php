<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\posts\models\PostsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posts-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <p>
        <?= Html::a(Yii::t('posts', '<i class="icon-list-add"></i> Создать статью', [
            'modelClass' => 'Posts',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-bordered'],
                    'filterModel' => $searchModel,
                    'layout' => "{items}\n{pager}",
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'title',
                        [
                            'attribute' => 'category_id',
                            'class' => 'yii\grid\DataColumn',
                            'header' => 'Категории',
                            'value' => function ($data) {
                                $listCategories = '';
                                foreach ($data->categories as $category) {
                                    $listCategories .= $category->title . '<br>';
                                }
                                return $listCategories;
                            },
                            'format' => 'html',
                            'filter' => \app\modules\categories\models\Categories::getTreeDropdownList(false)
                        ],
                        [
                            'attribute' => 'status_id',
                            'value' => function ($data) {
                                return \app\modules\pages\models\Pages::$statuses[$data->status_id];
                            },
                            'filter' => \app\modules\pages\models\Pages::$statuses
                        ],
                        'publish_date',
                        'views',

                        [
                            'class' => \app\components\admin\RFAActionColumn::className(),
                            'template' => '{update} {remove}'
                        ],
                    ],
                ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
