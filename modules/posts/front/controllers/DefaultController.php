<?php

namespace app\modules\posts\front\controllers;


use app\components\FrontController;
use app\components\Pagination;
use app\modules\categories\models\Categories;
use app\modules\comments\models\Comments;
use app\modules\posts\models\Posts;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class DefaultController extends FrontController
{
    public $count = 2;
    public function actionView($category_slug, $post_slug)
    {
        \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'] =
            ArrayHelper::merge(
                \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'],
                ['social', 'ajax', 'helpers']
            );

        $model = Posts::getBySlug($post_slug);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        Posts::updateAllCounters(['views' => 1], 'id = ' . $model->id);

        return $this->render('view', [
            'model' => $model,
            'comment_type' => array_search($category_slug, Comments::$types)
        ]);
    }

    public function actionIndex($category_slug)
    {
        $model = Categories::findOne(['slug' => $category_slug]);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        $posts = Posts::find()
            ->withPotsCategories()
            ->andWhere([
                'pc.category_id' => $model->id
            ]);

        $countQuery = clone $posts;

        $pager = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => $this->count]);

        $data = $posts
            ->withFilesLogo()
            ->addSelect([
                'posts.id',
                'posts.title',
                //'c.title AS fa_categoryTitle',
                'posts.slug',
                'posts.short_text',
                'CONCAT_WS("/", pfl.subdir, pfl.basename) AS fa_logo'
            ])
            ->limit($this->count)
            ->offset($pager->offset)
            ->all();

        return $this->render('index', [
            'data' => $data,
            'model' => $model,
            'pager' => $pager
        ]);
    }
}
