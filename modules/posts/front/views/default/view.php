<?php
    $this->registerJs('mAjax = new wAjax();', \yii\web\View::POS_END);
?>
<div class="clearfix single-post post">

    <h1 class="entry-title"><?= $model->title; ?></h1>
    <div class="entry-info">
        <strong>Опубликовано:</strong>
        <abbr class="published"><?= date('d.m.Y', strtotime($model->publish_date)); ?></abbr>
        <span class="entry-cat"><strong>Просмотров: </strong><?= $model->views; ?></span>
        <?php if($model->fa_tags): $tags = \yii\helpers\StringHelper::parseGroupConcat($model->fa_tags, ':', ';'); ?>
            <span class="tags"><strong>Метки:</strong>
                <?php
                $cnt = [];
                    foreach($tags as $v) {
                        $cnt[] = '<a href="' . \yii\helpers\Url::to(['/tags/', 'tag' => $v[0]]) . '">' . $v[1] . '</a>';
                    }
                    echo implode(', ', $cnt);
                ?>
            </span>
        <?php endif; ?>
        <div class="entry-photo">
            <img title="<?= $model->title; ?>" alt="<?= $model->title; ?>" src="<?= \yii\helpers\Url::imgUrl($model->fa_logo, 'posts', [618, null])?>">
        </div>



    </div>

    <div class="entry-content">
        <?= $model->text; ?>

    </div>


    <div class="social-likes">
        <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
        <div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
        <div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
        <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
        <div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
        <div class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</div>
    </div>


    <div class="about-author clearfix">
        Если у вас возникли какие-нибудь вопросы или замечания по данному материалу, может быть просто нужна помощь, <a href="<?= \yii\helpers\Url::to(['/contact_us/']); ?>">пишите</a>. Постараюсь ответить, помочь.
    </div>

</div>

<a name="comments"></a>

<?php \yii\widgets\Pjax::begin(['id' => 'updateComments']); ?>
<?php echo \app\modules\comments\front\components\Comments::widget([
    'objectID' => $model->id,
    'objectTypeID' => $comment_type
]);?>
<?php \yii\widgets\Pjax::end(); ?>