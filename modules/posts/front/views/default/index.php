<h1 class="home-title"><?= $model-> title; ?></h1>

<div id="archive-posts">
    <ul class="hfeed posts-quick clearfix">
        <?php foreach($data as $v): ?>
            <li class="post category-- clearfix">
                <div class="entry-thumbnails">
                    <a class="entry-thumbnails-link" href="<?= \yii\helpers\Url::to(['/news/' . $v['slug'] .'/']); ?>">
                        <img src="<?= \yii\helpers\Url::imgUrl($v['fa_logo'], 'posts', [205, 110])?>">
                    </a>
                </div>
                <h3 class="entry-title"><a href="<?= \yii\helpers\Url::to(['/news/' . $v['slug'] .'/']); ?>"><?= $v['title']; ?></a></h3>
                <div class="entry-summary">
                    <?= strip_tags($v['short_text'])?>
                    <p class="quick-read-more"><a href="<?= \yii\helpers\Url::to(['/news/' . $v['slug'] .'/']); ?>">
                            Читать полностью						</a></p>
                </div>

            </li>
        <?php endforeach; ?>
    </ul>


        <?php echo \app\components\widgets\LinkPager::widget([
            'pagination' => $pager,
            'options' => ['class' => 'navigation clearfix'],
            'activePageCssClass' => 'active',
            //'linkClass' => 'link pagination-link',
            'tag' => 'li',
            //'tagClass' => 'pagination-item',
            'nextPageLabel' => '>',
            'prevPageLabel' => '<',
            //'nextPageCssClass' => 'link pagination-link',
            //'prevPageCssClass' => 'link pagination-link',
            'registerLinkTags' => true,
            'maxButtonCount' => 9
        ]);?>

</div><!-- #archive-posts -->
