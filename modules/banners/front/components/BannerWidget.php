<?php
namespace app\modules\banners\front\components;

use app\modules\banners\models\Banners;
use app\modules\banners\models\BannersStatistics;
use app\modules\languages\models\Languages;
use yii\base\Widget;
use yii\db\Expression;

class BannerWidget extends Widget
{
    public $banner = [];
    public $containerCss = null;
    public $view = null;
    public $colxs = 'col-xs-4';
    public function run()
    {
        $cache_name = 'banners_'
            . $this->banner['object_type'] . '_'
            . $this->banner['object_id'] . '_'
            . $this->banner['zone_id'] . '_'
            . Languages::getCurrent()->id
            . \Yii::$app->language;
        $banner = \Yii::$app->getCache()->get($cache_name);
        if ($banner === false) {
            $banner = Banners::find()
                ->leftJoin('banners_files_swf AS bfs', 'bfs.banner_id = banners.id')
                ->leftJoin('banners_files_image AS bfi', 'bfi.banner_id = banners.id')
                ->andWhere([
                    'OR',
                    [
                        'AND',
                        ['object_type' => $this->banner['object_type']],
                        ['object_id' => $this->banner['object_id']],
                        ['zone_id' => $this->banner['zone_id']],
                    ],
                    [
                        'AND',
                        ['object_type' => $this->banner['object_type']],
                        ['object_id' => null],
                        ['zone_id' => $this->banner['zone_id']],
                    ],
                    [
                        'AND',
                        ['object_type' => null],
                        ['object_id' => null],
                        ['zone_id' => $this->banner['zone_id']],
                    ]
                ])
                ->andWhere(['OR',
                    ['from' => null],
                    ['<=', 'from', new Expression('NOW()')]
                ])
                ->andWhere(['OR',
                    ['to' => null],
                    ['>=', 'to', new Expression('NOW()')]
                ])
                ->orderBy([
                    'view_time' => SORT_ASC
                ])
                ->addSelect([
                    'banners.*',
                    'CONCAT_WS("/", bfi.subdir, bfi.basename) AS image',
                    'CONCAT_WS("/", bfs.subdir, bfs.basename) AS swf_file'
                ])
                ->limit(1)
                ->asArray()
                ->one();
            \Yii::$app->getCache()->set($cache_name, $banner, 3600*24);
        }

        if ($banner) {
            \Yii::$app->db->createCommand()->update(
                'banners',
                ['view_time' => new Expression('NOW()')],
                ['id' => $banner['id']]
            );


            /*$_banner = Banners::findOne($banner['id']);
            $_banner->view_time = new Expression('NOW()');
            $_banner->save();*/

            $banner = $this->render($this->view ? 'banner-' .$this->view : 'banner-widget', [
                'model' => $banner,
                'banner' => $this->banner,
                'containerCss' => $this->containerCss,
                'colxs' => $this->colxs
            ]);
            return $banner;
        }
    }
}
