<?php
$banners = new \app\modules\banners\models\Banners();
if (!$model['html'] && $model['swf_file'])
{
    $this->registerJs('
        var el = document.getElementById("banner' . $model['id'] . '");
        swfobject.embedSWF("' . \yii\helpers\Url::imgUrl($model['swf_file'], 'banners') . ($model['link'] ? '?clickTAG=' . $model['link'] : '') . '", el, ' . $banners::$banner_sizes[$banner['zone_id']]['w'] . ', ' . $banners::$banner_sizes[$banner['zone_id']]['h'] . ', 9,null,null,{wmode:"transparent"}, {});
    ');
}
?>

<figure class="banner--secondary"><?php if ($model['html']): echo strtr($model['html'], ['<p>'=>'', '</p>'=>'']); else : ?>
    <span id="banner<?php echo $model['id']; ?>">
            <?php if($model['image']): ?>
                <?php if ($model['link']): ?>
                    <a href="<?= $model['link']; ?>">
                        <img src="<?php echo \yii\helpers\Url::imgUrl($model['image'], 'banners', [], true); ?>" />
                    </a>
                <?php else: ?>
                    <img src="<?php echo \yii\helpers\Url::imgUrl($model['image'], 'banners', [], true); ?>" />
                <?php endif; ?>
            <?php endif; ?>
        </span>
    <?php endif; ?></a></figure>
