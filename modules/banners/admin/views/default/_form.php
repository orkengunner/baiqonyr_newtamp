<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if((!$model->object_type && $model->object_id === null) || !$model::$object_types[$model->object_type]) $this->registerJs('
    $(".field-banners-object_id").css(\'display\', \'none\');
');
?>

<div class="pages-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-9">
            <div class="widget">
                <div class="widget-content padding">

                    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

                    <?= $form->field($model, 'html')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'link')->textInput(['maxlength' => 255]) ?>

                    <?= $form->field($model, 'from')->widget(\janisto\timepicker\TimePicker::className(), [
                        //'language' => 'fi',
                        'mode' => 'datetime',
                        'addon' => '',
                        'clientOptions'=>[
                            'dateFormat' => 'dd/mm/yy',
                            'timeFormat' => 'HH:mm',
                        ]
                    ])->textInput(['value' => $model->from ? date('d/m/Y H:i', strtotime($model->from)) : '', 'placeholder' => date('d/m/Y H:i')]) ?>

                    <?= $form->field($model, 'to')->widget(\janisto\timepicker\TimePicker::className(), [
                        //'language' => 'fi',
                        'mode' => 'datetime',
                        'addon' => '',
                        'clientOptions'=>[
                            'dateFormat' => 'dd/mm/yy',
                            'timeFormat' => 'HH:mm',
                        ]
                    ])->textInput(['value' => $model->to ? date('d/m/Y H:i', strtotime($model->to)) : '', 'placeholder' => date('d/m/Y H:i')]) ?>

                    <?= $form->field($model, 'zone_id')->dropDownList(\app\modules\banners\models\BannerZone::getDropdownList()) ?>

                    <?= $form->field($model, 'object_type')->dropDownList($model::$object_types_title, [
                        'prompt' => 'Выберите раздел',
                        'onchange' => 'mAjax.doit("/admin.php/banners/get-objects", {object_type: this.value});'
                    ]) ?>


                    <?php
                        $object_ids = [];
                        if ($model->object_type) {
                            $tbl = $model::$object_types[$model->object_type];
                            if ($tbl) {
                                $tbl = new $tbl();
                                $object_ids = \yii\helpers\ArrayHelper::merge(
                                    [0=> 'Главная (список)'],
                                    $tbl->getDropdownList('title', $tbl instanceof \app\modules\news\models\News ? 'newsID' : 'id')
                                );
                            }
                        }

                        echo $form->field($model, 'object_id')->dropDownList($object_ids, ['prompt' => 'Выберите страницу',])
                    ?>

                </div>
            </div>

            <div class="widget">
                <div class="widget-content padding">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('places', 'Создать') : Yii::t('places',
                        'Изменить'), [
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget">
                <div class="widget-header">
                    <h2>Статус</h2>
                </div>
                <div class="widget-content padding">
                    <?= $form->field($model,'status_id', ['template' => "{input}"])->checkbox() ?>
                </div>
            </div>

            <div class="widget">
                <div class="widget-header">
                    <h2>Заглушка</h2>
                </div>
                <div class="widget-content padding gallery-wrap">
                    <?php
                    echo \app\components\widgets\ImageUploadAsync::widget([
                        'model' => $model,
                        'attribute' => 'upload_image',
                        'form' => $form,
                        //'hint' => 'Рекомендуемый размер в пропорции 1,5',
                        'croppable' => false,
                        'previewPath' => $model->getFilePath('image', false),
                    ]); ?>
                </div>
            </div>

            <div class="widget">
                <div class="widget-header">
                    <h2>SWF файл</h2>
                </div>
                <div class="widget-content padding gallery-wrap">
                    <?php
                    echo \app\components\widgets\ImageUploadAsync::widget([
                        'model' => $model,
                        'attribute' => 'upload_swf',
                        'form' => $form,
                        //'hint' => 'Рекомендуемый размер в пропорции 1,5',
                        'croppable' => false,
                        'previewPath' => $model->getFilePath('swf', false),
                        'update_container' => false
                    ]); ?>
                </div>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script src="/js/ckeditor/ckeditor.js?<?= time() ?>"></script>
<script>
    CKEDITOR.replace('banners-html', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName === 'table') {
            var infoTab = dialogDefinition.getContents('info');
            var border = infoTab.get('txtBorder');
            border['default'] = "0";
        }
    });

    var setObjectId = function(data) {
        if(data === null) {
            $(".field-banners-object_id").css('display', 'none');
            $(".field-banners-object_id select").val('');
        } else {
            $(".field-banners-object_id").css('display', '');
            var $el = $(".field-banners-object_id select");
            $el.html(' ');
            $el.append($("<option></option>")
                .attr("value", '').text('Выберите страницу'));
            $el.append($("<option></option>")
                .attr("value", 0).text('Главная (список)'));
            $.each(data, function(key, value) {
                $el.append($("<option></option>")
                    .attr("value", key).text(value));
            });
        }
    }

</script>