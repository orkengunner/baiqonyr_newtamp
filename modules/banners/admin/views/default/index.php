<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pages\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('banners', 'Баннеры');
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="pages-index">
    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <p>
        <?= Html::a(Yii::t('banners', '<i class="icon-list-add"></i> Добавить баннер', [
            'modelClass' => 'Banners',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div class="widget">
        <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'get']); ?>

        <?php \yii\widgets\ActiveForm::end(); ?>
    </div>

    <div class="widget">
        <div class="widget-content">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'title',
                    [
                        'attribute' => 'bzTitle',
                        'value' => function ($data) {
                            return $data->bzTitle;
                        },
                        'filter' => \app\modules\banners\models\BannerZone::getDropdownList()
                    ],
                    [
                        'attribute' => 'status_id',
                        'value' => function ($data) {
                            return \app\modules\banners\models\Banners::$statuses[$data->status_id];
                        },
                        'filter' => \app\modules\banners\models\Banners::$statuses
                    ],

                    ['class' => \app\components\admin\RFAActionColumn::className(), 'template' => '{update}{delete}',],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
