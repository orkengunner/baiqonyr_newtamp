<?php

namespace app\modules\banners\admin\controllers;

use app\components\AdminController;
use app\modules\banners\models\BannersSearch;
use Yii;
use app\modules\banners\models\Banners;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;

/**
 * DefaultController implements the CRUD actions for Banners model.
 */
class DefaultController extends AdminController
{

    protected $_modelName = '\app\modules\banners\models\Banners';

    /**
     * Lists all Banners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banners model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banners();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Banners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Banners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        //$model->unlinkAll('bannerPlaces', true);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteFile($id) {
        $model = Banners::findOne($id);

        if ($model) {
            $dir = explode('/', $model->swf_file)[0];
            FileHelper::deleteDirectories(Yii::getAlias('@media') . '/banners/' . $dir);
            $model->swf_file = null;
            $model->save();
        }

    }

    public function actionGetObjects($object_type)
    {
        $data = null;
        $tbl = $object_type ? Banners::$object_types[$object_type] : null;
        if ($tbl) {
            $tbl = new $tbl();
            $data = $tbl->getDropdownList('title', $object_type == 2 ? 'newsID' : 'id');
        }

        return json_encode([
            'data' => $data,
            'callback' => 'setObjectId'
        ]);
    }

}
