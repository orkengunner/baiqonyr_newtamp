<?php

namespace app\modules\banners\models;

use yii\base\Model;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class BannersSearch extends Banners
{

    public function rules()
    {
        return [
            [['title'], 'string'],
            [['status_id'], 'integer'],
        ];
    }

    public function scenarios()
    {
        $scenarios = Model::scenarios();
        $scenarios['search'] = ['title', 'status_id', 'bzTitle'];
        return $scenarios;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Banners::find()
            ->innerJoin('banner_zone AS bz', 'banners.zone_id = bz.id')
            ->select('banners.id, banners.title, banners.status_id, bz.title AS bzTitle');;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['title' => SORT_ASC   ]],

        ]);

        $dataProvider->setSort([
            'attributes' => [
                'title',
                'status_id',
                'bzTitle',
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'like', 'banners.title', $this->title,
        ]);

        if (!empty($this->status_id)) {
            $query->andWhere(['banners.status_id' => $this->status_id]);
        }

        if (!empty($this->bzTitle)) {
            $query->andWhere(['banners.zone_id' => $this->bzTitle]);
        }

        return $dataProvider;
    }
}
