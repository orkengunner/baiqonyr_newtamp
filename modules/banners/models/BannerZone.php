<?php

namespace app\modules\banners\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "banner_zone".
 *
 * @property string $id
 * @property string $title
 *
 * @property Banners[] $banners
 */
class BannerZone extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner_zone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners()
    {
        return $this->hasMany(Banners::className(), ['zone_id' => 'id']);
    }
}
