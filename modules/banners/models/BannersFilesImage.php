<?php

namespace app\modules\banners\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "banners_files_image".
 *
 * @property string $banner_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Banners $banner
 */
class BannersFilesImage extends ActiveRecord
{
    public static $keyField = 'banner_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banners_files_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['banner_id', 'subdir', 'basename'], 'required'],
            [['banner_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255],
            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banners::className(), 'targetAttribute' => ['banner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'banner_id' => 'Banner ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banners::className(), ['id' => 'banner_id']);
    }
}
