<?php

namespace app\modules\banners\models;

use app\components\ActiveRecord;
use app\components\traits\UploadableAsync;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "banners".
 *
 * @property string $id
 * @property string $title
 * @property string $html
 * @property integer $status_id
 * @property string $zone_id
 * @property string $link
 * @property integer $object_type
 * @property string $object_id
 * @property string $view_time
 * @property string $from
 * @property string $to
 *
 * @property BannerZone $zone
 * @property BannersFilesImage[] $bannersFilesImages
 * @property BannersFilesSwf[] $bannersFilesSwfs
 */
class Banners extends ActiveRecord
{
    use UploadableAsync;

    const OBJECT_TYPE_TEXT_PAGE = 1;
    const OBJECT_TYPE_NEWS = 2;


    public static $statuses = [
        1 => 'Активный',
        0 => 'Неактивный',
    ];

    public static $object_types = [
        self::OBJECT_TYPE_TEXT_PAGE => 'app\modules\pages\models\Pages',
        self::OBJECT_TYPE_NEWS => 'app\modules\news\models\News',
    ];

    public static $object_types_title = [
        self::OBJECT_TYPE_NEWS => 'Новости',
        self::OBJECT_TYPE_TEXT_PAGE => 'Текстовые страницы',
    ];

    const BANNER_ZONE_SIDEBAR_240x400 = 1;
    const BANNER_ZONE_INSIDE = 2;
    const BANNER_ZONE_STOCK = 3;
    const BANNER_ZONE_IN_CONTENT_240x400 = 4;

    public static $banner_sizes = [
        self::BANNER_ZONE_SIDEBAR_240x400 => [
            'w' => 240,
            'h' => 400
        ],
        self::BANNER_ZONE_INSIDE => [
            'w' => 285,
            'h' => null
        ],
        self::BANNER_ZONE_STOCK => [
            'w' => 285,
            'h' => null
        ],
        self::BANNER_ZONE_IN_CONTENT_240x400 => [
            'w' => 240,
            'h' => 400
        ],
    ];


    public $bzTitle;

    public $upload_image = null;
    public $upload_swf = null;

    public $file_attributes = [
        'image',
        'swf'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'zone_id'], 'required'],
            [['html'], 'string'],
            [['status_id', 'zone_id', 'object_type', 'object_id'], 'integer'],
            [['view_time', 'from', 'to'], 'safe'],
            [['title', 'link'], 'string', 'max' => 255],
            [['zone_id'], 'exist', 'skipOnError' => true, 'targetClass' => BannerZone::className(), 'targetAttribute' => ['zone_id' => 'id']],
            [
                ['upload_image'],
                'image',
                'on' => 'upload'
            ],
            [
                ['upload_swf'],
                'file',
                'on' => 'upload'
            ],
        ];
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'title' => 'Название банера',
            'link' => 'Ссылка',
            'html' => 'Html',
            'object_type' => 'Раздел',
            'zone_id' => 'Расположение на странице / Тип',
            'bzTitle' => 'Расположение на странице / Тип',
            'status_id' => 'Активный',
            'from' => 'Показывать с',
            'to' => 'Показывать до',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZone()
    {
        return $this->hasOne(BannerZone::className(), ['id' => 'zone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannersFilesImages()
    {
        return $this->hasMany(BannersFilesImage::className(), ['banner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannersFilesSwfs()
    {
        return $this->hasMany(BannersFilesSwf::className(), ['banner_id' => 'id']);
    }

    public function getSwf()
    {
        return $this->hasOne(BannersFilesSwf::className(), ['banner_id' => 'id']);
    }

    public static function find()
    {
        $query = new BannersQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query;
        }
        return $query->isActive('banners');
    }

    public function afterSave($insert, $changedAttributes)
    {

        if ($this->hasAsyncTempFile('upload_image')) {
            $image = $this->setFile('image', $this->saveAsyncFile('upload_image'));
        }

        if ($this->hasAsyncTempFile('upload_swf')) {
            $image = $this->setFile('swf', $this->saveAsyncFile('upload_swf'));
        }

        parent::afterSave($insert, $changedAttributes);
        return true;

    }

    public function beforeSave($insert)
    {
        if($this->from && $dt = \DateTime::createFromFormat('d/m/Y H:i', $this->from)) {
            $this->from = $dt->format('Y-m-d H:i');
        }
        if($this->to && $dt = \DateTime::createFromFormat('d/m/Y H:i', $this->to)) {
            $this->to = $dt->format('Y-m-d H:i');
        }

        return parent::beforeSave($insert);
    }
}
