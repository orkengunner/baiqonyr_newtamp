<?php

namespace app\modules\socialnetworks\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "lookup_soc_nets".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 *
 * @property SocNetsTokens[] $socNetsTokens
 */
class LookupSocNets extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_soc_nets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocNetsTokens()
    {
        return $this->hasMany(SocNetsTokens::className(), ['soc_net_id' => 'id']);
    }

    public static function getBySlug($slug)
    {
        return self::findOne(['slug' => $slug]);
    }

    public static function getEauth($service = null)
    {
        if($service) return \Yii::$app->get('eauth')->getIdentity($service);
        $eauth = [];
        foreach(self::find()->all() as $v) {
            $eauth[$v->id] = \Yii::$app->get('eauth')->getIdentity($v->slug);
        }
        return $eauth;
    }
}
