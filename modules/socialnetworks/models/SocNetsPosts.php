<?php

namespace app\modules\socialnetworks\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "soc_nets_posts".
 *
 * @property string $id
 * @property integer $soc_net_id
 * @property string $soc_post_id
 * @property string $object_id
 * @property integer $object_type
 * @property string $create_date
 *
 * @property LookupSocNets $socNet
 */
class SocNetsPosts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'soc_nets_posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['soc_net_id', 'soc_post_id', 'object_id', 'object_type'], 'required'],
            [['soc_net_id', 'object_id'], 'integer'],
            [['create_date'], 'safe'],
            [['object_type'], 'string', 'max' => 50],
            [['soc_post_id'], 'string', 'max' => 255],
            [['soc_net_id'], 'exist', 'skipOnError' => true, 'targetClass' => LookupSocNets::className(), 'targetAttribute' => ['soc_net_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'soc_net_id' => 'Soc Net ID',
            'soc_post_id' => 'Soc Post ID',
            'object_id' => 'Object ID',
            'object_type' => 'Object Type ID',
            'create_date' => 'Create Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocNet()
    {
        return $this->hasOne(LookupSocNets::className(), ['id' => 'soc_net_id']);
    }

    public function behaviors()
    {
        return [
            $this->timestampBehaviorCreate
        ];
    }
}
