<?php

namespace app\modules\socialnetworks\models;

use app\components\ActiveRecord;
use app\modules\users\models\Users;
use Yii;

/**
 * This is the model class for table "soc_nets_tokens".
 *
 * @property string $id
 * @property string $user_id
 * @property integer $soc_net_id
 * @property integer $soc_user_id
 * @property string $token
 * @property string $token_expires
 * @property string $create_date
 * @property string $update_date
 *
 * @property LookupSocNets $socNet
 * @property Users $user
 */
class SocNetsTokens extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'soc_nets_tokens';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'soc_net_id', 'token', 'soc_user_id'], 'required'],
            [['user_id', 'soc_net_id'], 'integer'],
            [['token_expires', 'create_date', 'update_date'], 'safe'],
            [['token'], 'string', 'max' => 255],
            [['soc_net_id'], 'exist', 'skipOnError' => true, 'targetClass' => LookupSocNets::className(), 'targetAttribute' => ['soc_net_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'soc_net_id' => 'Soc Net ID',
            'token' => 'Token',
            'token_expires' => 'Token Expires',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocNet()
    {
        return $this->hasOne(LookupSocNets::className(), ['id' => 'soc_net_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior
        ];
    }
}
