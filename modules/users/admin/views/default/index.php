<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\users\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'username',
                        'first_name',
                        'last_name',
                        // 'password',
                        'email:email',
                        // 'create_date',
                        // 'update_date',
                        // 'is_active',
                        [
                            'attribute' => 'role_id',
                            'value' => function ($data) {
                                return \app\modules\lookupusersroles\models\LookupUsersRoles::getDropdownList()[$data->role_id];
                            },
                            'filter' => \app\modules\lookupusersroles\models\LookupUsersRoles::getDropdownList()
                        ],
                        // 'about',
                        // 'phone',
                        // 'forgot_token',
                        // 'is_subscribe',
                        // 'lang_id',
                        // 'delete_by',

                        [
                            'class' => \app\components\admin\RFAActionColumn::className(),
                            'template' => '{update} {remove}'
                        ],
                    ],
                ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
