<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php  $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-9">
            <div class="widget">
                <div class="widget-content padding">

                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'about')->textarea() ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'value' => '']) ?>

                    <?= $form->field($model, 'role_id')->dropDownList(\app\modules\lookupusersroles\models\LookupUsersRoles::getDropdownList()) ?>

                    <?= $form->field($model, 'is_subscribe')->checkbox() ?>




                </div>
            </div>
        </div>
        <div class="col-md-3">
            <?= \app\components\widgets\PublishData::widget([
                'model' => $model,
                'form' => $form,
                'exclude' => ['publish_date', 'slug']
            ])?>
            <div class="widget">
                <div class="widget-header">
                    <h2>Миниатюра записи</h2>
                </div>
                <div class="widget-content padding gallery-wrap">
                    <?php
                    echo \app\components\widgets\ImageUploadAsync::widget([
                        'model' => $model,
                        'attribute' => 'upload_avatar',
                        'form' => $form,
                        //'hint' => 'Рекомендуемый размер в пропорции 1,5',
                        'croppable' => false,
                        'previewPath' => $model->getFilePath('avatar', false),
                    ]); ?>
                </div>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
