<?php

namespace app\modules\users\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email'], 'required', 'message' => 'Введите почту'],
            [['password'], 'required', 'message' => 'Введите пароль'],
            [['email'], 'email', 'message' => 'Укажите корректную почту'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('front-reg', 'Имя пользвоателя'),
            'email' => Yii::t('front-reg', 'Почта'),
            'password' => Yii::t('front-reg', 'Пароль'),
            'rememberMe' => Yii::t('front-reg', 'Запомнить меня')
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Неверный логин или пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            if (Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0))
            {
                $user = Users::findOne($this->_user->id);
                $user->auth_key = $this->_user->auth_key;
                $user->permissions = serialize($user->permissions);
                $user->save();
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return UserIdentity|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserIdentity::findByEmail($this->email);
        }

        return $this->_user;
    }
}
