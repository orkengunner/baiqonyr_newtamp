<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "users_files_avatar".
 *
 * @property string $user_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Users $user
 */
class UsersFilesAvatar extends \yii\db\ActiveRecord
{
    public static $keyField = 'user_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_files_avatar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'subdir', 'basename'], 'required'],
            [['user_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
