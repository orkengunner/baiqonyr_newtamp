<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 7/24/14
 * Time: 13:49
 */

namespace app\modules\users\models;


use app\components\wUniSender;
use yii\base\Model;

class RegistrationForm extends Model
{
    public $username;
    public $password;
    public $confirmPassword;
    public $email;
    public $phone;
    public $firstName;

    public function rules()
    {
        return [
            [['email'], 'required', 'message' => 'Введите почту'],
            [['firstName'], 'required', 'message' => 'Введите имя'],
            [['phone'], 'required', 'message' => 'Введите телефон'],
            [['email'], 'unique', 'targetClass' => 'app\modules\users\models\Users', 'targetAttribute' => ['email']],
            [['phone'], 'unique', 'targetClass' => 'app\modules\users\models\Users', 'targetAttribute' => ['phone']],
            //[['username'], 'unique', 'targetClass' => 'app\modules\users\models\Users', 'targetAttribute' => ['username']],
            [['password', 'confirmPassword'], 'required', 'message' => 'Введите пароль'],
            [['email'], 'email', 'message' => 'Укажите корректную почту'],
            //[['phone'], 'safe'],
            [['confirmPassword'], 'compare', 'compareAttribute' => 'password']
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('front-reg', 'Имя пользователя'),
            'password' => \Yii::t('front-reg', 'Пароль'),
            'confirmPassword' => \Yii::t('front-reg', 'Потверждение пароля'),
            'email' => \Yii::t('front-reg', 'Почта'),
            'phone' => \Yii::t('front-reg', 'Телефон'),
            'firstName' => \Yii::t('front-reg', 'Как вас зовут?'),
        ];
    }

    public function registration()
    {
        if ($this->validate()) {
            $model = new Users();
            $model->username = $this->username ? $this->username : 'Неопознанный киноман';
            $model->password = md5($this->password);
            $model->email = $this->email;
            $model->role = Users::ROLE_USER;
            $model->phone = $this->phone;
            $model->firstName = $this->firstName;

            if ($model->save()) {
                $sender = new wUniSender(\Yii::$app->params['uniSender']['key']);
                $sender->request('subscribe',[
                    'list_ids' => \Yii::$app->params['uniSender']['digestList'],
                    'fields[email]' => $this->email,
                    'fields[ID]' => $model->id,
                    'double_optin' => 1
                ]);
                return $model->id;
            }
        }
        //$model->validate();
        //print_r($this->errors);exit;
        return false;
    }
}
