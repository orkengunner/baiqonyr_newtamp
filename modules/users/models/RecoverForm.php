<?php

namespace app\modules\users\models;

use app\components\wMail;
use app\modules\emailtemplates\models\EmailTemplates;
use Yii;
use yii\base\Model;
use yii\helpers\StringHelper;

class RecoverForm extends Model
{
    public $email;
    public $token;
    public $password;
    public $user = null;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email'], 'required', 'on' => 'firstStep'],
            [['password'], 'required', 'on' => 'secondStep'],
            [['email'], 'exist', 'targetClass' => 'app\modules\users\models\Users', 'targetAttribute' => ['email']],
            [['email'], 'email'],
            [['password'], 'string', 'min' => 6, 'max' => 12],
            [['token'], 'string', 'min' => 32, 'max' => 32],
            [['password'], 'checkToken'],
        ];
    }

    public function checkToken($attribute)
    {
        $this->user = Users::findOne(['forgot_token' => $this->token]);

        if (!$this->user || !($this->user->forgot_token == md5($this->user->id . date('Y-m-d', strtotime('-1 day'))) || $this->user->forgot_token = md5($this->user->id . date('Y-m-d'))))
            $this->addError($attribute, \Yii::t('user', 'Время истекло. Начните востановление занова'));
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('users', 'Почта'),
            'password' => 'Пароль',
        ];
    }

    protected function sendFirstMail()
    {

    }

   public function recover()
   {

       if (!$this->token) {
           $this->scenario = 'firstStep';
           if ($this->validate())
           {
               $user = Users::findOne(['email' => $this->email]);
               $user->forgot_token = md5($user->id . date('Y-m-d'));
               if ($user->save())
               {
                   wMail::send(
                       EmailTemplates::FORGOT_PASSWORD,
                       [$this->email],
                       [
                           '%name%' => $user->name,
                           '%link%' => 'http://' . $_SERVER['HTTP_HOST'] . '/reset/' . $user->forgot_token . '/',
                       ]
                   );
                   return 1;
               }
           }
       } else if ($this->token) {
           $this->scenario = 'secondStep';

           if ($this->validate()) {
               //$paswd = StringHelper::random(5,5);

               $this->user->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);
               $this->user->forgot_token = null;
               $this->user->save();
               wMail::send(
                   EmailTemplates::FORGOT_PASSWORD_FINAL,
                   [$this->user->email],
                   [
                       '%name%' => $this->user->name,
                       '%password%' => $this->password,
                   ]
               );

               return 2;
           }
       }

       return false;
   }
}
