<?php
/**
 * Created by PhpStorm.
 * User: yevgeniy
 * Date: 11/4/14
 * Time: 5:05 PM
 */

namespace app\modules\users\models;




use app\components\ActiveQuery;

class UsersQuery extends ActiveQuery
{
    public function withUsersSocNets($join = 'inner')
    {
        return $this->{$join . 'Join'}('users_soc_nets AS usn', 'usn.user_id = users.id');
    }
}
