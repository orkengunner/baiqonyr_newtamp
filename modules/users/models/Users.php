<?php

namespace app\modules\users\models;

use app\components\ActiveRecord;
use app\components\traits\UploadableAsync;
use app\modules\images\models\Images;
use app\modules\languages\models\Languages;
use app\modules\lookupusersroles\models\LookupUsersRoles;
use app\modules\socialnetworks\models\LookupSocNets;
use app\modules\socialnetworks\models\SocNetsTokens;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property string $socUserID
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $firstName
 * @property string $lastName
 * @property string $about
 * @property string $birthDay
 * @property integer $sex
 * @property string $fbProfile
 * @property string $twProfile
 * @property string $vkProfile
 * @property integer $userType
 * @property integer $subscribe
 * @property integer $blog
 * @property string $created
 * @property string $auth_key
 * @property string $permissions
 * @property integer $socNet
 * @property integer $balance
 * @property integer $statusID
 * @property string $role
 * @property string $phone
 * @property string $rhID
 * @property integer $lang_id
 *
 * @property Comments[] $comments
 * @property Comments[] $removedComments
 * @property Gallery[] $galleries
 * @property Gallery[] $removedGalleries
 * @property Pages[] $pages
 * @property Pages[] $removedPages
 * @property Posts[] $posts
 * @property Posts[] $removedPosts
 * @property Languages $lang
 * @property UsersFilesAvatar[] $usersFilesAvatars
 * @property SocNetsTokens[] $socNetsTokens
 */
class Users extends ActiveRecord
{
    const USER_ADMIN = 1;
    const ROLE_USER = 'user';
    const PERMISSION_BAYKONUR = 32;
    const PERMISSION_BAYKONUR_JURI = 34;
    use UploadableAsync;
    public static $statuses = [
        self::STATUS_INACTIVE => 'Неактивный',
        self::STATUS_ACTIVE => 'Активный',
    ];
    public $_password = null;
    public $upload_avatar = null;

    public $file_attributes = [
        'avatar',
    ];

    public $avatar;

    public static function getDb(){
        return \Yii::$app->dbBrod;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about', 'permissions'], 'string'],
            [['birthDay', 'created'], 'safe'],
            [['sex', 'userType', 'subscribe', 'blog', 'socNet', 'balance', 'statusID', 'rhID', 'lang_id'], 'integer'],
            [['socUserID'], 'string', 'max' => 20],
            [['username', 'email', 'firstName', 'lastName', 'fbProfile', 'twProfile', 'vkProfile', 'auth_key', 'role', 'phone'], 'string', 'max' => 255],
            //[['password'], 'string', 'max' => 50],
            [['phone'], 'unique'],
            [['email'], 'email', 'on' => 'recover', 'message' => 'Укажите корректную почту'],
            [['email'], 'exist', 'on' => 'recover', 'message' => 'E-mail не найден'],
            [['email'], 'required', 'on' => 'recover', 'message' => 'Введите почту'],
            [['username', 'role', 'password', 'socUserID'], 'required', 'on' => ['login_sn']],
        ];
    }

    public function getUserName()
    {
        return $this->firstName || $this->lastName ? $this->firstName . ' ' . $this->lastName : $this->username;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'role' => 'Роль',
            'forgot_token' => 'Forgot Token',
            'is_subscribe' => 'Подписка на рассылку',
            'about' => 'Обо мне',
            'password' => 'Пароль',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemovedComments()
    {
        return $this->hasMany(Comments::className(), ['delete_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleries()
    {
        return $this->hasMany(Gallery::className(), ['delete_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemovedGalleries()
    {
        return $this->hasMany(Gallery::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Pages::className(), ['delete_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemovedPages()
    {
        return $this->hasMany(Pages::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Posts::className(), ['delete_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemovedPosts()
    {
        return $this->hasMany(Posts::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocNetsTokens()
    {
        return $this->hasMany(SocNetsTokens::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(LookupUsersRoles::className(), ['id' => 'role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersFilesAvatars()
    {
        return $this->hasMany(UsersFilesAvatar::className(), ['user_id' => 'id']);
    }

    public function isAdmin()
    {
        return $this->role == LookupUsersRoles::ROLE_ADMIN;
    }

    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function beforeValidate()
    {
        if(!$this->password) {
            $this->password = $this->_password;
        }
        /*else {
            $this->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }*/
        return parent::beforeValidate();
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->_password = $this->password;
        $this->password = null;

        if ($this->permissions && is_array(unserialize($this->permissions))) $this->permissions = unserialize($this->permissions);
        else $this->permissions = [];

        return true;
    }

    public function setAvatar($path, $name)
    {
        /*$logo = $this->getAvatar();

        if ($logo) { return false;
        } else {
            $images = new Images();
        }*/

        $images = new Images();
        $images->name = $name;
        $images->objectID = $this->id;
        $images->oTypeID = Images::IMAGE_USER_PROFILE;
        if ($images->save()) {
            $sizes = $this->getParam('avatar.sizes');

            foreach ($sizes as $size) {
                $pathinfo = pathinfo($name);



                Image::$driver = [Image::DRIVER_GD2, Image::DRIVER_IMAGICK];
                Image::thumbnail(
                    $path . '/' .  $pathinfo['basename'],
                    $size[0], $size[1]
                )->save($path . '/' . $pathinfo['filename'] . '-' . $size[0] . 'x' . $size[1] . "." . $pathinfo['extension']);
            }

        }
    }



    public function getAvatar()
    {
        $images = new Images();
        $res = $images->getImageByObjectID($this->id, Images::IMAGE_USER_PROFILE)->one();
        return $res;
    }


}
