<?php

namespace app\modules\users\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\users\models\Users;

/**
 * UsersSearch represents the model behind the search form about `app\modules\users\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id', 'role_id', 'is_subscribe', 'lang_id', 'delete_by'], 'integer'],
            [['username', 'first_name', 'last_name', 'password', 'email', 'create_date', 'update_date', 'about', 'phone', 'forgot_token'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'status_id' => $this->status_id,
            'role_id' => $this->role_id,
            'is_subscribe' => $this->is_subscribe,
            'lang_id' => $this->lang_id,
            'delete_by' => $this->delete_by,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'about', $this->about])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'forgot_token', $this->forgot_token]);

        return $dataProvider;
    }
}
