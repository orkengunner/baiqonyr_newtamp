<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 7/18/14
 * Time: 10:24
 */

namespace app\modules\users\models;


use app\components\ActiveRecord;
use app\components\wMail;
use app\modules\emailtemplates\models\EmailTemplates;
use app\modules\lookupsocnets\models\LookupSocNets;
use app\modules\lookupusersroles\models\LookupUsersRoles;
use app\modules\statuses\models\Statuses;
use nodge\eauth\ErrorException;
use yii\base\NotSupportedException;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

class UserIdentity extends Users implements IdentityInterface
{
    /**
     * @var array EAuth attributes
     */
    public $profile;

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.logo)
     */
    public static function findIdentity($id)
    {
        if (\Yii::$app->getSession()->has('user-'.$id)) {
            return new self(\Yii::$app->getSession()->get('user-'.$id));
        }
        else
            return static::findOne(['id' => $id, 'statusID' => static::STATUS_ACTIVE]);

    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        //print_r($authKey);
        //print_r($this->getAuthKey());

        //exit;
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Finds user by username
     * @param $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username, 'statusID' => static::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return self::findOne(['email' => $email, 'statusID' => static::STATUS_ACTIVE]);
    }

    public function getSnByName($name)
    {
        return $this->{$name};
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return md5($password) == $this->_password;
    }

    /**
     * Check user role
     * @param $role string|array
     * @return bool
     */
    public function checkRole($role)
    {
        if (is_array($role)) {
            return in_array($this->role, $role);
        } else {
            return $this->role === $role;
        }
    }

    public function userAttributes()
    {
        $data = new \stdClass();

        foreach ($this->attributes() as $v)
            $data->{$v} = $this->{$v};
           /* $logo = $this->getIdentitiyLogo();
        if (isset($logo->basename))
            $data->logo = $logo;
        else
            $data->logo = $logo;
*/
        return $data;
    }

    public function getPermissions()
    {
        return $this->permissions;
    }

    public static function findByEAuth($service)
    {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        //$id = $service->getId();
        $attributes = $service->getAttributes();
		switch($attributes['service']) {
			case 'vkontakte':
				$user = Users::find()->andWhere(['AND',
					['vkSocUserID' => $attributes['socUserID']],
					['<>', 'statusID', ActiveRecord::STATUS_MERGE_SN]
				])->one();
				break;
			case 'facebook':
				$user = Users::find()->andWhere(['AND',
					['socUserID' => $attributes['socUserID']],
					['<>', 'statusID', ActiveRecord::STATUS_MERGE_SN]
				])->one();
				break;
			case 'twitter':
				$user = Users::find()->andWhere(['AND',
					['twSocUserID' => $attributes['socUserID']],
					['<>', 'statusID', ActiveRecord::STATUS_MERGE_SN]
				])->one();
				break;
		}


        //print_r($attributes);exit;

        if (!$user) {
            $user = new Users(['scenario' => 'login_sn']);
            $user->load(['Users' => $attributes]);

			$user->socUserID = null;

			switch($attributes['service']) {
				case 'vkontakte':
					if(!$user->vkSocUserID) $user->vkSocUserID = $attributes['socUserID'];
					break;
				case 'facebook':
					if(!$user->socUserID) $user->socUserID = $attributes['socUserID'];
					break;
				case 'twitter':
					if(!$user->twSocUserID) $user->twSocUserID = $attributes['socUserID'];
					break;
			}

			$user->merge_sn_flag = 1;

            $user->role = Users::ROLE_USER;
            $user->statusID = ActiveRecord::STATUS_ACTIVE;
            $user->password = md5('p'.$attributes['socUserID']);
            $user->subscribe = 0;
            if (!$user->save()) $user = null;
            else {
                if(isset($attributes['photo']) && !$user->getAvatar()) {
                    $path = '/var/www/html/web/media/users';

                    $subdir = FileHelper::generateSubdir($path);
                    $path = $path . '/' . $subdir;
                    FileHelper::forceDirectories($path);

                    file_put_contents($path . '/image.jpg', file_get_contents($attributes['photo']));
                    $user->avatar = $subdir . '/image.jpg';
                    $user->setAvatar($path, $subdir . '/image.jpg');
                }

                $replacement = [
                    '%username%' => $user->getUserName(),
                    '%userprofile%' => '<a href="http://brod.kz/user/profile/?id=' . $user->id . '">http://brod.kz/user/profile/?id=' . $user->id . '</a>',
                ];
                if($user->email) wMail::send(
                    EmailTemplates::REGISTRATION,
                    [$user->email],
                    $replacement
                );

                wMail::send(
                    EmailTemplates::NEW_USER,
                    [/*'kenzhibayev@gmail.com'*/'prokhonenkov@gmail.com'],
                    $replacement
                );
            }
        }
		$user = self::findOne($user->id);
		if ($user && $user->statusID == ActiveRecord::STATUS_ACTIVE)
            return $user;
        return false;
    }
}
