<?php
/**
 * Created by PhpStorm.
 * User: Vital
 * Date: 03.03.2015
 * Time: 16:29
 */
namespace app\modules\users\front\components;

use app\components\wMail;
use app\modules\emailtemplates\models\EmailTemplates;
use app\modules\users\models\InfoForm;
use app\modules\users\models\LoginForm;
use app\modules\users\models\PhoneForm;
use app\modules\users\models\RegistrationForm;
use app\modules\users\models\UserIdentity;
use app\modules\users\models\Users;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;

class AuthPopUp extends Widget
{
    public $type = 'login';

    public function run()
    {
        $js = false;
        $success = false;
        $error = 0;
        $watch = false;
        if (isset($_COOKIE['watch']) && $_COOKIE['watch'] == 1) $watch = true;
        setcookie('watch', 0, time()-3600, '/');
        switch ($this->type) {
            case 'login':
                $model = new LoginForm();

                if (\Yii::$app->request->post('LoginForm') && \Yii::$app->request->get('action') == 'login') {
                    if ($model->load(\Yii::$app->request->post()) && $model->login()) {

                        echo '<script>window.location.assign("' . rtrim(Url::to([
                                explode('?', \Yii::$app->request->url)[0] . ($watch ? '?watch=1' : '')
                            ]), '/') . '")</script>';

                        /*header('Location: ' . rtrim(Url::to([
                                explode('?', \Yii::$app->request->url)[0] . ($watch ? '?watch=1' : '')
                            ]), '/'));*/

                        exit;
                    }
                    $js = true;
                }
                return $this->render('login_popup', [
                    'model' => $model,
                    'js' => $js
                ]);
            case 'reg':
                $model = new RegistrationForm();

                if (\Yii::$app->request->post('RegistrationForm') && \Yii::$app->request->get('action') == 'reg') {
                    if ($model->load(\Yii::$app->getRequest()->post())) {
                        $id = $model->registration();
                        if ($id && \Yii::$app->getUser()->login(UserIdentity::findIdentity($id)))
                        {

                            $user = Users::findOne($id);
                            $replacement = [
                                '%username%' => $user->getUserName(),
                                '%userprofile%' => '<a href="http://brod.kz/user/profile/?id=' . $user->id . '">http://brod.kz/user/profile/?id=' . $user->id . '</a>',
                            ];
                            wMail::send(
                                EmailTemplates::REGISTRATION,
                                [$user->email],
                                $replacement
                            );

                            wMail::send(
                                EmailTemplates::NEW_USER,
                                ['kenzhibayev@gmail.com'],
                                $replacement
                            );


                            $url = Url::parse_url();

                           /* if($url['path'] == '/')
                                return \Yii::$app->getResponse()->redirect(rtrim(Url::to(['/?action=info']), '/'));
                            else*/
                           /* print_r(Url::to(
                                ArrayHelper::merge([$url['path']], $url['params'])
                            ));exit;*/
                            $params = [];
                            foreach($url['params'] as $k => $v) {
                                $params[] = $k . '=' . $v;
                            }
                                 header('Location: ' . $url['path'] . ($url['params'] ? '?' . implode('&', $params) : ''));

                                exit;
                        }
                    }
                    $js = true;
                }

                return $this->render('reg_popup', [
                    'model' => $model,
                    'js' => $js
                ]);

            case 'info':
                $model = new InfoForm();
                $model->firstName = \Yii::$app->controller->user->firstName;
                $model->phone = \Yii::$app->controller->user->phone;

                if (\Yii::$app->request->get('action') == 'info' || !\Yii::$app->controller->user->firstName || \Yii::$app->controller->user->firstName == 'Неопознанный киноман' || !\Yii::$app->controller->user->phone) {
                    //print_r(\Yii::$app->getRequest()->post());
                    if ($model->load(\Yii::$app->getRequest()->post())) {
                        $id = $model->save();
                        if ($id)
                        {
                            $url = Url::parse_url();

                            if($url['path'] == '/')
                                header('Location: /');
                            else {
                                foreach($url['params'] as $k => $v) {
                                    if($k != 'action')
                                    $params[] = $k . '=' . $v;
                                }
                                header('Location: ' . $url['path'] . ($url['params'] ? '?' . implode('&', $params) : ''));
                            }

                            exit;
                        }
                    } elseif(!\Yii::$app->controller->user->firstName || \Yii::$app->controller->user->firstName == 'Неопознанный киноман' || !\Yii::$app->controller->user->phone) {
                        $model->validate();
                    }
                    $js = true;
                }

                return $this->render('info_popup', [
                    'model' => $model,
                    'js' => $js
                ]);
            case 'phone':
                $model = new PhoneForm();
                $model->email = $model->oldEmail = \Yii::$app->controller->user->email;
                $model->phone = $model->oldPhone = \Yii::$app->controller->user->phone;
                //if (\Yii::$app->request->get('action') == 'phone') {
                    //print_r(\Yii::$app->getRequest()->post());
                    if ($model->load(\Yii::$app->getRequest()->post())) {
                        $id = $model->save();
                        if ($id)
                        {
                            $url = Url::parse_url();
                            $params = [];
                            if($url['path'] == '/')
                                header('Location: /');
                            else {
                                foreach($url['params'] as $k => $v) {
                                    if($k != 'action')
                                        $params[] = $k . '=' . $v;
                                }
                                header('Location: ' . $url['path'] . ($url['params'] ? '?' . implode('&', $params) : ''));
                            }
                            exit;
                        }
                    }
                    $js = true;
                //}

                return $this->render('phone_popup', [
                    'model' => $model,
                    'js' => $js
                ]);
            case 'recover':
                $model = new Users(['scenario' => 'recover']);
                $model->scenario = 'recover';

                if (\Yii::$app->request->post('Users') && \Yii::$app->request->get('action') == 'recover'){
                    if ($model->load(\Yii::$app->getRequest()->post()) && $model->validate('recover')) {
                        $identity = UserIdentity::findByEmail($model->email);

                        if ($identity)
                        {
                            $user = Users::findOne($identity->id);
                            $paswd = StringHelper::random(5,5);
                            $user->password = md5($paswd);
                            $user->permissions = serialize($user->permissions);

                            if ($user->save(false)) {

                                $replacement = [
                                    '%password%' => $paswd,
                                    '%username%' => $user->getUserName(),
                                    '%userprofile%' => '<a href="http://brod.kz/user/profile/edit/?id=' . $user->id . '">http://brod.kz/user/profile/edit/?id=' . $user->id . '</a>',
                                ];
                                wMail::send(
                                    EmailTemplates::FORGOT_PASSWORD,
                                    [$user->email],
                                    $replacement
                                );

                                //Users::sendEmailRecover($user, $paswd);
                                $success = true;
                            } else {
                                $error = 1;
                            }
                        }
                    } else {
                        $error = 2;
                    }
                    $js = true;
                }

                return $this->render('recover_popup', [
                    'model' => $model,
                    'js' => $js,
                    'error' => $error,
                    'success' => $success,
                ]);
        }
    }
}
