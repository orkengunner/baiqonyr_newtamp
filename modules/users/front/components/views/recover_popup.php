<?php if ($js)echo $this->registerJs("$('#recoverPopup').modal('show')");?>
<div class="modal fade modal--auth modal--pass" id="recoverPopup" tabindex="-1" role="dialog" aria-labelledby="modalPassLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon icon--close"></span></button>
                <div class="modal-title">
                    <?php
                    if ($error == 1) {
                        echo \Yii::t('front-reg', 'Внутренняя ошибка сервера. Повторите попытку позже.');
                    } else {
                        echo \Yii::t('front-reg', 'Восстановление пароля');
                    }
                    ?>
                </div>
            </div>
            <div class="modal-body">
                <?php if ($success): ?>
                    <div class="form-group" style="text-align: center">
                        <label class="control-label"><?= \Yii::t('front-reg', "Пароль отправлен вам на E-mail"); ?></label>
                    </div>
                <?php endif; ?>
                    <?php if (!$success && $error != 1): ?>
                    <?php $form = \yii\widgets\ActiveForm::begin(['action' => '?action=recover', 'options' =>  ['class'=> 'form form--auth']]); ?>
                    <?= $form->field($model, 'email')
                            ->textInput([
                                'placeholder'=>\Yii::t('front-reg', 'Укажите свою электронную почту'),
                                'onfocus'=>"if(this.placeholder  == '" . \Yii::t('front-reg', 'Укажите свою электронную почту') . "') {this.placeholder = '';}", 'onblur'=>"if(this.value == '') { this.placeholder = '" . \Yii::t('front-reg', 'Укажите свою электронную почту') . "'; }"
                            ]); ?>

                    <div class="form-group form-group--submit">
                        <?= \yii\helpers\Html::submitButton(\Yii::t('front-reg', 'Отправить'), ['class' => 'btn btn--primary', 'name' => 'login-button']) ?>
                    </div>
                    <?php \yii\widgets\ActiveForm::end(); ?>
                <?php endif; ?>
            </div><!-- end .modal-body -->

                <div class="modal-footer">
                    <p><?php if (!$success) echo Yii::t('front-reg', 'Нет, спасибо.') ?><a href="javascript:;" class="link link--form js-open-next-modal" data-dismiss="modal" data-next-modal="#loginPopup"><?= \Yii::t('front-reg', 'Я вспомнил пароль')?></a></p>
                </div><!-- end .modal-footer -->
        </div><!-- end .modal-content -->
    </div><!-- end .modal-dialog -->
</div><!-- end .modal -->