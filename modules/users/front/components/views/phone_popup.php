<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($js) $this->registerJs("$('#phonePopup').modal('show');$(\"#phoneform-phone\").mask(\"999 999 99 99\");");
echo $this->render('@app/modules/users/front/views/default/_terms', ['popup' => '#phonePopup']);
?>
<div class="modal fade modal--auth modal--about" id="phonePopup" tabindex="-1" role="dialog" aria-labelledby="modalAboutLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title"><?= \Yii::t('front-reg', "Укажите свой номер телефон"); ?></div>
            </div>
            <div class="modal-body">
                <?php $form = \yii\widgets\ActiveForm::begin(['action' => '?action=phone', 'id' => 'regForm', 'options'=>['class'=>'form form--auth']]); ?>
                <?= $form->field($model, 'email')->textInput(); ?>
                <div class="form-group">
                    <div class="phone-group clearfix<?= isset($model->errors['phone']) ? ' has-error' : ''; ?>">
                        <span class="phone-helper pull-left">+7</span>
                        <?= Html::activeTextInput($model, 'phone', [
                            'class' => 'form-control pull-right js-masked',
                            'placeholder'=>"___ ___ __ __",
                            'onfocus'=>"if(this.placeholder  == '___ ___ __ __') {this.placeholder = '';}",
                            'onblur'=>"if(this.value == '') { this.placeholder = '___ ___ __ __'; }"
                        ]) ?>
                        <?= Html::error($model, 'phone', ['class' => 'help-block']); ?>
                    </div>
                </div>
                <div class="form-help">
                    <div class="form-group checkbox">
                        <input type="checkbox" class="form-control" id="userConditions" name="user_conditions" />
                        <label for="userConditions" class="control-label"><?= \Yii::t('front-reg', "Я согласен с условиями"); ?> <a href="javascript:;" class="link" data-toggle="modal" data-target="#modalConditions" data-dismiss="modal"><?= \Yii::t('front-reg', "пользовательского соглашения"); ?></a></label>
                    </div>
                </div>
                <div class="form-group form-group--submit">
                    <input type="submit" class="btn btn--primary" value="<?= \Yii::t('front-reg', "Сохранить"); ?>">
                </div>
                </form>
            </div><!-- end .modal-body -->
        </div><!-- end .modal-content -->
    </div><!-- end .modal-dialog -->
</div><!-- end .modal -->