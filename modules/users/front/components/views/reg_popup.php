<?php if ($js) $this->registerJs("$('#regPopup').modal('show');");
 echo $this->render('@app/modules/users/front/views/default/_terms', ['popup' => '#regPopup']);
$this->registerJs("$(\"#registrationform-phone\").mask(\"999 999 99 99\");");
?>

<div class="modal fade modal--auth modal--reg" id="regPopup" tabindex="-1" role="dialog" aria-labelledby="modalRegLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon icon--close"></span></button>
                <a href="#" class="link link--title js-open-next-modal" data-dismiss="modal"  data-next-modal="#loginPopup">Вход</a>
                <div class="modal-title"><?= \Yii::t('front-reg', "Регистрация"); ?></div>
            </div>
            <div class="modal-body">
                <?php $form = \yii\widgets\ActiveForm::begin(['action' => '?action=reg', 'id' => 'regForm', 'options'=>['class'=>'form form--auth']]); ?>
                    <?= $form->field($model, 'firstName')->textInput(['class' => 'form-control', 'placeholder'=> \Yii::t('front-reg', 'Введите ваше имя')]); ?>
                    <?= $form->field($model, 'email')->textInput(['class' => 'form-control', 'placeholder'=> \Yii::t('front-reg', 'Введите электронный адрес')]); ?>

                    <div class="form-group form-group--phone<?= isset($model->errors['phone']) ? ' has-error' : ''; ?>">
                        <label for="userPhone" class="control-label"><?= \Yii::t('front-reg', 'Телефон'); ?></label>
                        <?= \yii\helpers\Html::activeTextInput($model, 'phone', [
                            'class' => 'form-control pull-right js-masked',
                            'placeholder'=>"+7 ___ ___ __ __",
                            'onfocus'=>"if(this.placeholder  == '+7 ___ ___ __ __') {this.placeholder = '';}",
                            'onblur'=>"if(this.value == '') { this.placeholder = '+7 ___ ___ __ __'; }"
                        ]) ?>
                        <?= \yii\helpers\Html::error($model, 'phone', ['class' => 'help-block']); ?>
                    </div>

                    <?= $form->field($model, 'password')->passwordInput(['placeholder'=> \Yii::t('front-reg', 'Придумайте пароль')]); ?>
                    <?= $form->field($model, 'confirmPassword')->passwordInput(['placeholder'=> \Yii::t('front-reg', 'Повторите пароль')]); ?>
                    <div class="form-help">
                        <div class="form-group checkbox">
                            <input type="checkbox" class="form-control" id="userConditions" name="user_conditions" />
                            <label for="userConditions" class="control-label"><?= \Yii::t('front-reg', "Я согласен с условиями"); ?> <a href="javascript:;" class="link js-open-next-modal" data-dismiss="modal" data-next-modal="#modalConditions"><?= \Yii::t('front-reg', "пользовательского соглашения"); ?></a></label>
                        </div>
                    </div>
                    <div class="form-group form-group--submit">
                        <?= \yii\helpers\Html::submitButton(\Yii::t('front-reg', \Yii::t('front-reg', 'Зарегистрироваться')), ['class' => 'btn btn--primary', 'id' => 'regBtn']) ?>
                    </div>
                <?= \yii\helpers\Html::hiddenInput('action', 'reg'); ?>

                <?php \yii\widgets\ActiveForm::end(); ?>
                <?php echo $this->render('@app/views/layouts/front/social_login', []); ?>
            </div><!-- end .modal-body -->
            <div class="modal-footer">
                <p><?= \Yii::t('front-reg', "Уже есть аккаунт?"); ?><a href="javascript:;" class="link link--form" data-toggle="modal" data-target="#loginPopup" data-dismiss="modal"><?= \Yii::t('front-reg', "Войти"); ?></a></p>
            </div><!-- end .modal-footer -->
        </div><!-- end .modal-content -->
    </div><!-- end .modal-dialog -->
</div><!-- end .modal -->