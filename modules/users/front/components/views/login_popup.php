<?php if ($js)echo $this->registerJs("$('#loginPopup').modal('show')");?>
<div class="modal fade modal--auth" id="loginPopup" tabindex="-1" role="dialog" aria-labelledby="modalLoginLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="icon icon--close"></span></button>
                <div class="modal-title"><?= \Yii::t('front-reg', 'Вход'); ?></div>
                <a href="#" class="link link--title js-open-next-modal" data-dismiss="modal" data-next-modal="#regPopup"><?= \Yii::t('front-reg', 'Регистрация'); ?></a>
            </div>
            <div class="modal-body">
                <div class="form-tip"><?= \Yii::t('front-reg', 'Вы можете использовать аккаунт brod.'); ?>kz</div>
                <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'login-form', 'action' => '?action=login', 'options' =>  ['class'=> 'form form--auth']]); ?>

                <?= $form->field($model, 'email')->textInput(['placeholder' => \Yii::t('front-reg', 'Введите электронный адрес'), 'onfocus'=>"if(this.placeholder  == '" . \Yii::t('front-reg', 'Укажите электронный адрес') . "') {this.placeholder = '';}", 'onblur'=>"if(this.value == '') { this.placeholder = '" . \Yii::t('front-reg', 'Укажите электронный адрес') . "'; }"]) ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>\Yii::t('front-reg', "Введите пароль"), 'onfocus'=>"if(this.placeholder  == '" . \Yii::t('front-reg', "Введите пароль") . "') {this.placeholder = '';}", 'onblur'=>"if(this.value == '') { this.placeholder = '" . \Yii::t('front-reg', "Введите пароль") . "'; }"]) ?>
                    <div class="form-help clearfix">
                        <div class="form-group checkbox hidden">
                            <?php $form->field($model, 'rememberMe')->checkbox(['class' => 'form-control'])->render(function($cnt){
                                preg_match_all('/<input.*?>/', $cnt, $input);
                                echo $input[0][0] . $input[0][1];
                            }) ?>

                            <label for="loginform-rememberme" class="control-label"><?= \Yii::t('front-reg', "Запомнить меня"); ?></label>
                        </div>
                    </div>
                    <div class="form-group form-group--submit">
                        <input type="submit" class="btn btn--primary" value="<?= \Yii::t('front-reg', 'Войти'); ?>">
                        <a href="javascript:;" class="link link--form js-open-next-modal" data-dismiss="modal" data-next-modal="#recoverPopup"><?= \Yii::t('front-reg', "Напомнить пароль?"); ?></a>
                    </div>
                <?php \yii\widgets\ActiveForm::end(); ?>
                <?php echo $this->render('@app/views/layouts/front/social_login', []); ?>
            </div><!-- end .modal-body -->
            <div class="modal-footer">
                <p><?= \Yii::t('front-reg', 'Нет аккаунта?'); ?><a href="javascript:;" class="link link--form" data-toggle="modal" data-target="#regPopup" data-dismiss="modal"><?= \Yii::t('front-reg', 'Зарегистрироваться'); ?></a></p>
            </div><!-- end .modal-footer -->
        </div><!-- end .modal-content -->
    </div><!-- end .modal-dialog -->
</div><!-- end .modal -->
