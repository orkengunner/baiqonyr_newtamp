<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($js) $this->registerJs("$('#infoPopup').modal('show');$(\"#infoform-phone\").mask(\"999 999 99 99\");");
?>
<div class="modal fade modal--auth modal--about" id="infoPopup" tabindex="-1" role="dialog" aria-labelledby="modalAboutLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title"><?= \Yii::t('front-reg', 'Расскажите немного о себе :)'); ?></div>
            </div>
            <div class="modal-body">
                <?php $form = \yii\widgets\ActiveForm::begin(['action' => '?action=info', 'id' => 'regForm', 'options'=>['class'=>'form form--auth']]); ?>
                <?= $form->field($model, 'firstName')->textInput(); ?>

                    <div class="form-group">
                        <label for="userPhone" class="control-label"><?= \Yii::t('front-reg', 'Телефон'); ?></label>
                        <div class="phone-group clearfix<?= isset($model->errors['phone']) ? ' has-error' : ''; ?>">
                            <span class="phone-helper pull-left">+7</span>
                            <?= Html::activeTextInput($model, 'phone', [
                                'class' => 'form-control pull-right js-masked',
                                'placeholder'=>"___ ___ __ __",
                                'onfocus'=>"if(this.placeholder  == '___ ___ __ __') {this.placeholder = '';}",
                                'onblur'=>"if(this.value == '') { this.placeholder = '___ ___ __ __'; }"
                            ]) ?>
                            <?= Html::error($model, 'phone', ['class' => 'help-block']); ?>
                        </div>
                    </div>
                    <?= $form->field($model, 'birthDay')->widget(\janisto\timepicker\TimePicker::className(), [
                            //'language' => 'fi',
                            'mode' => 'date',
                            'addon' => '',
                            'clientOptions'=>[
                                'dateFormat' => 'yy-mm-dd',
                                // 'timeFormat' => 'HH:mm:ss',
                            ]
                        ]
                    ); ?>
                    <?= $form->field($model, 'sex')->dropDownList([1 => \Yii::t('front-reg', 'Мужской'), 0 => \Yii::t('front-reg', 'Женский')]); ?>
                    <div class="form-group form-group--submit">
                        <input type="submit" class="btn btn--primary" value="Сохранить">
                    </div>
                </form>
            </div><!-- end .modal-body -->
        </div><!-- end .modal-content -->
    </div><!-- end .modal-dialog -->
</div><!-- end .modal -->