<?php $form = \yii\widgets\ActiveForm::begin([
    'options' => ['class' => 'form form-auth']
]); ?>
    <h2 class="title-form">Регистрация</h2>
    <?php if(!$slug): ?>
        <?= $this->render('//layouts/front/_login-sn'); ?>
    <?php else: ?>
        <p class="auth-meta"><span class="campaign-name"><?= $company->title; ?></span></p>
    <?php endif; ?>
    <div class="reg-primary">
        <section class="form-name-section">
            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя']); ?>
        </section>
        <section class="form-login-section">
            <?= $form->field($model, 'email')->textInput(['placeholder' => 'duseton@gmail.com']); ?>
        </section>
        <section class="form-email-section">
            <?= $form->field($model, 'username')->textInput(['placeholder' => '@mathew']); ?>
        </section>
        <section class="form-password-section">
            <div class="form-group">
              <span class="icon-pass">
                <img src="/images/icon-pass-hidden.png" alt="">
              </span>
              <label for="userPassword" class="control-label">Пароль</label>
              <?= \yii\helpers\Html::activePasswordInput($model, 'password', [
                  'placeholder' => '•••••••••••••',
                  'class' => 'form-control'
                ])?>
              <?= $form->field($model, 'password', ['template' => '{error}']); ?>
            </div>
        </section>
        <?= $form->field($model, 'token', ['template'=>'{input}'])->hiddenInput(['value'=> $slug]); ?>
        <section class="form-group form-submit-section">
            <?= \yii\helpers\Html::submitInput('Зарегистрироваться', ['class' => 'btn btn-primary']) ?>
        </section>
    </div><!-- end .reg-primary -->
<?php \yii\widgets\ActiveForm::end(); ?>
