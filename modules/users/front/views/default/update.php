<?php
$this->registerJs('
var uploads = new wUploads();
uploads.init({
container: ".upload-file", callback:"setPhotoCallback"
});

$("#users-phone").mask("+7 (999) 999 99 99");
');

$uploads = new \app\components\wFileUpload($model, 'upload_logo', 'logo');
?>


<h2 class="title-primary">
    Настройки профиля
</h2>

<div class="row">
    <div class="col-xs-5">
        <?php $form = \yii\widgets\ActiveForm::begin(['options' => ['class' => 'form-main']]); ?>
            <div class="form-settings-section">
                <h4 class="title-form-section">Основные настройки</h4>
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Игорь Николаев']); ?>
                <?= $form->field($model, 'username')->textInput(['placeholder' => '@nikolaev']); ?>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'nikolaev.i@mail.ru']); ?>
                <?= $form->field($model, 'password')->passwordInput(); ?>
            </div>

            <div class="form-settings-section">
                <h4 class="title-form-section" id="unsubscribe">Отписаться от рассылки</h4>
                <?= $form->field($model, 'unsubscribe', ['template' => '{input}'])->checkboxList([
                    \app\modules\emailtemplates\models\EmailTemplates::NEW_POST_MODERATION => 'Новые посты для модерации',
                    \app\modules\emailtemplates\models\EmailTemplates::POST_APPROVE  => 'Посты были отмодерированы',
                    \app\modules\emailtemplates\models\EmailTemplates::NEW_POST_COMMENT =>'Новый комментарий',
                    \app\modules\emailtemplates\models\EmailTemplates::PUBLISH_AUTH_ERROR => 'Ошибка авторизации в соц сети',
                    \app\modules\emailtemplates\models\EmailTemplates::PUBLISH_ERROR => 'Ошибка публикации'
                ]); ?>
            </div>

            <div class="form-services-section">
                <h4 class="title-form-section">Контакты и сервисы</h4>
                <?= $form->field($model, 'phone')->textInput(['placeholder' => '+7 (777) 902 233 23 09']); ?>
                <div class="form-group<?= in_array($lsn::FACEBOOK, $userSNarray) ? ' is-plugged' : ''; ?> clearfix facebook">
                    <?php if (in_array($lsn::FACEBOOK, $userSNarray)): ?>
                        <button onclick="window.location.assign('<?= \yii\helpers\Url::to(['sn-unset', 'sn' => $lsn::FACEBOOK, 'cmp_id' => $model->id])?>');" type="button" class="btn btn-status pull-right"><span>Отключить</span></button>
                    <?php else: ?>
                        <button type="button" data-social="facebook" class="btn btn-status pull-right social"><span>Привязать</span></button>
                    <?php endif; ?>

                    <figure class="services-item-name">
                        <span class="font-icons">&#228;</span><span>Фейсбук</span>
                    </figure>
                    <?php if (\Yii::$app->request->get('sn_id') == $lsn::FACEBOOK): ?>
                        <div class="error-block"><?= \Yii::$app->request->get('error'); ?></div>
                    <?php endif; ?>
                </div>

                <div class="form-group<?= in_array($lsn::VKONTAKTE, $userSNarray) ? ' is-plugged' : ''; ?> clearfix vkontakte">
                    <?php if (in_array($lsn::VKONTAKTE, $userSNarray)): ?>
                        <button onclick="window.location.assign('<?= \yii\helpers\Url::to(['sn-unset', 'sn' => $lsn::VKONTAKTE, 'cmp_id' => $model->id])?>');" type="button" class="btn btn-status pull-right"><span>Отключить</span></button>
                    <?php else: ?>
                        <button type="button" data-social="vkontakte" class="btn btn-status pull-right social"><span>Привязать</span></button>
                    <?php endif; ?>

                    <figure class="services-item-name">
                        <span class="font-icons">&#230;</span><span>ВКонтакте</span>
                    </figure>
                    <?php if (\Yii::$app->request->get('sn_id') == $lsn::VKONTAKTE): ?>
                        <div class="error-block"><?= \Yii::$app->request->get('error'); ?></div>
                    <?php endif; ?>
                </div>

                <div class="form-group<?= in_array($lsn::TWITTER, $userSNarray) ? ' is-plugged' : ''; ?> clearfix twitter">
                    <?php if (in_array($lsn::TWITTER, $userSNarray)): ?>
                        <button onclick="window.location.assign('<?= \yii\helpers\Url::to(['sn-unset', 'sn' => $lsn::TWITTER, 'cmp_id' => $model->id])?>');" type="button" class="btn btn-status pull-right"><span>Отключить</span></button>
                    <?php else: ?>
                        <button type="button" data-social="twitter" class="btn btn-status pull-right social"><span>Привязать</span></button>
                    <?php endif; ?>

                    <figure class="services-item-name">
                        <span class="font-icons">&#229;</span><span>Твиттер</span>
                    </figure>
                    <?php if (\Yii::$app->request->get('sn_id') == $lsn::TWITTER): ?>
                        <div class="error-block"><?= \Yii::$app->request->get('error'); ?></div>
                    <?php endif; ?>
                </div>

            </div><!-- end .form-services-section -->

            <div class="form-group form-submit-section">
                <?= \yii\helpers\Html::submitInput('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
    </div><!-- end .col-xs-5 -->

    <div class="col-xs-4">
        <div class="form-avatar-section upload-file">
            <figure class="user-avatar">
                <?= $uploads->img('/images/avatar.jpg', ['class' => 'avatar avatar-lg']); ?>
            </figure>
            <div class="avatar-controls">
                <?= $uploads->fileInput(); ?>
                <button onclick="$('#users-upload_logo').click(); " type="button" class="avatar-controls-item btn-status">
                    <span>Загрузить новый</span>
                </button>
                <?= $uploads->remove(isset($model->logo->id) ? $model->logo->id : null, [
                    'class' => 'avatar-controls-item btn btn-link',
                    'type' => 'button',
                    'html' => [
                        'tag' => 'button',
                        'content' => \yii\helpers\Html::tag('span', 'Удалить аватар'),
                    ]
                ]); ?>
                <?= $uploads->error(); ?>
            </div><!-- end .avatar-controls -->
        </div><!-- end .form-avatar-section -->
    </div><!-- end .col-xs-4 -->
    <?php \yii\widgets\ActiveForm::end(); ?>
</div><!-- end .row -->