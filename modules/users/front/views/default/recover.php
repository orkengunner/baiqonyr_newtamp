<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = \yii\widgets\ActiveForm::begin(['options' => [
    'class' => 'form form-auth'
]]); ?>
<h2 class="title-form title-form-secondary">Восстановление пароля</h2>
<p class="auth-meta">
    <a href="<?= \yii\helpers\Url::to([
        '/login/'
    ]); ?>" class="link-primary">
        <span class="icon">&larr;</span>
        <span>Я вспомнил пароль</span>
    </a>
</p>
<div class="reg-primary">
    <section class="form-email-section">
        <?php if(!$token && !$recover) : ?>
            <?= $form->field($model, 'email')->textInput(['placeholder' => 'your@email.com']) ?>
        <?php elseif(!$token && $recover == 1): ?>
            <p class="auth-meta">На ваш адрес <?= $model->email; ?> было отправлено волшебное письмо.</p>
        <?php elseif($token && !$recover): ?>
            <?= $form->field($model, 'password')->textInput(['placeholder' => 'Введите новый пароль']) ?>
            <?= Html::activeHiddenInput($model, 'token', ['value' => \Yii::$app->request->get('token')]); ?>
        <?php elseif($token && $recover == 2): ?>
            <p class="auth-meta">Пароль успешно востановлен</p>
        <?php endif; ?>
    </section>
    <?php if(!$recover) : ?>
        <section class="form-group form-submit-section">
            <?= Html::submitButton('Сбросить пароль', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </section>
    <?php endif; ?>
</div><!-- end .reg-primary -->
<?php if($recover) echo 'Good';?>
<?php ActiveForm::end(); ?>
