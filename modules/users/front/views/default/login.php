<?php $form = \yii\widgets\ActiveForm::begin(['id' => 'login-form', 'options' => [
    'class' => 'form form-auth'
]]); ?>
    <h2 class="title-form">Вход</h2>

    <?= $this->render('//layouts/front/_login-sn'); ?>
    <div class="reg-primary">
        <section class="form-email-section">
            <?= $form->field($model, 'email', [
                'inputOptions' => [
                  'class' => 'form-control',
                  'placeholder' => 'your@email.com'
                ]
              ]) ?>
        </section>
        <section class="form-password-section">
            <?= $form->field($model, 'password', [
                'inputOptions' => [
                  'class' => 'form-control',
                  'placeholder' => '•••••••••••••'
                ]
              ])->passwordInput() ?>
        </section>
        <section class="form-group form-submit-section">
            <?= \yii\helpers\Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            <a href="<?= \yii\helpers\Url::to([
                '/reset/'
            ]); ?>" class="link-secondary"><span>Я забыл пароль</span></a>
        </section>
    </div><!-- end .reg-primary -->
<?php \yii\widgets\ActiveForm::end(); ?>
