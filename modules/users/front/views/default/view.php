<div class="user-info-block">
    <figure class="user-avatar"><img src="<?= \yii\helpers\Url::imgUrl(isset($model->logo->filepath) ? $model->logo->filepath : '', 'users', [100,100])?>" alt="" class="avatar avatar-lg"></figure>
    <div class="user-data">
        <h2 class="user-name"><?= $model->name; ?>
            <?php if ($model->id == \Yii::$app->user->id): ?>
                <a href="<?= \yii\helpers\Url::to(['/profile/'])?>"><span class="font-icons">&#0063;</span></a>
            <?php endif; ?>
        </h2>
        <p class="user-profession"><span><?= $role; ?></span> с <span>20 мая 2012</span></p>
        <ul class="user-contacts">
            <?php if($model->email): ?>
                <li><a href="mailto:<?= $model->email; ?>"><span><?= $model->email; ?></span></a></li>
            <?php endif; ?>
            <?php if (isset($userSNarray[$lsn::FACEBOOK])): ?>
                <li><a target="_blank" href="https://www.facebook.com/<?= $userSNarray[$lsn::FACEBOOK]->soc_acc_id; ?>"><span><span class="font-icons">&#228;</span>Фейсбук</span></a></li>
            <?php endif; ?>
            <?php if (isset($userSNarray[$lsn::VKONTAKTE])): ?>
                <li><a target="_blank" href="https://vk.com/id<?= $userSNarray[$lsn::VKONTAKTE]->soc_acc_id; ?>"><span><span class="font-icons">&#230;</span>ВКонтакте</span></a></li>
            <?php endif; ?>
            <?php if (isset($userSNarray[$lsn::TWITTER])): ?>
                <li><a target="_blank" href="https://twitter.com/intent/user?user_id=<?= $userSNarray[$lsn::TWITTER]->soc_acc_id; ?>"><span><span class="font-icons">&#234;</span>Твиттер</span></a></li>
            <?php endif; ?>
            <?php if ($model->phone): ?>
                <li><a href="tel:<?= $model->phone; ?>"><span><span class="font-icons">&#124;</span><?= $model->phone; ?></span></a></li>
            <?php endif; ?>
        </ul><!-- end .user-contacts -->
    </div><!-- end .user-data -->
</div><!-- end .user-info-block -->


<?php if ($model->projects): ?>
    <div class="user-projects">
        <h3 class="title-primary">
            Проекты <span><?php count($model->projects); ?></span>
        </h3>
        <?php if (in_array($model->role_id, $projectRoles) && $model->id == \Yii::$app->user->id): ?>
            <a class="btn btn-primary" href="<?= \yii\helpers\Url::to('/companies/update/')?>"><span>Добавить проект</span></a>
        <?php endif; ?>
        <div class="user-projects-list">
            <div class="list-row">
                <?php
                foreach ($model->projects as $k => $v):
                    $is_view = in_array($v->fa_cmpu_role_id, $projectRoles) && $model->id == \Yii::$app->user->id;
                    if ($k && $k % 3 == 0) echo '</div><div class="list-row">'; ?>
                    <div class="user-projects-item">
                        <div class="project-controls">
                            <h4 class="project-name">
                                <a class="<?= $is_view ? '' : 'no-link'; ?>" href="<?= \yii\helpers\Url::to(['/' . $v->fa_slug . '/settings/'])?>"><span><?= $v->fa_title; ?></span></a>
                            </h4>
                            <?php if ($is_view): ?>
                                <a class="post-edit" href="<?= \yii\helpers\Url::to(['/' . $v->fa_slug . '/settings/'])?>"><span class="font-icons">?</span></a>
                            <?php endif; ?>
                            </div>
                        <p class="project-date">с <?= \yii\helpers\StringHelper::translateDate(date('j F Y', strtotime($v->create_date)), 1); ?></p>
                        <ul class="project-team">
                            <?php foreach(explode(';', $v->fa_employees) as $employee): $_employee = explode(':', $employee); ?>
                                <li>
                                    <a href="<?= \yii\helpers\Url::to(['/user/' .  $_employee[3]])?>">
                                        <img src="<?= \yii\helpers\Url::imgUrl(isset($_employee[4]) && isset($_employee[5]) ? $_employee[4] . '/' . $_employee[5] : '', 'users', [100,100] )?>" alt="" class="avatar avatar-sm" title="<?= $_employee[1]; ?> -<?= $_employee[2]; ?>">
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div><!-- end .user-projects-item -->
                <?php endforeach; ?>
            </div>
        </div><!-- end .user-projects-list -->
    </div><!-- end .user-project -->
<?php endif; ?>

<?php if (in_array($model->role_id, [
    \app\modules\lookupusersroles\models\LookupUsersRoles::ROLE_ADMIN,
    \app\modules\lookupusersroles\models\LookupUsersRoles::ROLE_ACCOUNT_MANAGER,
    \app\modules\lookupusersroles\models\LookupUsersRoles::ROLE_SMM_MANAGER]) ):?>
<?php endif; ?>
