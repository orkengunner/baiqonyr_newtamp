<div class="row">
    <div class="col-md-12">
        <h1>Завершение регистрации</h1>

        <?php $form = \yii\widgets\ActiveForm::begin([]); ?>
        <?= $form->field($model, 'username')->textInput(); ?>
        <?= $form->field($model, 'password')->passwordInput(); ?>
        <?= $form->field($model, 'email')->textInput(); ?>
        <?= $form->field($model, 'phone')->textInput(); ?>
        <?= \yii\helpers\Html::submitInput('Закончить регистрацию', ['class' => 'btn btn-success']) ?>

        <?php \yii\widgets\ActiveForm::end(); ?>
    </div>

</div>
