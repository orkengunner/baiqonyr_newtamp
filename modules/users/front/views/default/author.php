<div class="container">
    <figure class="media media-author">
        <span class="media-object" style="background-image: url('<?= $user->getAvatarUrl() ?>');"></span>
        <figcaption class="media-body">
            <h1 class="media-heading title title-main"><?= $user->getUserFio() ?></h1>
            <p><?= $user->about ?></p>
        </figcaption>
    </figure>
    <h1 class="title title-main">Публикации Автора <sup><?= $user->getPages()->count(); ?></sup></h1>
    <div class="author-posts js-load-container">
        <div class="row">
            <?php foreach ($user->getPages()->limit(9)->all() as $page) { ?>
            <div class="col-md-6 col-sm-9">
                <figure class="thumbnail thumbnail-main">
                    <img src="<?= $page->getImageUrl('s2') ?>" alt="">
                    <figcaption class="caption">
                        <ul class="list-inline">
                            <li><?= $page->category->title ?></li>
                        </ul>
                        <h1 class="title title-main">
                            <a class="link" href="<?= \yii\helpers\Url::toRoute(['/pages/default/index', 'sefname' => $page->sefname, 'category' => $page->category->sefname]) ?>"><span><span><?= $page->title ?></span></span></a>
                        </h1>
                    </figcaption>
                </figure>
            </div>
            <?php } ?>
        </div>
    </div>
    <footer class="text-center">
        <button class="link link-dashed is-more js-load-more" id="spinner" data-loading-text="Загрузка..."
                data-url="{{load-more-url}}" type="button">
            <span><span>Загрузить еще</span></span>
        </button>
    </footer>
</div>
