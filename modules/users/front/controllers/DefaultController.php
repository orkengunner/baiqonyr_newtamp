<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 7/24/14
 * Time: 12:43
 */

namespace app\modules\users\front\controllers;


use app\components\FrontController;
use app\components\wMail;
use app\modules\companies\models\Companies;
use app\modules\companies\models\CompaniesUsers;
use app\modules\emailtemplates\models\EmailTemplates;
use app\modules\invites\models\Invites;
use app\modules\logs\models\Logs;
use app\modules\lookupsocnets\models\LookupSocNets;

use app\modules\posts\models\Posts;
use app\modules\socialpages\models\SocialPages;
use app\modules\statistics\models\Statistics;
use app\modules\users\models\LoginForm;
use app\modules\users\models\RecoverForm;
use app\modules\users\models\RegistrationForm;
use app\modules\users\models\UserIdentity;
use app\modules\users\models\Users;
use app\modules\users\models\UsersSocNets;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class DefaultController extends FrontController
{
    protected $_modelName = 'app\modules\users\models\Users';


    public function init()
    {
        parent::init();
        //if (!\Yii::$app->user->isGuest)
        //$this->_user->u_role_id = $this->_user->role_id;
    }

    public function actionRegistration($slug)
    {
        $this->layout = '//front/not-auth';
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }
        if ($slug) {
            $invite = Invites::findOne(['slug' => $slug]);
            if (!$invite || Users::findOne(['email' => $invite->email]))
                return $this->redirect('/login/');
        }
        $model = new RegistrationForm();

        if ($model->load(\Yii::$app->getRequest()->post())) {
            $id = $model->registration();

            if ($id && \Yii::$app->getUser()->login(UserIdentity::findIdentity($id))){
                Logs::log(2);
                return $this->redirect('/');
            }
        }

        return $this->render('registration', [
                'model' => $model,
                'slug' => $slug,
                'company' => $slug && $invite ? Companies::findOne($invite->company_id) : null
            ]);
    }


    public function actionLogin()
    {
        /*if (!\Yii::$app->getUser()->getIsGuest()) {
            $this->goHome();
        }*/

        $serviceName = \Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = \Yii::$app->get('eauth')->getIdentity($serviceName);

            $redirectUrl = \Yii::$app->getRequest()->getQueryParam('redirect');
            $eauth->setRedirectUrl($redirectUrl);
            $eauth->setCancelUrl(\Yii::$app->getUrlManager()->createAbsoluteUrl($redirectUrl));

            try {
                if ($eauth->authenticate()) {

                    $identity = UserIdentity::findByEAuth($eauth);

                    \Yii::$app->getUser()->login($identity);
                    if (\Yii::$app->request->cookies->get('current_url'))
                        $url = \Yii::$app->request->cookies->get('current_url')->value;
                    else $url = '/';
                    \Yii::$app->response->cookies->add(new \yii\web\Cookie([
                        'name' => 'current_url',
                        'value' => null,
                        'expire' => time(),
                        'path' => '/'
                    ]));
                    return $this->redirect($url);

                }
                else {

                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later

                \Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

        $model = new LoginForm;

        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            if (\Yii::$app->request->get('return')) {
                return $this->redirect(\Yii::$app->request->get('return'));
            }
            return $this->goHome();

        }
        \Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => 'current_url',
            'value' => null,
            'expire' => time(),
            'path' => '/'
        ]));
        return $this->render('login', ['model' => $model]);
    }

    public function actionForgot($token = null)
    {
        $this->layout = '//front/not-auth';
        $model = new RecoverForm();

        $recover = false;

        if ($model->load(\Yii::$app->getRequest()->post()) && $recover = $model->recover()) {

        }

        return $this->render('recover', [
            'model' => $model,
            'recover' => $recover,
            'token' => $token
        ]);

    }

    public function actionLogout()
    {
        \Yii::$app->response->cookies->remove('current_brand');
        $user_id = $this->_user->id;

        \Yii::$app->getUser()->logout();
        return $this->goHome();
    }

    public function actionSetPhoto($id)
    {
        $model = Users::findOne($id);
        $model->save();

        return json_encode([
            'data' => [
                'src' => $model->getFilePath('logo', false, [100,100]),
                'id' => $model->logo->id
            ],
            'callback' => 'setPhotoCallback'
        ]);
    }

    public function actionUpdate()
    {
        $id = $this->_user->id;
        $this->cssClass = 'profile-edit';

        $model = Users::findOne($id);

        if (!$model || $id != $this->_user->id) {
            throw new NotFoundHttpException();
        }

        $password = $model->password;

        if ($model->load(\Yii::$app->request->post())) {
            if ($model->password) {
                $model->password = \Yii::$app->getSecurity()->generatePasswordHash($model->password);
            } else {
                $model->password = $password;
            }
            if($model->save()) {
                $this->redirect(['/user/' . $model->username]);
            }
        }

        $model->password = '';

        $usersSocNets = $model->usersSocNets;

        $userSNarray = [];
        foreach ($usersSocNets as $v) $userSNarray[] = $v->soc_net_id;

        return $this->render('update', [
            'model' => $model,
            'userSNarray' => $userSNarray,
            'lsn' => new LookupSocNets(),
            'sp' => new SocialPages(),

        ]);
    }

    public function behaviors() {
        return ArrayHelper::merge(parent::behaviors(), array(
            'eauth' => array(
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => array('login'),
            ),
        ));
    }

}
