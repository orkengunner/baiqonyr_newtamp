<?php

namespace app\modules\menu\models;


use app\components\ActiveQuery;
use creocoder\nestedsets\NestedSetsQueryBehavior;

class MenuItemsQuery extends ActiveQuery
{
    public function behaviors()
    {
        return [
            [
                'class' => NestedSetsQueryBehavior::className(),
            ],
        ];
    }
}
