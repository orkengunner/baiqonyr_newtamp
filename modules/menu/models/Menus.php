<?php

namespace app\modules\menu\models;

use app\components\ActiveRecord;
use app\modules\languages\models\Languages;
use Yii;

/**
 * This is the model class for table "menus".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 * @property integer $lang_id
 *
 * @property Languages $lang
 * @property MenuItems[] $menuItems
 */
class Menus extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['is_active', 'lang_id'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('menu', 'ID'),
            'title' => Yii::t('menu', 'Название'),
            'is_active' => Yii::t('menu', 'Активность'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItems()
    {
        return $this->hasMany(MenuItems::className(), ['menu_id' => 'id']);
    }

    public function getItems()
    {
        $menuItemsCache = Yii::$app->getCache()->get('menuItems_' . $this->id . '_' . Languages::getCurrent()->id);
        if ($menuItemsCache === false) {
            $menuItems = $this->getMenuItems()
                ->where([
                    'is_active' => 1,
                    'lang_id' => Languages::getCurrent()->id
                ])
                ->orderBy([
                'root' => SORT_ASC,
                'lft' => SORT_ASC
            ])->all();

            $data = array();
            foreach ($menuItems as $item) {
                $node = ['label' => $item->title, 'url' => $item->getUrl(), 'new_window' => $item->is_new_window];

                if ($item->level == 0) {
                    $data[$item->id] = $node;
                } elseif ($item->level > 0) {
                    if (!isset($data[$item->parent_id])) {
                        $data[$item->parent_id] = [];
                    }
                    $data[$item->parent_id]['items'][] = $node;
                } else {
                    $data[$item->id] = $node;
                }
            }


            Yii::$app->getCache()->set('menuItems_' . $this->id . '_' . Languages::getCurrent()->id, $data, 6000);

            return $data;
        }

        return $menuItemsCache;
    }

    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->getCache()->set('menu-' . $this->id, $this);

        parent::afterSave($insert, $changedAttributes);
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    public static function find()
    {
        $query = new MenusQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] != 'admin') {
            $query->andWhere([
                'menus.is_active' => self::STATUS_ACTIVE
            ]);
        }
        return $query;
    }
}
