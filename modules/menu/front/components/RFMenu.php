<?php
/**
 * Created by PhpStorm.
 * User: yevgeniy
 * Date: 2/23/15
 * Time: 3:04 PM
 */

namespace app\modules\menu\front\components;


use app\modules\languages\models\Languages;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class RFMenu extends \yii\widgets\Menu
{
    public $replaceLinkTemplate = false;
    public $tag = 'ul';
    public $itemTagContainer = 'li';
    public $firstItemActiveExclude = false;
    private $isFirst = false;

    public function run()
    {
        if ($this->route === null && \Yii::$app->controller !== null) {
            $this->route = \Yii::$app->controller->getRoute();
        }
        if ($this->params === null) {
            $this->params = \Yii::$app->request->getQueryParams();
        }
        $items = $this->normalizeItems($this->items, $hasActiveChild);
        if (!empty($items)) {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', $this->tag);

            if ($tag !== false) {
                echo Html::tag($tag, $this->renderItems($items), $options);
            } else {
                echo $this->renderItems($items);
            }
        }
    }

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item)
    {
//print_r($item);
        if ($this->replaceLinkTemplate) {
            $this->linkTemplate = '<a href="{url}" class="link">{label}</a>';
        }
        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);
            $template = str_replace('{active}', $item['active'] && $this->activateItems ? ' active' : '', $template);
            return strtr($template, [
                '{url}' => Html::encode(urldecode(Url::to($item['url']))),
                '{label}' => $item['label'],
            ]);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{label}' => $item['label'],
            ]);
        }
    }

    protected function isItemActive($item)
    {
        if (!empty($this->route) && isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            $url = preg_replace(
                '/^\/' . Languages::$current->code . '\//',
                '/',
                Url::to($item['url'])
            );
            $isActive = strpos(
                $this->route,
                $url
            ) === 0
                /*&& !in_array(
                    Url::to($item['url']),
                    $this->parentExclude
                ) */
                && (
                    !$this->firstItemActiveExclude || ($this->firstItemActiveExclude && $this->isFirst) || $this->route == $url
                );

            $this->isFirst = true;
            if ($isActive) {
                return true;
            } else {
                return parent::isItemActive($item);
            }
        }

        return false;
    }

    protected function renderItems($items)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            if($this->itemTagContainer) {
                $tag = ArrayHelper::remove($options, 'tag', $this->itemTagContainer);
            }
            $class = [];
            if ($item['active']) {
                $class[] = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }

            $menu = $this->renderItem($item);
            if (!empty($item['items'])) {
                $submenuTemplate = ArrayHelper::getValue($item, 'submenuTemplate', $this->submenuTemplate);
                $menu .= strtr($submenuTemplate, [
                    '{items}' => $this->renderItems($item['items']),
                ]);
            }
            if (!isset($tag)) {
                $lines[] = $menu;
            } else {
                $lines[] = Html::tag($tag, $menu, $options);
            }
        }

        return implode("\n", $lines);
    }
}
