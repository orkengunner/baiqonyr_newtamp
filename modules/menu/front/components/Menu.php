<?php

namespace app\modules\menu\front\components;

use app\modules\menu\models\Menus;
use yii\base\Widget;

class Menu extends Widget
{

    public $menuId;
    public $cssClass;
    public $viewFile = 'menu';
    public $id;
    public $tag = 'ul';
    public $itemCssClass = '';
    public $itemTagContainer = 'li';
    public $itemContainerCssClass = '';
    public $activateItems = true;
    public $firstItemActiveExclude = [];

    public function run()
    {
        if (empty($this->menuId)) {
            return false;
        }

        $menu = \Yii::$app->db->cache(function () {
            return Menus::find()->andWhere(['menus.id' => $this->menuId])->one();
        }, 1000);

        if ($menu === null) {
            return false;
        }

        return $this->render($this->viewFile, [
            'menu' => $menu,
            'cssClass' => $this->cssClass,
            'id' => $this->id,
            'tag' => $this->tag,
            'itemCssClass' => $this->itemCssClass,
            'itemTagContainer' => $this->itemTagContainer,
            'itemContainerCssClass' => $this->itemContainerCssClass,
            'activateItems' => $this->activateItems,
            'firstItemActiveExclude' => $this->firstItemActiveExclude
        ]);
    }

}
