<?php
/**
 * @var app\modules\menu\models\Menus $menu
 */

$items = [];
foreach ($menu->getItems() as $item) {

	if(\Yii::$app->request->get('year')) {
		$item['url']['year'] = \Yii::$app->request->get('year');
	}
	$items[] = $item;
}
//print_r($items);exit;
echo $this->render('menu', [
	'items' => $items,
	'menu' => $menu,
	'cssClass' => $cssClass,
	'id' => $id,
	'tag' => $tag,
	'itemCssClass' => $itemCssClass,
	'itemTagContainer' => $itemTagContainer,
	'itemContainerCssClass' => $itemContainerCssClass,
	'activateItems' => $activateItems,
	'firstItemActiveExclude' => $firstItemActiveExclude
]);
