<?php
/**
 * @var app\modules\menu\models\Menus $menu
 */

echo \app\modules\menu\front\components\RFMenu::widget([
    'items' => isset($items) ? $items : $menu->getItems(),
    'tag' => $tag,
    'itemTagContainer' => $itemTagContainer,
    'activateItems' => $activateItems,
    'firstItemActiveExclude' => $firstItemActiveExclude,
    'options' => [
        'class' => $cssClass,

        //'id' => $id ? $id : 'menu-glavnoe-menyu'
    ],
    'itemOptions' => [
        'class' => $itemContainerCssClass
    ],
    'linkTemplate' => '<a class="' . $itemCssClass . '{active}" href="{url}" class="link">{label}</a>',
    'route' => Yii::$app->request->url,
]);
