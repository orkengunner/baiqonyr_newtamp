<?php

    namespace app\modules\menu;

use app\components\rocket\RFModule;

class Module extends RFModule
{
    public $allowedLinks = [];

    public function init()
    {

        parent::init();

        // custom initialization code goes here
    }
}
