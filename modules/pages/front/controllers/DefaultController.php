<?php

namespace app\modules\pages\front\controllers;

use app\components\FrontController;
use app\components\Pagination;
use app\modules\banners\models\Banners;
use app\modules\festivals\models\Festivals;
use app\modules\gallery\models\Gallery;
use app\modules\gallery\models\GalleryFilesImage;
use app\modules\jobs\models\Genries;
use app\modules\jobs\models\Jobs;
use app\modules\jobs\models\JobsCategories;
use app\modules\jobs\models\JobsGenries;
use app\modules\jobs\models\RequestForm;
use app\modules\juri\models\Juri;
use app\modules\languages\models\Languages;
use app\modules\leading\models\Leading;
use app\modules\news\models\News;
use app\modules\nominations\models\Nominations;
use app\modules\pages\models\Pages;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\View;

class DefaultController extends FrontController
{

    public $count = 6;

    public function actionIndex($page_slug)
    {
        $this->setBanner(Banners::BANNER_ZONE_SIDEBAR_240x400, Banners::OBJECT_TYPE_TEXT_PAGE);
        $model = Pages::findBySlug($page_slug);
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $news = \Yii::$app->getCache()->get('main_page_news_3_' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($news === false) {
            $news = News::findLastNews(3);
            \Yii::$app->getCache()->set('main_page_news_3_' . Languages::getCurrent()->id . \Yii::$app->language, $news, 3600*24*7);
        }

        Pages::updateAllCounters(['views' => 1], 'id = ' . $model->id);

        return $this->render('index', [
            'model' => $model,
            'news' => $news
        ]);
    }

    public function actionAbout()
    {
        $this->isAbout = true;
        $model = Pages::findBySlug('about');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $about = Festivals::getFest(['description_full']);
        $rules = Festivals::getFest(['rules']);
        $out_of_competition = Festivals::getFest(['out_of_competition']);
        $team = Festivals::getFest(['team']);
        $contacts = Festivals::getFest(['contacts']);

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $news = \Yii::$app->getCache()->get('main_page_news_3_' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($news === false) {
            $news = News::findLastNews(3);
            \Yii::$app->getCache()->set('main_page_news_3_' . Languages::getCurrent()->id . \Yii::$app->language, $news, 3600*24*7);
        }

        return $this->render('about', [
            'model' => $model,
            'about' => $about,
            'news' => $news,

            'rules' => $rules,
            'nominations' => Nominations::findNomination(),
            'out_of_competition' => $out_of_competition,
            'team' => $team,
            'contacts' => $contacts
        ]);
    }

    public function actionTicket()
    {
        $this->isAbout = true;
        $model = Pages::findBySlug('ticket');
        if (!$model) {
            throw new NotFoundHttpException();
        }


        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );


        return $this->render('ticket', [
            'model' => $model,

        ]);
    }

    public function actionStatuette()
    {
        $this->cssClass = 'main-page statuette';
        $this->isAbout = true;
        $model = Pages::findBySlug('statuette');
        if (!$model) {
            throw new NotFoundHttpException();
        }


        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );


        return $this->render('statuette', [
            'model' => $model,

        ]);
    }

    public function actionRules()
    {
        $this->isAbout = true;
        $model = Pages::findBySlug('rules');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $rules = Festivals::getFest(['rules']);

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $news = \Yii::$app->getCache()->get('main_page_news_' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($news === false) {
            $news = News::findLastNews(5);
            \Yii::$app->getCache()->set('main_page_news_' . Languages::getCurrent()->id . \Yii::$app->language, $news, 3600*24*7);
        }

        return $this->render('rules', [
            'model' => $model,
            'rules' => $rules,
            'news' => $news
        ]);
    }

    public function actionTeam()
    {
        $this->isAbout = true;
        $model = Pages::findBySlug('team');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $team = Festivals::getFest(['team']);

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $news = \Yii::$app->getCache()->get('main_page_news_' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($news === false) {
            $news = News::findLastNews(5);
            \Yii::$app->getCache()->set('main_page_news_' . Languages::getCurrent()->id . \Yii::$app->language, $news, 3600*24*7);
        }

        return $this->render('team', [
            'model' => $model,
            'team' => $team,
            'news' => $news
        ]);
    }

    public function actionContacts()
    {
        $this->isAbout = true;
        $model = Pages::findBySlug('contact-us');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $contacts = Festivals::getFest(['contacts']);

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $news = \Yii::$app->getCache()->get('main_page_news_' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($news === false) {
            $news = News::findLastNews(5);
            \Yii::$app->getCache()->set('main_page_news_' . Languages::getCurrent()->id . \Yii::$app->language, $news, 3600*24*7);
        }

        return $this->render('contacts', [
            'model' => $model,
            'contacts' => $contacts,
            'news' => $news
        ]);
    }

    public function actionProgram($program_slug = null)
    {
        $model = Pages::findBySlug($program_slug ? 'program/' . $program_slug : 'program');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        //$program = Festivals::getFest(['program']);

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $leading = Leading::findLeading();

        return $this->render('program', [
            'model' => $model,
            'program' => $model->text,
            'leading' => $leading
        ]);
    }

    public function actionRequest($step = null)
    {
        $model = new RequestForm();
        $model->step = $step;
        $model->isRequest = true;
        $isSend = false;
        if($step && \Yii::$app->request->isAjax && $model->load(['RequestForm' => \Yii::$app->request->post()])) {
            $data = [];

            $result = $model->send();
            if(is_bool($result) && $result) {
                $data = ['success' => true];
            } else {
                $data = [
                    'success' => false,
                    'errors' => $result
                ];
            }
//print_r($data);
            return json_encode($data);
        }

		$this->setMeta(
			\Yii::t('front-about', 'Подача заявки')
		);

        \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'] =
            ArrayHelper::merge(
                \Yii::$app->assetManager->bundles['app\assets\AppAsset']['packages'],
                ['ajax', 'form-react']
            );

            $this->view->registerJs('wAjax = new wAjax();', View::POS_END) ;

        $fest = Festivals::getFest(['request', 'rules']);
        $request = $fest['request'];
        $rules = $fest['rules'];

        $genres = [];
        foreach(Genries::findAllDD() as $k => $v) {
            $genres[] = [
                'id' => $k,
                'title' => $v
            ];
        }

        $categories = [];
        foreach(JobsCategories::getDropdownList() as $k => $v) {
            $categories[] = [
                'id' => $k,
                'title' => $v
            ];
        }


        $labels = RequestForm::attributeLabelsData();
        $placeholders = RequestForm::attributePlaceholdersData();
        $imageNotice = RequestForm::imageNotice();

        $data = [
            'genres' => $genres,
            'categories' => $categories,
            'labels' => $labels,
            'placeholders' => $placeholders,
            'imageNotice' => $imageNotice
        ];

        $this->view->registerJs('
            var siteData = ' . json_encode($data) . ';
        ', View::POS_BEGIN);


        return $this->render('request', [
            'model' => $model,
            'request' => $request,
            'isSend' => $isSend,
            'rules' => $rules
        ]);
    }

    /*public function actionRequest()
    {
        if(\Yii::$app->controller->fest->registration_close) {
            return $this->redirect(['/'], 301);
        }
        $model = Pages::findBySlug('request');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $fest = Festivals::getFest(['request', 'rules']);
        $request = $fest['request'];
        $rules = $fest['rules'];
        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );
//print_r(\Yii::$app->request->referrer);
        $model = new RequestForm();
        $model->isRequest = true;
        $isSend = false;
        if($model->load(\Yii::$app->request->post()) && $model->send()) {
            $isSend = true;
        }

        return $this->render('request', [
            'model' => $model,
            'request' => $request,
            'isSend' => $isSend,
            'rules' => $rules
        ]);
    }*/

    public function actionHistory($year = null)
    {
        $this->isAbout = true;
        $model = Pages::findBySlug('history');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $fest = new Festivals();
        $fest::$oldFest = true;
        $about = $fest::getFest(['description_full'], $year);
        $place = $fest::getFest(['place'], $year);
        $date = strtotime($fest::getFest(['date'], $year));
        $date_end = strtotime($fest::getFest(['date_end'], $year));
        $job = new Jobs();
        $job::$oldFest = true;
        /*$participants = $job::findLastJobs(null, false, [], [
            'jobs.is_participant' => Jobs::IS_PARTICIPANT,
            'jobs.festival_id' => $fest::getFest(['id'])
        ], true);*/
        $gallery = GalleryFilesImage::find()
            ->innerJoinWith('gallery', false)
            ->andWhere([
                'gallery.title' => 'retro-' . date('Y', $date)
            ])
            ->orderBy(['position' => SORT_ASC])
            ->asArray()
            ->all();

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );



        return $this->render('history', [
            'model' => $model,
            'about' => $about,
            'fest' => $fest,
            'date' => $date,
            'date_end' => $date_end,
            'place' => $place,
            'participants' => 0,//$participants,
            'gallery' => $gallery,
            'title' => $fest::getFest(['title'], $year)
        ]);
    }

    public function actionHistoryJury($page_slug, $year = null)
    {
        $this->isAbout = true;
        $model = Pages::findBySlug('history/jury');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $fest = new Festivals();
        $fest::$oldFest = true;
        $fest = $fest::getFest([], $year);
//print_r($fest->attributes);exit;
        $jury = new Juri();
        $jury::$isOldFest = true;
        $jury = $jury::findJuri(true)
        ->andWhere(['festival_id' => $fest->id])
            ->asArray()
        ->all();

//print_r($jury);exit;
        return $this->render('history-jury', [
            'model' => $model,
            'jury' => $jury,
            'title' => $fest::getFest(['title'], $year)
        ]);
    }

    public function actionHistoryProgram($page_slug, $year = null)
    {
        $this->isAbout = true;
        $model = Pages::findBySlug('history/program');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $fest = new Festivals();
        $fest::$oldFest = true;
        $fest = $fest::getFest([], $year);

        $pages = new Pages();
        $pages::$oldFest = true;
        $program = $pages::find()
            ->andWhere([
                'festival_id' => $fest->id,
                'type_id' => Pages::TYPE_PROGRAM
            ])
            ->addSelect([
                'text',
                'title'
            ])
            ->asArray()->all();

        $fest = new Festivals();
        $fest::$oldFest = true;
        $fest = $fest::getFest([], $year);

        $nominations = new Nominations();
        $nominations::$isOldFest = true;
        $nominations = $nominations::findNomination($fest->id);

        return $this->render('history-program', [
            'model' => $model,
            'program' => $program,
            'title' => $fest::getFest(['title'], $year),
            'nominations' => $nominations
        ]);
    }

    public function actionHistoryParticipants($page_slug, $category = null, $t = null, $type = 'active', $year = null)
    {
        $count = 20;
        $this->isAbout = true;
        $model = Pages::findBySlug('history/participants');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $fest = new Festivals();
        $fest::$oldFest = true;
        $fest = $fest::getFest([], $year);

        $jobs = new Jobs();
        $jobs::$oldFest = true;

        $where = [];

        $typeID = null;
        if($type) {
            $typeID = array_search($type, Jobs::$jobsTypesUrl);
            $where = ['jobs.is_participant' => $typeID];
        }

        $participants = $jobs::findLastJobs(null, true, ['publish_date' => SORT_DESC], ArrayHelper::merge($where, ['festival_id' => $fest->id]));

        if($category) {
            $participants->andWhere([
                'jobs.category_i' => $category
            ]);
        }
        if($t) {
            $participants->andWhere([
                'like', 'jobs.title', $t
            ]);
        }

        $countQuery = clone $participants;
        $pager = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => $count]);
        $participants = $participants
            ->limit($count)
            ->offset($pager->offset)
            ->asArray()
            ->all();

        //print_r($participants);
        $_genres = [];
//print_r($participants);exit;
        $genres = Genries::findAllDD();
        $usedGenres = JobsGenries::findGenresIDs(null, 'oldJob', $fest->id);

        foreach($usedGenres as $v) {
        	if(isset($genres[$v])) {
				$_genres[$v] = $genres[$v];
			}
        }

        $categories = JobsCategories::getDropdownList();

        return $this->render('history-participants', [
            'model' => $model,
            'participants' => $participants,
            'genres' => $_genres,
            'pager' => $pager,
            'title' => $fest::getFest(['title'], $year),
            'genre' => null,
            'type' => $type,
            'categories' => $categories,
            'category' => $category,
			'year' => \Yii::$app->request->get('year')
        ]);
    }

    public function actionHistoryWinners($page_slug, $year = null)
    {
        $count = 20;
        $this->isAbout = true;
        $model = Pages::findBySlug('history/winners');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        $fest = new Festivals();
        $fest::$oldFest = true;
        $fest = $fest::getFest([], $year);

        $job = new Jobs();
        $job::$oldFest = true;
        $winners = $job::_findWinners(null, $fest->id);

        $genres = Genries::findAllDD();


        return $this->render('history-winners', [
            'model' => $model,
            'winners' => $winners,
            'genres' => $genres,
            'title' => $fest::getFest(['title'], $year)
        ]);
    }

}
