<section class="about about--secondary">
    <div class="container">
        <div class="grid">
            <article class="grid-item grid-item--secondary grid-item--statue">
                <figure class="statue"><img src="/images/statue.png" alt=""></figure>
                <?= $model->text; ?>
            </article>
        </div><!-- end .grid -->
    </div><!-- end .container -->
</section>

<?= \app\modules\sponsors\front\components\SponsorsWidget::widget()?>



<?= \app\modules\jobs\front\components\RequestFormWidget::widget()?>