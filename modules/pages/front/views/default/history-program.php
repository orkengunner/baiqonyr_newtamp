
<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-history', $title); ?></h1>
        <h3 class="title-section title-section--primary">
			<?= \app\components\widgets\FilterYears::widget(); ?>
        </h3>
        <?= \app\modules\menu\front\components\Menu::widget([
            'menuId' => 6,
            'tag' => 'nav',
			'viewFile' => 'menu-retro',
            'itemCssClass' => 'link link--primary',
            'cssClass' => 'nav nav--inner',
            'itemTagContainer' => null,
            'firstItemActiveExclude' => true
        ]); ?>
    </div>
</div>
<div class="main-body">
    <div class="container">
        <div class="program">
            <div class="grid">
                <div class="grid-item grid-item--content">
                    <h3 class="title--secondary"><?= Yii::t('front-history', 'Программа фестиваля'); ?></h3>
                    <?php foreach($program as $v): ?>
                        <h5 class="title--secondary"><?= $v['title']?></h5>
                        <?= $v['text']; ?>
                    <?php endforeach; ?>
                </div>
                <div class="grid-item--sidebar">
                    <?php if($nominations): ?>
                        <aside class="nominations">
                          <div class="title--secondary">Номинации</div>
                          <ul class="nominations-list">
                              <?php foreach($nominations as $v): ?>
                                   <li class="nominations-item"><?= $v['title']; ?></li>
                              <?php endforeach; ?>
                          </ul>
                        </aside>
                    <?php endif; ?>             
                </div>
            </div><!-- end .grid -->
        </div><!-- end .about-main -->
    </div>
</div>