<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-history', $title); ?></h1>
        <h3 class="title-section title-section--primary">
			<?= \app\components\widgets\FilterYears::widget(); ?>
        </h3>
        <?= \app\modules\menu\front\components\Menu::widget([
            'menuId' => 6,
            'tag' => 'nav',
			'viewFile' => 'menu-retro',
            'itemCssClass' => 'link link--primary',
            'cssClass' => 'nav nav--inner',
            'itemTagContainer' => null,
            'firstItemActiveExclude' => true
        ]); ?>
    </div>
</div>
<div class="main-body">
    <div class="container">

        <form class="form form--secondary form--search pull-right">
            <div class="form-group">
                <input class="form-control" type="text" name="t" value="<?= \Yii::$app->request->get('t')?>" id="searchWord" placeholder="<?= Yii::t('front-jobs', 'Найти на Байконуре'); ?>">
                <button type="button" class="icon icon--search" onclick="$('.form--search').submit();">
                    <span class="fa fa-search"></span>
                </button>
            </div>
        </form>

        <?php if($participants || $category || (!$participants && $type)): ?>
            <section class="inner-nav inner-nav-participants">
                <ul class="nav-pills nav--default nav--secondary">
                    <li <?= $type == 'applicant' ? 'class="active"' : ''; ?>><a class="link link--primary" href="<?= \yii\helpers\Url::to(['/history' . ($year ? '/' . $year : '') . '/participants/applicant'])?>"><?= Yii::t('front-job
                    s', 'Заявители'); ?></a></li>
                    <li <?= !$type  || $type == 'active' ? 'class="active"' : ''; ?>><a class="link link--primary"  href="<?= \yii\helpers\Url::to(['/history' . ($year ? '/' . $year : '') . '/participants'])?>"><?= Yii::t('front-jobs', 'Участники'); ?></a></li>
                    <li <?= $type == 'out-of-competition' ? 'class="active"' : ''; ?>><a class="link link--primary" href="<?= \yii\helpers\Url::to(['/history' . ($year ? '/' . $year : '') . '/participants/out-of-competition'])?>"><?= Yii::t('front-jobs', 'Вне конкурса'); ?></a></li>
                </ul>
            </section>
        <?php endif; ?>

        <div class="movies">
            <p class="movie-tab-description"><?= Yii::t('front-history', 'До участия в конкурсе допускаются режиссеры, чьи фильмы прошли проверку отборочной комиссии, на данном этапе вы являетесь заявителем.  Далее следует проверка основного жюри, и успешно прошедшие являются участниками. А также если вы участник вне конкурса ваша работа помечается флажком в списке.'); ?></p>
            <ul class="movies-list">
                <?php foreach($participants as $v) echo $this->render('@app/modules/jobs/front/views/default/_item', ['v' => $v, 'genres' => $genres]) ?>
            </ul>
            <?php echo \app\components\widgets\LinkPager::widget([
                'pagination' => $pager,
                'options' => ['class' => 'pagination--primary pagination--industry'],
                'activePageCssClass' => 'active',
                'linkClass' => 'link pagination-link',
                'tag' => 'li',
                'tagClass' => 'pagination-item',
                'nextPageLabel' => '<span aria-hidden="true">»</span>',
                'prevPageLabel' => '<span aria-hidden="true">«</span>',
                'nextPageCssClass' => 'link pagination-link',
                'prevPageCssClass' => 'link pagination-link',
                'registerLinkTags' => true,
                'maxButtonCount' => 9
            ]);?>
        </div><!-- end .movies -->
    </div>
</div>