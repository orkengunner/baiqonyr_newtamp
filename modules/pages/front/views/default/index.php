<section class="articles">
    <div class="container">
        <?= \app\modules\mainpage\front\components\BreadCrumbs::widget([
            'data' => [
                'Главная' => ['..'],
                $model->title => ''
            ]
        ])?>
        <div class="grid">
            <div class="grid-item grid-item--content">
                <article class="article">
                    <h1><?= $model->title; ?></h1>
                    <h2 class="title--primary"><?= $model->title; ?></h2>
                    <ul class="article-meta">
                        <li class="meta-item meta-date"><?= \yii\helpers\StringHelper::rudate(['j F, Y', 'j F'], strtotime($model->publish_date))?></li>
                        <li class="meta-item meta-views"><span class="icon icon--views icon--lg"></span><?= $model->views?></li>
                    </ul>
                    <!-- <figure class="article-cover"><img src="images/article-cover.jpg" alt=""></figure> -->
                    <figure class="article-cover"><img alt="<?= $model->title; ?>" src="<?= \yii\helpers\Url::imgUrl($model->fa_logo, 'pages', [725, null])?>"></figure>
                    <?= $model->text; ?>

                    <ul class="article-meta">
                        <li class="meta-item meta-date"><?= \yii\helpers\StringHelper::rudate(['j F, Y', 'j F'], strtotime($model->publish_date))?></li>
                        <li class="meta-item meta-views"><span class="icon icon--views icon--lg"></span><?= $model->views?></li>
                    </ul>
                </article>

                <?= $this->render('@app/views/layouts/front/social_shares', ['js' => true]); ?>
            </div><!-- grid-item -->

            <div class="grid-item grid-item--sidebar">
                <?= \app\modules\banners\front\components\BannerWidget::widget([
                    'banner' => \Yii::$app->controller->banners[
                    \app\modules\banners\models\Banners::BANNER_ZONE_SIDEBAR_240x400
                    ],
                ]);?>

                <?php foreach($news as $v): ?>
                    <div class="thumb thumb--article is-news">
                        <div class="thumb-meta">
                            <figure class="thumb-image"><a href="<?= \yii\helpers\Url::to(['/news/default/view',
                                    'news_slug' => $v['url']
                                ]); ?>" class="link--image"><img src="http://brod.kz<?= \yii\helpers\Url::imgUrl($v['img'], 'news', [235, 132], false)?>" alt="Иконка статьи"></a></figure>
                            <a href="<?= \yii\helpers\Url::to(['/news/default/index-category',
                                'news_types_slug' => $v['tnUrl']
                            ]); ?>" class="label--default label--primary label--news link--label"><?= $v['tnName']; ?></a>
                        </div>
                        <p class="thumb-date"><?= \yii\helpers\StringHelper::rudate(['j F, Y', 'j F'], strtotime($v['publishDate']))?></p>
                        <h4 class="thumb-title"><a href="<?= \yii\helpers\Url::to(['/news/default/view',
                                'news_slug' => $v['url']
                            ]); ?>" class="link"><?= $v['title']; ?></a></h4>
                        <p class="thumb-description"><?= strip_tags($v['previewText']); ?></p>
                    </div>
                <?php endforeach; ?>
            </div><!-- end .grid-item -->
        </div><!-- end .grid -->
    </div><!-- end .container -->

</section>
<?= \app\modules\sponsors\front\components\SponsorsWidget::widget()?>



<?= \app\modules\jobs\front\components\RequestFormWidget::widget()?>