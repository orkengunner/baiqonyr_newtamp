<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-contacta', 'О фестивале'); ?> <span class="secondary">/ <?= \Yii::t('front-contacta', 'Контакты'); ?></span></h1>
        <h3 class="title-section title-section--primary"><?= \Yii::t('front-contacta', 'О фестивале'); ?><span class="secondary">/ <?= \Yii::t('front-contacta', 'Контакты'); ?></span></h3>

        <?php echo \app\modules\menu\front\components\Menu::widget([
            'menuId' => 3,
            //'tag' => 'nav',
            'viewFile' => 'menu',
            'itemCssClass' => 'link link--primary',
            'cssClass' => 'nav nav--inner',
            'itemTagContainer' => null,
        ]); ?>
    </div>
</div>

<div class="main-body">
    <div class="container">
        <div class="grid">
            <div class="grid-item grid-item--content">
                <article class="article">
                    <?= $contacts; ?>
                </article>
            </div>
            <div class="grid-item grid-item--sidebar">
                <?php foreach($news as $v): ?>
                    <div class="thumb thumb--article is-news">
                        <div class="thumb-meta">
                            <figure class="thumb-image"><a href="<?= \yii\helpers\Url::to(['/news/default/view',
                                    'news_slug' => $v['url']
                                ]); ?>" class="link--image"><img src="http://brod.kz<?= \yii\helpers\Url::imgUrl($v['img'], 'news', [235, 132], false)?>" alt="Иконка статьи"></a></figure>
                            <a href="<?= \yii\helpers\Url::to(['/news/default/index-category',
                                'news_types_slug' => $v['tnUrl']
                            ]); ?>" class="label--default label--primary label--news link--label"><?= $v['tnName']; ?></a>
                        </div>
                        <p class="thumb-date"><?= \yii\helpers\StringHelper::rudate(['j F, Y', 'j F'], strtotime($v['publishDate']))?></p>
                        <h4 class="thumb-title"><a href="<?= \yii\helpers\Url::to(['/news/default/view',
                                'news_slug' => $v['url']
                            ]); ?>" class="link"><?= $v['title']; ?></a></h4>
                        <p class="thumb-description"><?= strip_tags($v['previewText']); ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div><!-- end .container -->
</div><!-- end .movies-online -->

<?= \app\modules\sponsors\front\components\SponsorsWidget::widget()?>



<?= \app\modules\jobs\front\components\RequestFormWidget::widget()?>