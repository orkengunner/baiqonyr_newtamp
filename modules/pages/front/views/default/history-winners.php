<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-history', $title); ?></h1>
        <h3 class="title-section title-section--primary">
			<?= \app\components\widgets\FilterYears::widget(); ?>
        </h3>
        <?= \app\modules\menu\front\components\Menu::widget([
            'menuId' => 6,
            'tag' => 'nav',
			'viewFile' => 'menu-retro',
            'itemCssClass' => 'link link--primary',
            'cssClass' => 'nav nav--inner',
            'itemTagContainer' => null,
            'firstItemActiveExclude' => true
        ]); ?>
    </div>
</div>
<div class="main-body">
    <div class="container">
        <section class="movies movies--retro">
            <ul class="movies-list">
                <?php foreach($winners as $v): ?>
                    <?php
                    $authors = [];
                    foreach(explode(';', $v['partic']) as $_v){
                        $p = explode(':', $_v);
                        if($p[0] == \app\modules\lookupposts\models\LookupPosts::POST_PRODUCER) {
                            $authors[] = $p[1];
                        }
                    }
                    ?>
                    <li class="movies-item">
                        <div class="movies-item-visual">
                            <a href="<?= \yii\helpers\Url::to(['/jobs/default/view', 'job_slug' => $v['slug']])?>" class="link--overlay"></a>
                            <figure class="movies-item-cover"><img src="<?= \yii\helpers\Url::imgUrl($v['fa_logo'], 'jobs', [167, 239])?>"></figure>
                            <span class="label--default label--primary label--news link--label"><?= $v['nominationTitle']?></span>
                        </div>
                        <div class="movies-item-body">
                            <h5 class="movies-item-title"><a href="<?= \yii\helpers\Url::to(['/jobs/default/view', 'job_slug' => $v['slug']])?>" class="link"><?= $v['title']?></a></h5>
                            <div class="movies-item-author"><span class="highlight"><?= \Yii::t('front-jobs', count($authors) > 1 ? 'Режиссеры' : 'Режиссер')?></span>: <?= $authors ? implode(', ', $authors) : ($v['first_name'] . ' ' . $v['last_name'] . ($v{'last_name_2'} && $v['first_name_2'] ? ', ' .  $v['first_name_2'] . ' ' . $v['last_name_2'] : '' )); ?></div>
                            <div class="movies-item-description"><?= implode(', ', array_map(function($_v) use ($genres) {
                                    return isset($genres[$_v]) ? $genres[$_v] : null;
                                }, explode(';', $v['genres']))); ?>, <?= $v['year']?></div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </section>
    </div>
</div>