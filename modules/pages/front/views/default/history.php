<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-history', $title); ?></h1>
        <h3 class="title-section title-section--primary">
            <?= \app\components\widgets\FilterYears::widget(); ?>
        </h3>

        <?= \app\modules\menu\front\components\Menu::widget([
            'menuId' => 6,
            'tag' => 'nav',
            'viewFile' => 'menu-retro',
            'itemCssClass' => 'link link--primary',
            'cssClass' => 'nav nav--inner',
            'itemTagContainer' => null,
            //'parent' => $parents
        ]); ?>
    </div>
</div>
<div class="main-body">
    <div class="container">
        <section class="about">
            <div class="container">
                <section class="about-main">
                    <div class="grid">
                        <div class="grid-item grid-item--content">
                            <article class="article clearfix">
                                <h3 class="title--secondary"><?= \Yii::t('front-history', 'О фестивале'); ?></h3>
                                <figure class="about-avatar pull-left"><img src="/images/avatar-festival.png" class="img" alt=""></figure>
                                <div class="article-body">
                                    <?= $about; ?>
                                </div>
                            </article>
                            <div class="about-info">
                                <div class="item">
                                    <div class="text"><?= \Yii::t('front-history', 'Место проведения'); ?></div>
                                    <div class="title"><?= $place; ?></div>
                                </div>
                                <div class="item">
                                    <div class="text"><?= \Yii::t('front-history', 'Дата проведения'); ?></div>
                                    <div class="title"><?= date('d', $date)?>-<?= \yii\helpers\StringHelper::rudate('d F', $date_end); ?></div>
                                </div>
                                <!--div class="item">
                                    <div class="text"><?= \Yii::t('front-history', 'Кол-во участников'); ?></div>
                                    <div class="title"><?= $participants . ' ' . \yii\helpers\StringHelper::declension($participants, Yii::t('front-history', 'фильм'), Yii::t('front-history', 'фильма'), Yii::t('front-history', 'фильмов'))?> </div>
                                </div-->
                            </div>
                        </div>
                    </div>
                </section><!-- end .about-main -->

                <?php if($gallery): ?>

                    <section class="about-photos">
                        <h3 class="title--secondary"><?= Yii::t('front-history', 'Фотоотчёт'); ?></h3>
                        <div class="grid">
                            <div class="grid-item grid-item--content">
                                <article class="article clearfix">
                                    <ul id="gallery-about">
                                        <?php foreach($gallery as $v):?>
                                            <li data-thumb="<?= \yii\helpers\Url::imgUrl($v['subdir'] . '/' . $v['basename'], 'gallery', [114, 69])?>">
                                                <img src="<?= \yii\helpers\Url::imgUrl($v['subdir'] . '/' . $v['basename'], 'gallery', [620, 500])?>" />
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </article>
                            </div>
                        </div>
                    </section><!-- end .about-team -->
                <?php endif; ?>
            </div><!-- end .container -->
        </section>
    </div>
</div>