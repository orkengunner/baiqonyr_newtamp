
<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-about', 'Всё о «Байконуре'); ?>»</h1>
        <h3 class="title-section title-section--primary"><?= \Yii::t('front-about', 'Всё о «Байконуре'); ?>»</h3>
    </div>
</div>

<div class="main-body">
    <section class="about">
        <div class="container">
            <section class="about-main">
                <div class="grid">
                    <div class="grid-item grid-item--content">
                        <article class="article clearfix">
                            <h3 class="title--secondary"><?= \Yii::t('front-about', 'О фестивале'); ?></h3>
                            <figure class="about-avatar pull-left"><img src="/images/avatar-festival.png" class="img" alt=""></figure>
                            <?= $about; ?>
                        </article>
                        <div class="tabs">
                            <ul class="nav nav--tabs" role="tablist">
                                <li class="nav-item active" role="presentation">
                                    <a href="#festRules" class="nav-link" aria-controls="festRules" role="tab" data-toggle="tab"><?= \Yii::t('front-about', 'Регламент'); ?></a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a href="#festCategories" class="nav-link" aria-controls="festCategories" role="tab" data-toggle="tab"><?= \Yii::t('front-about', 'Номинации'); ?></a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a href="#festOutside" class="nav-link" aria-controls="festOutside" role="tab" data-toggle="tab"><?= \Yii::t('front-about', 'Команда фестиваля'); ?></a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="festRules">
                                    <article class="article">
                                        <?= $rules; ?>
                                    </article>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="festCategories">
                                    <article class="article">
                                        <p><?= \Yii::t('front-about', 'Жюри конкурсной программы определяют победителей в номинациях:'); ?></p>
                                        <?php if($nominations): ?>
                                            <ul>
                                                <?php foreach($nominations as $v): ?>
                                                    <li>&ndash; «<?= $v['title']; ?>»</li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <p><?= \Yii::t('front-about', 'В каждой категории побеждает лишь один фильм. Каждый участник может стать победителем не более двух номинаций.'); ?></p>
                                    </article>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="festOutside">
                                    <article class="article">
                                        <div class="grid-item grid-item--content">
                                            <div class="people">
												<?= $team; ?>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item grid-item--secondary">
                        <div class="thumb thumb--dashed">
                            <a href="<?= \yii\helpers\Url::to(['/pages/default/index', 'page_slug' => 'history']); ?>" class="link--overlay"></a>
                            <div class="thumb-title"><?= \Yii::t('front-about', 'Ретроспектива'); ?></div>
                            <p class="thumb-description"><?= \Yii::t('front-about', 'История «Байконура»'); ?></p>
                        </div>

                        <aside class="contacts hidden">
                            <?= $contacts; ?>
                        </aside>
                        <aside class="contacts">
                            <div class="title--secondary"><?= \Yii::t('front-about', 'Контакты');?></div>
                            <div class="contacts-item">
                                <div class="title">Ануар Кенжибаев</div>
                                <div class="text"><?= \Yii::t('front-about', 'Директор фестиваля');?></div>
                                <div class="meta"><a href="tel:+7 707 777 1771" class="link">+7 707 777 1771</a></div>
                            </div>
                            <div class="contacts-item">
                                <div class="title">Айдана Мамаева</div>
                                <div class="text"><?= \Yii::t('front-about', 'Пресс-служба фестиваля (аккредитация)');?></div>
                            </div>
                            <div class="contacts-item">
                                <div class="meta">
                                    <strong class="strong"><?= \Yii::t('front-about', 'Почта');?>:</strong> <a href="mailto:info@brod.kz" class="link">info@brod.kz</a>
                                </div>
                            </div>
                        </aside>
                        <div class="grid-item grid-item--secondary">
                            <div class="thumb thumb--dashed">
                                <a href="//brod.kz" class="link--overlay"></a>
                                <div class="thumb-title"><img src="/images/logo-brod.svg" alt=""></div>
                                <p class="thumb-description"><?= \Yii::t('front-about', '«Бродвей» — главный организатор фестиваля'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- end .about-main -->


            <section class="about-team">
                <div class="grid">


                </div><!-- end .grid -->
            </section><!-- end .about-team -->
        </div><!-- end .container -->
    </section>
</div><!-- end .main-body -->