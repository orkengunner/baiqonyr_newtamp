<?php
    $this->registerJsFile('
        http://maps.api.2gis.ru/2.0/loader.js?pkg=basic&lazy=true
    ');
?>


<div class="main-head">
    <div class="container">
        <h1><?= Yii::t('front-programm', 'Программа'); ?></h1>
        <h3 class="title-section title-section--primary"><?= Yii::t('front-programm', 'Программа'); ?></h3>
    </div>
</div>

<div class="main-body">
    <section class="program">
        <div class="container clearfix">
            <div class="program-people">
                <h3 class="title--secondary"><?= Yii::t('front-programm', 'Ведущие'); ?></h3>
                <?php if($leading): ?>
                    <div class="people">
                        <ul class="people-list">
                            <?php foreach($leading as $v): ?>
                                <li class="people-item">
                                    <div class="people-body">
                                        <?php if($v['link']): ?>
                                            <a href="<?= $v['link'];?>" target="_blank" class="link--overlay"></a>
                                        <?php endif; ?>
                                        <figure class="people-image"><img src="<?= \yii\helpers\Url::imgUrl($v['fa_logo'], 'leading', [220, 129])?>" alt=""></figure>
                                        <h5 class="people-name"><?= $v['title']?></h5>
                                    </div>
                                    <p class="people-meta people-meta--secondary"><?= $v['short_text'];?></p>
                                </li>
                            <?php  endforeach; ?>
                        </ul>
                    </div>
                <?php else: ?>
                    <div class="soon">
                        <p class="text"><?= \Yii::t('front-program', 'Информация о ведущих скоро появится на сайте.'); ?></p>
                    </div>
                <?php endif; ?>
            </div>

            <div class="program-schedule">
                <h3 class="title--secondary"><?= Yii::t('front-programm', 'Программа'); ?></h3>
                <div class="grid">
                    <div class="grid-item grid-item--content tab-content">
                        <?php $menu = \app\modules\menu\front\components\Menu::widget([
                            'menuId' => 4,
                            //'tag' => 'nav',
                            'viewFile' => 'menu',
                            'itemCssClass' => 'link link--primary',
                            'cssClass' => 'nav-pills nav--default nav--secondary',
                            'firstItemActiveExclude' => true
                        ]);
                        if($menu): ?>
                        <section class="inner-nav">
                            <?= $menu; ?>
                        </section>
                        <?php endif; ?>
                        <?= $program; ?>
                    </div>
                    <!--div class="grid-item grid-item--sidebar">
                        <div class="map" id="map"></div>
                        <div class="map-info">
                            <p class="info small">Как добраться к нам?</p>
                            <p class="info">Чтобы добраться на фестиваль, нужно перечесь торговый центр, и отметиться на стойке регистрации.</p>
                        </div>
                    </div-->
                </div><!-- end .grid -->
            </div><!-- end .program-schedule -->
        </div><!-- end .container -->
    </section><!-- end .program -->
</div>