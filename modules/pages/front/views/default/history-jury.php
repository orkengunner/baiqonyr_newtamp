<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-history', $title); ?></h1>
        <h3 class="title-section title-section--primary">
			<?= \app\components\widgets\FilterYears::widget(); ?>
        </h3>
        <?= \app\modules\menu\front\components\Menu::widget([
            'menuId' => 6,
            'tag' => 'nav',
			'viewFile' => 'menu-retro',
            'itemCssClass' => 'link link--primary',
            'cssClass' => 'nav nav--inner',
            'itemTagContainer' => null,
            'firstItemActiveExclude' => true
        ]); ?>
    </div>
</div>
<div class="main-body">
    <div class="container">
        <div class="about-main">
            <div class="grid">
                <div class="grid-item grid-item--content">
                    <h3 class="title--secondary"><?= Yii::t('front-history', 'Жюри фестиваля'); ?></h3>
                    <div class="people">
                        <ul class="people-list">
                            <?php foreach($jury as $v): ?>
                                <li class="people-item">
                                    <a href="#" class="link--overlay"></a>
                                    <figure class="people-image"><img src="<?= \yii\helpers\Url::imgUrl($v['fa_logo'], 'juri', [128, 128])?>" alt=""></figure>
                                    <h5 class="people-name"><?= $v['name']?></h5>
                                    <p class="people-meta people-meta--primary"><?= $v['short_text']; ?></p>
                                </li>
                            <?php endforeach;; ?>
                        </ul>
                    </div>
                </div>
            </div><!-- end .grid -->
        </div><!-- end .about-main -->
    </div>
</div>