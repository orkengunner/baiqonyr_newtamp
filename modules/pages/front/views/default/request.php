<?= $this->render('@app/modules/users/front/views/default/_terms');?>
<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-about', 'Подача заявки'); ?></h1>
        <h3 class="title-section title-section--primary"><?= \Yii::t('front-about', 'Подача заявки'); ?></h3>
    </div>
</div>

<div class="main-body">
    <section class="application">
        <div class="container">
            <article class="article">
                <?= $request; ?>
            </article>


            <!-- REACT FORM -->
            <section id="formReact" data-lang="<?= \app\modules\languages\models\Languages::$current->locale; ?>" class="form-app"></section>
            <!-- ********** -->


            <div class="form-sidebar">
                <div class="form-rules">
                    <div class="title"><?= \Yii::t('request-form', 'Правила подачи заявки'); ?></div>
                    <ul class="list">
                        <li class="list-item"><?= \Yii::t('request-form', 'внимательно заполните все поля;')?></li>
                        <li class="list-item"><?= \Yii::t('request-form', 'все поля необходимы для заполонения.');?></li>
                    </ul>
                </div>
                <div class="form-scheme">
                    <ul class="form-scheme-list">
                        <li class="form-scheme-item active">
                            <div class="form-scheme-item-wrapper">
                                <h3 class="form-scheme-item-title"><?= \Yii::t('request-form', '1 июня по 31 августа')?></h3>
                                <p class="form-scheme-item-description"><?= \Yii::t('request-form', 'приём заявок')?></p>
                            </div>
                        </li>
                        <li class="form-scheme-item">
                            <div class="form-scheme-item-wrapper">
                                <h3 class="form-scheme-item-title"><?= \Yii::t('request-form', '1 сентября по 5 сентября')?></h3>
                                <p class="form-scheme-item-description"><?= \Yii::t('request-form', ' отбор кандидатов')?></p>
                            </div>
                        </li>
                        <li class="form-scheme-item">
                            <div class="form-scheme-item-wrapper">
                                <h3 class="form-scheme-item-title"><?= \Yii::t('request-form', '6 сентября')?></h3>
                                <p class="form-scheme-item-description"><?= \Yii::t('request-form', 'объявление участников')?></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div><!-- end .container -->
    </section><!-- end .application -->
</div>
