<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pages\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('pages', 'Системные страницы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">
    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <p>
        <?= Html::a(Yii::t('pages', '<i class="icon-list-add"></i> Создать страницу', [
            'modelClass' => 'Pages',
        ]), ['create'], ['class' => 'btn btn-success']) ?>

    </p>


    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'pager' => [
                    'lastPageLabel' => 'Последняя',
                    'firstPageLabel' => 'Первая'
                ],
                'layout' => "{items}\n{pager}",
                /*'rowOptions' => function ($model, $key, $index, $grid) {
                    if ($model->status_id == \app\modules\pages\models\Pages::STATUS_PUBLISHED) {
                        return ['style' => 'background:#d3fcd9;'];
                    } elseif ($model->status_id == \app\modules\pages\models\Pages::STATUS_NEW) {
                        return ['style' => 'background:#edd;'];
                    } elseif ($model->status_id==\app\modules\pages\models\Pages::STATUS_DRAFT) {
                        return ['style' => 'background:#ccc;'];
                    }
                },*/
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'title',
                    [
                        'attribute' => 'status_id',
                        'value' => function ($data) {
                            return \app\modules\pages\models\Pages::$statuses[$data->status_id];
                        },
                        'filter' => \app\modules\pages\models\Pages::$statuses
                    ],
                    'publish_date',
                    /*[
                        'attribute' => 'user_id',
                        'value' => function ($data) {
                            return $data->user->username;
                        },
                        'header' => 'Автор',
                        'filter' => \app\modules\users\models\Users::getAuthors()
                    ],*/
                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{update}'
                    ],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
