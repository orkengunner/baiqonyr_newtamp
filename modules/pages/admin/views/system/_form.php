<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\pages\models\Pages */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="pages-form">
    <?php $form = ActiveForm::begin(['id' => 'pages-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-9">
            <div class="widget">
                <div class="widget-content padding">

                    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
                    <?= $form->field($model, 'title_kz')->textInput(['maxlength' => 255]) ?>
                    <?= $form->field($model, 'text')->textarea(['rows' => 4]); ?>


                    <?= $form->field($model, 'text_kz')->textarea(['rows' => 4]); ?>
                    <?= Html::activeHiddenInput($model, 'is_system', ['value' => 1]) ?>

                </div>
            </div>

            <?= \app\components\widgets\MetaTagFields::widget([
                'model' => $model,
                'form' => $form,
                'isLang' => true
            ]) ?>

            <div class="widget">
                <div class="widget-content padding">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('pages', 'Создать') : Yii::t('pages',
                        'Изменить'), [
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                        'id' => 'submit-page'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget">
                <div class="widget-header">
                    <h2>Опубликовать</h2>
                </div>
                <div class="widget-content padding">
                    <?= $form->field($model, 'publish_date', [
                        'template' => '{label}{input}',
                        //'options' => ['class' => 'form-group form-post-date-section'],
                        //'labelOptions' => ['class' => 'control-label title-form-section']
                    ])->widget(\janisto\timepicker\TimePicker::className(), [
                        //'language' => 'fi',
                        'mode' => 'datetime',
                        'addon' => '',
                        'clientOptions'=>[
                            'dateFormat' => 'dd/mm/yy',
                            // 'timeFormat' => 'HH:mm:ss',
                        ]
                    ])->textInput(['value' => $model->publish_date ? date('d/m/Y H:i', strtotime($model->publish_date)) : date('d/m/Y H:i')]);?>

                    <?= $form->field($model,
                        'status_id')->dropDownList($model::$statuses) ?>

                    <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>


                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>