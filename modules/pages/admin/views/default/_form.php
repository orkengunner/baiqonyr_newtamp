<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\pages\models\Pages */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("
    setInterval(function(){
        $(\".jcrop-tracker\").click(function(){
            return false;
        });
    }, 500);

");
?>

<div class="pages-form">
    <?php $form = ActiveForm::begin(['id' => 'pages-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-9">
            <div class="widget">
                <div class="widget-content padding">

                    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

                    <?= $form->field($model, 'text')->textarea(['rows' => 4]); ?>

                    <?= $form->field($model, 'title_kz')->textInput(['maxlength' => 255]) ?>

                    <?= $form->field($model, 'text_kz')->textarea(['rows' => 4]); ?>

                </div>
            </div>

            <?= \app\components\widgets\MetaTagFields::widget([
                'model' => $model,
                'form' => $form,
                'isLang' => true
            ]) ?>

            <div class="widget">
                <div class="widget-content padding">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('pages', 'Создать') : Yii::t('pages',
                        'Изменить'), [
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                        'id' => 'submit-page'
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget">
                <div class="widget-header">
                    <h2>Опубликовать</h2>
                </div>
                <div class="widget-content padding">
                    <?= $form->field($model, 'publish_date', [
                        'template' => '{label}{input}',
                        //'options' => ['class' => 'form-group form-post-date-section'],
                        //'labelOptions' => ['class' => 'control-label title-form-section']
                    ])->widget(\janisto\timepicker\TimePicker::className(), [
                        //'language' => 'fi',
                        'mode' => 'datetime',
                        'addon' => '',
                        'clientOptions'=>[
                            'dateFormat' => 'dd/mm/yy',
                            // 'timeFormat' => 'HH:mm:ss',
                        ]
                    ])->textInput(['value' => $model->publish_date ? date('d/m/Y H:i', strtotime($model->publish_date)) : date('d/m/Y H:i')]);?>

                    <?= $form->field($model,
                        'status_id')->dropDownList($model::$statuses) ?>

                    <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>
                    <?= $form->field($model, 'type_id')->checkbox() ?>


                </div>
            </div>

            <div class="widget">
                <div class="widget-header">
                    <h2>Миниатюра записи</h2>
                </div>
                <div class="widget-content padding gallery-wrap">
                    <?php
                    $sizes = $model->getParam('logo.sizes');
                    echo \app\components\widgets\ImageUploadAsync::widget([
                        'model' => $model,
                        'attribute' => 'upload_logo',
                        'form' => $form,
                        //'hint' => 'Рекомендуемый размер в пропорции 1,5',
                        'croppable' => false,
                        'previewPath' => $model->getFilePath('logo', false, [$sizes[0][0], $sizes[0][1]]),
                    ]); ?>
                </div>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script src="/js/ckeditor/ckeditor.js?<?= time() ?>"></script>
<script>

    var EDITOR_GALLERIES = <?=\yii\helpers\Json::encode(\app\modules\gallery\models\Gallery::getDropdownList())?>;

    CKEDITOR.replace('pages-text', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('pages-text_kz', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName === 'table') {
            var infoTab = dialogDefinition.getContents('info');
            var border = infoTab.get('txtBorder');
            var width = infoTab.get('txtWidth');
            width['default'] = "100%";
            border['default'] = "0";
        }
    });
</script>
