<?php

namespace app\modules\pages\models;

use app\components\ActiveRecord;
use app\modules\categories\models\Categories;
use Yii;

/**
 * This is the model class for table "pages_categories".
 *
 * @property string $page_id
 * @property integer $category_id
 *
 * @property Categories $category
 * @property Pages $page
 */
class PagesCategories extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'category_id'], 'required'],
            [['page_id', 'category_id'], 'integer'],
            [['page_id', 'category_id'], 'unique', 'targetAttribute' => ['page_id', 'category_id'], 'message' => 'The combination of Page ID and Category ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_id' => 'Page ID',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Pages::className(), ['id' => 'page_id']);
    }
}
