<?php

namespace app\modules\pages\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "pages_files_meta_image".
 *
 * @property string $page_id
 * @property string $subdir
 * @property string $basename
 */
class PagesFilesMetaImage extends ActiveRecord
{
    public static $keyField = 'page_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages_files_meta_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'subdir', 'basename'], 'required'],
            [['page_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'page_id' => 'Page ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }
}
