<?php
namespace app\modules\pages\models;

use app\components\ActiveQuery;

class PagesQuery extends ActiveQuery
{
    public function isSystem() {
        return $this->andWhere(['pages.is_system' => Pages::PAGE_SYSTEM]);
    }

    public function isNotSystem() {
        return $this->andWhere(['<>', 'pages.is_system', Pages::PAGE_SYSTEM]);
    }
}
