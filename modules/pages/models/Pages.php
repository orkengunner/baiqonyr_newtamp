<?php

namespace app\modules\pages\models;

use app\components\ActiveRecord;
use app\components\behaviors\WithMetaTags;
use app\components\LanguageActiveRecord;
use app\components\traits\UploadableAsync;
use app\modules\categories\models\Categories;

use app\modules\festivals\models\Festivals;
use app\modules\languages\models\Languages;
use app\modules\logs\models\AdminLogs;
use app\modules\users\models\Users;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "pages".
 *
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property string $text
 * @property string $slug
 * @property string $publish_date
 * @property integer $status_id
 * @property string $create_date
 * @property string $update_date
 * @property string $delete_date
 * @property string $delete_by
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $lang_id
 * @property integer $is_system
 * @property string $views
 * @property string $title_kz
 * @property string $text_kz
 * @property string $meta_title_kz
 * @property string $meta_description_kz
 * @property string $meta_keywords_kz
 * @property string $festival_id
 * @property string $type_id
 *
 * @property Festivals $festival
 * @property Languages $lang
 * @property PagesCategories[] $pagesCategories
 * @property Categories[] $categories
 * @property PagesFilesLogo $pagesFilesLogo
 * @property PagesFilesMetaImage $pagesFilesMetaImage
 */
class Pages extends ActiveRecord
{
    const PAGE_SYSTEM = 1;
    const TYPE_PROGRAM = 1;
    use UploadableAsync;

    public $listCategories;

    public $categorySefname;

    public $upload_logo = null;

    public static $oldFest = null;

    /**
     * @var string helper attribute to work with tags
     */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    public function attributeLabels()
	{
		return ArrayHelper::merge(parent::attributeLabels(), [
			'type_id' => 'Программа фестиваля'
		]);
	}

	/**
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        $query = new PagesQuery(get_called_class());

        if(!self::$oldFest) {
            $query->andWhere(['OR',
                ['festival_id' => self::festID()],
                //['festival_id' => null]
            ]);
        }
        if (Yii::$app->params['yiiEnd'] == 'admin') {
            $query = $query->isNoDeleted('pages');
            if(\Yii::$app->controller->id == 'system') {
                $query->isSystem();
            } else {
                $query->isNotSystem();
            }
            if(!self::$oldFest) {
                $query->andWhere(['OR',
                    ['festival_id' => self::festID()],
                  //  ['festival_id' => null]
                ]);
            }
            return $query;
        }

        return $query->isPublish('pages')->isNoDeleted('pages')->withFilesMetaImage('pages', 'page_id');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title'], 'required'],
            [['user_id', 'status_id', 'lang_id', 'is_system', 'views', 'type_id'], 'integer'],
            [['text', 'meta_description', 'text_kz', 'meta_description_kz'], 'string'],
            [['publish_date', 'create_date', 'update_date', 'listCategories'], 'safe'],
            [['title', 'title_kz', 'slug', 'meta_title', 'meta_title_kz', 'meta_keywords',  'meta_keywords_kz'], 'string', 'max' => 255],
            [['slug', 'status_id', 'festival_id'], 'unique', 'targetAttribute' => ['slug', 'status_id', 'festival_id'], 'message' => 'The combination of Slug and Status ID has already been taken.'],
            [
                ['upload_logo'],
                'image',/* 'minWidth' => 620, 'minHeight'=>400,*/
                'on' => 'upload'
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestival()
    {
        return $this->hasOne(Festivals::className(), ['id' => 'festival_id']);
    }

    /**
     * @inheritdoc
     */


    public function getDeleteBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'delete_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['id' => 'category_id'])->viaTable('pages_categories',
            ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagesCategories()
    {
        return $this->hasMany(PagesCategories::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    public function getPagesFilesMetaImages()
    {
        return $this->hasMany(PagesFilesMetaImage::className(), ['page_id' => 'id']);
    }

    public function getPagesFilesLogos()
    {
        return $this->hasMany(PagesFilesLogo::className(), ['page_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior,
            [
                'class' => WithMetaTags::className()
            ],
        ];
    }

    public function beforeValidate()
    {
        $this->user_id = Yii::$app->getUser()->id;

        if ($this->getScenario() != 'search') {

            /*if ($this->scenario != 'search' && $this->scenario != 'upload' && (empty($this->listCategories) || !is_array($this->listCategories) || array_sum($this->listCategories) == 0)) {
                $this->addError('listCategories', 'Не выбрана категория');
            }*/
            if (!$this->slug) {
                $this->slug = StringHelper::url($this->title);
            }
        }

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if(\Yii::$app->request->post('Pages')){
            $this->publish_date = StringHelper::dateFormat($this->publish_date, 'd/m/Y H:i', 'Y-m-d H:i:s');
        }

        $this->festival_id =  self::festID();

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

        if ($this->hasAsyncTempFile('upload_logo')) {
            $logo = $this->setFile('logo', $this->saveAsyncFile('upload_logo'), true);
        }

        //$this->saveCategories('page_id', PagesCategories::className());

        parent::afterSave($insert, $changedAttributes);

        return true;

    }


    public function afterFind()
    {
        parent::afterFind();

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            $this->listCategories = ArrayHelper::map($this->categories, 'id', 'id');
        }

        return true;
    }

    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id'])->viaTable(PagesCategories::tableName(),
            ['page_id' => 'id']);
    }

    public static function findBySlug($slug, $is_query = false)
    {
        if ($slug == '404') {
            self::$oldFest = true;
        }

        $model = self::find()
            ->withFilesLogo(self::tableName(), 'page_id')
            ->andWhere(['pages.slug' => $slug])
            ->addSelect([
                'pages.*',
            ])
        ->getLangField('pages.title')
        ->getLangField('pages.text')
        ->getLangField('pages.meta_title')
        ->getLangField('pages.meta_description')
        ->getLangField('pages.meta_keywords')
        ;
        if(!$is_query) {
            $_model = clone($model);
            $model = \Yii::$app->getCache()->get('page_' . $slug . Languages::getCurrent()->id . \Yii::$app->language);
            //if ($model === false) {
                $model = $_model->one();
                //\Yii::$app->getCache()->set('page_' . $slug . Languages::getCurrent()->id . \Yii::$app->language, $model, 3600*24*7);
            //}

            return $model;
        }
        return $model;
    }
}
