<?php

namespace app\modules\pages\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;


/**
 * PagesSearch represents the model behind the search form about `app\modules\pages\models\Pages`.
 */
class PagesSearch extends Pages
{
    public $category_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'lang_id', 'category_id'], 'integer'],
            [
                ['title', 'text', 'text', 'sefname', 'publish_date', 'pub_date_from', 'pub_date_to', 'status_id'],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {

        $scenarios = Model::scenarios();
        $scenarios['search'] = ['title', 'publish_date', 'status_id', 'category_id'];
        return $scenarios;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['publish_date' => SORT_DESC]],

        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'pages.lang_id' => $this->lang_id,
            'publish_date' => $this->publish_date,
        ]);

        if (!empty($this->status_id)) {
            $query->andWhere(['pages.status_id' => $this->status_id]);
        }

        if (!empty($this->category_id)) {
            $query->joinWith('categories');
            $query->andWhere(['categories.id' => $this->category_id]);
        }

        $query->andFilterWhere(['like', 'pages.title', $this->title])
            ->andFilterWhere(['like', 'pages.publish_date', $this->publish_date]);


        return $dataProvider;
    }
}
