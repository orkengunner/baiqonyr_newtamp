<?php

namespace app\modules\juri\admin\controllers;

use app\components\AdminController;
use Yii;
use app\modules\festivals\models\FestivalsJuri;
use app\modules\juri\models\FestivalsJuriSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FestivalsController implements the CRUD actions for FestivalsJuri model.
 */
class FestivalsController extends AdminController
{

    public function actionIndex()
    {
        $searchModel = new FestivalsJuriSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FestivalsJuri model.
     * @param string $festival_id
     * @param string $juri_id
     * @return mixed
     */
    public function actionView($festival_id, $juri_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($festival_id, $juri_id),
        ]);
    }

    /**
     * Creates a new FestivalsJuri model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FestivalsJuri();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FestivalsJuri model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $festival_id
     * @param string $juri_id
     * @return mixed
     */
    public function actionUpdate($festival_id, $juri_id)
    {
        $model = $this->findModel($festival_id, $juri_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FestivalsJuri model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $festival_id
     * @param string $juri_id
     * @return mixed
     */
    public function actionDelete($festival_id, $juri_id)
    {
        $this->findModel($festival_id, $juri_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FestivalsJuri model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $festival_id
     * @param string $juri_id
     * @return FestivalsJuri the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($festival_id, $juri_id)
    {
        if (($model = FestivalsJuri::findOne(['festival_id' => $festival_id, 'juri_id' => $juri_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
