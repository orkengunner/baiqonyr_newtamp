<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\festivals\models\FestivalsJuri */

$this->title = 'Добавление жюри';
$this->params['breadcrumbs'][] = ['label' => 'Festivals Juris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivals-juri-create">

    <div class="page-heading">
        <h1><i class="icon-menu"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
