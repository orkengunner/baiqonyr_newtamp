<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\festivals\models\FestivalsJuri */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="festivals-juri-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="widget">
        <div class="widget-content padding">
            <?= $form->field($model, 'juri_id')->dropDownList(\app\modules\juri\models\Juri::getDropdownList('name'), ['prompt' => 'Выберите']) ?>
            <?= $form->field($model, 'type')->dropDownList(\app\modules\juri\models\Juri::$types) ?>

        </div>
    </div>
    <div class="widget">
        <div class="widget-content padding">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('posts', 'Создать') : Yii::t('posts',
                'Изменить'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                'id' => 'submit-page'
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>


</div>
