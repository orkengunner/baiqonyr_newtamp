<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\juri\models\FestivalsJuriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Жюри фестиваля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivals-juri-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('festivals_juri', '<i class="icon-list-add"></i> Добавить человека', [
            'modelClass' => 'FestivalsJuri',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
        <a href="/admin.php/juri" class="btn btn-default"><i class="fa fa-align-justify"></i> Жюри</a>
    </p>
    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'pager' => [
                    'lastPageLabel' => 'Последняя',
                    'firstPageLabel' => 'Первая'
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'name',
                        'value' => function ($data) {
                            return $data->juri->name;
                        },
                    ],

                    [
                        'attribute' => 'type',
                        'value' => function ($data) {
                            return \app\modules\juri\models\Juri::$types[$data->type];
                        },
                        'filter' => \app\modules\juri\models\Juri::$types
                    ],

                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{update} {delete}'
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>

