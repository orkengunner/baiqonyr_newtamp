<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\festivals\models\FestivalsJuri */

$this->title = 'Редактирование жюри';
$this->params['breadcrumbs'][] = ['label' => 'Festivals Juris', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->festival_id, 'url' => ['view', 'festival_id' => $model->festival_id, 'juri_id' => $model->juri_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="festivals-juri-update">

    <div class="page-heading">
        <h1><i class="icon-menu"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
