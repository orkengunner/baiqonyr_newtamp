<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\juri\models\JuriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Жюри';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juri-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('jury', '<i class="icon-list-add"></i> Добавить', [
            'modelClass' => 'Jury',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
        <a href="/admin.php/juri/festivals/" class="btn btn-default"><i class="fa fa-align-justify"></i> Жюри фестиваля</a>
    </p>
    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],


            'name',

                    [
                        'attribute' => 'status_id',
                        'value' => function ($data) {
                            return \app\modules\pages\models\Pages::$statuses[$data->status_id];
                        },
                        'filter' => \app\modules\pages\models\Pages::$statuses
                    ],
            // 'slug',
            // 'meta_title',
            // 'meta_keywords',
            // 'meta_description:ntext',
            // 'create_date',
            // 'update_date',
            // 'delete_date',
            // 'delete_by',
            // 'lang_id',

                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{update} {remove}'
                    ],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
