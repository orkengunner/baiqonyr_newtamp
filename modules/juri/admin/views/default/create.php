<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\juri\models\Juri */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Juris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juri-create">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
