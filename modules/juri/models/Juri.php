<?php

namespace app\modules\juri\models;

use app\components\ActiveRecord;
use app\components\behaviors\WithMetaTags;
use app\components\LanguageActiveRecord;
use app\components\traits\UploadableAsync;
use app\modules\festivals\models\Festivals;
use app\modules\festivals\models\FestivalsJuri;
use app\modules\languages\models\Languages;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "juri".
 *
 * @property string $id
 * @property string $user_id
 * @property string $name
 * @property string $short_text
 * @property string $text
 * @property integer $status_id
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $create_date
 * @property string $update_date
 * @property string $delete_date
 * @property string $delete_by
 * @property string $link
 * @property integer $lang_id
 *
 * @property FestivalsJuri[] $festivalsJuris
 * @property Festivals[] $festivals
 * @property Languages $lang
 */
class Juri extends ActiveRecord
{
    use UploadableAsync;
	public static $types = [
		self::TYPE_NATIONAL => 'Национальное жюри',
		self::TYPE_WORLD => 'Международное жюри'
	];

    public $upload_logo = null;
    public $file_attributes = [
        'logo',
        'metaImage'
    ];

    public static $isOldFest = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'juri';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_text', 'text', 'status_id', 'link'], 'required'],
            [['user_id', 'status_id', 'delete_by', 'lang_id'], 'integer'],
            [['short_text', 'short_text_kz', 'meta_description_kz', 'text_kz', 'text', 'meta_description'], 'string'],
            [['create_date', 'update_date', 'delete_date'], 'safe'],
            [['name', 'slug', 'meta_title', 'meta_keywords', 'link'], 'string', 'max' => 255],
            [['name_kz', 'meta_title_kz', 'meta_keywords_kz'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'name' => 'Имя',
            'name_kz' => 'Имя (каз)',
        ]);
    }

    public static function find()
    {
        $query = new JuriQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query->isNoDeleted('juri');
        } else {
            $query->innerJoinWith('festivalsJuris', false);
        }

        if(!self::$isOldFest) {
            $query->andWhere(['festival_id' => self::festID()]);
        }
        return $query->isActive('juri')->isNoDeleted('juri');
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior,
            [
                'class' => WithMetaTags::className()
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestivalsJuris()
    {
        return $this->hasMany(FestivalsJuri::className(), ['juri_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestivals()
    {
        return $this->hasMany(Festivals::className(), ['id' => 'festival_id'])->viaTable('festivals_juri', ['juri_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuriFilesLogos()
    {
        return $this->hasMany(JuriFilesLogo::className(), ['juri_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuriFilesMetaImages()
    {
        return $this->hasMany(JuriFilesMetaImage::className(), ['juri_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->hasAsyncTempFile('upload_logo')) {
            $logo = $this->setFile('logo', $this->saveAsyncFile('upload_logo'), true);
        }

        parent::afterSave($insert, $changedAttributes);
        return true;

    }

    public function beforeValidate()
    {
        //$this->festival_id = $this->festID();
        if ($this->getScenario() != 'search') {
            $this->user_id = Yii::$app->getUser()->id;
            if (!$this->slug) {
                $this->slug = StringHelper::url($this->name);
            }
        }
        return parent::beforeValidate();
    }

    public static function 	findJuri($isQuery =  false)
    {
        $query = self::find()
            ->withFilesLogo('juri', 'juri_id')
            ->getLangField('juri.name')
            ->getLangField('juri.short_text')
            ->addSelect([
                'juri.id',
                'juri.slug',
                'juri.link',
            ])
			->orderBy('position');
        if($isQuery) return $query;

        return $query->asArray()->all();
    }
}
