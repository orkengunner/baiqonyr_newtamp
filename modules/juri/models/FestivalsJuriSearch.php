<?php

namespace app\modules\juri\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\festivals\models\FestivalsJuri;

/**
 * FestivalsJuriSearch represents the model behind the search form about `app\modules\festivals\models\FestivalsJuri`.
 */
class FestivalsJuriSearch extends FestivalsJuri
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['festival_id', 'juri_id', 'type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
        ->innerJoinWith('juri');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'festival_id' => $this->festival_id,
            'juri_id' => $this->juri_id,
            'type' => $this->type,
        ]);

        return $dataProvider;
    }
}
