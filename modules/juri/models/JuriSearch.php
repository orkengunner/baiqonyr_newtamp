<?php

namespace app\modules\juri\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\juri\models\Juri;

/**
 * JuriSearch represents the model behind the search form about `app\modules\juri\models\Juri`.
 */
class JuriSearch extends Juri
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'status_id', 'delete_by', 'lang_id'], 'integer'],
            [['name', 'short_text', 'text', 'slug', 'meta_title', 'meta_keywords', 'meta_description', 'create_date', 'update_date', 'delete_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = Model::scenarios();
        $scenarios['search'] = ['title', 'status_id'];
        return $scenarios;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Juri::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'status_id' => $this->status_id,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_date' => $this->delete_date,
            'delete_by' => $this->delete_by,
            'lang_id' => $this->lang_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'short_text', $this->short_text])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description]);

        return $dataProvider;
    }
}
