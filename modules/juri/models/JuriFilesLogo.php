<?php

namespace app\modules\juri\models;

use Yii;

/**
 * This is the model class for table "juri_files_logo".
 *
 * @property string $juri_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Juri $juri
 */
class JuriFilesLogo extends \yii\db\ActiveRecord
{
    public static $keyField = 'juri_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'juri_files_logo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['juri_id', 'subdir', 'basename'], 'required'],
            [['juri_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255],
            [['juri_id'], 'exist', 'skipOnError' => true, 'targetClass' => Juri::className(), 'targetAttribute' => ['juri_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'juri_id' => 'Juri ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJuri()
    {
        return $this->hasOne(Juri::className(), ['id' => 'juri_id']);
    }
}
