<?php
    $this->registerJsFile('
        http://maps.api.2gis.ru/2.0/loader.js?pkg=basic&lazy=true
    ');
?>

<div class="main-head">
    <div class="container">
        <h1><?= \Yii::t('front-jobs', 'Жюри'); ?></h1>
        <h3 class="title-section title-section--primary"><?= \Yii::t('front-jobs', 'Жюри'); ?></h3>
    </div>
</div>
<div class="main-body">
    <div class="container">
        <div class="about-main">
            <div class="grid">
                <div class="grid-item grid-item--content">
                    <p class="text"><?= $model->text ?></p>
                    <div class="people">
                        <?php if($worldJury): ?>
                            <h3 class="title--secondary"><?= \Yii::t('front-jury', 'Международное жюри')?></h3>
                            <ul class="people-list">
                                <?php foreach($worldJury as $v): ?>
                                    <li class="people-item">
                                        <div class="people-body">
                                            <a href="<?= $v['link']; //\yii\helpers\Url::to(['/juri/default/view', 'juri_slug' => $v['slug']])?>" class="link--overlay"></a>
                                            <figure class="people-image"><img src="<?= \yii\helpers\Url::imgUrl($v['fa_logo'], 'juri', [128, 128])?>"></figure>
                                            <h5 class="people-name"><?= $v['name']?></h5>
                                        </div>
                                        <p class="people-meta people-meta--secondary"><?= strip_tags($v['short_text'])?></p>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>


                        <?php if($nationalsJury): ?>
                            <h3 class="title--secondary"><?= \Yii::t('front-jury', 'Национальное жюри')?></h3>
                            <ul class="people-list">
                                <?php foreach($nationalsJury as $v): ?>
                                    <li class="people-item">
                                        <div class="people-body">
                                            <a href="<?= $v['link']; //\yii\helpers\Url::to(['/juri/default/view', 'juri_slug' => $v['slug']])?>" class="link--overlay"></a>
                                            <figure class="people-image"><img src="<?= \yii\helpers\Url::imgUrl($v['fa_logo'], 'juri', [128, 128])?>"></figure>
                                            <h5 class="people-name"><?= $v['name']?></h5>
                                        </div>
                                        <p class="people-meta people-meta--secondary"><?= strip_tags($v['short_text'])?></p>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
                <!--div class="grid-item grid-item--sidebar">
                  <div class="map" id="map"></div>
                  <div class="map-info">
                    <p class="info small"><?= \Yii::t('front-jobs', 'Как добраться к нам?'); ?></p>
                    <p class="info"><?= \Yii::t('front-jobs', 'Чтобы добраться на фестиваль, нужно пересечь торговый центр, и отметиться на стойке регистрации.'); ?></p>
                  </div>
                </div-->
            </div><!-- end .grid -->
        </div><!-- end .about-main -->
    </div><!-- end .container -->
</div>