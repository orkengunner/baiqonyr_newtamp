<?php

namespace app\modules\juri\front\controllers;

use app\components\FrontController;
use app\components\Pagination;
use app\modules\jobs\models\Genries;
use app\modules\jobs\models\Jobs;
use app\modules\jobs\models\JobsGenries;
use app\modules\juri\models\Juri;
use app\modules\languages\models\Languages;
use app\modules\pages\models\Pages;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class DefaultController extends FrontController
{

    public function actionIndex()
    {
        $model = Pages::findBySlug('juri');
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );


        $juri = Juri::findJuri(true);

		$nationalsJury =  (clone $juri)->andWhere(['type' => Juri::TYPE_NATIONAL])->asArray()->all();
        $worldJury =  (clone $juri)->andWhere(['type' => Juri::TYPE_WORLD])->asArray()->all();

        return $this->render('index', [
            'juri' => $juri,
            'model' => $model,
			'nationalsJury' => $nationalsJury,
			'worldJury' => $worldJury
        ]);
    }
}
