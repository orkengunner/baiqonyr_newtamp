<?php

namespace app\modules\leading\models;

use app\components\behaviors\WithMetaTags;
use app\components\LanguageActiveRecord;
use app\components\traits\UploadableAsync;
use app\modules\festivals\models\Festivals;
use app\modules\languages\models\Languages;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "leading".
 *
 * @property string $id
 * @property string $festival_id
 * @property string $title
 * @property string $short_text
 * @property string $text
 * @property string $site
 * @property string $link
 * @property integer $status_id
 * @property string $slug
 * @property string $user_id
 * @property string $create_date
 * @property string $update_date
 * @property string $meta_title
 * @property string $delete_by
 * @property string $delete_date
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $lang_id
 *
 * @property Festivals $festival
 * @property Languages $lang
 * @property LeadingFilesLogo[] $leadingFilesLogos
 * @property LeadingFilesMetaImage[] $leadingFilesMetaImages
 */
class Leading extends LanguageActiveRecord
{
    use UploadableAsync;
    public $upload_logo = null;

    public $file_attributes = [
        'logo',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leading';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_text', 'status_id'], 'required'],
            [['festival_id', 'status_id', 'user_id', 'delete_by', 'lang_id'], 'integer'],
            [['short_text', 'text', 'meta_description'], 'string'],
            [['create_date', 'update_date', 'delete_date'], 'safe'],
            [['title', 'site', 'link', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['festival_id', 'slug'], 'unique', 'targetAttribute' => ['festival_id', 'slug'], 'message' => 'The combination of Festival ID and Slug has already been taken.'],
            [['festival_id'], 'exist', 'skipOnError' => true, 'targetClass' => Festivals::className(), 'targetAttribute' => ['festival_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [
                ['upload_logo'],
                'image',/* 'minWidth' => 620, 'minHeight'=>400,*/
                'on' => 'upload'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [

        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeadingFilesLogos()
    {
        return $this->hasMany(LeadingFilesLogo::className(), ['leading_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior,
        ];
    }

    public function beforeValidate()
    {
        if ($this->getScenario() != 'search') {
            $this->user_id = Yii::$app->getUser()->id;
            if (!$this->slug) {
                $this->slug = StringHelper::url($this->title);
            }
        }
        $this->festival_id = self::festID();
        return parent::beforeValidate();
    }

    public function afterSave($insert, $changedAttributes)
    {

        if ($this->hasAsyncTempFile('upload_logo')) {
            $logo = $this->setFile('logo', $this->saveAsyncFile('upload_logo'), true);
        }

        parent::afterSave($insert, $changedAttributes);
        return true;

    }

    public static function find()
    {
        $query = new LeadingQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query->setLanguage()->isNoDeleted(self::tableName())
                ->andWhere(['festival_id' => self::festID()]);
        }

        return $query->isActive(self::tableName())->isNoDeleted(self::tableName())->setLanguage()
            ->andWhere(['festival_id' => self::festID()]);
    }

    public static function findLeading($limit = null)
    {
        $query = self::find()
        ->withFilesLogo(self::tableName(), 'leading_id')
        ->addSelect([
            'leading.id',
            'leading.title',
            'leading.short_text',
            'leading.link',
            'leading.slug',
        ]);

        if($limit) {
            $query->limit($limit);
        }

        return $query->asArray()->all();
    }
}
