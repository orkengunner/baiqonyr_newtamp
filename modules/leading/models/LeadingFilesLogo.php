<?php

namespace app\modules\leading\models;

use Yii;

/**
 * This is the model class for table "leading_files_logo".
 *
 * @property string $leading_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Leading $leading
 */
class LeadingFilesLogo extends \yii\db\ActiveRecord
{
    public static $keyField = 'leading_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leading_files_logo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leading_id', 'subdir', 'basename'], 'required'],
            [['leading_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255],
            [['leading_id'], 'exist', 'skipOnError' => true, 'targetClass' => Leading::className(), 'targetAttribute' => ['leading_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'leading_id' => 'Leading ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeading()
    {
        return $this->hasOne(Leading::className(), ['id' => 'leading_id']);
    }
}
