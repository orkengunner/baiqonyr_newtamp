<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\sponsors\models\Sponsors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="leading-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-9">
            <div class="widget">
                <div class="widget-content padding">

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'short_text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <?= \app\components\widgets\PublishData::widget([
                'model' => $model,
                'form' => $form,
                'exclude' => ['publish_date', 'slug'],
            ])?>

            <div class="widget">
                <div class="widget-header">
                    <h2>Миниатюра записи</h2>
                </div>
                <div class="widget-content padding gallery-wrap">
                    <?php
                    //$sizes = $model->getParam('logo.sizes');
                    echo \app\components\widgets\ImageUploadAsync::widget([
                        'model' => $model,
                        'attribute' => 'upload_logo',
                        'form' => $form,
                        //'hint' => 'Рекомендуемый размер в пропорции 1,5',
                        'croppable' => false,
                        'previewPath' => $model->getFilePath('logo', false),
                    ]); ?>
                </div>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>


</div>
<script src="/js/ckeditor/ckeditor.js?<?= time() ?>"></script>
<script>
    var EDITOR_GALLERIES = <?=\yii\helpers\Json::encode(\app\modules\gallery\models\Gallery::getDropdownList())?>;

    CKEDITOR.replace('leading-text', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('leading-short_text', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName === 'table') {
            var infoTab = dialogDefinition.getContents('info');
            var border = infoTab.get('txtBorder');
            border['default'] = "0";
        }
    });
</script>
