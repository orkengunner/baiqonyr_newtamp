<?php

namespace app\modules\gallery\models;

use app\components\ActiveRecord;
use app\components\traits\UploadableAsync;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "gallery_files_image".
 *
 * @property string $id
 * @property string $gallery_id
 * @property string $title
 * @property string $description
 * @property string $subdir
 * @property string $basename
 * @property string $create_date
 * @property string $update_date
 * @property integer $position
 *
 * @property Gallery $gallery
 */
class GalleryFilesImage extends ActiveRecord
{
    use UploadableAsync;

    public static $keyField = 'gallery_id';

    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_files_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery_id', 'position'], 'required'],
            [['gallery_id', 'position'], 'integer'],
            [['description'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['title', 'subdir', 'basename'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('gallery', 'ID'),
            'gallery_id' => Yii::t('gallery', 'Галерея'),
            'title' => Yii::t('gallery', 'Заголовок'),
            'description' => Yii::t('gallery', 'Описание'),
            'position' => Yii::t('gallery', 'Позиция'),
            'is_active' => Yii::t('gallery', 'Активность'),
            'create_date' => Yii::t('gallery', 'Дата создания'),
            'update_date' => Yii::t('gallery', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior,
        ];
    }

    public function getImage()
    {
        $class = $this->className() . 'FilesImage';
        return $this->hasOne($class, ['gallery_id' => 'id']);
    }

    public function getImageUrl($width, $height)
    {
        if (!file_exists(Yii::getAlias('@media') . '/gallery_images/' . $this->filename)) {
            return Url::to(['/pages/default/no-image', 'wh' => $width . 'x' . $height]);
        } else {
            return '/media/gallery_images/' . $this->filename;
        }
    }

}
