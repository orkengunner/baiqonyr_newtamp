<?php

namespace app\modules\gallery\models;

use app\components\ActiveRecord;
use app\components\traits\UploadableAsync;
use Yii;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "gallery".
 *
 * @property string $id
 * @property string $title
 * @property string $user_id
 * @property integer $status_id
 * @property string $create_date
 * @property string $update_date
 * @property string $date_delete
 * @property string $delete_by
 *
 * @property Users $user
 * @property Users $deleteBy
 * @property GalleryFilesImage[] $galleryFilesImage
 * @property GalleryObjects[] $galleryObjects
 */
class Gallery extends ActiveRecord
{
    use UploadableAsync;

    const TYPE_JOB_PHOTO = 1;

    public $upload_image = null;

    public $file_attributes = [
        'image'
    ];

    const CONTENT_PLACEHOLDER='__gallery__';

    public $uploadedImages = array();

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['user_id', 'status_id', 'delete_by'], 'integer'],
            [['create_date', 'update_date', 'date_delete'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [
                ['upload_image'],
                'image',
                'on' => 'upload'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'user_id' => 'User ID',
            'status_id' => 'Status ID',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'date_delete' => 'Date Delete',
            'delete_by' => 'Delete By',
        ];
    }

    public function getImages()
    {
        return $this->image;
    }

    public function getImage()
    {
        return $this->hasMany(GalleryFilesImage::className(), ['gallery_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeleteBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'delete_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getGalleryFilesImage()
    {
        return $this->hasMany(GalleryFilesImage::className(), ['gallery_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryObjects()
    {
        return $this->hasMany(GalleryObjects::className(), ['gallery_id' => 'id']);
    }



    public function behaviors()
    {
        return [
            $this->timestampBehavior,
            $this->userIdBehavior
        ];
    }

    public function getImagesCount()
    {
        return $this->hasMany(GalleryFilesImage::className(), ['gallery_id' => 'id'])->count();
    }

    public function afterSave($insert, $changedAttributes)
    {
        //if ($this->hasAsyncTempFile('upload_image')) {
            $this->setFile('image', $this->saveAsyncFile('upload_image', null), true, true, \Yii::$app->request->post('GalleryFilesImage'));
        //}

        parent::afterSave($insert, $changedAttributes);

        return true;

    }

    public static function parseShortCode($text, $template = '@app/modules/gallery/front/views/default/_gallery')
    {
        preg_match_all('/<div class="gallery-placeholder __gallery__(.*?)__">gallery<\/div>/', $text, $match);
        if($match) {
            $files = GalleryFilesImage::find()
                ->andWhere([
                    'gallery_id' => $match[1]
                ])
                ->orderBy(['position' => SORT_ASC])
                ->asArray()
                ->all();
            $_files = [];
            foreach ($files as $v) {
                $_files[$v['gallery_id']][] = $v;
            }

            foreach ($match[0] as $k => $v) {

                $text = str_replace(
                    $v,
                    \Yii::$app->getView()->render($template, [
                        'data' => $_files[$match[1][$k]]
                    ]),
                    $text
                );
            }

        }
        return $text;
    }

    public static function setGalleryObject($gallery_id, $object_id, $type_id)
    {
        $gal = new GalleryObjects();
        $gal->object_id = $object_id;
        $gal->gallery_id = $gallery_id;
        $gal->type_id = $type_id;
        $gal->save();
    }
}
