<?php

namespace app\modules\gallery\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "gallery_objects".
 *
 * @property string $gallery_id
 * @property string $object_id
 * @property integer $type_id
 *
 * @property Gallery $gallery
 */
class GalleryObjects extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_objects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery_id', 'object_id', 'type_id'], 'required'],
            [['gallery_id', 'object_id', 'type_id'], 'integer'],
            [['object_id', 'type_id'], 'unique', 'targetAttribute' => ['object_id', 'type_id'], 'message' => 'The combination of Object ID and Type ID has already been taken.'],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gallery_id' => 'Gallery ID',
            'object_id' => 'Object ID',
            'type_id' => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }
}
