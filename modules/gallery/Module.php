<?php

namespace app\modules\gallery;

use app\components\rocket\RFModule;

class Module extends RFModule
{

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
