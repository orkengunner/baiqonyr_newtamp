<?php if(!isset($index))$index = md5(microtime()); ?>
<div class="col-md-3 gallery-image">
    <div class="thumbnail">
        <img  src="<?= $src; ?>"/>

        <div class="caption">

            <?= \yii\helpers\Html::activeHiddenInput($model, "[$index]key", ['id' => null, 'class' => 'image-id-field', 'value' => $key]) ?>
            <?= \yii\helpers\Html::activeHiddenInput($model, "[$index]id", ['id' => null, 'class' => 'image-id-field']) ?>
            <?= \yii\helpers\Html::activeTextInput($model, "[$index]title", array('placeholder' => 'Заголовок', 'class' => 'form-control image-title-field', 'id' => null)) ?>
            <?= \yii\helpers\Html::activeTextArea($model, "[$index]description", array('placeholder' => 'комментарий', 'class' => 'form-control image-comment-field', 'id' => null)) ?>
            <div style="float: left">

            </div>
            <br/>
            <a class="delete-image btn btn-danger form-control remove-img"
               data-url="admin.php/gallery/remove-file?model_id=<?= $model_id; ?>&recAttribute=<?= $recAttribute; ?>&attribute=<?=$attribute; ?>&id=<?= $model->id ? $model->id : $key; ?>&is_temp=<?= $model->id ? 0 : 1; ?>"
               data-container=".gallery-image"
               title="Удалить изображение"
               href="javascript:;">
                Удалить
            </a>
        </div>
    </div>
</div>