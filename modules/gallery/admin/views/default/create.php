<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\gallery\models\Gallery */
/* @var $imagesModel app\modules\gallery\models\GalleryImages */

$this->title = Yii::t('gallery', 'Создание галереи');
$this->params['breadcrumbs'][] = ['label' => Yii::t('gallery', 'Галереи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-create">

    <div class="page-heading">
        <h1><i class="icon-th-thumb"></i><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'js' => $js
    ]) ?>

</div>
