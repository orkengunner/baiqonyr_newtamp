<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\gallery\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('gallery', 'Галереи');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">
    <div class="page-heading">
        <h1><i class="icon-th-thumb"></i><?= Html::encode($this->title) ?></h1>
    </div>
    <p>
        <?= Html::a('<i class="icon-list-add"></i> Создать галерею', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="widget">
        <div class="widget-content">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'title',
                    /*[
                        'class' => \app\components\admin\RFAToggleColumn::className(),
                        'attribute' => 'is_active',
                    ],*/
                    [
                        'value' => function ($data) {
                            return $data->imagesCount;
                        },
                        'header' => 'Количество изображений'
                    ],
                    ['class' => \app\components\admin\RFAActionColumn::className()],
                ],
            ]); ?>
        </div>
    </div>
</div>
