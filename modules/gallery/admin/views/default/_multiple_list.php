<?php
    $uploads = new \app\components\wFileUpload($model, 'upload_image', 'image');
    echo $uploads->multipleList(['encode' => false], [
    'img' => [],
    'remove' => [
        'tag' => 'button',
        'content' => '<span class="font-icons">&#206;</span>',
        'options' => ['class' => 'btn cancel', 'type' => 'button']
    ]
]); ?>