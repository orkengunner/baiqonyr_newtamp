<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$uploads = new \app\components\wFileUpload($model, 'upload_image', 'image');
$this->registerJs('
var uploads = new wUploads();
uploads.init({
container: ".upload-file",
data_url: "' . $uploads->getDataUrl(null, 0) . '", multiple:true,
template: "#itemTemplate"
});

');
/* @var $this yii\web\View */
/* @var $model app\modules\gallery\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if ($js): ?>
    <script>
        window.opener.setGallery(<?= $model->id; ?>);
        window.close();
    </script>
<?php exit; endif; ?>
<style>
    .gallery-form .thumbnails .thumbnail {
        height: 360px;
        text-align: center;
        margin-left: 0;
        margin-right: 20px;
        position: relative;
        background: #eee;
    }

    .gallery-form .thumbnails .thumbnail img {
        max-height: 150px;
    }

    .gallery-form .thumbnails .thumbnail .caption {
        width: 99%;
        position: absolute;
        bottom: 0;
    }

    .gallery-form .thumbnail input {
        margin: 5px 0;
    }

    .gallery-form .thumbnails a.delete-image {
        opacity: 0.5;
        text-align: center;
    }

    .gallery-form .thumbnails a.delete-image:hover {
        opacity: 1;
    }
    .dropzone {
        background-color: #cccccc;
        border: 1px dotted;
        margin: 10px 19px 10px 0;
        padding: 10px;
        text-align: center;
    }
</style>

<span id="itemTemplate" style="display: none;">
    <?= $this->render('@app/modules/gallery/admin/views/default/_gallery_template', [
        'model' => new \app\modules\gallery\models\GalleryFilesImage(),
        'key' => '{{key}}',
        'src' => '{{src}}',
        'attribute' => 'upload_image',
        'recAttribute' => 'image',
        'model_id' => $model->id,
        'index' => '{{index}}'
    ]); ?>
</span>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(['id' => 'gallery-form']); ?>
    <div class="widget">
        <div class="widget-content padding">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

            <div class="row upload-file">
                <div class="col-md-12">
                    <h4>Изображения галереи:</h4>
                    <div class="dropzone" id="dropzone">
                        Перетащите сюда изображение
                    </div>

                    <?= $uploads->multipleListTemplate('@app/modules/gallery/admin/views/default/_gallery_template', [
                        'model' => new \app\modules\gallery\models\GalleryFilesImage(),
                        'containerClass' => 'row thumbnails'
                    ]); ?>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-info">
                                <em>
                                    <!--Минимальный размер изображения - 690x300 пикселей.-->
                                    Изображения можно сортировать, перетаскивая мышкой.
                                    <!--После сохранения можно просмотреть, как будет выглядеть галерея в посте.-->
                                </em>
                            </p>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php

                        echo $uploads->fileInput(true);

                        echo $uploads->error();
                        echo \yii\helpers\Html::error($model, 'upload_file', ['class' => 'error-block']);

                        ?>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="widget">
        <div class="widget-content padding">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('categories', 'Создать') : Yii::t('categories',
                'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>


</script>