<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\gallery\models\Gallery */

$this->title = Yii::t('gallery', 'Изменение галереи:') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('gallery', 'Galleries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('gallery', 'Update');
?>
<div class="gallery-update">


    <div class="page-heading">
        <h1><i class="icon-th-thumb"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'js' => $js
    ]) ?>

</div>
