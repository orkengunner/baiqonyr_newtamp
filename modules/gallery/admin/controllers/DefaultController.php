<?php

namespace app\modules\gallery\admin\controllers;

use app\components\AdminController;
use app\modules\gallery\models\GalleryImages;
use Yii;
use app\modules\gallery\models\Gallery;
use app\modules\gallery\models\GallerySearch;
use app\components\RFAController;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * DefaultController implements the CRUD actions for Gallery model.
 */
class DefaultController extends AdminController
{
    protected $_modelName = 'app\modules\gallery\models\Gallery';
    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();
        if (\Yii::$app->request->get('title'))
            $model->title = \Yii::$app->request->get('title');
        $js = false;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (\Yii::$app->request->get('model_id') == null)
                return $this->redirect(['index']);
            else $js = true;
        }
        return $this->render('create', [
            'model' => $model,
            'js' => $js
        ]);
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $js = false;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if (\Yii::$app->request->get('model_id') == null)
                return $this->redirect(['index']);
            else $js = true;
        }
        return $this->render('update', [
            'model' => $model,
            'js' => $js
        ]);

    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Загружает изображения через AJAX
     */
    public function actionAddImages()
    {
        $files = UploadedFile::getInstancesByName('files');
        $result = Gallery::uploadImages($files);
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $result;
    }

    /**
     * Удаление загруженного изображение. Когда еще не сохранена галерея. Удаляет только из сессии и сам файл
     * @throws \Exception
     */
    public function actionRemoveImage()
    {
        $id = \Yii::$app->getRequest()->post('id');
        if (is_numeric($id)) {
            $row = GalleryImages::findOne($id);
            if ($row !== null)
                $row->delete();
        } else {
            $model = new GalleryImages;
            $data = Yii::$app->session->get($model->tableName());
            if (!empty($data['files']['imageFile'])) {
                $data = $data['files']['imageFile'];
            } else {
                return false;
            }

            $index = array_search($id, $data);
            if ($index >= 0) {
                $model->removeAsyncTempFile('imageFile', true, $index);
            }
        }
    }

    /**
     * Сортирует изображения
     */
    public function actionSortImage()
    {
        $model = new GalleryImages();
        if (isset($_POST['image']) && is_array($_POST['image'])) {
            $i = 0;
            foreach ($_POST['image'] as $id) {
                if ($id == 0 || strpos($id, "0") === 0)
                    continue;
                $model->updateAll(array('position' => $i), ['id' => $id]);
                $i++;
            }
        }
    }
}
