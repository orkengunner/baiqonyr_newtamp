<div class="gallery">
    <?php foreach($data as $v): ?>
        <a href="<?= \yii\helpers\Url::imgUrl($v['subdir'] . '/' . $v['basename'], 'gallery'); ?>" rel="group<?= $v['gallery_id']?>">
            <img src="<?= \yii\helpers\Url::imgUrl($v['subdir'] . '/' . $v['basename'], 'gallery', [135, 102]); ?>" />
        </a>
    <?php endforeach;?>
</div>