<?php

namespace app\modules\comments\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\comments\models\Comments;

/**
 * CommentsSearch represents the model behind the search form about `app\modules\comments\models\Comments`.
 */
class CommentsSearch extends Comments
{
    public $fullName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['commentID', 'typeID', 'parentID', 'isApprove', 'seasonID', 'seriesID', 'statusID'], 'integer'],
            [['fullName'], 'string'],
            [['comment', 'created', 'fullName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
        ->innerJoin('users AS u', 'u.id = comments.userID')
            ->addSelect([
                'CONCAT_WS(" ", u.firstName, u.lastName) AS fullName',
                'comments.commentID',
                'comments.created',
                'comments.typeID',
                'comments.comment',
                'comments.userID',
                'comments.objectID',
                'comments.statusID',
            ])
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                  'created',
                  'fullName',
                  'statusID',
                ],
                'defaultOrder' => ['created' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'commentID' => $this->commentID,
            'objectID' => $this->objectID,
            'typeID' => $this->typeID,
            'parentID' => $this->parentID,
            'userID' => $this->userID,
            'isApprove' => $this->isApprove,
            'seasonID' => $this->seasonID,
            'seriesID' => $this->seriesID,
            'comments.statusID' => $this->statusID,
        ]);

        $query->andFilterWhere([
            'OR',
            ['like', 'u.username' , $this->fullName],
            ['like', 'u.firstName' , $this->fullName],
            ['like', 'u.lastName' , $this->fullName]
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);
        $query->andFilterWhere(['like', 'created', $this->created]);

        return $dataProvider;
    }
}
