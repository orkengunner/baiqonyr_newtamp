<?php

namespace app\modules\comments\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "comments_viewed".
 *
 * @property string $viewed_id
 * @property string $comment_id
 * @property integer $is_viewed
 * @property string $user_id
 *
 * @property Comments $comment
 * @property Users $user
 */
class CommentsViewed extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments_viewed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment_id', 'user_id'], 'required'],
            [['comment_id', 'is_viewed', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'viewed_id' => 'Viewed ID',
            'comment_id' => 'Comment ID',
            'is_viewed' => 'Is Viewed',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comments::className(), ['id' => 'comment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
