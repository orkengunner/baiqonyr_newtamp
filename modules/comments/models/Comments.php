<?php

namespace app\modules\comments\models;

use app\components\ActiveRecord;
use app\components\FrontController;
use app\modules\languages\models\Languages;
use app\modules\statuses\models\Statuses;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "comments".
 *
 * @property string $commentID
 * @property string $objectID
 * @property integer $typeID
 * @property string $comment
 * @property string $parentID
 * @property string $userID
 * @property string $created
 * @property integer $isApprove
 * @property integer $seasinID
 * @property integer $seriesID
 * @property integer $statusID
 */
class Comments extends ActiveRecord
{
    const COMMENT_TYPE_FILM = 1;
    const COMMENT_TYPE_NEWS = 2;
    const COMMENT_TYPE_PAGES = 3;
    const COMMENT_TYPE_PERSONS = 4;
    const COMMENT_TYPE_PLACES = 5;
    const COMMENT_TYPE_POSTERS = 6;
    const COMMENT_TYPE_CLIPS = 7;
    const COMMENT_TYPE_BLOGS = 8;
    const COMMENT_TYPE_ADS = 9;
    const COMMENT_TYPE_ONLINE = 10;
    const COMMENT_TYPE_ARTICLES = 11;
    const COMMENT_TYPE_BKR_JOBS = 12;

    const STATUS_VIEWED = 1;

    public static $types = [
        self::COMMENT_TYPE_FILM => 'Фильмы',
        self::COMMENT_TYPE_NEWS => 'Новости',
        self::COMMENT_TYPE_PAGES => 'Страницы',
        self::COMMENT_TYPE_PERSONS => 'Персоны',
        self::COMMENT_TYPE_POSTERS => 'Афиша',
        self::COMMENT_TYPE_BLOGS => 'Колонки',
        self::COMMENT_TYPE_ADS => 'Кастинги',
        self::COMMENT_TYPE_ARTICLES => 'Статьи',
        self::COMMENT_TYPE_BKR_JOBS => 'Работы ',
    ];


    protected $commentsArray = array();
    protected $commentsSortArray = array();

    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public static function getDb(){
        return \Yii::$app->dbBrod;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['objectID', 'typeID', 'comment', 'userID'], 'required'],
            [['objectID', 'typeID', 'parentID', 'userID', 'isApprove', 'seasonID', 'seriesID', 'statusID'], 'integer'],
            [['comment'], 'string'],
            [['created'], 'safe']
        ];
    }

    public static function find()
    {
        $query = new CommentsQuery(get_called_class());
        return $query;
    }

    public static function getCommentsByUserID($userID, $isQuery = false)
    {
        $comments = Comments::find()
            ->addSelect([
                'comments.userID',
                'comments.comment',
                'comments.created',
                'comments.objectID',
                'comments.typeID',
                'comments.seriesID',
                'comments.seasonID',

                'CONCAT_WS(",", f.filmID, p.personID, pl.placeID, ps.posterID, pg.pageID, n.newsID, ads.advID) AS concatID',

                'f.title AS filmTitle',
                'concat_ws(" ", p.firstName, p.lastName) as personTitle',
                'pl.title AS placeTitle',
                'ps.title AS posterTitle',
                'pg.title AS pageTitle',
                'n.title AS newsTitle',
                'ads.title AS adsTitle',

                'f.titleLink AS filmTitleLink',
                'concat_ws(" ", p.firstName, p.lastName) as personTitleLink',
                'pl.titleLink AS placeTitleLink',
                'ps.titleLink AS posterTitleLink',
                'pg.titleLink AS pageTitleLink',
                'n.titleLink AS newsTitleLink',
                'ads.titleLink AS adsTitleLink',

                'f.url AS filmUrl',
                'p.url AS personUrl',
                'pl.url AS placeUrl',
                'ps.url AS posterUrl',
                'pg.url AS pageUrl',
                'n.url As newsUrl',
                'ads.url AS adsUrl'
            ])
            ->leftJoin('films AS f', 'f.filmID = comments.objectID AND comments.typeID = ' . self::COMMENT_TYPE_FILM )
            ->leftJoin('persons AS p', 'p.personID = comments.objectID AND comments.typeID = ' . self::COMMENT_TYPE_PERSONS )
            ->leftJoin('places AS pl', 'pl.placeID = comments.objectID AND comments.typeID = ' . self::COMMENT_TYPE_PLACES )
            ->leftJoin('posters AS ps', 'ps.posterID = comments.objectID AND comments.typeID = ' . self::COMMENT_TYPE_POSTERS )
            ->leftJoin('pages AS pg', 'pg.pageID = comments.objectID AND comments.typeID = ' . self::COMMENT_TYPE_PAGES )
            ->leftJoin('news AS n', 'n.newsID = comments.objectID AND comments.typeID = ' . self::COMMENT_TYPE_NEWS )
            ->leftJoin('advertizing AS ads', 'ads.advID = comments.objectID AND comments.typeID = ' . self::COMMENT_TYPE_ADS . ' AND ads.statusID = 1' )
            ->andWhere([
                'comments.userID' => $userID
            ])->asArray()->orderBy([
                'comments.created' => SORT_DESC
            ])->having([
                '<>', 'concatID', ''
            ]);

        if($isQuery) return $comments;
        else return $comments->all();


    }

    public function getLastComments($limit = 4)
    {
        return $this->query('
            SELECT
              c.userID, c.typeID, c.comment, c.created, IF((u.firstName IS NOT NULL AND u.firstName <> "") || (u.lastName IS NOT NULL AND u.lastName <> ""), TRIM(CONCAT_WS(" ", u.firstName, u.lastName)), u.username) AS userFullName, u.username, c.objectID, c.typeID, c.userID, b.userID AS bUserID, c.seriesID, c.seasonID,

              f.title AS filmTitle,
              concat_ws(" ", p.firstName, p.lastName) as personTitle,
              pl.title AS placeTitle,
              ps.title AS posterTitle,
              pg.title AS pageTitle,
              n.title AS newsTitle,
              b.title AS blogTitle,
              o.title AS onlineTitle,
              ads.title AS adsTitle,

              f.titleLink AS filmTitleLink,
              concat_ws(" ", p.firstName, p.lastName) as personTitleLink,
              pl.titleLink AS placeTitleLink,
              ps.titleLink AS posterTitleLink,
              pg.titleLink AS pageTitleLink,
              n.titleLink AS newsTitleLink,
              b.titleLink AS blogTitleLink,
              o.title AS onlineTitleLink,
              ads.titleLink AS adsTitleLink,

              f.url AS filmUrl,
              p.url AS personUrl,
              pl.url AS placeUrl,
              ps.url AS posterUrl,
              pg.url AS pageUrl,
              n.url As newsUrl,
              b.url AS blogUrl,
              ofi.url AS onlineUrl,
              ads.url AS adsUrl,

              CONCAT_WS("", f.langID, p.langID, pl.langID, ps.langID, pg.langID, n.langID, b.langID, ofi.langID, ads.langID) AS lang


            FROM comments AS c
            INNER JOIN users AS u ON c.userID = u.id
            LEFT JOIN films AS f ON f.filmID = c.objectID AND c.typeID = ' . self::COMMENT_TYPE_FILM . '
            LEFT JOIN persons AS p ON p.personID = c.objectID AND c.typeID = ' . self::COMMENT_TYPE_PERSONS . '
            LEFT JOIN places AS pl ON pl.placeID = c.objectID AND c.typeID = ' . self::COMMENT_TYPE_PLACES . '
            LEFT JOIN posters AS ps ON ps.posterID = c.objectID AND c.typeID = ' . self::COMMENT_TYPE_POSTERS . '
            LEFT JOIN pages AS pg ON pg.pageID = c.objectID AND c.typeID = ' . self::COMMENT_TYPE_PAGES . '
            LEFT JOIN news AS n ON n.newsID = c.objectID AND c.typeID = ' . self::COMMENT_TYPE_NEWS . '
            LEFT JOIN online AS o ON o.onlineID = c.objectID AND c.typeID = ' . self::COMMENT_TYPE_ONLINE . '
            LEFT JOIN films AS ofi ON ofi.filmID = o.filmID
            LEFT JOIN blogs AS b ON b.blogID = c.objectID AND c.typeID = ' . self::COMMENT_TYPE_BLOGS . ' AND b.statusID = 1
            LEFT JOIN advertizing AS ads ON ads.advID = c.objectID AND c.typeID = ' . self::COMMENT_TYPE_ADS . ' AND ads.statusID = 1
          WHERE
              (c.typeID = ' . self::COMMENT_TYPE_FILM . ')
            OR
              (c.typeID = ' . self::COMMENT_TYPE_PERSONS . ')
            OR
              (c.typeID = ' . self::COMMENT_TYPE_PLACES . ')
            OR
              (c.typeID = ' . self::COMMENT_TYPE_POSTERS . ')
            OR
              (c.typeID = ' . self::COMMENT_TYPE_PAGES . ')
            OR
              (c.typeID = ' . self::COMMENT_TYPE_NEWS . ')
            OR
              (c.typeID = ' . self::COMMENT_TYPE_ONLINE . ')
            OR
              (c.typeID = ' . self::COMMENT_TYPE_BLOGS . ' AND b.userID IS NOT NULL)
            OR
              (c.typeID = ' . self::COMMENT_TYPE_ADS . ')
              HAVING lang = ' . Languages::getCurrent()->id . '
            ORDER BY c.created DESC
            LIMIT ' . $limit
        )->queryAll();
    }

    public function getCountCommentsByObjectID($objectID, $typeID)
    {
        switch ($typeID)
        {
            case self::COMMENT_TYPE_FILM:
                $join = 'INNER JOIN films AS f ON c.objectID = ' . $objectID . ' AND c.typeID = ' . self::COMMENT_TYPE_FILM;
                break;
            case self::COMMENT_TYPE_PERSONS:
                $join = 'INNER JOIN persons AS p ON c.objectID = ' . $objectID . ' AND c.typeID = ' . self::COMMENT_TYPE_PERSONS;
                break;
            case self::COMMENT_TYPE_PLACES:
                $join = 'INNER JOIN places AS pl ON c.objectID = ' . $objectID . ' AND c.typeID = ' . self::COMMENT_TYPE_PLACES;
                break;
            case self::COMMENT_TYPE_POSTERS:
                $join = 'INNER JOIN posters AS ps ON c.objectID = ' . $objectID . ' AND c.typeID = ' . self::COMMENT_TYPE_POSTERS;
                break;
            case self::COMMENT_TYPE_PAGES:
                $join = 'INNER JOIN pages AS pg ON c.objectID = ' . $objectID . ' AND c.typeID = ' . self::COMMENT_TYPE_PAGES;
                break;
            case self::COMMENT_TYPE_NEWS:
                $join = 'INNER JOIN news AS n ON c.objectID = ' . $objectID . ' AND c.typeID = ' . self::COMMENT_TYPE_NEWS;
                break;
            case self::COMMENT_TYPE_ONLINE:
                $join = 'INNER JOIN online AS o ON c.objectID = ' . $objectID . ' AND c.typeID = ' . self::COMMENT_TYPE_ONLINE;
                break;
            case self::COMMENT_TYPE_BLOGS:
                $join = 'INNER JOIN blogs AS b ON c.objectID = ' . $objectID . ' AND c.typeID = ' . self::COMMENT_TYPE_BLOGS . ' AND b.statusID = 1';
                break;
            case self::COMMENT_TYPE_ADS:
                $join = 'INNER JOIN advertizing AS ads ON c.objectID = ' . $objectID . ' AND c.typeID = ' . self::COMMENT_TYPE_ADS . ' AND ads.statusID = 1';
                break;
        }

        $sql = '
	      SELECT COUNT(*) FROM (SELECT commentID FROM comments AS c ' . $join . ' GROUP BY c.commentID) AS t';
        #print_r($sql);
        return $this->query($sql)->queryScalar();
    }
}
