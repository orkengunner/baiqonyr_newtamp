<?php

namespace app\modules\comments;

use app\components\rocket\RFModule;

class Module extends RFModule
{

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
