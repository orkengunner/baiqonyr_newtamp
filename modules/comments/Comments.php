<?php

namespace app\modules\comments;

use app\modules\users\models\Users;

class Comments extends \app\components\Module
{
    public $permission = Users::PERMISSION_COMMENTS;
    public function init()
    {
        parent::init();
    }
}
