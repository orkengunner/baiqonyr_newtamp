<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\comments\models\CommentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Комнтарии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--p>
        <?= Html::a('Create Comments', ['create'], ['class' => 'btn btn-success']) ?>
    </p-->

    <div class="widget">
        <div class="widget-content">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
    <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'created',
            [
                'attribute' => 'fullName',
                'value' => function($data){
                    return '<a target="_blank" href="' . \yii\helpers\Url::to(['/users/update', 'id' => $data->userID]) . '">' . $data->fullName. '</a>';
                },
                'format' => 'raw'
            ],
            'comment:ntext',
            [
                'attribute' => 'objectID',
                'format' => 'raw',
                'value' => function ($data) {
                    switch($data->typeID) {
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_ADS:
                            $q = \app\modules\advertizing\models\Advertizing::findOne($data->objectID);
                            if($q) return '<a target="_blank" href="' . \yii\helpers\Url::to('/ads/' . $q->url . '/') . '">' . $q->title . '</a>';
                            break;

                        case \app\modules\comments\models\Comments::COMMENT_TYPE_ARTICLES:
                            $q = \app\modules\news\models\News::findOne($data->objectID);
                            if($q) return '<a target="_blank" href="' . \yii\helpers\Url::to('/articles/' . $q->url . '/') . '">' . $q->title . '</a>';
                            break;

                        case \app\modules\comments\models\Comments::COMMENT_TYPE_BLOGS:
                            $q = \app\modules\blogs\models\Blogs::findOne($data->objectID);
                            if($q) return '<a target="_blank" href="' . \yii\helpers\Url::to('/blogs/' . $q->url . '/') . '">' . $q->title . '</a>';
                            break;

                        case \app\modules\comments\models\Comments::COMMENT_TYPE_FILM:
                            $q = \app\modules\films\models\Films::findOne($data->objectID);
                            if($q) return '<a target="_blank" href="' . \yii\helpers\Url::to('/films/' . $q->url . '/') . '">' . $q->title . '</a>';
                            break;

                        case \app\modules\comments\models\Comments::COMMENT_TYPE_NEWS:
                            $q = \app\modules\news\models\News::findOne($data->objectID);
                            if($q) return '<a target="_blank" href="' . \yii\helpers\Url::to('/news/' . $q->url . '/') . '">' . $q->title . '</a>';
                            break;

                        case \app\modules\comments\models\Comments::COMMENT_TYPE_PAGES:
                            $q = \app\modules\pages\models\Pages::findOne($data->objectID);
                            if($q) return '<a target="_blank" href="' . \yii\helpers\Url::to('/pages/' . $q->url . '/') . '">' . $q->title . '</a>';
                            break;

                        case \app\modules\comments\models\Comments::COMMENT_TYPE_PERSONS:
                            $q = \app\modules\persons\models\Persons::findOne($data->objectID);
                            if($q) return '<a target="_blank" href="' . \yii\helpers\Url::to('/persons/' . $q->url . '/') . '">' . $q->firstName . ' ' . $q->lastName . '</a>';
                            break;

                        case \app\modules\comments\models\Comments::COMMENT_TYPE_POSTERS:
                            $q = \app\modules\posters\models\Posters::findOne($data->objectID);
                            if($q) return '<a target="_blank" href="' . \yii\helpers\Url::to('/posters/' . $q->url . '/') . '">' . $q->title . '</a>';
                            break;

                        case \app\modules\comments\models\Comments::COMMENT_TYPE_PLACES:
                            $q = \app\modules\places\models\Places::findOne($data->objectID);
                            if($q) return '<a target="_blank" href="' . \yii\helpers\Url::to('/places/' . $q->url . '/') . '">' . $q->title . '</a>';
                            break;
                    }

                },
                //'filter' => \app\modules\statuses\models\Statuses::$statuses_2
            ],
            [
                'attribute' => 'typeID',
                'value' => function ($data) {
                    return isset(\app\modules\comments\models\Comments::$types[$data->typeID]) ? \app\modules\comments\models\Comments::$types[$data->typeID] : null;
                },
                'filter' => \app\modules\comments\models\Comments::$types
            ],

            [
                'class' => \app\components\admin\RFAToggleColumn::className(),
                'attribute' => 'statusID',
                'filter' => [
                    1 => 'Прочитан',
                    2 => 'Непрочитан'
                ]
            ],

            [
                'class' => \app\components\admin\RFAActionColumn::className(),
                'template' => '{delete}'
            ],
        ],
    ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
