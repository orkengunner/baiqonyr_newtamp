<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\comments\models\CommentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'commentID') ?>

    <?= $form->field($model, 'objectID') ?>

    <?= $form->field($model, 'typeID') ?>

    <?= $form->field($model, 'comment') ?>

    <?= $form->field($model, 'parentID') ?>

    <?php // echo $form->field($model, 'userID') ?>

    <?php // echo $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'isApprove') ?>

    <?php // echo $form->field($model, 'seasonID') ?>

    <?php // echo $form->field($model, 'seriesID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
