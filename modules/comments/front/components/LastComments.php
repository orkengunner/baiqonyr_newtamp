<?php
/**
 * Created by PhpStorm.
 * User: vital
 */

namespace app\modules\comments\front\components;

use app\modules\images\models\Images;

use yii\base\Widget;


class LastComments extends Widget
{


    public function run()
    {
        $comments = new \app\modules\comments\models\Comments();
        $_comments = $comments->getLastComments(4);

        return $this->render('lastComments',[
            'comments' => $_comments
        ]);
    }

}
