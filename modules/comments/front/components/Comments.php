<?php
/**
 * Created by PhpStorm.
 * User: vital
 */

namespace app\modules\comments\front\components;

use app\components\ActiveRecord;
use app\modules\advertizing\models\Advertizing;
use app\modules\blogs\models\Blogs;
use app\modules\films\models\Films;
use app\modules\images\models\Images;
use app\modules\news\models\News;
use app\modules\online\models\Online;
use app\modules\pages\models\Pages;
use app\modules\persons\models\Persons;
use app\modules\places\models\Places;
use app\modules\posters\models\Posters;
use app\modules\statuses\models\Statuses;
use app\modules\users\models\Users;
use yii\base\Widget;
use yii\db\Expression;
use yii\helpers\Url;

class Comments extends Widget
{
    public $objectID;
    public $objectTypeID;
    public $getCount = false;

    protected $commentsArray = [];
    protected $commentsSortArray = [];

    protected $drawComments = '';
    protected $countComments = 0;

    public function run($params = [])
    {
        if(isset($params['objectID'])) $this->objectID = $params['objectID'];
        if(isset($params['objectTypeID'])) $this->objectTypeID = $params['objectTypeID'];
        if(isset($params['getCount'])) $this->getCount = $params['getCount'];

        $post = \Yii::$app->request->post('Comments');
        if ($post && !\Yii::$app->user->isGuest) {
            if ($this->setComment()) {
                //return \Yii::$app->response->redirect($_SERVER['REQUEST_URI'] . '#comments');
                header('Location: ' . $_SERVER['REQUEST_URI'] . '#comments');exit;
                //$post['parentID'] = 0;
            }
        }

        $socBox = null;
        //if (\Yii::$app->user->isGuest)
            //$socBox = $this->render('//layouts/front/social_login',[]);

        $comments = $this->getComments();
        $userImage = Images::findOne(['objectID' => \Yii::$app->user->getId(), 'oTypeID' => Images::IMAGE_USER_PROFILE]);
        $commentsForm = $this->render('commentsForm',['model' => new \app\modules\comments\models\Comments(), 'avatar' => $userImage]);
        $commentsList = $this->drawComments($comments, 0, $commentsForm, $post);

        $comments = $this->render('comments',[
            'socBox' => $socBox,
            'commentsForm' => $commentsForm,
            'commentsList' => $commentsList,
            'post' => $post,
            'countComments' => count($comments)
        ]);

        if($this->getCount)
            return [
                'count' => $this->countComments,
                'data' => $comments
            ];
        else
        return $comments;
    }



    function drawComments($comments, $parentID, $commentsForm, $post, $inc = 0)
    {
        $inc++;

        $cnt = '';
        foreach ($comments as $v) {
            array_shift($comments);
            if ($v['parentID'] != $parentID) {
                continue;
            }
            $cnt .= $this->render('commentsList', ['data' => $v, 'commentsForm' => $commentsForm, 'post' => $post, 'draw' => $this->drawComments($comments, $v['commentID'], $commentsForm, $post, $inc), 'inc' => $inc]);
        }
        return $cnt;
    }

    protected function setComment()
    {
        $params = \Yii::$app->request->post('Comments');
        $exist = \app\modules\comments\models\Comments::findOne([
            'objectID' => $this->objectID,
            'typeID' => $this->objectTypeID,
            'comment' => strip_tags($params['comment']),
            'userID' => \Yii::$app->user->getId()
        ]);
        if($exist) {
            return $exist->commentID;
        }

        $model = new \app\modules\comments\models\Comments();
        $model->objectID = $this->objectID;
        $model->typeID = $this->objectTypeID;
        $model->comment = strip_tags($params['comment']);
        $model->parentID = (int)$params['parentID'];
        $model->isApprove = ActiveRecord::STATUS_ACTIVE;
        $model->userID = \Yii::$app->user->getId();

        $model->seasonID = \Yii::$app->request->get('season') != 1 ? \Yii::$app->request->get('season') : null;
        $model->seriesID = \Yii::$app->request->get('series') != 1 ? \Yii::$app->request->get('series') : null;

        if ($model->save())
        {
            /*if($model->parentID) {
                $title = '';
                $user = Users::getUserByCommentsID($model->parentID);
                if($user['id'] != \Yii::$app->user->id) {
                    switch($user['typeID']) {
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_BLOGS:
                            $data = Blogs::findOne($user['objectID']);
                            $url = 'blogs';
                            break;
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_NEWS:
                            $data = News::findOne($user['objectID']);
                            $url = 'news';
                            break;
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_ARTICLES:
                            $data = News::findOne($user['objectID']);
                            $url = 'articles';
                            break;
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_ADS:
                            $data = Advertizing::findOne($user['objectID']);
                            $url = 'ads';
                            break;
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_PLACES:
                            $data = Places::findOne($user['objectID']);
                            $url = 'places';
                            break;
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_FILM:
                            $data = Films::findOne($user['objectID']);
                            $url = 'films';
                            break;
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_PERSONS:
                            $data = Persons::findOne($user['objectID']);
                            $url = 'persons';
                            $title = $data->firstName . ' ' . $data->lastName;
                            break;
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_POSTERS:
                            $data = Posters::findOne($user['objectID']);
                            $url = 'posters';
                            break;
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_PAGES:
                            $data = Pages::findOne($user['objectID']);
                            $url = 'pages';
                            break;
                        case \app\modules\comments\models\Comments::COMMENT_TYPE_ONLINE:
                            $data = Online::findOne($user['objectID']);
                            $url = 'online';
                            break;

                    }

                    $url .= '/' . $data->url;
                    if(!$title) $title = $data->title;

                    Pages::sendEmail(
                        Pages::PAGE_EMAIL_RE_COMMENT,
                        [
                            $user['email'] => ($user['firstName'] || $user['lastName'] ? implode(' ', [$user['firstName'], $user['lastName']]) : $user['username'])
                        ],
                        [
                            '%post_link%' => 'http://' . $_SERVER['HTTP_HOST'] . Url::to(['/' . $url . '/']) . '#comments',
                            '%objectName%' => $title,
                            '%user_link%' => 'http://' . $_SERVER['HTTP_HOST'] . Url::to([
                                    '/user/profile/',
                                    'id' => \Yii::$app->controller->user->id
                                ]),
                            '%userName%' => \Yii::$app->controller->user->firstName || \Yii::$app->controller->user->lastName ? implode(' ', [\Yii::$app->controller->user->firstName, \Yii::$app->controller->user->lastName])  : \Yii::$app->controller->user->username
                        ]);
                }

            }*/

            return $model->commentID;
        }
        return false;
    }

    protected function getComments()
    {
        $seasonID = \Yii::$app->request->get('season') != 1 ? \Yii::$app->request->get('season') : null;
        $seriesID = \Yii::$app->request->get('series') != 1 ? \Yii::$app->request->get('series') : null;

        $comments = \app\modules\comments\models\Comments::find()
            ->innerJoin('users AS u', 'comments.userID = u.id')
            ->leftJoin('images AS i', 'i.objectID = u.id AND i.oTypeID = ' . Images::IMAGE_USER_PROFILE)
            ->where([
                'AND',
                ['comments.isApprove' => ActiveRecord::STATUS_ACTIVE],
                ['comments.objectID' => $this->objectID],
                ['comments.typeID' => $this->objectTypeID],
                ['comments.seasonID' => $seasonID],
                ['comments.seriesID' => $seriesID],
            ])
            ->orderBy([
                'comments.parentID' => SORT_ASC,
                'comments.commentID' => SORT_ASC
            ])
            ->select([
                'comments.commentID',
                'comments.comment',
                'comments.parentID',
                'comments.created',
                'u.id',
                'i.name AS avatar',
                new Expression('IF((u.firstName IS NOT NULL AND u.firstName <> "") || (u.lastName IS NOT NULL AND u.lastName <> ""), TRIM(CONCAT_WS(" ", u.firstName, u.lastName)), u.username) AS userName')
            ])
            ->asArray()
            ->all();
        $this->countComments = count($comments);
        $this->sortItems($comments, -1, 0, true);

        return $this->commentsSortArray;
    }

    protected function sortItems($comments, $level = -1, $commentID = 0, $flag = false)
    {
        $level++;
        foreach($comments as $v)
        {
            if ($flag)
            {
                $flag = false;
                $this->commentsSortArray[] = array_merge($v, array('level' => $level));
                array_shift($comments);
                $this->sortItems($comments, $level, $v['commentID']);
            }
            else if ($v['parentID'] == $commentID)
            {
                $this->commentsSortArray[] = array_merge($v, ['level' => $level]);
                array_shift($comments);
                $this->sortItems($comments, $level, $v['commentID']);
            }
        }
    }
}
