<?php
/**
 * Created by PhpStorm.
 * User: Vital
 * Date: 16.02.2015
 * Time: 12:56
 */
$this->registerJs('

    var comment = new Comments("form");
    comment.initComments("' . ($post['parentID'] ? $post['parentID'] : '') . '");

');

?>

<div class="comments" id="comments">
    <div class="container">
        <div class="comments-header">
            <h4 class="comments-title">
                <?= $countComments ? $countComments . ' ' . \yii\helpers\StringHelper::declension($countComments, \Yii::t('front-comments', 'комментарий'), \Yii::t('front-comments', 'комментария'), \Yii::t('front-comments', 'комментариев')) : \Yii::t('front-comments', 'Нет комментариев, оставьте первый'); ?>
            </h4>
            <!-- <button type="button" class="btn btn--login" data-toggle="modal" data-target="#modalLogin">Войти</button> -->
            <?php if(\Yii::$app->user->isGuest): ?>
            <p class="tip pull-right">
                <button type="button" class="btn link" data-toggle="modal" data-target="#loginPopup"><?= \Yii::t('front-comments', 'Войдите'); ?></button>
                <?= \Yii::t('front-comments', ', чтобы оставить комментарий'); ?>
            </p>
            <?php endif; ?>
        </div>


        <?php if (!\Yii::$app->user->isGuest): ?>
            <span id="form">
              <?php if (!$post['parentID']) echo $commentsForm;?>
            </span>
        <?php else: echo $socBox; ?>
        <?php endif ;?>
        <?php echo $commentsList; ?>

    </div>
</div>


