<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\comments\models\Comments */
/* @var $form ActiveForm */
?>

<div class="comments-item  submit-form">
    <div class="comments-item-wrapper clearfix">
        <figure class="comments-avatar pull-left">
            <a href="http://brod.kz<?= \yii\helpers\Url::to([
                '/user/profile/',
                'id' => \Yii::$app->user->getId()
            ]); ?>" class="link--image"><img src="http://brod.kz<?=  \yii\helpers\Url::imgUrl($avatar['name'], 'users', [40, 41], false, 'jpg'); ?>" class="comments-avatar-image" alt=""></a>
        </figure>
        <div class="comments-body">
            <?php $form = ActiveForm::begin([
                'id'=>'commentsForm',
                'action' => '#comments',
                'options' => ['class' => 'form form--primary clearfix']
            ]); ?>
                <?= $form->field($model, 'comment', ['template' => '{input}'])->textInput([
                    'placeholder' => \Yii::t('front-comments', 'Ваш комментарий...'),
                    'onfocus' => "if(this.placeholder  == '" . \Yii::t('front-comments', 'Ваш комментарий...') . "') {this.placeholder = '';}",
                    'onblur' => "if(this.value == '') { this.placeholder = '" . \Yii::t('front-comments', 'Ваш комментарий...') . "'; }",
                ]); ?>

                <input type="hidden" value="0" id="Comments_parentID" name="Comments[parentID]">
                <input type="hidden" id="curContainer" value="<?php echo 'form'; ?>" />

                <?= Html::submitButton(\Yii::t('front-comments', 'Комментировать'), ['class' => 'btn btn--primary pull-right']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div><!-- end .comments-item -->