<div class="comments-item">
    <div class="comments-item-wrapper clearfix">
        <figure class="comments-avatar pull-left">
            <a href="http://brod.kz<?= \yii\helpers\Url::to([
                '/user/profile/',
                'id' => $data['id']
            ]); ?>" class="link--image"><img src="http://brod.kz<?=  \yii\helpers\Url::imgUrl($data['avatar'], 'users', [40, 41], false, 'jpg'); ?>" class="comments-avatar-image" alt=""></a>
        </figure>
        <div class="comments-body">
            <div class="comments-meta">
                <p class="comments-author"><a href="http://brod.kz<?= \yii\helpers\Url::to([
                        '/user/profile/',
                        'id' => $data['id']
                    ]); ?>" class="link"><?php echo $data['userName']; ?></a></p>
                <span class="icon--disc"></span>
                <span class="comments-time"><?= \yii\helpers\StringHelper::rudate('d F Y, H:i', strtotime($data['created'])) ?></span>
            </div>
            <p class="comments-text"><?php echo $data['comment']; ?></p>
            <div class="comments-control">
                <?php if (!\Yii::$app->user->isGuest && !\Yii::$app->controller->isMobile || \Yii::$app->controller->isMobile && $inc < 2): ?>
                    <button type="button" class="btn link reply" <?php echo $post['parentID'] == $data['commentID'] ? 'style="display: none;"' : ''; ?> id="btnReply<?php echo $data['commentID']; ?>" data-id="<?php echo $data['commentID']; ?>"><?= \Yii::t('front-comments', 'Ответить'); ?></button>

                    <button type="button" class="btn link reply" <?php echo $post['parentID'] == $data['commentID'] ? '' : 'style="display: none;"'; ?> id="btnReplyCancel<?php echo $data['commentID']; ?>" data-id=""><?= \Yii::t('front-comments', 'Отменить'); ?></button>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php echo $draw; ?>
    <span id="form<?php echo $data['commentID']; ?>">
        <?php if ((int)$post['parentID'] && $post['parentID'] == $data['commentID']) echo $commentsForm; ?>
    </span>
</div>

