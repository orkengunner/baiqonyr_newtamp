<div class="live live--comments">
    <h5 class="title--secondary"><?= \Yii::t('front-comments', 'Сейчас обсуждают'); ?></h5>
<?php
$_comments = new \app\modules\comments\models\Comments();

foreach ($comments as $v)
{

$title = (
    $v['filmTitle'] .
    $v['personTitle'] .
    $v['placeTitle'] .
    $v['posterTitle'] .
    $v['pageTitle'] .
    $v['newsTitle'] .
    $v['onlineTitle'] .
    $v['adsTitle'] .
    $v['blogTitle']

);
$titleLink = (
    $v['filmTitleLink'] .
    $v['personTitleLink'] .
    $v['placeTitleLink'] .
    $v['posterTitleLink'] .
    $v['pageTitleLink'] .
    $v['newsTitleLink'] .
    $v['onlineTitleLink'] .
    $v['adsTitleLink'] .
    $v['blogTitleLink']

);
$url = (
    ($v['filmUrl'] ? 'films/' . $v['filmUrl'] : '') .
    ($v['personUrl'] ? 'persons/' . $v['personUrl'] : '') .
    ($v['placeUrl'] ? 'places/' . $v['placeUrl'] : '') .
    ($v['posterUrl'] ? 'posters/' . $v['posterUrl'] : '') .
    $v['pageUrl'] .
    ($v['newsUrl'] ? 'news/' . $v['newsUrl'] : '') .
    ($v['onlineUrl'] ? 'films/online/' . $v['onlineUrl'] : '') .
    ($v['adsUrl'] ? 'ads/' . $v['adsUrl'] : '') .
    ($v['blogUrl'] ? 'blogs/' . $v['blogUrl'] : '')
);

if ($v['typeID'] == $_comments::COMMENT_TYPE_ONLINE) {
    $seasonID = !$v['seasonID'] ? 1 : $v['seasonID'];
    $seriesID = !$v['seriesID'] ? 1 : $v['seriesID'];
    $url = $url . '/?season=' . $seasonID . '&series=' . $seriesID;
}

    $from1 = new \DateTime($v['created']);
    $from2 = new \DateTime();
    $fromInterval = $from2->diff($from1);
    $time = strtotime($v['created']);

    if($fromInterval->y) {
        $date = \yii\helpers\StringHelper::translateDate(date('d F', $time), 1) . ', ' . date('Y', $time);
    } elseif(!$fromInterval->y && $fromInterval->m) {
        $date = \yii\helpers\StringHelper::translateDate(date('d F H:i', $time), 1);
    } elseif(!$fromInterval->y && !$fromInterval->m && $fromInterval->d == 1) {
        $date = \Yii::t('front', 'Вчера в') . ' ' .  date('H:i', $time);
    } elseif(!$fromInterval->y && !$fromInterval->m && $fromInterval->d) {
        $date = \yii\helpers\StringHelper::translateDate(date('d F H:i', $time),1);
    } elseif(!$fromInterval->y && !$fromInterval->m && !$fromInterval->d && $fromInterval->h == 1) {
        $date = \Yii::t('front', 'Час назад');
    } elseif(!$fromInterval->y && !$fromInterval->m && !$fromInterval->d && $fromInterval->h) {
        $date = \Yii::t('front', 'Сегодня в') . ' ' .  date('H:i', $time);
    } elseif(!$fromInterval->y && !$fromInterval->m && !$fromInterval->d && !$fromInterval->h) {
        $date = $fromInterval->i . \Yii::t('front', ' мин. назад');
    }
?>
    <div class="live-item">
        <h6 class="live-title"><a href="<?= \yii\helpers\Url::to(['/' . $url])?>#comments" class="link" title="<?= $titleLink; ?>"><?= $title; ?></a></h6>
        <p class="live-description"><?= strip_tags($v['comment'])?></p>
        <div class="live-meta">
            <p class="meta-author"><a href="<?= \yii\helpers\Url::to(['/user/profile/', 'id' => $v['userID']])?>" class="link"><?= $v['userFullName']; ?></a></p>
            <p class="meta-date"><?= $date; ?></p>
        </div>
    </div>

    <?php } ?>
</div>
