<?php
/**
 * Created by PhpStorm.
 * User: Vital
 * Date: 7/24/14
 * Time: 11:20
 */

namespace app\modules\comments\front\controllers;


use app\components\RFController;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;


class DefaultController extends RFController
{
    public function actionSetComment()
    {
        print_r(\Yii::$app->request->post());
    }
}
