<?php

namespace app\modules\bugreport\models;

use app\components\wMail;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "bug_report".
 *
 * @property string $id
 * @property string $url
 * @property integer $code
 * @property string $message
 * @property string $trace
 * @property string $created
 * @property string $fixed
 */
class BugReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bug_report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'message', 'trace'], 'required'],
            [['code'], 'integer'],
            [['message', 'trace'], 'string'],
            [['created', 'fixed'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код ошибки',
            'message' => 'Сообщение',
            'trace' => 'Трейс',
            'created' => 'Дата ошибки',
            'fixed' => 'Дата исправления',
            'url' => 'Url ошибки',
        ];
    }

    public static function setError()
    {
        $params = \Yii::$app->params;;
        $exception = \Yii::$app->errorHandler->exception;
        if ($exception === null || !isset($params['BugReport']) || !$params['BugReport'] || !$exception->getMessage() || self::isBot()) return false;
        $bugReport = new BugReport();
        $bugReport->code = $exception->getCode();
        $bugReport->message = $exception->getMessage();
        $bugReport->trace = $exception->getTraceAsString();
        $bugReport->created = date('Y-m-d H:i:s');
        $bugReport->url = $_SERVER['REQUEST_URI'];
        $bugReport->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $bugReport->save();

        wMail::sendBugReport(
            $params['wMail']['mails']['bug-report']['developers'],
            [
                '%link%' => 'http://' . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '' ) . '/admin.php/bugreport/view?id=' . $bugReport->id,
                '%trace%' => nl2br(print_r($bugReport->attributes, true)),
                '%server%' => nl2br(print_r($_SERVER, true))
            ]
        );
    }

    public static function isBot()
    {
        $bots = array(
            'rambler','googlebot','aport','yahoo','msnbot','turtle','mail.ru','omsktele',
            'yetibot','picsearch','sape.bot','sape_context','gigabot','snapbot','alexa.com',
            'megadownload.net','askpeter.info','igde.ru','ask.com','qwartabot','yanga.co.uk',
            'scoutjet','similarpages','oozbot','shrinktheweb.com','aboutusbot','followsite.com',
            'dataparksearch','google-sitemaps','appEngine-google','feedfetcher-google',
            'liveinternet.ru','xml-sitemaps.com','agama','metadatalabs.com','h1.hrn.ru',
            'googlealert.com','seo-rus.com','yaDirectBot','yandeG','yandex',
            'yandexSomething','Copyscape.com','AdsBot-Google','domaintools.com',
            'Nigma.ru','bing.com','dotnetdotcom','MJ12bot','Java/', 'masscan', 'Baiduspider',
            'AhrefsBot', 'facebookexternalhit', 'LWP::Simple', 'Twitterbot',
        );
        foreach($bots as $bot)
            if(stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false){
                return $bot;
            }
        return false;
    }
}
