<?php

namespace app\modules\bugreport\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\bugreport\models\BugReport;

/**
 * BugReportSearch represents the model behind the search form about `app\modules\bugreport\models\BugReport`.
 */
class BugReportSearch extends BugReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'code'], 'integer'],
            [['message', 'trace', 'created', 'fixed'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BugReport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'code' => $this->code,
            'created' => $this->created,
            'fixed' => $this->fixed,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'trace', $this->trace]);

        return $dataProvider;
    }
}
