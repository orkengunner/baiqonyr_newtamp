<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\bugreport\models\BugReport */

$this->title = $model->message;
$this->params['breadcrumbs'][] = ['label' => 'Bug Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bug-report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Пофиксить', ['fixed', 'id' => $model->id, 'val' => 1], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'url',
            'code',
            'message:ntext',
            'trace:ntext',
            'created',
            [
                'attribute' => 'fixed',
                /*'format' => 'raw',
                'value' => $model->fixed === null
                    ? '<span id="container_fixed"><a href="javascript:;" onclick="wAjax.doit(\'admin.php/bugreport/fixed\', {id: ' . $model->id .  ', val:1});">Пофиксить</a></span>'
                    : $model->fixed*/
            ],
        ],
    ]) ?>

</div>
