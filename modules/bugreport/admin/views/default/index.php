<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\bugreport\models\BugReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bug Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bug-report-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="widget">
        <div class="widget-content">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'layout' => "{items}\n{pager}",
                'rowOptions' => function ($model, $key, $index, $grid) {
                    if ($model->fixed) {
                        return ['style' => 'background:#d3fcd9;'];
                    } else {
                        return ['style' => 'background:#edd;'];
                    }
                },
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'code',
                    'url',
                    'message:ntext',
                    'created',
                    'fixed',

                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{view}{delete}'
                    ],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>


</div>
