<?php

namespace app\modules\tags\models;

use app\components\ActiveRecord;
use app\components\LanguageActiveRecord;
use app\modules\languages\models\LanguageSearch;
use app\modules\posts\models\PostsTags;
use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property string $id
 * @property string $name
 * @property string $frequency
 * @property integer $lang_id
 *
 * @property PostsTags[] $postsTags
 * @property Languages $lang
 */
class Tags extends LanguageActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['frequency', 'lang_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'frequency' => 'Frequency',
            'lang_id' => 'Lang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostsTags()
    {
        return $this->hasMany(PostsTags::className(), ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(LanguageSearch::className(), ['id' => 'lang_id']);
    }

    public static function findAllByName($query)
    {
        return self::find()->filterWhere(['like', 'name', $query])->all();
    }
}
