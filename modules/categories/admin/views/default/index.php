<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\categories\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('categories', 'Категории');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">
    <div class="page-heading">
        <h1><i class="icon-list-nested"></i> <?= Html::encode($this->title) ?></h1>
    </div>
    <p>
        <?= Html::a(Yii::t('categories', '<i class="icon-list-add"></i> Создать категорию', [
            'modelClass' => 'Categories',
        ]), ['create'], ['class' => 'btn btn-success', 'data-pjax' => 0]) ?>
    </p>
    <?php
    if (Yii::$app->getSession()->hasFlash('error_delete')) {
        ?>
        <p class="alert alert-danger">
            <?= Yii::$app->getSession()->getFlash('error_delete'); ?>
        </p>
    <?php
    }
    ?>
    <div class="widget">
        <div class="widget-content">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout' => "{items}\n{pager}",
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'class' => 'app\components\grid\TreeColumn',
                            'attribute' => 'title'
                        ],
                        'slug',
                        [
                            'class' => \app\components\grid\AddMenuItemColumn::className(),
                            'type' => 'category_view',
                            'routeParams' => ['slug' => 'slug']
                        ],
                        [
                            'value' => function ($data) {
                                return Html::a('<i class="glyphicon glyphicon-arrow-up"></i>',
                                    ['move', 'id' => $data->id, 'type' => 'up'], ['class' => 'btn btn-info'])
                                . '&nbsp;&nbsp;' . Html::a('<i class="glyphicon glyphicon-arrow-down"></i>',
                                    ['move', 'id' => $data->id, 'type' => 'down'], ['class' => 'btn btn-info']);
                            },
                            'format' => 'raw',
                            'options' => array('align' => 'center')
                        ],
                        [
                            'class' => \app\components\admin\RFAToggleColumn::className(),
                            'attribute' => 'is_active',
                        ],
                        ['class' => \app\components\admin\RFAActionColumn::className()],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
