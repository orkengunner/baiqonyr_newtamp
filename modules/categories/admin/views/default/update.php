<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\categories\models\Categories */

$this->title = Yii::t('categories', 'Изменение категории: ', [
    'modelClass' => 'Categories',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('categories', 'Категории'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('categories', 'Изменение');
?>
<div class="categories-update">

    <div class="page-heading">
        <h1><i class="icon-list-nested"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
