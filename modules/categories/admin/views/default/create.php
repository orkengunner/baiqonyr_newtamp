<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\categories\models\Categories */

$this->title = Yii::t('categories', 'Создание категории', [
    'modelClass' => 'Категории',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('categories', 'Категории'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-create">
    <div class="page-heading">
        <h1><i class="icon-list-nested"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
