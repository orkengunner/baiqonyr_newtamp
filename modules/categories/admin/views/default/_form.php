<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\languages\models;

/* @var $this yii\web\View */
/* @var $model app\modules\categories\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="widget">
        <div class="widget-content padding">

            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>
            <?= $form->field($model, 'lang_id')->dropDownList(models\Languages::getDropdownList()) ?>
            <?= $form->field($model, 'description')->widget(\vova07\imperavi\Widget::className()) ?>

            <?= $form->field($model,
                'parent_id')->dropDownList(\app\modules\categories\models\Categories::getTreeDropdownList()) ?>

            <?= $form->field($model, 'is_active')->checkbox() ?>

        </div>
    </div>
    <?= \app\components\widgets\MetaTagFields::widget([
        'model' => $model,
        'form' => $form
    ]) ?>
    <div class="widget">
        <div class="widget-content padding">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('categories', 'Создать') : Yii::t('categories',
                'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

