<?php

namespace app\modules\categories\admin\controllers;

use app\components\AdminController;
use dosamigos\grid\ToggleAction;
use Yii;
use app\modules\categories\models\Categories;
use app\modules\categories\models\CategoriesSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * DefaultController implements the CRUD actions for Categories model.
 */
class DefaultController extends AdminController
{
    protected $_modelName = 'app\modules\categories\models\Categories';

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Categories();

        if ($model->load(Yii::$app->request->post())) {
            /**
             * Если родитель не выбран, то делаем категорию корневой
             */
            if ($model->parent_id == 0) {
                if ($model->makeRoot()) {
                    return $this->redirect(['index']);
                }
            } else {
                /**
                 * Если найден, то добавлем его к дереву
                 */
                $rootCategory = Categories::findOne($model->parent_id);
                if ($rootCategory === null) {
                    $model->addError('parent_id', 'Родительская категория не найдена');
                } else {
                    if ($model->appendTo($rootCategory)) {
                        return $this->redirect(['index']);
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldParentId = $model->parent_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if ($oldParentId != $model->parent_id && $model->parent_id != 0) {
                $parentCategory = Categories::findOne($model->parent_id);
                $model->appendTo($parentCategory);
            } else {
                if ($oldParentId != $model->parent_id && $model->parent_id == 0) {
                    $model->makeRoot();
                }
            }

            return $this->redirect(['index']);


        }
        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $category = $this->findModel($id);
        if (empty($category->pages)) {
            $category->deleteWithChildren();
        } else {
            Yii::$app->getSession()->setFlash('error_delete',
                'Категория "' . $category->title . '" содержит статьи, сначала удалите статьи!');
            //throw new Exception('Категория содержит статьи, сначала удалите статьи!');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionMove($id, $type)
    {
        /**
         * @var \yii\db\ActiveRecord | \creocoder\nestedsets\NestedSetsBehavior $model
         */
        $model = Categories::findOne((int)$id);

        if ($model === null) {
            throw new NotFoundHttpException(404, 'Страница не найдена');
        }

        if (!$model->isRoot()) {
            if ($type == 'up') {
                $prev = $model->prev()->one();
                if ($prev !== null) {
                    $model->insertBefore($prev);
                }
            } else {
                $next = $model->next()->one();
                if ($next !== null) {
                    $model->insertAfter($next);
                }
            }
        } else {
            $curPos = $model->root;
            if ($type == 'up') {
                $prevCategory = Categories::find()->where(
                    [
                        'lang_id' => $model->lang_id,
                        'level' => 0
                    ])
                    ->andWhere('root<:root', [':root' => $model->root])
                    ->addOrderBy(['root' => SORT_DESC])->one();

                if ($prevCategory !== null) {
                    $newPos = $prevCategory->root;

                    $allDescendantsCurModel = $model->leaves()->all();
                    $allDescendantsPrevModel = $prevCategory->leaves()->all();

                    foreach ($allDescendantsCurModel as $curItem) {
                        $curItem->root = $newPos;
                        $curItem->save(false);
                    }

                    $model->root = $newPos;
                    $model->save(false);

                    foreach ($allDescendantsPrevModel as $prevItem) {
                        $prevItem->root = $curPos;
                        $prevItem->save();
                    }

                    $prevCategory->root = $curPos;
                    $prevCategory->save();
                }

            } else {
                $nextCategory = Categories::find()->where(
                    [
                        'lang_id' => $model->lang_id,
                        'level' => 0
                    ])
                    ->andWhere('root>:root', [':root' => $model->root])
                    ->addOrderBy(['root' => SORT_ASC])->one();

                if ($nextCategory !== null) {
                    $newPos = $nextCategory->root;

                    $allDescendantsCurModel = $model->leaves()->all();
                    $allDescendantsNextModel = $nextCategory->leaves()->all();


                    foreach ($allDescendantsCurModel as $curItem) {
                        $curItem->root = $newPos;
                        $curItem->save(false);
                    }

                    $model->root = $newPos;
                    $model->save();

                    foreach ($allDescendantsNextModel as $prevItem) {
                        $prevItem->root = $curPos;
                        $prevItem->save(false);
                    }

                    $nextCategory->root = $curPos;
                    $nextCategory->save(false);
                }

            }
        }
        $this->redirect(array('index'));
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'toggle' => [
                'class' => ToggleAction::className(),
                'modelClass' => $this->getModelName(),
                'onValue' => 1,
                'offValue' => 0
            ],

        ]);
    }

}
