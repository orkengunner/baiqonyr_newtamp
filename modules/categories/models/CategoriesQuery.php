<?php

namespace app\modules\categories\models;

use app\components\ActiveQuery;
use creocoder\nestedsets\NestedSetsQueryBehavior;

class CategoriesQuery extends ActiveQuery
{
    public function behaviors()
    {
        return [
            [
                'class' => NestedSetsQueryBehavior::className(),
            ],
        ];
    }
}
