<?php

namespace app\modules\categories\models;

use app\components\ActiveRecord;
use app\components\behaviors\WithMetaTags;
use app\components\LanguageActiveRecord;
use app\components\traits\NestedSetTree;
use app\components\traits\UploadableAsync;
use app\modules\languages\models\Languages;
use app\modules\pages\models\Pages;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property integer $is_active
 * @property string $create_date
 * @property string $update_date
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $meta_image
 * @property string $meta_title
 * @property integer $lang_id
 *
 * @property Languages $lang
 * @property CategoriesFilesMetaImage[] $categoriesFilesMetaImages
 * @property PagesCategories[] $pagesCategories
 * @property Pages[] $pages
 * @property PostsCategories[] $postsCategories
 * @property Posts[] $posts
 */
class Categories extends LanguageActiveRecord
{
    use NestedSetTree, UploadableAsync;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'is_active', 'root', 'lft', 'rgt', 'level', 'lang_id'], 'integer'],
            [['title', 'slug'], 'required'],
            [['description'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['title', 'slug', 'meta_description', 'meta_keywords', 'meta_image', 'meta_title'], 'string', 'max' => 255],
            [['slug'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('categories', 'ID'),
            'lang_id' => Yii::t('categories', 'Язык'),
            'parent_id' => Yii::t('categories', 'Родитель'),
            'title' => Yii::t('categories', 'Наименование'),
            'slug' => Yii::t('categories', 'ЧПУ'),
            'description' => Yii::t('categories', 'Описание'),
            'is_active' => Yii::t('categories', 'Активность'),
            'create_date' => Yii::t('categories', 'Дата создания'),
            'update_date' => Yii::t('categories', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Categories::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Pages::className(), ['id' => 'page_id'])->viaTable('pages_categories',
            ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesFilesMetaImages()
    {
        return $this->hasMany(CategoriesFilesMetaImage::className(), ['category_id' => 'id']);
    }


    public function behaviors()
    {
        return [
            $this->timestampBehavior,
            [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'root',
                'depthAttribute' => 'level'
            ],
            'Sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'immutable' => true
            ],
            [
                'class' => WithMetaTags::className()
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function findBySlug($slug)
    {
        $model = self::findOne(['slug' => $slug, 'is_active' => self::STATUS_ACTIVE]);

        return $model;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Yii::$app->getCache()->flush();

        return true;
    }

    public static function find()
    {
        $query = new CategoriesQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query->setLanguage();
        }

        return $query->setLanguage()->isActive('categories');
    }
}
