<?php

namespace app\modules\categories\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\categories\models\Categories;
use yii\helpers\VarDumper;

/**
 * CategoriesSearch represents the model behind the search form about `app\modules\categories\models\Categories`.
 */
class CategoriesSearch extends Categories
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lang_id', 'parent_id', 'is_active', 'root', 'lft', 'rgt', 'level'], 'integer'],
            [['title', 'slug', 'description', 'create_date', 'update_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['root' => SORT_ASC, 'lft' => SORT_ASC]],
            'pagination' => false
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lang_id' => $this->lang_id,
            'parent_id' => $this->parent_id,
            'is_active' => $this->is_active,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'root' => $this->root,
            'lft' => $this->lft,
            'rgt' => $this->rgt,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'description', $this->description]);

        $query->orderBy('root ASC, lft ASC');


        return $dataProvider;
    }
}
