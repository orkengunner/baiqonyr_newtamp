<?php

namespace app\modules\categories\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "categories_files_meta_image".
 *
 * @property integer $category_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Categories $category
 */
class CategoriesFilesMetaImage extends ActiveRecord
{
    public static $keyField = 'category_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories_files_meta_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'subdir', 'basename'], 'required'],
            [['category_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }
}
