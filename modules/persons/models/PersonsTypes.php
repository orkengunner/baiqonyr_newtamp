<?php

namespace app\modules\persons\models;

use Yii;

/**
 * This is the model class for table "persons_types".
 *
 * @property string $pTypeID
 * @property string $title
 * @property string $sort
 * @property integer $langID
 * @property string $title_kz
 * @property string $url
 * @property string $metaTitle
 * @property string $metaKeywords
 * @property string $metaDescription
 * @property string $metaTitle_kz
 * @property string $metaKeywords_kz
 * @property string $metaDescription_kz
 * @property string $second_title
 * @property string $second_title_kz
 */
class PersonsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persons_types';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'sort', 'url'], 'required'],
            [['sort', 'langID'], 'integer'],
            [['metaDescription', 'metaDescription_kz'], 'string'],
            [['title', 'title_kz', 'url', 'metaTitle', 'metaKeywords', 'metaTitle_kz', 'metaKeywords_kz', 'second_title', 'second_title_kz'], 'string', 'max' => 255],
            [['title_kz'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pTypeID' => 'P Type ID',
            'title' => 'Title',
            'sort' => 'Sort',
            'langID' => 'Lang ID',
            'title_kz' => 'Title Kz',
            'url' => 'Url',
            'metaTitle' => 'Meta Title',
            'metaKeywords' => 'Meta Keywords',
            'metaDescription' => 'Meta Description',
            'metaTitle_kz' => 'Meta Title Kz',
            'metaKeywords_kz' => 'Meta Keywords Kz',
            'metaDescription_kz' => 'Meta Description Kz',
            'second_title' => 'Second Title',
            'second_title_kz' => 'Second Title Kz',
        ];
    }
}
