<?php

namespace app\modules\persons\models;

use Yii;

/**
 * This is the model class for table "persons".
 *
 * @property string $personID
 * @property string $views
 * @property string $firstName
 * @property string $lastName
 * @property string $previewText
 * @property string $text
 * @property integer $statusID
 * @property string $titleLink
 * @property string $url
 * @property string $metaTitle
 * @property string $metaKeywords
 * @property string $metaDescription
 * @property string $created
 * @property string $lastFilm
 * @property string $lastFilmLink
 * @property string $growth
 * @property string $firstFilm
 * @property string $firstFilmLink
 * @property string $birthDay
 * @property string $placeBirth
 * @property string $totalFilms
 * @property string $contact
 * @property string $interview
 * @property string $email
 * @property string $address
 * @property string $facts
 * @property string $userID
 * @property integer $langID
 * @property integer $isDelete
 * @property string $ratings
 * @property double $rating
 * @property integer $listView
 * @property string $instagramm
 * @property string $twAccount
 * @property string $firstName_kz
 * @property string $lastName_kz
 * @property string $previewText_kz
 * @property string $text_kz
 * @property string $growth_kz
 * @property string $placeBirth_kz
 * @property string $interview_kz
 * @property string $titleLink_kz
 * @property string $metaTitle_kz
 * @property string $metaKeywords_kz
 * @property string $metaDescription_kz
 * @property integer $inKz
 * @property string $facts_kz
 */
class Persons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persons';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['views', 'statusID', 'userID', 'langID', 'isDelete', 'ratings', 'listView', 'inKz'], 'integer'],
            [['firstName', 'lastName', 'statusID', 'url', 'created'], 'required'],
            [['previewText', 'text', 'metaDescription', 'interview', 'facts', 'previewText_kz', 'text_kz', 'interview_kz', 'metaDescription_kz', 'facts_kz'], 'string'],
            [['created'], 'safe'],
            [['rating'], 'number'],
            [['firstName', 'lastName', 'titleLink', 'url', 'metaTitle', 'metaKeywords', 'lastFilm', 'lastFilmLink', 'growth', 'firstFilm', 'firstFilmLink', 'birthDay', 'placeBirth', 'totalFilms', 'contact', 'email', 'address', 'instagramm', 'twAccount', 'firstName_kz', 'lastName_kz', 'growth_kz', 'placeBirth_kz', 'titleLink_kz', 'metaTitle_kz', 'metaKeywords_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'personID' => 'Person ID',
            'views' => 'Views',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'previewText' => 'Preview Text',
            'text' => 'Text',
            'statusID' => 'Status ID',
            'titleLink' => 'Title Link',
            'url' => 'Url',
            'metaTitle' => 'Meta Title',
            'metaKeywords' => 'Meta Keywords',
            'metaDescription' => 'Meta Description',
            'created' => 'Created',
            'lastFilm' => 'Last Film',
            'lastFilmLink' => 'Last Film Link',
            'growth' => 'Growth',
            'firstFilm' => 'First Film',
            'firstFilmLink' => 'First Film Link',
            'birthDay' => 'Birth Day',
            'placeBirth' => 'Place Birth',
            'totalFilms' => 'Total Films',
            'contact' => 'Contact',
            'interview' => 'Interview',
            'email' => 'Email',
            'address' => 'Address',
            'facts' => 'Facts',
            'userID' => 'User ID',
            'langID' => 'Lang ID',
            'isDelete' => 'Is Delete',
            'ratings' => 'Ratings',
            'rating' => 'Rating',
            'listView' => 'List View',
            'instagramm' => 'Instagramm',
            'twAccount' => 'Tw Account',
            'firstName_kz' => 'First Name Kz',
            'lastName_kz' => 'Last Name Kz',
            'previewText_kz' => 'Preview Text Kz',
            'text_kz' => 'Text Kz',
            'growth_kz' => 'Growth Kz',
            'placeBirth_kz' => 'Place Birth Kz',
            'interview_kz' => 'Interview Kz',
            'titleLink_kz' => 'Title Link Kz',
            'metaTitle_kz' => 'Meta Title Kz',
            'metaKeywords_kz' => 'Meta Keywords Kz',
            'metaDescription_kz' => 'Meta Description Kz',
            'inKz' => 'In Kz',
            'facts_kz' => 'Facts Kz',
        ];
    }
}
