<?php

namespace app\modules\persons\models;

use Yii;

/**
 * This is the model class for table "relation_persons_types".
 *
 * @property string $relID
 * @property string $personID
 * @property string $pTypeID
 * @property integer $sort
 */
class RelationPersonsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relation_persons_types';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['personID', 'pTypeID'], 'required'],
            [['personID', 'pTypeID', 'sort'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'relID' => 'Rel ID',
            'personID' => 'Person ID',
            'pTypeID' => 'P Type ID',
            'sort' => 'Sort',
        ];
    }
}
