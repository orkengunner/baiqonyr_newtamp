<?php

namespace app\modules\sponsors\models;

use Yii;

/**
 * This is the model class for table "sponsors_files_logo".
 *
 * @property string $sponsor_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Sponsors $sponsor
 */
class SponsorsFilesLogo extends \yii\db\ActiveRecord
{
    public static $keyField = 'sponsor_id';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sponsors_files_logo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sponsor_id', 'subdir', 'basename'], 'required'],
            [['sponsor_id'], 'integer'],
            [['subdir', 'basename'], 'string', 'max' => 255],
            [['sponsor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sponsors::className(), 'targetAttribute' => ['sponsor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sponsor_id' => 'Sponsor ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Sponsors::className(), ['id' => 'sponsor_id']);
    }
}
