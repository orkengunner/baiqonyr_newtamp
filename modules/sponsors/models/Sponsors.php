<?php

namespace app\modules\sponsors\models;

use app\components\ActiveRecord;
use app\components\behaviors\WithMetaTags;
use app\components\LanguageActiveRecord;
use app\components\traits\UploadableAsync;
use app\modules\festivals\models\Festivals;
use app\modules\languages\models\Languages;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "sponsors".
 *
 * @property string $id
 * @property string $festival_id
 * @property string $title
 * @property string $short_text
 * @property string $text
 * @property string $site
 * @property string $link
 * @property integer $status_id
 * @property string $slug
 * @property string $user_id
 * @property string $create_date
 * @property string $update_date
 * @property string $meta_title
 * @property string $delete_by
 * @property string $delete_date
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $lang_id
 * @property integer $position
 *
 * @property Festivals $festival
 * @property Languages $lang
 * @property SponsorsFilesLogo[] $sponsorsFilesLogos
 * @property SponsorsFilesMetaImage[] $sponsorsFilesMetaImages
 */
class Sponsors extends ActiveRecord
{
    use UploadableAsync;
    public $upload_logo = null;

    public $file_attributes = [
        'logo',
        'metaImage'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sponsors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'short_text', 'status_id'], 'required'],
            [['festival_id', 'status_id', 'user_id', 'delete_by', 'lang_id', 'position'], 'integer'],
            [['short_text', 'text', 'meta_description'], 'string'],
            [['short_text_kz', 'text_kz', 'meta_description_kz'], 'string'],
            [['create_date', 'update_date', 'delete_date'], 'safe'],
            [['title', 'title_kz', 'site', 'link', 'slug', 'meta_title', 'meta_keywords', 'meta_title_kz', 'meta_keywords_kz'], 'string', 'max' => 255],
            [['festival_id', 'slug'], 'unique', 'targetAttribute' => ['festival_id', 'slug'], 'message' => 'The combination of Festival ID and Slug has already been taken.'],
            [['festival_id'], 'exist', 'skipOnError' => true, 'targetClass' => Festivals::className(), 'targetAttribute' => ['festival_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [
                ['upload_logo'],
                'image',/* 'minWidth' => 620, 'minHeight'=>400,*/
                'on' => 'upload'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [

        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorsFilesLogos()
    {
        return $this->hasMany(SponsorsFilesLogo::className(), ['sponsor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSponsorsFilesMetaImages()
    {
        return $this->hasMany(SponsorsFilesMetaImage::className(), ['sponsor_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior,
            [
                'class' => WithMetaTags::className()
            ],
        ];
    }

    public function beforeValidate()
    {
        if ($this->getScenario() != 'search') {
            $this->user_id = Yii::$app->getUser()->id;
            if (!$this->slug) {
                $this->slug = StringHelper::url($this->title);
            }
        }
        $this->festival_id = self::festID();
        return parent::beforeValidate();
    }

    public function afterSave($insert, $changedAttributes)
    {

        if ($this->hasAsyncTempFile('upload_logo')) {
            $logo = $this->setFile('logo', $this->saveAsyncFile('upload_logo'));
        }

        parent::afterSave($insert, $changedAttributes);
        return true;

    }

    public static function find()
    {
        $query = new SponsorsQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query->isNoDeleted(self::tableName())
                ->andWhere(['festival_id' => self::festID()]);
        }

        return $query->isActive(self::tableName())->isNoDeleted(self::tableName())->andWhere(['festival_id' => self::festID()]);
    }

    public static function findSponsors($limit = null)
    {
        $query = self::find()
        ->withFilesLogo(self::tableName(), 'sponsor_id')
            ->getLangField('sponsors.title')
            ->getLangField('sponsors.short_text')
        ->addSelect([
            'sponsors.id',
            'sponsors.link',
            'sponsors.slug',
        ])
		->orderBy(['position' => SORT_ASC]);

        if($limit) {
            $query->limit($limit);
        }

        return $query->asArray()->all();
    }
}
