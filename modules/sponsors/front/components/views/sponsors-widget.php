<section class="partners">
    <div class="partners-list">
        <?php foreach($data as $v): ?>
            <div class="partners-item">
                <div class="partners-body">
                    <?php if($v['link']): ?>
                        <a target="_blank" href="<?= $v['link'] ? $v['link'] : \yii\helpers\Url::to(['/sponsors/default/view',
                            'sponsor_slug' => $v['slug']
                        ]); ?>" class="link--overlay"></a>
                    <?php endif; ?>
                    <figure class="partners-image"><img src="<?= \yii\helpers\Url::imgUrl($v['fa_logo'], 'sponsors')?>" alt=""></figure>
                    <h5 class="partners-name"><?= $v['title']?></h5>
                    <p class="partners-meta"><?= strip_tags($v['short_text'])?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>