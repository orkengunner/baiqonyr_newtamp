<?php
/**
 * Created by PhpStorm.
 * User: Vitaliy Prokhonenkov
 * Date: 06.05.2016
 * Time: 14:59
 */

namespace app\modules\sponsors\front\components;

use app\modules\languages\models\Languages;
use app\modules\sponsors\models\Sponsors;
use yii\base\Widget;

class SponsorsWidget extends Widget
{
    public $data = [];
    public function run()
    {
        $sponsors = \Yii::$app->getCache()->get('sponsors_widget_' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($sponsors === false) {
            $sponsors = Sponsors::findSponsors();
            \Yii::$app->getCache()->set('sponsors_widget' . Languages::getCurrent()->id . \Yii::$app->language, $sponsors, 3600*24*7);
        }

        return $this->render('sponsors-widget',[
            'data' => $sponsors
        ]);
    }
}