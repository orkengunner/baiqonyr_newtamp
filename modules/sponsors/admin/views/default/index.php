<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sponsors\models\SponsorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Спонсоры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sponsors-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <p>
        <?= Html::a(Yii::t('sponsors', '<i class="icon-list-add"></i> Добавить спонсора', [
            'modelClass' => 'Sponsors',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

            'title',
            // 'link',
            // 'meta_title',
            // 'meta_description:ntext',
            // 'meta_keywords',

                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{update} {remove}'
                    ],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
