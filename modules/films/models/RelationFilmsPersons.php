<?php

namespace app\modules\films\models;

use Yii;

/**
 * This is the model class for table "relation_films_persons".
 *
 * @property string $relID
 * @property string $filmID
 * @property string $personID
 * @property string $pTypeID
 * @property integer $position
 */
class RelationFilmsPersons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relation_films_persons';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filmID', 'personID', 'pTypeID'], 'required'],
            [['filmID', 'personID', 'pTypeID', 'position'], 'integer'],
            [['filmID', 'personID', 'pTypeID'], 'unique', 'targetAttribute' => ['filmID', 'personID', 'pTypeID'], 'message' => 'The combination of Film ID, Person ID and P Type ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'relID' => 'Rel ID',
            'filmID' => 'Film ID',
            'personID' => 'Person ID',
            'pTypeID' => 'P Type ID',
            'position' => 'Position',
        ];
    }
}
