<?php

namespace app\modules\films\models;

use Yii;

/**
 * This is the model class for table "films".
 *
 * @property string $filmID
 * @property string $title
 * @property string $originalTitle
 * @property string $year
 * @property string $premier
 * @property string $previewText
 * @property string $text
 * @property string $duration
 * @property string $productionID
 * @property string $statusID
 * @property string $titleLink
 * @property string $url
 * @property string $metaTitle
 * @property string $metaKeywords
 * @property string $metaDescription
 * @property string $created
 * @property string $views
 * @property string $coun`try
 * @property string $datePublish
 * @property string $chronometry
 * @property string $typeID
 * @property integer $isDelete
 * @property string $age
 * @property string $dvd
 * @property string $premierWorld
 * @property string $slogan
 * @property string $kinopoiskID
 * @property string $kinozavrID
 * @property double $rating
 * @property integer $langID
 * @property integer $listView
 * @property string $title_kz
 * @property string $originalTitle_kz
 * @property string $previewText_kz
 * @property string $text_kz
 * @property string $titleLink_kz
 * @property string $metaTitle_kz
 * @property string $metaKeywords_kz
 * @property string $metaDescription_kz
 * @property string $slogan_kz
 * @property string $country_kz
 * @property integer $inKz
 * @property string $chronometry_kz
 */
class Films extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'films';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'previewText', 'text', 'url', 'created', 'datePublish'], 'required'],
            [['year', 'productionID', 'statusID', 'views', 'typeID', 'isDelete', 'kinopoiskID', 'kinozavrID', 'langID', 'listView', 'inKz'], 'integer'],
            [['premier', 'created', 'datePublish', 'premierWorld'], 'safe'],
            [['previewText', 'text', 'metaTitle', 'previewText_kz', 'text_kz', 'metaDescription_kz'], 'string'],
            [['rating'], 'number'],
            [['title', 'originalTitle', 'titleLink', 'url', 'metaKeywords', 'metaDescription', 'country', 'chronometry', 'age', 'dvd', 'slogan', 'title_kz', 'originalTitle_kz', 'titleLink_kz', 'metaTitle_kz', 'metaKeywords_kz', 'slogan_kz', 'country_kz', 'chronometry_kz'], 'string', 'max' => 255],
            [['duration'], 'string', 'max' => 50],
            [['title_kz'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'filmID' => 'Film ID',
            'title' => 'Title',
            'originalTitle' => 'Original Title',
            'year' => 'Year',
            'premier' => 'Premier',
            'previewText' => 'Preview Text',
            'text' => 'Text',
            'duration' => 'Duration',
            'productionID' => 'Production ID',
            'statusID' => 'Status ID',
            'titleLink' => 'Title Link',
            'url' => 'Url',
            'metaTitle' => 'Meta Title',
            'metaKeywords' => 'Meta Keywords',
            'metaDescription' => 'Meta Description',
            'created' => 'Created',
            'views' => 'Views',
            'country' => 'Country',
            'datePublish' => 'Date Publish',
            'chronometry' => 'Chronometry',
            'typeID' => 'Type ID',
            'isDelete' => 'Is Delete',
            'age' => 'Age',
            'dvd' => 'Dvd',
            'premierWorld' => 'Premier World',
            'slogan' => 'Slogan',
            'kinopoiskID' => 'Kinopoisk ID',
            'kinozavrID' => 'Kinozavr ID',
            'rating' => 'Rating',
            'langID' => 'Lang ID',
            'listView' => 'List View',
            'title_kz' => 'Title Kz',
            'originalTitle_kz' => 'Original Title Kz',
            'previewText_kz' => 'Preview Text Kz',
            'text_kz' => 'Text Kz',
            'titleLink_kz' => 'Title Link Kz',
            'metaTitle_kz' => 'Meta Title Kz',
            'metaKeywords_kz' => 'Meta Keywords Kz',
            'metaDescription_kz' => 'Meta Description Kz',
            'slogan_kz' => 'Slogan Kz',
            'country_kz' => 'Country Kz',
            'inKz' => 'In Kz',
            'chronometry_kz' => 'Chronometry Kz',
        ];
    }
}
