<?php

namespace app\modules\films\models;

use Yii;

/**
 * This is the model class for table "online".
 *
 * @property string $onlineID
 * @property string $title
 * @property integer $price
 * @property string $filmID
 * @property integer $discount
 * @property string $note
 * @property integer $statusID
 * @property string $created
 * @property integer $langID
 * @property string $text
 * @property integer $onHome
 * @property integer $countSeasons
 * @property integer $typeID
 * @property string $rightholder
 * @property integer $is_not_auth
 * @property string $title_kz
 * @property string $text_kz
 * @property integer $inKz
 * @property string $note_kz
 * @property integer $is_fest
 */
class Online extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'online';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'filmID', 'statusID', 'created', 'text', 'typeID'], 'required'],
            [['price', 'filmID', 'discount', 'statusID', 'langID', 'onHome', 'countSeasons', 'typeID', 'rightholder', 'is_not_auth', 'inKz', 'is_fest'], 'integer'],
            [['note', 'text', 'text_kz', 'note_kz'], 'string'],
            [['created'], 'safe'],
            [['title', 'title_kz'], 'string', 'max' => 255],
            [['title_kz'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'onlineID' => 'Online ID',
            'title' => 'Title',
            'price' => 'Price',
            'filmID' => 'Film ID',
            'discount' => 'Discount',
            'note' => 'Note',
            'statusID' => 'Status ID',
            'created' => 'Created',
            'langID' => 'Lang ID',
            'text' => 'Text',
            'onHome' => 'On Home',
            'countSeasons' => 'Count Seasons',
            'typeID' => 'Type ID',
            'rightholder' => 'Rightholder',
            'is_not_auth' => 'Is Not Auth',
            'title_kz' => 'Title Kz',
            'text_kz' => 'Text Kz',
            'inKz' => 'In Kz',
            'note_kz' => 'Note Kz',
            'is_fest' => 'Is Fest',
        ];
    }
}
