<?php

namespace app\modules\films\models;

use Yii;

/**
 * This is the model class for table "relation_films_genries".
 *
 * @property string $relID
 * @property string $genreID
 * @property string $filmID
 */
class RelationFilmsGenries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relation_films_genries';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['genreID', 'filmID'], 'required'],
            [['genreID', 'filmID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'relID' => 'Rel ID',
            'genreID' => 'Genre ID',
            'filmID' => 'Film ID',
        ];
    }
}
