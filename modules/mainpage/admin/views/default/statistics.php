<div class="page-heading">
    <h1><i class="icon-chart-bar-2"></i>Статистика</h1>
</div>

<div class="widget">
    <div class="widget-content padding">
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <td class="col-md-4">Зарегистрированных пользователей</td>
                <td><?= \app\modules\users\models\Users::find()->where(['is_active' => 1])->andWhere('DATE(login_date)<>"0000-00-00"')->count() ?></td>
            </tr>
            <tr>
                <td>из них с подтвержденными данными:</td>
                <td><?= \app\modules\users\models\Users::find()->where([
                        'is_active' => 1,
                        'is_confirm' => 1
                    ])->count() ?></td>
            </tr>
            <tr>
                <td>Загруженных работ всего</td>
                <td><?= \app\modules\contest\models\Contest::find()->count() ?></td>
            </tr>
            <tr>
                <td>из них одобренных:</td>
                <td><?= \app\modules\contest\models\Contest::find()->where([
                        'is_active' => 1,
                        'is_approved' => 1
                    ])->count() ?></td>
            </tr>
            <tr>
                <td><strong>Работы по категориям:</strong></td>
                <td></td>
            </tr>
            <?php foreach (\app\modules\contest\models\Contest::$categories as $id => $category) { ?>
                <tr>
                    <td style="padding-left: 50px;"><?= $category ?></td>
                    <td><?= \app\modules\contest\models\Contest::find()->where([
                            'category' => $id
                        ])->count() ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td>Всего комментариев:</td>
                <td><?= \app\modules\comments\models\Comments::find()->count() ?></td>
            </tr>
            <tr>
                <td>Активировано кодов пользователями:</td>
                <td><?= \app\modules\contest\models\Points::find()->where(['type' => 40])->count() ?></td>
            </tr>
            <tr>
                <td>Активировано подарочных кодов:</td>
                <td><?= \app\modules\contest\models\PromoCodes::find()->where(['is_activated' => 1])->count() ?></td>
            </tr>
        </table>
    </div>
</div>
