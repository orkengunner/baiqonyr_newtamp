<?php

namespace app\modules\mainpage\admin\controllers;

use app\components\AdminController;
use yii\web\ErrorAction;

class DefaultController extends AdminController
{

    public function actionIndex()
    {
        $this->redirect(['/jobs/default/index']);
    }

    public function actionCacheFlush()
    {
        \Yii::$app->getCache()->flush();

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className(),
            ],
        ];
    }
}
