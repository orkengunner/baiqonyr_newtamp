<?php

namespace app\modules\mainpage\front\controllers;

use app\components\FrontController;
use app\components\RFController;
use app\components\social_shares\SocialShares;
use app\modules\blocks\models\Blocks;
use app\modules\categories\models\Categories;
use app\modules\config\models\Config;
use app\modules\festivals\models\Festivals;
use app\modules\jobs\models\Genries;
use app\modules\jobs\models\Jobs;
use app\modules\languages\models\Languages;
use app\modules\news\models\News;
use app\modules\pages\models\Pages;
use app\modules\posts\models\Posts;
use app\modules\slide\models\Slide;
use app\modules\stocks\models\Stocks;
use yii\caching\DbDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class DefaultController extends FrontController
{
    public $layout = '//front/main';

    public function actionIndex()
    {
        $this->cssClass = 'main-page';
        $this->isMain = true;
        $model = Pages::findBySlug('/');

        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->metaImage
        );

        $mainPage = \Yii::$app->getCache()->get('main_page_main_page_' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($mainPage === false) {
            $mainPage = Festivals::getFest(['main_page']);
            \Yii::$app->getCache()->set('main_page_main_page_' . Languages::getCurrent()->id . \Yii::$app->language, $mainPage, 3600*24*7);
        }

        /*$about = \Yii::$app->getCache()->get('main_page_about_' . Languages::getCurrent()->id . \Yii::$app->language);
        if ($about === false) {
            $about = Festivals::getFest(['description']);
            \Yii::$app->getCache()->set('main_page_about_' . Languages::getCurrent()->id . \Yii::$app->language, $about, 3600*24*7);
        }*/

        //$jobs = \Yii::$app->getCache()->get('main_page_jobs_' . Languages::getCurrent()->id . \Yii::$app->language);
        //if ($jobs === false) {
            //$jobs = Jobs::findLastJobs(5, false, 'RAND()');
            //\Yii::$app->getCache()->set('main_page_jobs_' . Languages::getCurrent()->id . \Yii::$app->language, $jobs, 3600*24*7);
        //}

        $genres = Genries::findAllDD();

        return $this->render('index', [
            'mainPage' => $mainPage,
            //'genres' => $genres,
            //'about' => $about,
        ]);
    }

    public function actionError()
    {
        $this->cssClass = 'main-page';
        //$this->isHidden = true;
        $model = Pages::findBySlug('404');
        $this->setMeta(
            $model->metaTitle,
            $model->getMetaDescription('meta_description'),
            $model->metaKeywords,
            $model->joinMetaImage
        );

        if (\Yii::$app->request->post() && !is_null(\Yii::$app->request->post('errorMessage')) && !is_null(\Yii::$app->request->post('location'))) {

            $errorMessage = sprintf('Страница: %s <br> Ошибка: %s', \Yii::$app->request->post('location'), \Yii::$app->request->post('errorMessage'));

            $mail = \Yii::$app
                ->mailer
                ->compose(
                    "layouts/html",
                    ['content' => $errorMessage]
                )
                ->setFrom(['postmaster@mg.baiqonyr.kz' => 'Info Baiqonyr.kz'])
                ->setTo('support@rocketfirm.com')
                ->setSubject('Ошибка с сайта Baiqonyr.kz');

            if ($mail->send()) {
                return true;
            } else {
                return false;
            }
        }else{
            $error = \Yii::$app->getErrorHandler()->exception->getMessage();
        }

        return $this->render('error', ['model' => $model, 'error' => $error]);
    }

    public function actionSocialShare()
    {
        $url = isset(\Yii::$app->request->get()['url']) ? \Yii::$app->request->get()['url'] : null;
        $shares = new SocialShares($url);

        return json_encode([
            'data' => [
                'facebook' => $shares->facebook(),
                'twitter' => $shares->twitter(\Yii::$app->request->get('title')),
                'watsup' => $shares->watsup(),
                'vkontakte' => $shares->vkontakte(
                    \Yii::$app->request->get('description'),
                    \Yii::$app->request->get('title'),
                    \Yii::$app->request->get('image')
                )
            ],
            'callback' => 'socialCallback2'
        ]);
    }

}
