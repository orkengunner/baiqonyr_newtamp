<div class="container">
    <div class="error-page">
        <div class="error-body">
            <h2 class="title--primary"><?= \Yii::t('front-404', 'Упс, мы потерялись...'); ?></h2>
            <p class="error-page-description"><?= \Yii::t('front-404', 'К сожалению страница не найдена, можете пройти по одной из ссылок, расположенных выше.'); ?></p>

            <button class="btn btn--primary">Сообщить в тех.поддержку</button>
            <p class="error-message hidden" data-csrf="<?=\Yii::$app->request->csrfToken?>"><?= $error?></p>
            <div class="alert-info alert hidden">Ваше сообщение успешно отправлено</div>
            <div class="alert-danger alert hidden">Ошибка при отправке сообщения</div>
        </div>
        <figure class="error-hero"><img src="/images/human.png" alt=""></figure>
    </div>
</div><!-- end .container -->