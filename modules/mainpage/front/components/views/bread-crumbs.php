<ul class="breadcrumbs">
    <?php
    foreach($data as $k => $v) if($v) {
        echo '<li class="breadcrumbs-item"><a class="link" href="' . \yii\helpers\Url::to($v) . '">' . $k . '</a></li>' . "\n";
    } else {
        echo '<li class="breadcrumbs-item active">' . $k . '</li>';
    }
    ?>
</ul>