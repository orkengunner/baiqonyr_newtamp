<?php
/**
 * Created by PhpStorm.
 * User: Vitaliy Prokhonenkov
 * Date: 06.05.2016
 * Time: 14:59
 */

namespace app\modules\mainpage\front\components;

use yii\base\Widget;

class BreadCrumbs extends Widget
{
    public $data = [];
    public function run()
    {
        return $this->render('bread-crumbs',[
            'data' => $this->data
        ]);
    }
}