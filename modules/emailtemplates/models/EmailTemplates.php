<?php

namespace app\modules\emailtemplates\models;

use app\components\ActiveRecord;
use app\components\FrontController;
use Yii;

/**
 * This is the model class for table "email_templates".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $alt_text
 * @property integer $is_alt_text
 *
 */
class EmailTemplates extends ActiveRecord
{
    const FORGOT_PASSWORD = 3;
    const REGISTRATION = 1;
    const NEW_USER = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['text', 'alt_text'], 'string'],
            [['is_alt_text'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок письма',
            'text' => 'Уведомление на почту',
            'alt_text' => 'Уведомление на сайте',
        ];
    }

    public static function find()
    {
        $query = new EmailTemplatesQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query;
        }
        return $query;
    }
}
