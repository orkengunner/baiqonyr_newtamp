<?php

namespace app\modules\emailtemplates;

use app\components\Module;

class EmailTemplates extends Module
{
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
