<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\emailtemplates\models\EmailTemplates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="widget">
        <div class="widget-content padding">

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

            <?= $model->is_alt_text || $model->isNewRecord ? $form->field($model, 'alt_text')->textarea(['rows' => 6]) : '' ?>

        </div>
    </div>
    <div class="widget">
        <div class="widget-content padding">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('comments', 'Создать') : Yii::t('comments',
                'Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="/js/ckeditor/ckeditor.js?<?= time() ?>"></script>
<script>
    CKEDITOR.replace('emailtemplates-text', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
    CKEDITOR.replace('emailtemplates-alt_text', <?=\yii\helpers\Json::encode(Yii::$app->params['ckeditor'])?>);
</script>