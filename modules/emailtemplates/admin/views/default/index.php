<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\emailtemplates\models\EmailTemplatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Шаблоны уведомлений';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-templates-index">

    <div class="page-heading">
        <h1><i class="icon-mail-3"></i> <?= Html::encode($this->title) ?></h1>
    </div>

    <div class="widget">
        <div class="widget-content">
            <div class="table-responsive">
                <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-bordered'],
                    'filterModel' => $searchModel,
                    'layout' => "{items}\n{pager}",
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'title',
                        'alt_text',
                        'text',

                        ['class' =>
                            \app\components\admin\RFAActionColumn::className(),
                            'template' => '{update}',
                        ],
                    ],
                ]); ?>
                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
