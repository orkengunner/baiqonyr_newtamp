<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\emailtemplates\models\EmailTemplates */

$this->title = 'Create Email Templates';
$this->params['breadcrumbs'][] = ['label' => 'Email Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-templates-create">

    <div class="page-heading">
        <h1><i class="icon-mail-3"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>