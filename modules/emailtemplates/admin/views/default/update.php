<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\emailtemplates\models\EmailTemplates */

$this->title = 'Редактирование шаблона: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Email Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="email-templates-update">

    <div class="page-heading">
        <h1><i class="icon-mail-3"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
