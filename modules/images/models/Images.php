<?php

namespace app\modules\images\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "images".
 *
 * @property string $imageID
 * @property string $name
 * @property string $objectID
 * @property string $oTypeID
 * @property string $position
 * @property string $description
 * @property string $title
 * @property string $file
 * @property string $created
 * @property integer $lang_id
 */
class Images extends ActiveRecord
{
    const IMAGE_USER_PROFILE = 14;
    const IMAGE_FILM_LOGO = 2;
    const IMAGE_ONLINE_VIDEO = 31;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbBrod');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'objectID', 'oTypeID'], 'required'],
            [['objectID', 'oTypeID', 'position', 'lang_id'], 'integer'],
            [['created'], 'safe'],
            [['name', 'description', 'title', 'file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'imageID' => 'Image ID',
            'name' => 'Name',
            'objectID' => 'Object ID',
            'oTypeID' => 'O Type ID',
            'position' => 'Position',
            'description' => 'Description',
            'title' => 'Title',
            'file' => 'File',
            'created' => 'Created',
            'lang_id' => 'Lang ID',
        ];
    }

    public function getImageByObjectID($id, $tID)
    {
        return $this->find()->andWhere(['objectID' => $id, 'oTypeID' => $tID]);
    }
}
