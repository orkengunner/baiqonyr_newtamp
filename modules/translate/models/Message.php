<?php

namespace app\modules\translate\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * @property SourceMessage $source
 */
class Message extends \app\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('translate', 'ID'),
            'language' => Yii::t('translate', 'Язык'),
            'translation' => Yii::t('translate', 'Перевод'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
    }

    public static function findStringByTranslation($message){
        $messages = Message::find(['translation'=>$message])->all();

        if(!empty($messages)){
            $out = array();
            foreach($messages as $message){
                $out[$message->language] = array($message->sourse->message=>$message->translation);
            }

            return $out;
        }else return Yii::t('custom', $message);
    }
}
