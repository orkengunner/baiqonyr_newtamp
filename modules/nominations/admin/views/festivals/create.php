<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\nominations\models\FestivalsNominations */

$this->title = 'Добавить номинацию';
$this->params['breadcrumbs'][] = ['label' => 'Festivals Nominations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivals-nominations-create">

    <div class="page-heading">
        <h1><i class="icon-menu"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
