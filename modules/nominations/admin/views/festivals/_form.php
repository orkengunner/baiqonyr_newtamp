<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\modules\nominations\models\Nominations;

/* @var $this yii\web\View */
/* @var $model app\modules\nominations\models\FestivalsNominations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="festivals-nominations-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="widget">
        <div class="widget-content padding">
            <?= $form->field($model, 'nomination_id')->dropDownList(
                    \app\modules\nominations\models\Nominations::getDropdownList(
						'CONCAT_WS(" - ", title, IF(type = ' . $model::TYPE_NATIONAL . ', "' . Nominations::$types[$model::TYPE_NATIONAL] . '", "' . Nominations::$types[$model::TYPE_WORLD] . '"))'
                    ),
                    ['prompt' => 'Выберите']
            ) ?>

        </div>
    </div>
    <div class="widget">
        <div class="widget-content padding">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('posts', 'Создать') : Yii::t('posts',
                'Изменить'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                'id' => 'submit-page'
            ]) ?>
        </div>
    </div>




    <?php ActiveForm::end(); ?>

</div>
