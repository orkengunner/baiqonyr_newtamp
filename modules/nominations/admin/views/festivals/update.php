<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\nominations\models\FestivalsNominations */

$this->title = 'Редактировать номинацию';
$this->params['breadcrumbs'][] = ['label' => 'Festivals Nominations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->festival_id, 'url' => ['view', 'festival_id' => $model->festival_id, 'nomination_id' => $model->nomination_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="festivals-nominations-update">

    <div class="page-heading">
        <h1><i class="icon-menu"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
