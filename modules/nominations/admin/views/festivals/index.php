<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\nominations\models\FestivalsNominationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Номинации фестиваля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivals-nominations-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <p>
        <?= Html::a(Yii::t('festivals_nominations', '<i class="icon-list-add"></i> Добавить номинацию', [
            'modelClass' => 'FestivalsNominations',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
        <a href="/admin.php/nominations" class="btn btn-default"><i class="fa fa-align-justify"></i> Номинации</a>
    </p>
    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'pager' => [
                    'lastPageLabel' => 'Последняя',
                    'firstPageLabel' => 'Первая'
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'nomination_title',
                        'value' => function ($data) {
                            return $data->nomination->title;
                        },
                    ],

                    [
                        'attribute' => 'nomination_type',
                        'value' => function($model) {
                            return \app\modules\nominations\models\Nominations::$types[$model->nomination->type];
                        },
                        'filter' => \app\modules\nominations\models\Nominations::$types
                    ],
                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{update} {delete}'
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
