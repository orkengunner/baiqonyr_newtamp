<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\nominations\models\Nominations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nominations-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="widget">
        <div class="widget-header">
            <h2>Миниатюра записи</h2>
        </div>
        <div class="widget-content padding gallery-wrap">
			<?php
			echo \app\components\widgets\ImageUploadAsync::widget([
				'model' => $model,
				'attribute' => 'upload_logo',
				'form' => $form,
				//'hint' => 'Рекомендуемый размер в пропорции 1,5',
				'croppable' => false,
				'previewPath' => $model->getFilePath('logo', false),
			]); ?>
        </div>
    </div>

            <div class="widget">
                <div class="widget-content padding">
					<?= $form->field($model, 'type')->dropDownList(\app\modules\juri\models\Juri::$types) ?>
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
            <div class="widget">
                <div class="widget-content padding">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('posts', 'Создать') : Yii::t('posts',
                        'Изменить'), [
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                        'id' => 'submit-page'
                    ]) ?>
                </div>
            </div>


    <?php ActiveForm::end(); ?>

</div>
