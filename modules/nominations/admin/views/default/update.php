<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\nominations\models\Nominations */

$this->title = 'Редактировать номинацию: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Nominations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nominations-update">

    <div class="page-heading">
        <h1><i class="icon-menu"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
