<?php

namespace app\modules\nominations\admin\controllers;

use app\components\AdminController;
use Yii;
use app\modules\nominations\models\FestivalsNominations;
use app\modules\nominations\models\FestivalsNominationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FestivalsController implements the CRUD actions for FestivalsNominations model.
 */
class FestivalsController extends AdminController
{
    /**
     * Lists all FestivalsNominations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FestivalsNominationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FestivalsNominations model.
     * @param string $festival_id
     * @param string $nomination_id
     * @return mixed
     */
    public function actionView($festival_id, $nomination_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($festival_id, $nomination_id),
        ]);
    }

    /**
     * Creates a new FestivalsNominations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FestivalsNominations();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FestivalsNominations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $festival_id
     * @param string $nomination_id
     * @return mixed
     */
    public function actionUpdate($festival_id, $nomination_id)
    {
        $model = $this->findModel($festival_id, $nomination_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FestivalsNominations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $festival_id
     * @param string $nomination_id
     * @return mixed
     */
    public function actionDelete($festival_id, $nomination_id)
    {
        $this->findModel($festival_id, $nomination_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FestivalsNominations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $festival_id
     * @param string $nomination_id
     * @return FestivalsNominations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($festival_id, $nomination_id)
    {
        if (($model = FestivalsNominations::findOne(['festival_id' => $festival_id, 'nomination_id' => $nomination_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
