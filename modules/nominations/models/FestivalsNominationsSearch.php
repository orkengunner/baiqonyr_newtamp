<?php

namespace app\modules\nominations\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\nominations\models\FestivalsNominations;

/**
 * FestivalsNominationsSearch represents the model behind the search form about `app\modules\nominations\models\FestivalsNominations`.
 */
class FestivalsNominationsSearch extends FestivalsNominations
{
    public $nomination_title;
    public $nomination_type;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['festival_id', 'nomination_id', 'nomination_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
        ->innerJoinWith('nomination');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'festival_id' => $this->festival_id,
            'nomination_id' => $this->nomination_id,
            'nominations.type' => $this->nomination_type,
        ]);

        return $dataProvider;
    }
}
