<?php

namespace app\modules\nominations\models;

use app\components\ActiveRecord;
use app\modules\festivals\models\Festivals;
use Yii;

/**
 * This is the model class for table "festivals_nominations".
 *
 * @property string $festival_id
 * @property string $nomination_id
 *
 * @property Festivals $festival
 * @property Nominations $nomination
 */
class FestivalsNominations extends ActiveRecord
{
    public static $isOlfFest = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'festivals_nominations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['festival_id', 'nomination_id'], 'required'],
            [['festival_id', 'nomination_id'], 'integer'],
            [['festival_id'], 'exist', 'skipOnError' => true, 'targetClass' => Festivals::className(), 'targetAttribute' => ['festival_id' => 'id']],
            [['nomination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nominations::className(), 'targetAttribute' => ['nomination_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomination_id' => 'Номинация',
            'nomination_title' => 'Наименование',
            'nomination_type' => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestival()
    {
        return $this->hasOne(Festivals::className(), ['id' => 'festival_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomination()
    {
        return $this->hasOne(Nominations::className(), ['id' => 'nomination_id']);
    }

    public static function find()
    {
        $query = new FestivalsNominationsQuery(get_called_class());

        if(!self::$isOlfFest) {
            $query->andWhere(['festivals_nominations.festival_id' => self::festID()]);
        }
        return $query;
    }

    public function beforeValidate()
    {
        $this->festival_id = self::festID();
        return parent::beforeValidate();
    }
}
