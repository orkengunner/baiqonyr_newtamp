<?php

namespace app\modules\nominations\models;

use Yii;

/**
 * This is the model class for table "nominations_files_logo".
 *
 * @property int $nomination_id
 * @property string $subdir
 * @property string $basename
 *
 * @property Nominations $nomination
 */
class NominationsFilesLogo extends \yii\db\ActiveRecord
{
	public static $keyField = 'nomination_id';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nominations_files_logo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subdir', 'basename'], 'string', 'max' => 255],
            [['nomination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nominations::className(), 'targetAttribute' => ['nomination_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nomination_id' => 'Nomination ID',
            'subdir' => 'Subdir',
            'basename' => 'Basename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomination()
    {
        return $this->hasOne(Nominations::className(), ['id' => 'nomination_id']);
    }
}
