<?php

namespace app\modules\nominations\models;

use app\components\LanguageActiveRecord;
use app\components\traits\UploadableAsync;
use app\modules\languages\models\Languages;
use Symfony\Component\ExpressionLanguage\Expression;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "nominations".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $user_id
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $create_date
 * @property string $update_date
 * @property string $delete_date
 * @property string $delete_by
 * @property integer $lang_id
 * @property integer $position
 * @property integer $type
 *
 * @property FestivalsNominations[] $festivalsNominations
 * @property Festivals[] $festivals
 * @property JobsNominations[] $jobsNominations
 * @property Jobs[] $jobs
 * @property Languages $lang
 * @property Winners[] $winnersg
 * @property NominationsFilesLogo $nominationsFilesLogo
 */
class Nominations extends LanguageActiveRecord
{
	use UploadableAsync;

    public static $isOldFest = false;
	public $upload_logo = null;
	public $file_attributes = [
		'logo',
	];

	public static $types = [
		self::TYPE_NATIONAL => 'Национальный конкурс',
		self::TYPE_WORLD => 'Международный конкурс'
	];


	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nominations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'position'], 'required'],
            [['description', 'meta_description'], 'string'],
            [['user_id', 'delete_by', 'lang_id', 'position', 'type'], 'integer'],
            [['create_date', 'update_date', 'delete_date'], 'safe'],
            [['title', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
			[
				['upload_logo'],
				'image',/* 'minWidth' => 620, 'minHeight'=>400,*/
				'on' => 'upload'
			],
        ];
    }

    public function attributeLabels()
	{
		return ArrayHelper::merge(parent::attributeLabels(), [
			'type' => 'Категория'
		]);
	}

	public static function find()
    {
        $query = new NominationsQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query->setLanguage()->isNoDeleted(self::tableName());
        }

        return $query->isNoDeleted(self::tableName())->setLanguage();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestivalsNominations()
    {
        $FestivalsNominations = new FestivalsNominations();
        if(self::$isOldFest) {
            $FestivalsNominations::$isOlfFest = true;
        }
        return $this->hasMany($FestivalsNominations::className(), ['nomination_id' => 'id']);
    }

    public function getOldFestivalsNominations()
    {
        $nomin = new FestivalsNominations();
        $nomin::$isOlfFest = true;
        return $this->hasMany($nomin::className(), ['nomination_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFestivals()
    {
        return $this->hasMany(Festivals::className(), ['id' => 'festival_id'])->viaTable('festivals_nominations', ['nomination_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsNominations()
    {
        return $this->hasMany(JobsNominations::className(), ['nomination_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Jobs::className(), ['id' => 'job_id'])->viaTable('jobs_nominations', ['nomination_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWinners()
    {
        return $this->hasMany(Winners::className(), ['nomination_id' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getNominationsFilesLogo()
	{
		return $this->hasOne(NominationsFilesLogo::className(), ['nomination_id' => 'id']);
	}

	public function beforeValidate()
    {
        if ($this->getScenario() != 'search') {
            $this->user_id = Yii::$app->getUser()->id;
            if (!$this->slug) {
                $this->slug = StringHelper::url($this->title);
            }
        }

        return parent::beforeValidate();
    }

    public static function findNomination($fest_id = null)
    {
        $date = self::find()
            ->innerJoinWith('festivalsNominations', false)
			->withFilesLogo(self::tableName(), 'nomination_id')
            ->addSelect([
                'id',
                'title',
                'type'
            ]);
        if($fest_id) {
            $date->andWhere(['festival_id' => $fest_id]);
        }

        return $date->orderBy(['position' => SORT_ASC])
            ->all();
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior
        ];
    }

    public function afterSave($insert, $changedAttributes)
	{
		if ($this->hasAsyncTempFile('upload_logo')) {
			$logo = $this->setFile('logo', $this->saveAsyncFile('upload_logo'), true);
		}
		parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
	}
}
