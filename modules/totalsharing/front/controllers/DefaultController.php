<?php

namespace app\modules\totalsharing\front\controllers;

use app\components\FrontController;
use app\components\RFController;
use Yii;
use app\modules\totalsharing\models\TotalSharing;


/**
 * DefaultController implements the CRUD actions for TotalSharing model.
 */
class DefaultController extends FrontController
{


    public function actionIndex($sn, $link)
    {
        $total = TotalSharing::find()->andWhere([
            'url' => $link,
            'soc_net' => $sn
        ])->one();
        if($total) {
            $total->total = $total->total+1;
        } else {
            $total = new TotalSharing();
            $total->soc_net = $sn;
            $total->url = $link;
        }
        $total->save();

        /*return json_encode([
            'data' => $redirect,
            'callback' => 'redirect'
        ]);*/
    }

}
