<?php

namespace app\modules\totalsharing\models;

use app\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "total_sharing".
 *
 * @property string $id
 * @property string $soc_net
 * @property string $url
 * @property string $total
 */
class TotalSharing extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'total_sharing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['soc_net', 'url'], 'required'],
            [['total'], 'integer'],
            [['soc_net'], 'string', 'max' => 2],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'soc_net' => 'Soc Net',
            'url' => 'Url',
            'total' => 'Total',
        ];
    }
}
