<?php
/**
 * Created by PhpStorm.
 * User: Vitaly Prokhonenkov
 * Date: 09.06.2015
 * Time: 16:10
 * Email prokhonenkov@gmail.com
 */


namespace app\modules\lookupusersroles\models;

use app\components\ActiveRecord;
use app\modules\users\models\Users;
use Yii;

/**
 * This is the model class for table "lookup_users_roles".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Users[] $users
 */
class LookupUsersRoles extends ActiveRecord
{
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lookup_users_roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['role_id' => 'id']);
    }

}
