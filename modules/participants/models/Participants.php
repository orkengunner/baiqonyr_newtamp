<?php

namespace app\modules\participants\models;

use app\components\ActiveRecord;
use app\components\LanguageActiveRecord;
use app\modules\jobs\models\JobsParticipants;
use app\modules\languages\models\Languages;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "participants".
 *
 * @property string $id
 * @property string $name
 * @property string $second_name
 * @property string $last_name
 * @property string $link
 * @property string $position
 * @property string $create_date
 * @property string $update_date
 * @property string $delete_date
 * @property string $delete_by
 * @property integer $lang_id
 *
 * @property JobsParticipants[] $jobsParticipants
 * @property Languages $lang
 */
class Participants extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['position', 'delete_by', 'lang_id'], 'integer'],
            [['create_date', 'update_date', 'delete_date'], 'safe'],
            [['name', 'last_name', 'second_name','link'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'name' => 'Имя',
        ]);
    }

    public static function find()
    {
        $query = new ParticipantsQuery(get_called_class());

        if (Yii::$app->params['yiiEnd'] == 'admin') {
            return $query->isNoDeleted(self::tableName());
        }

        return $query->isNoDeleted(self::tableName());
    }

    public function behaviors()
    {
        return [
            $this->timestampBehavior
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobsParticipants()
    {
        return $this->hasMany(JobsParticipants::className(), ['participant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    public static function findByJobID($job_id)
    {
        return self::find()
            ->innerJoin('jobs_participants AS jp' , 'jp.participant_id = participants.id AND jp.job_id = ' . $job_id)
            ->innerJoin('lookup_posts AS p', 'p.id = jp.post_id')
            ->addSelect([
                'participants.id',
                'participants.name',
                'participants.second_name',
                'participants.last_name',
                'participants.link',
                'p.title',
                'p.id AS post_id'
            ])
            //->groupBy('p.id')
                ->orderBy(['jp.position' => SORT_ASC])
            ->asArray()
            ->all();
    }
}
