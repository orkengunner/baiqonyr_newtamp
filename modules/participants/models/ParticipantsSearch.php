<?php

namespace app\modules\participants\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\participants\models\Participants;

/**
 * ParticipantsSearch represents the model behind the search form about `app\modules\participants\models\Participants`.
 */
class ParticipantsSearch extends Participants
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'position', 'delete_by', 'lang_id'], 'integer'],
            [['name', 'create_date', 'update_date', 'delete_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Participants::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'position' => $this->position,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_date' => $this->delete_date,
            'delete_by' => $this->delete_by,
            'lang_id' => $this->lang_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
