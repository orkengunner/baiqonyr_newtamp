<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\participants\models\ParticipantsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Съемочная группа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participants-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>
    <p>
        <?= Html::a(Yii::t('participants', '<i class="icon-list-add"></i> Добавить перосну', [
            'modelClass' => 'Participants',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'pager' => [
                    'lastPageLabel' => 'Последняя',
                    'firstPageLabel' => 'Первая'
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'second_name',
            'last_name',


                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{update} {remove}'
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
