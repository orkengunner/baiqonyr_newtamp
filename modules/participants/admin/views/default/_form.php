<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\participants\models\Participants */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participants-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="widget">
        <div class="widget-content padding">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'second_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
    <div class="widget">
        <div class="widget-content padding">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('posts', 'Создать') : Yii::t('posts',
                'Изменить'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                'id' => 'submit-page'
            ]) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>



</div>
