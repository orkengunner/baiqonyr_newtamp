<?php

namespace app\modules\participants\admin\controllers;

use app\components\AdminController;
use Yii;
use app\modules\participants\models\Participants;
use app\modules\participants\models\ParticipantsSearch;
use yii\bootstrap\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DefaultController implements the CRUD actions for Participants model.
 */
class DefaultController extends AdminController
{
    /**
     * Lists all Participants models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParticipantsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Participants model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Participants model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Participants();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Participants model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Participants model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Participants model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Participants the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Participants::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSearch($term, $job_id)
    {

        $data = [];
        $users = Participants::find()
            ->andWhere(['OR',
                ['like', 'name',  $term],
                ['like', 'second_name',  $term],
                ['like', 'last_name',  $term],
            ])
            ->addSelect([
                'id',
                'name',
                'second_name',
                'last_name'
            ])
            ->limit(10)
            ->all();

        \Yii::$app->response->format = Response::FORMAT_JSON;

        foreach($users as $v) {
            $data[] = [
                'label' => $v->name . ' ' . $v->second_name . ' ' . $v->last_name,
                'value' => $v->id,
            ];
        }
        return $data;
    }

    public function actionAdd($name, $post_id)
    {
        $participant = new Participants();
        $participant->name = $name;
        $participant->save();

        return json_encode([
            'data' => [
                'id' => $participant->id,
                'post_id' => $participant->post_id
            ],
            'callback' => 'participantsCallback'
        ]);
    }
}
