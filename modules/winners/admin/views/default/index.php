<?php

use app\modules\nominations\models\Nominations;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\winners\models\WinnersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Победители';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="winners-index">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <p>
        <?= Html::a(Yii::t('winners', '<i class="icon-list-add"></i> Добавить', [
            'modelClass' => 'Winners',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="widget">
        <div class="widget-content padding">
            <?php \yii\widgets\Pjax::begin(['id' => 'pjaxgrid']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered'],
                'filterModel' => $searchModel,
                'layout' => "{items}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'job_title',

                    [
                        'attribute' => 'nomination_id',
                        'value' => function ($data) {
                            return $data->nomination_title . ' - ' . Nominations::$types[$data->nomination->type];
                        },
                        'filter' => Nominations::getDropdownList(
							'CONCAT_WS(" - ", title, IF(type = ' . Nominations::TYPE_NATIONAL . ', "' . Nominations::$types[Nominations::TYPE_NATIONAL] . '", "' . Nominations::$types[Nominations::TYPE_WORLD] . '"))'
						),
                    ],

                    'place',

                    [
                        'class' => \app\components\admin\RFAActionColumn::className(),
                        'template' => '{update} {delete}'
                    ],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
