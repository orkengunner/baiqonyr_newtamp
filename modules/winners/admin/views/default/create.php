<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\winners\models\WInners */

$this->title = 'Добавить победителя';
$this->params['breadcrumbs'][] = ['label' => 'Winners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="winners-create">

    <div class="page-heading">
        <h1><i class="icon-newspaper"></i><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
