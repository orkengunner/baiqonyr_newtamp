<?php

use app\modules\nominations\models\Nominations;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\winners\models\WInners */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('
    $("#submit-page").click(function(){
        $("form").submit();
    });
');
?>

<div class="winners-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="widget">
        <div class="widget-content padding">

            <?= \app\components\widgets\AutoFill::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => 'job_id'
            ])?>

            <?= $form->field($model, 'nomination_id')->dropDownList(
				\app\modules\nominations\models\Nominations::getDropdownList(
					'CONCAT_WS(" - ", title, IF(type = ' . $model::TYPE_NATIONAL . ', "' . Nominations::$types[$model::TYPE_NATIONAL] . '", "' . Nominations::$types[$model::TYPE_WORLD] . '"))'
				)
            ) ?>




        </div>
    </div>
    <div class="widget">
        <div class="widget-content padding">
            <?= Html::button($model->isNewRecord ? Yii::t('winners', 'Создать') : Yii::t('winners',
                'Изменить'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                'id' => 'submit-page'
            ]) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
