<?php

namespace app\modules\winners\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * WinnersSearch represents the model behind the search form about `app\modules\winners\models\WInners`.
 */
class WinnersSearch extends Winners
{
    public  $job_title;
    public  $nomination_title;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'job_id', 'place', 'nomination_id'], 'integer'],
            [['nomination_title', 'job_title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = Model::scenarios();
        $scenarios['search'] = ['job_title', 'nomination_id', 'place'];
        return $scenarios;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->innerJoinWith('job')
            ->innerJoinWith('nomination')
        ->addSelect([
            'winners.id',
            'winners.job_id',
            'jobs.title AS job_title',
            'winners.place',
            'winners.nomination_id',
            'nominations.title AS nomination_title'
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'nomination_title',
                    'place',
                    'job_title'
                ],
                'defaultOrder' => ['nomination_title' => SORT_ASC, 'place' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'job_id' => $this->job_id,
            'place' => $this->place,
            'nomination_id' => $this->nomination_id,
        ]);


        $query->andFilterWhere(['like', 'jobs.title', $this->job_title])
        ;

        return $dataProvider;
    }
}
